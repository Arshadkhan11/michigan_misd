<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <description>Tracker: Sprint Management app. 

This version focuses on sprint management features and is aimed at those involved in the development and release process.</description>
    <label>Tracker: Sprint Management</label>
    <logo>Logo_s_Images/DD_Tracker_Logo2.gif</logo>
    <tab>TKR_Project__c</tab>
    <tab>TKR_Sprint__c</tab>
    <tab>Action_Sprint_Items</tab>
    <tab>TKR_Decision_Question__c</tab>
    <tab>TKR_Milestone__c</tab>
    <tab>TKR_User_Story__c</tab>
    <tab>TKR_Status_Reports__c</tab>
    <tab>About_Tracker</tab>
    <tab>Master_Relationship__c</tab>
    <tab>AssessmentQuestions__c</tab>
    <tab>AssessmentResponses__c</tab>
    <tab>CommonApp_Page__c</tab>
    <tab>CommonApp_Category__c</tab>
    <tab>CommunityResource__c</tab>
    <tab>Domain__c</tab>
    <tab>News__c</tab>
    <tab>Question__c</tab>
    <tab>Success_Plan_Master__c</tab>
</CustomApplication>
