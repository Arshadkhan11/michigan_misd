<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>SM_Home</defaultLandingTab>
    <description>This is a generic template for Custom Application.  With this template, you may adjust the default elements and values and add new elements and values.</description>
    <label>SMS Magic Interact</label>
    <logo>SMS_Magic/SMS_Magic_Interact_Logo.png</logo>
    <tab>standard-Contact</tab>
    <tab>SM_Home</tab>
    <tab>SMS_Magic_Settings</tab>
    <tab>SMS_Magic_Help</tab>
    <tab>smsMagic__c</tab>
    <tab>SMS_Template__c</tab>
    <tab>Incoming_SMS__c</tab>
    <tab>Master_Relationship__c</tab>
    <tab>AssessmentQuestions__c</tab>
    <tab>AssessmentResponses__c</tab>
    <tab>CommonApp_Page__c</tab>
    <tab>CommonApp_Category__c</tab>
    <tab>CommunityResource__c</tab>
    <tab>Domain__c</tab>
    <tab>News__c</tab>
    <tab>Question__c</tab>
    <tab>Success_Plan_Master__c</tab>
</CustomApplication>
