body {
  color: #051a37!important
}

header {
  background: #051a37;
  min-height: 75px;
}

a,
a:link,
a:active,
a:visited,
a:hover,
a:focus {
  outline: 0!important
}

.cc-logo {
  height: auto;
  max-width: 100%;
  vertical-align: middle;
  margin-left: 20px!important;
}

.input-group {
  border-collapse: separate;
  display: table;
  position: relative;
}

 .input-group-addon,
 .input-group-btn,
 .input-group .form-control {
   display: table-cell
 }

 .input-group .form-control {
   float: left;
   margin-bottom: 0;
   position: relative;
   width: 100%;
   z-index: 2;
 }

.input-lg,
.form-horizontal .form-group-lg .form-control {
  border-radius: 6px;
  font-size: 18px;
  height: 46px;
  line-height: 1.33;
  padding: 10px 16px;
}

.form-control {
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  box-shadow: 0 1px 1px rgba(0,0,0,0.075) inset;
  color: #555;
  display: block;
  font-size: 14px;
  height: 34px!important;
  line-height: 1.42857;
  transition: border-color .15s ease-in-out 0 box-shadow .15s ease-in-out 0;
  width: 100%;
  padding: 6px 12px;
}

.input-group-btn {
  font-size: 0;
  position: relative;
  white-space: nowrap;
}

.input-group-addon,
.input-group-btn {
  vertical-align: middle;
  white-space: nowrap;
  width: 1%;
}

.btn-lg,
.btn-group-lg > .btn {
  border-radius: 6px;
  font-size: 17px;
  line-height: 1.33;
  padding: 10px 16px;
}

.input-group .form-control:last-child,
.input-group-addon:last-child,
.input-group-btn:last-child > .btn,
.input-group-btn:last-child > .btn-group > .btn,
.input-group-btn:last-child > .dropdown-toggle,
.input-group-btn:first-child > .btn:not(:first-child),
.input-group-btn:first-child > .btn-group:not(:first-child) > .btn {
  border-bottom-left-radius: 0;
  border-top-left-radius: 0;
}

.sr-only {
  clip: rect(0px,0px,0px,0px);
  height: 1px;
  overflow: hidden;
  position: absolute;
  width: 1px;
  margin: -1px;
  padding: 0;
}

#helpmodal .modal-body {
  max-height: 425px;
  overflow-x: hidden;
  overflow-y: auto;
  position: relative;
  width: auto;
  padding: 0 15px 10px;
}

.modal-lg {
  width: 80%
}

.modal-header {
  border: none!important;
  padding: 15px!important;
}

.modal-title {
  font-size: 34px;
  color: #37465d;
}

.modal-body {
  overflow: auto;
  position: relative;
  max-height: 300px;
  padding: 0 15px 10px;
}

 .modal-body h3 {
   font-size: 24.5px;
   margin-bottom: 5px;
   color: #37465d;
 }

.subtext {
  color: #888
}

.helptext {
  color: #555;
  font-size: 10px;
}

.modal-body .w20p > input {
  width: 85%
}

.modal-footer {
  overflow: hidden;
  position: relative;
  text-align: right;
  border-top: none!important;
  padding: 15px;
}

sup.required {
  font-style: normal;
  color: #C00;
}

.required {
  background-color: #ffc!important
}

.lg-font {
  font-size: 160%
}

ol.progtrckr li {
  display: inline-block;
  text-align: center;
  line-height: 3em;
}

ol.progtrckr[data-progtrckr-steps=2] li {
  width: 49%
}

ol.progtrckr[data-progtrckr-steps=3] li {
  width: 33%
}

ol.progtrckr[data-progtrckr-steps=4] li {
  width: 24%
}

ol.progtrckr[data-progtrckr-steps=5] li {
  width: 19%
}

ol.progtrckr[data-progtrckr-steps=6] li {
  width: 16%
}

ol.progtrckr[data-progtrckr-steps=7] li {
  width: 14%
}

ol.progtrckr[data-progtrckr-steps=8] li {
  width: 12%
}

ol.progtrckr[data-progtrckr-steps=9] li {
  width: 11%
}

ol.progtrckr li.progtrckr-done {
  color: #000;
  border-bottom: 4px solid #9ACD32;
}

ol.progtrckr li.progtrckr-todo {
  color: silver;
  border-bottom: 4px solid silver;
}

ol.progtrckr li:after {
  content: "\00a0\00a0"
}

ol.progtrckr li:before {
  position: relative;
  bottom: -2.5em;
  float: left;
  left: 50%;
  line-height: 1em;
}

ol.progtrckr li.progtrckr-done:before {
  content: "\2713";
  color: #FFF;
  background-color: #9ACD32;
  height: 1.2em;
  width: 1.2em;
  line-height: 1.2em;
  border: none;
  border-radius: 1.2em;
}

ol.progtrckr li.progtrckr-todo:before {
  content: "\039F";
  color: silver;
  background-color: #FFF;
  font-size: 1.5em;
  bottom: -1.6em;
}

.tab-content {
  background: none repeat scroll 0 0 #fff
}

.blurredpop {
  display: none;
  position: fixed;
  top: 0;
  left: 0;
  background-color: #FFF;
  height: 100%;
  width: 100%;
  z-index: 99999999999;
  opacity: .6;
}

.blurredpopinternal {
  display: block;
  text-align: center;
  vertical-align: middle;
  height: auto;
  width: auto;
  position: relative;
  top: 40%;
  z-index: 100;
  margin: 0 auto;
}

a,
a label {
  cursor: pointer
}

.quicklinks {
  background-color: #fff;
  border-radius: 4px;
  margin-bottom: 10px;
  overflow: hidden;
  padding-top: 10px;
  position: relative;
}

.topservices {
  margin-right: 1.5%;
  width: 30%;
}

section.quicklinks h4 {
  border-bottom: 1px solid #eee;
  color: #37465d;
  padding-bottom: 10px;
}

section.quicklinks > ul {
  height: 185px;
  list-style: none outside none;
  margin-left: 0;
  overflow: auto;
}

 section.quicklinks > ul li {
   padding: 5px
 }

section.quicklinks p {
  margin-bottom: 0
}

section.quicklinks ul li a {
  display: block;
  margin-bottom: 5px;
}

.topservices li > span {
  float: left;
  margin-right: 10px;
  margin-top: -3px;
}

.topservices li img {
  border-radius: 8px;
  height: 28px;
  width: 28px;
}

.topservices li:last-child {
  bottom: -8px;
  position: absolute;
  right: 0;
  width: 97%;
}

.topnews li img {
  height: 60px;
  width: 60px;
  margin: 2px 10px 0;
}

.topnews li:last-child {
  background: none repeat scroll 0 0 rgba(0,0,0,0);
  bottom: -4px;
  position: absolute;
  right: 8px;
  width: 97%;
  margin: 0;
  padding: 0;
}

#cc-global-options {
  margin-top: 20px
}

.cc-help {
  background: #061528 url(img/cc-help-icon.png) no-repeat center center;
  display: block;
  width: 35px;
  height: 35px;
  margin-left: 10px;
  border-radius: 4px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
}

.cc-province {
  background: url(img/cc-state-icon.png) no-repeat left center;
  display: block;
  float: left;
  width: 30px;
  height: 22px;
}

.cc-srch-bg {
  background: url(img/cc-srch-bg.png) repeat-x top left;
  height: auto;
}

.cc-srch-bg > div > div {
  margin-top: 15px;
  margin-bottom: 15px;
  padding-left: 0px;
  padding-right: 0px;
}

.cc-quicklink {
  margin-top: 20px;
  margin-bottom: 10px;
}

 .cc-quicklink,
 .cc-quicklink li {
   list-style: none;
   margin: 0;
   padding: 0;
 }

  .cc-quicklink li + li {
    background: url(img/cc-srch-navsplit.png) repeat-y left center
  }

  .cc-quicklink li a {
    margin: 0 20px
  }

  .cc-quicklink li:first-child a {
    margin-left: 0
  }

  .cc-quicklink a:hover {
    color: #f1f2f7
  }

.cc-page-bg {
  background: #fff url(img/cc-page-bg.png) repeat-x left top
}

.cc-banner {
  background: url(img/cc-banner.jpg) repeat-x center bottom;
  background-size: cover;
  height: 463px;
}

.cc-banner-copy {
  color: #fff;
  font-size: 2.5em;
  font-weight: 700;
  margin-top: 325px;
}

.cc-login-box {
  background: #fff;
  border-radius: 4px;
  -moz-border-radius: 4px;
  -webkit-border-radius: 4px;
  margin-top: 0;
  padding: 20px;
}

#loginmodal .modal-header h2 {
  color: #37465d
}

#loginmodal h2 {
  font-size: 1.5em;
  margin-top: 0;
}

.cc-login-box form {
  padding: 0
}

.cc-banner h2 {
  font-weight: 700;
  font-size: 1.375em;
  color: #37465d;
  margin: 0 0 20px;
  padding: 0;
}

.cc-blu-bg {
  background: none repeat scroll 0 0 #051a37;
  border-radius: 4px;
  color: #bbbaba;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  margin: 0!important;
  padding: 20px;
}

.cc-lifeevents {
  margin-top: 30px
}

 .cc-lifeevents .nav-tabs li a.tabtitle,
 .cc-lifeevents .nav-tabs li a.tabtitle:active,
 .cc-lifeevents .nav-tabs li a.tabtitle:visited,
 .cc-lifeevents .nav-tabs li a.tabtitle:focus {
   color: #779324;
   font-weight: 700;
   padding-right: 5px;
 }

 .cc-lifeevents .tab-content .nav-tabs li p {
   margin: 0
 }

 .cc-lifeevents .tab-content .nav-tabs li {
   border-right: 1px dashed #777;
   float: left;
   height: 165px;
   margin-bottom: 10px;
   margin-top: 10px;
   overflow: hidden;
 }

 .cc-lifeevents .tab-content .nav-tabs li:last-child {
   border-right: 0 none
 }

 .cc-lifeevents .tab-content .nav-tabs > li > a:hover {
   background: none repeat scroll 0 0 transparent;
   border-color: transparent;
 }

 .cc-lifeevents .nav-tabs li a.tabtitle:hover {
   background-color: #fff
 }

 .cc-lifeevents .nav-tabs li.active a.tabtitle:hover {
   background-color: #fff!important
 }

.tab-pane {
  border: 1px solid #e3e3e3;
  border-top: 0;
  padding-top: 5px;
  background: #fff;
}

.cc-moments {
  margin: 30px
}

.cc-quicklinks-section {
  background: #f2f2f2;
  border-left: 1px solid #e3e3e3;
}

.cc-page-bg h2 {
  color: #37465d;
  font-size: 1.5em;
}

.nav-tabs {
  background: url(img/cc-nav-tab-bg.png) repeat-x bottom;
  border-bottom: 0!important;
}

.cc-tab-family {
  background: url(img/cc-family-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
}

.active .cc-tab-family {
  background: url(img/cc-family-icon-active.png) no-repeat 10px center
}

.cc-tab-finance {
  background: url(img/cc-finance-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
  border-radius: 0!important;
  -webkit-border-radius: 0!important;
  -moz-border-radius: 0;
}

.active .cc-tab-finance {
  background: url(img/cc-finance-icon-active.png) no-repeat 10px center
}

.cc-tab-home {
  background: url(img/cc-home-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
}

.active .cc-tab-home {
  background: url(img/cc-home-icon-active.png) no-repeat 10px center
}

.cc-tab-transport {
  background: url(img/cc-transport-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
}

.active .cc-tab-transport {
  background: url(img/cc-transport-icon-active.png) no-repeat 10px center
}

.cc-tab-citizenship {
  background: url(img/cc-citizen-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
}

.active .cc-tab-citizenship {
  background: url(img/cc-citizen-icon-active.png) no-repeat 10px center
}

.cc-tab-education {
  background: url(img/cc-education-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
}

.active .cc-tab-education {
  background: url(img/cc-education-icon-active.png) no-repeat 10px center
}

.cc-tab-job {
  background: url(img/cc-job-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
}

.active .cc-tab-job {
  background: url(img/cc-job-icon-active.png) no-repeat 10px center
}

.cc-tab-health {
  background: url(img/cc-health-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
}

.active .cc-tab-health {
  background: url(img/cc-health-icon-active.png) no-repeat 10px center
}

.cc-tab-trip {
  background: url(img/cc-trip-icon.png) no-repeat 10px center;
  padding-left: 50px!important;
  padding-right: 10px;
}

.active .cc-tab-trip {
  background: url(img/cc-trip-icon-active.png) no-repeat 10px center
}

.cc-aux-content {
  margin: 40px 0
}

.cc-placeholder {
  height: 95px;
  background: url(img/cc-placeholder.png) no-repeat center center;
  background-size: cover;
  margin: 40px 0;
}

.btn-default.cc-dark-bg {
  background: #061528!important
}

.cc-search-component {
  border-radius: 4px;
  -webkit-border-radius: 4px;
  -moz-border-radius: 4px;
  background: #f2f2f2 url(img/cc-search-icon.png) no-repeat 8px center;
  padding-left: 40px!important;
  padding-right: 0!important;
}

 .cc-search-component .form-control {
   background: none;
   border: 0;
   font-size: 1em;
   font-style: italic;
 }

button.btn.btn-default.dropdown-toggle.cc-dark-button {
  background: #061528;
  border: 0;
  color: #cec9c6;
  font-size: .9em;
}

footer {
  background-color: #fff;
  border-top: 1px solid #ccc;
  padding-top: 20px;
}

 footer p {
   color: #999;
   font-size: .875em;
   margin: 10px 0 40px;
 }

 footer a {
   border-right: 1px solid #efefef;
   padding-right: 20px;
   margin-left: 10px;
 }

  footer a + a {
    border-right: 0;
    margin-left: 20px;
  }

.mylabel {
  float: left;
  padding-top: 5px;
  margin-right: 100px;
}

.mylabelborn {
  margin-right: 159px
}

.mylabelcitizen {
  margin-right: 105px
}

select[title="ethnicity - chosen"] {
  height: 77px
}

select[title="ethnicity - available"] {
  width: 170px
}

.mylabelhousehold {
  margin-right: 92px
}

ul.ui-autocomplete {
  padding: 10px 0 10px 10px
}

.ui-autocomplete-category {
  background-color: #f2f2f2;
  border-radius: 4px;
  color: #37465d;
  font-size: 16px;
  font-weight: 700;
  margin-right: 10px;
  padding: 5px;
}

.ui-autocomplete .ui-menu-item {
  color: #37465d!important;
  font-size: 14px;
}

.ui-menu-item:hover,
.ui-menu-item:focus,
.ui-state-focus {
  background-color: transparent!important;
  background-image: none!important;
  border: medium none!important;
  text-decoration: underline;
  margin: 0!important;
}

.ui-autocomplete-category:hover,
.ui-autocomplete-category:focus,
.ui-autocomplete-category:active {
  text-decoration: none!important;
  background-color: #f2f2f2!important;
  border-radius: 4px!important;
  color: #37465d!important;
}

.search_heading h3 {
  font-weight: 700;
  padding-left: 30px;
}

section.topservices1 h5 {
  font-weight: 700;
  border: none;
  color: #000;
  padding: 10px;
}

section.topservices1 ul {
  padding-left: 10px
}

 section.topservices1 ul li {
   overflow: hidden;
   font-weight: 400;
 }

.topservices1 li:last-child {
  position: initial;
  right: 0;
  width: auto;
}

.topservices1 li > span {
  float: none;
  margin-right: 10px;
  margin-top: -3px;
}

.cc-searchresults-component {
  background-color: #fff!important;
  border-radius: 4px!important;
  margin-top: 10px!important;
  margin-bottom: 10px!important;
  padding: 5px 10px 10px!important;
}

 .cc-searchresults-component a {
   font-weight: 400
 }

 .cc-searchresults-component i {
   color: #868686
 }

.panel-heading {
  color: #868686;
  overflow: hidden;
}

.topservices1 {
  margin-top: 10px
}

.panel-body {
  border-top: 1px solid #eee;
  border-bottom: 1px solid #eee;
}

 .panel-body .label-apply {
   display: inline;
   font-size: 100%;
   font-weight: 700;
   line-height: 1;
   color: #fff;
   text-align: center;
   white-space: nowrap;
   vertical-align: baseline;
   border-radius: .25em;
   padding: .5em 3.2em;
 }

 .panel-body .label-add {
   display: inline;
   font-size: 100%;
   font-weight: 700;
   line-height: 1;
   color: #fff;
   text-align: center;
   white-space: nowrap;
   vertical-align: baseline;
   border-radius: .25em;
   padding: .5em 2em;
 }

.message_heading h3 {
  font-weight: 700;
  padding-left: 30px;
  margin: 0;
}

.date-time {
  color: #868686;
  font-weight: 400;
}

.panel-heading h5 {
  color: #000;
  font-weight: 700;
}

.inactive {
  color: #000;
  text-decoration: none;
}

.panel-inactive {
  background-color: #f2f2f2
}

a:link,
a:hover,
a:focus {
  color: rgba(49,176,213,0.70)
}

.img-circle,
.img-thumbnail {
  -webkit-animation: fadein 3s;
  -moz-animation: fadein 3s;
  -ms-animation: fadein 3s;
  -o-animation: fadein 3s;
  animation: fadein 3s;
}

.navbar {
  margin-bottom: 0!important
}

.navbar-custom .navbar-nav > li > a {
  color: #fff;
  line-height: 32px;
  text-transform: uppercase;
  padding: 19px 25px;
}

.navbar-custom .navbar-nav > .active > a,
.navbar-nav > .active > a:hover,
.navbar-nav > .active > a:focus {
  color: #fff;
  background-color: transparent;
}

.navbar-custom .navbar-nav > li > a:hover,
.nav > li > a:focus {
  text-decoration: none;
  background-color: #2a3f53;
}

.navbar-custom .navbar-brand {
  color: #eee;
  text-transform: uppercase;
  letter-spacing: 2px;
  font-weight: 700;
}

.navbar-custom .navbar-toggle,
.navbar-custom .nav .open>a,
.navbar-custom .nav .open>a:hover,
.navbar-custom .nav .open>a:focus {
  background-color: transparent
}

.navbar-custom .icon-bar {
  border: 1px solid #fff
}

.navbar-nav > li > a {
  color: #9dc02e!important
}

.cc-quicklink a,
.cc-quicklink a:link,
.cc-quicklink a:active,
.cc-quicklink a:visited,
.navbar-custom .navbar-nav .open .dropdown-menu>li>a,
.navbar-custom .navbar-nav .open .dropdown-menu {
  color: #9dc02e
}

.cc-blu-bg h2,
.cc-page-bg .cc-blu-bg h2,
.navbar-custom .navbar-brand:hover {
  color: #fff
}

.cc-moments,
.cc-moments li,
.cc-quicklinks,
.cc-quicklinks li {
  list-style: none
}

 .cc-moments li a,
 .cc-quicklinks li a {
   display: block;
   line-height: 35px;
   background: url(img/cc-bullet.png) no-repeat left center;
   padding-left: 20px;
 }

.search_heading,
.message_heading {
  color: #000;
  font-weight: 700;
  padding: 20px;
}

@media min-width 320px and max-device-width 568px {
    body {
      padding-left: 5px!important;
      padding-right: 5px!important;
    }

    .cc-logo {
      margin-left: 0!important;
      margin-top: 25px;
    }

    .navbar .btn-navbar {
      position: absolute;
      right: 0;
      top: 17px;
    }

    .nav-tabs > li {
      float: none
    }

    #loginmodal .reset_pwd {
      padding-left: 0;
      padding-right: 0;
    }
}

@media only screen and min-device-width 768px and max-device-width 1024px and orientation landscape {
    .dropdown {
      position: relative!important
    }

    header .container .row .span5 {
      width: 425px!important
    }
}

@media only screen and min-device-width 768px and max-device-width 1024px and orientation portrait {
    header .container .row .span4 {
      width: 278px!important
    }

    header #cc-global-options {
      margin-top: 20px!important;
      margin-left: 0!important;
    }
}

@media min-width 1200px {
    .row-fluid .offset2 {
      margin-left: 17%
    }
}

@media max-width 600px {
    .nav-tabs > li {
      width: 100%!important
    }

    .cc-lifeevents .tab-content .nav-tabs li {
      border-right: none;
      border-bottom: 1px dashed #777;
      float: left;
      height: 135px;
      margin-bottom: 0;
      margin-top: 10px;
      overflow: hidden;
    }

    .topservices {
      margin-right: 0;
      width: 100%;
    }
}

@media screen and min-width 768px {
    .navbar-custom .navbar-brand {
      padding-top: 24px
    }
}

@media screen and min-width 992px {

    .panel {
      width: 100%
    }
}

.customerName h3 {
  margin-top: 0px;
  color: #37465d;
}