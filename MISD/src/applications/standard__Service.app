<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>standard-Account</tab>
    <tab>standard-Contact</tab>
    <tab>standard-Case</tab>
    <tab>standard-Solution</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>Master_Relationship__c</tab>
    <tab>AssessmentQuestions__c</tab>
    <tab>AssessmentResponses__c</tab>
    <tab>CommonApp_Page__c</tab>
    <tab>CommonApp_Category__c</tab>
    <tab>CommunityResource__c</tab>
    <tab>Domain__c</tab>
    <tab>News__c</tab>
    <tab>Question__c</tab>
    <tab>Success_Plan_Master__c</tab>
</CustomApplication>
