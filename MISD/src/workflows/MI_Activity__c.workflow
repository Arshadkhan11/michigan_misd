<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Email_Sent</fullName>
        <field>Email_Sent__c</field>
        <literalValue>1</literalValue>
        <name>Update Email Sent</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CalendarRemindersForAll</fullName>
        <active>true</active>
        <formula>AND(RecordType.Name = &apos;Event&apos;, LEFT(Subject__c, 8) &lt;&gt; &apos;Transit-&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MI_Activity__c.Start_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Event Reminder</fullName>
        <active>false</active>
        <formula>true</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MI_Activity__c.Start_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TaskRemindersForCitizen</fullName>
        <active>true</active>
        <formula>AND(RecordType.Name = &apos;Task&apos;, AND(Owner:User.Profile.Name = &apos;Citizen Profile&apos;,LEFT(Subject__c, 8) &lt;&gt; &apos;Transit-&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MI_Activity__c.Start_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>TaskRemindersForOthers</fullName>
        <active>true</active>
        <formula>AND(RecordType.Name = &apos;Task&apos;, AND(Owner:User.Profile.Name &lt;&gt; &apos;Citizen Profile&apos;,LEFT(Subject__c, 8) &lt;&gt; &apos;Transit-&apos;))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_Email_Sent</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>MI_Activity__c.Start_Date__c</offsetFromField>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
