global class CitizenSuccessPlanCtrl{
            
    public string conId;
    public Map<String, List<id>> goalsMap {get;set;} // key: domain name; value: list of goals for the domain
    public Map<String, Map<String, List<Id>>> categoryGoalMap {get;set;}
    public Map<Id, SP_Transactional__c> milestoneMap {get;set;} // key: id of goal; value: Goal
    public Map<id, List<SP_Transactional__c>> milestoneItems {get;set;} // key: id of goal; value: list of steps for each goal    
    public Boolean isEdit {get;set;}
    public String mStepId {get;set;}
    public String SP_CaseId {get;set;}
    public Map<Id, List<SP_Transactional__c>> goalResources { get;set;}// key: id of goal; value: list of resources
    public String toDeleteStep {get;set;}
    public String toDeleteRsc {get;set;}
    public String activeTab {get;set;}
    public string rscId{get;set;}
    public Boolean displaySP { get;set;}
    Public String MyProflieName {get;set;}
    Public String MyProfileId {get;set;}
    Public String MyProfileName {get;set;}
    Public Contact MyProfile {get;set;}
    Public String LocationName {get;set;}
    //427
    public string servStat{get;set;}
    public string servId{get;set;}
    
    public Map<ID, Map<String, List<SP_Transactional__c>>> vShowServices {get; set;}
    public Map<ID, List<SP_Transactional__c>> vServices {get; set;}
    
    public Case contactCase{get;set;}
    //bugs
    public list<SP_Transactional__c> transList {get;set;}
    public list<SP_Transactional__c> mileStoneStepList {get;set;}
    public list<SP_Transactional__c> serviceList {get;set;}
    public Map<ID, List<SP_Transactional__c>> stepServiceMap {get;set;}
    public Map<ID, Map<String, List<SP_Transactional__c>>> stepSelectedServicesMap {get;set;}
    public Map<String, List<SP_Transactional__c>> domainMileStoneMap {get;set;} 
    public List<String> domainsList {get;set;}
    
    public Map<String, Map<String, SP_Transactional__c>> newUIMap{get;set;}
    
    public CitizenSuccessPlanCtrl(){
        
        LocationName='550 W. Western Ave. Muskegon, MI 49440';
        goalsMap = new Map<String, List<id>>();
        categoryGoalMap = new Map<String, Map<String, List<Id>>>();
        milestoneMap = new Map<Id, SP_Transactional__c>();
        milestoneItems = new Map<id, List<SP_Transactional__c>>();
        goalResources = new Map<Id, List<SP_Transactional__c>>();
        newUIMap = new Map<String, Map<String, SP_Transactional__c>>();
        domainsList = new List<STring>();
        contactCase = new Case();
        
        isEdit = false;
        displaySP = true;
        categoryGoalMap.put('Basic Needs',  new Map<String, List<id>>());
        categoryGoalMap.put('Living & Working',  new Map<String, List<id>>());
        categoryGoalMap.put('Health & Wellness',  new Map<String, List<id>>());
        categoryGoalMap.put('Education',  new Map<String, List<id>>());
        categoryGoalMap.put('Community',  new Map<String, List<id>>());
        system.debug('\n\n categoryGoalMap: ' + categoryGoalMap);
        conId  = ApexPages.currentPage().getParameters().get('contactId');
        SP_CaseId = ApexPages.currentPage().getParameters().get('SP_CaseId');
        //SP_CaseId = '50035000000cnybAAA';
        
        //Code for merging Success plan page with Needs Assessment page
        if(SP_CaseId == '' || SP_CaseId == null)
        {
            String needsconid = apexpages.currentpage().getparameters().get('AssessmentConId');
            Profile p = [SELECT id FROM Profile WHERE name = 'Citizen Profile'];
            if((needsconid == '' || needsconid == null) && UserInfo.getProfileId() == p.id)
            {
                needsconid = [SELECT id,ContactId FROM User WHERE id = :UserInfo.getUserId()].ContactId;
            }
            if(needsconid != '' && needsconid != null){
                SP_CaseId = [SELECT id FROM Case WHERE contactId = :needsconid LIMIT 1].id;
                SP_CaseId = SP_CaseId.subString(0,15);
            }
        }
        else{
            SP_CaseId = SP_CaseId.subString(0,15);
        }
        
        
        if(conId != null && conId != ''){
            contactCase = [SELECT id, contact.Name, Active_Goal__c 
                        FROM Case
                        WHERE contactId = : conId];  
        } else if(SP_CaseId != null && SP_CaseId != ''){
            contactCase = [SELECT id, contact.Name, Active_Goal__c 
                        FROM Case
                        WHERE id = : SP_CaseId];
        }else{
            displaySP = false;
        }
         
        //MyProfile = [select FirstName, LastName from Contact where Id = :contactCase.contact.Id]; 
        //MyProfileName = MyProfile.FirstName + ' ' + MyProfile.LastName + ' Family';
        
        if(displaySP){
            MyProfile = [select FirstName, LastName from Contact where Id = :contactCase.contact.Id]; 
            MyProfileId = MyProfile.Id;
            MyProfileName = MyProfile.FirstName + ' ' + MyProfile.LastName + ' Family';
            activeTab = contactCase.Active_Goal__c;
            populateMilestonesAndRes();
        }   
        
        //If-Code for merging Success plan page with Needs Assessment page
        if(SP_CaseId != '' && SP_CaseId != null){
            displayServices();  
        }
    }
     public void displayServices(){
     //Instantiation
        system.debug('inside display servies:');
        serviceList = new list<SP_Transactional__c>();
        mileStoneStepList = new list<SP_Transactional__c>();
        stepServiceMap = new Map<ID, List<SP_Transactional__c>>();
        domainMileStoneMap = new Map<String, List<SP_Transactional__c>>();
        stepSelectedServicesMap = new Map<ID, Map<String, List<SP_Transactional__c>>>();
         try{
             System.debug('-->Case Nmb'+SP_CaseId);
        transList = [Select id,Parent__c, Selected_Step__c, Email__c, Services_About_Us__c, Services_Button_Text__c, Services_Type__c, Name__c,Parent_Milestone__c,Parent_Milestone__r.name,Parent_Milestone__r.name__c,type__C,Goal__r.Domain__c,Parent_Milestone__r.status__c ,
                            Location__c, Phone_Number__c, Website__c,GoalDescription__c,Status__c,How_It_Works__c,What_To_Do__c 
                     from SP_Transactional__c
                     where Goal__r.Domain__c IN('Shelter','Food','Income','Adult Education') and Goal__r.Show_Domain__c = true 
                     and CaseId__c  = :SP_CaseId AND SuccessPlanStatus__c ='Active' AND Type__c IN ('MilestoneSteps','MilestoneHeads','Service','Resource' )
                     ORDER by Goal__r.Domain__c
                     ];   
        
             
        for(SP_Transactional__c s : transList){
            system.debug('s.Parent_Milestone__c '+s.Parent_Milestone__c);
            system.debug('s.type__c '+s.type__c);
            system.debug('s.Parent_Milestone__r.status__c '+s.Parent_Milestone__r.status__c);
            
            
            if((s.type__c == 'MilestoneSteps' || s.Type__c == 'MilestoneHeads') && s.Parent_Milestone__r.status__c)
            if(domainMileStoneMap.containsKey(s.Goal__r.Domain__c)){
                domainMileStoneMap.get(s.Goal__r.Domain__c).add(s);
            }else{
                list<SP_Transactional__c> tempList = new list<SP_Transactional__c>(); 
                tempList.add(s);
                domainMileStoneMap.put(s.Goal__r.Domain__c, tempList);
                domainsList.add(s.Goal__r.Domain__c);
            }           
        }
             
        List<SP_Transactional__c> transactionsList =  [Select id,Parent__c, Email__c, Services_About_Us__c, Name__c,Parent_Milestone__c,Parent_Milestone__r.name__c,type__C,Goal__r.Domain__c,Parent_Milestone__r.status__c ,
                                                                Location__c, Phone_Number__c, Website__c,GoalDescription__c,Status__c,How_It_Works__c,What_To_Do__c,Selected_Step__c,
                                                                (select id FROM SP_Transactional__r WHERE type__c='service')
                                                         from SP_Transactional__c
                                                         where Goal__r.Domain__c IN('Shelter','Food','Income','Adult Education') and Goal__r.Show_Domain__c = true AND Parent_Milestone__r.Status__c = true 
                                                         and CaseId__c  = :SP_CaseId AND SuccessPlanStatus__c='Active' AND Type__c IN ('MilestoneSteps','MilestoneHeads')
                                                         ORDER by Goal__r.Domain__c
                                                         ];   
             for(SP_Transactional__c s : transactionsList){
                if(newUIMap.containsKey(s.Goal__r.Domain__c)){
                    if(newUIMap.get(s.Goal__r.Domain__c).containsKey(s.Parent_Milestone__r.name__c)){
                        
                    }else{                        
                        Map<String, SP_Transactional__c> tempMap = new Map<String, SP_Transactional__c>();
                        if(s.SP_Transactional__r != null && s.SP_Transactional__r.size() > 0)
                            tempMap.put(s.Parent_Milestone__r.name__c, s.SP_Transactional__r[0]);
                        else{
                            tempMap.put(s.Parent_Milestone__r.name__c, new SP_Transactional__c());
                        }
                    }
                }else{
                    Map<String, SP_Transactional__c> tempMap = new Map<String, SP_Transactional__c>();
                    if(s.SP_Transactional__r != null && s.SP_Transactional__r.size() > 0)
                        tempMap.put(s.Parent_Milestone__r.name__c, s.SP_Transactional__r[0]);
                    else{
                        tempMap.put(s.Parent_Milestone__r.name__c, new SP_Transactional__c());
                    }
                    newUIMap.put(s.Goal__r.Domain__c, tempMap);
                }
            }

        system.debug('\n\n domainMileStoneMap:: ' + domainMileStoneMap + '\n\n newUIMap : ' + newUIMap );
             
             vShowServices = new Map<ID, Map<String, List<SP_Transactional__c>>> ();
             Map<String, List<SP_Transactional__c>> vServTypeMap = new Map<String, List<SP_Transactional__c>>();
             List<SP_Transactional__c> vSerDetails = new List<SP_Transactional__c> ();
             for(SP_Transactional__c srv : transList)
             {
                 if(srv.Type__c == 'Service' || srv.Type__c == 'Resource' )
                 {
                     if(vShowServices.containsKey(srv.Parent_Milestone__r.id))
                     {
                         if(srv.Services_Type__c != null && srv.Services_Type__c != '')
                         {
                             if(vShowServices.get(srv.Parent_Milestone__r.id).containsKey(srv.Services_Type__c)) 
                             {
                                 vShowServices.get(srv.Parent_Milestone__r.id).get(srv.Services_Type__c).add(srv);
                             } 
                             else
                             {
                                 List<SP_Transactional__c> vSerDetailsTmp = new List<SP_Transactional__c> ();
                                 vSerDetailsTmp.add(srv); 
                                 vShowServices.get(srv.Parent_Milestone__r.id).put(srv.Services_Type__c,vSerDetailsTmp); 
                                 
                             }
                         }
                    }
                     else
                     {
                         if(srv.Services_Type__c != null && srv.Services_Type__c != '')
                         {
                             List<SP_Transactional__c> vSerDetailsTmp = new List<SP_Transactional__c> ();
                             vSerDetailsTmp.add(srv);                         
                             Map<String, List<SP_Transactional__c>> vServTypeMapTmp = new Map<String, List<SP_Transactional__c>>();
                             vServTypeMapTmp.put(srv.Services_Type__c , vSerDetailsTmp);
                             vShowServices.put(srv.Parent_Milestone__r.id, vServTypeMapTmp); 
                         }
                         else
                         {
                             SP_Transactional__c srtemp = new SP_Transactional__c();
                             List<SP_Transactional__c> vSerDetailsTmp = new List<SP_Transactional__c> ();
                             vSerDetailsTmp.add(srtemp);                         
                             Map<String, List<SP_Transactional__c>> vServTypeMapTmp = new Map<String, List<SP_Transactional__c>>();
                             vServTypeMapTmp.put(srv.Services_Type__c , vSerDetailsTmp);
                             vShowServices.put(srv.Parent_Milestone__r.id, vServTypeMapTmp); 
                         }
                     }
                 }
             }
             
             System.debug('VVV - vShowServices : ' + vShowServices);
         
             /*Khushboo - Sprint 10 changes*/
             vServices = new Map<ID, List<SP_Transactional__c>> ();
             for(SP_Transactional__c srv : transList)
             {
                 if(srv.Type__c == 'Service' || srv.Type__c == 'Resource' )
                 {
                     if(vServices.containsKey(srv.Parent_Milestone__r.id))
                     {
                              vServices.get(srv.Parent_Milestone__r.id).add(srv);
                    }
                     else
                     {
                             List<SP_Transactional__c> vSerDetailstmp = new List<SP_Transactional__c> ();
                             vSerDetailstmp.add(srv);                         
                             vServices.put(srv.Parent_Milestone__r.id, vSerDetailstmp); 
                     }
                 }
             }
             
             
         /* Old Way - Selected Unselected undone */
         for(SP_Transactional__c s : transList){
             if(s.Type__c == 'Service' || s.Type__c == 'Resource' ){
                 if(stepSelectedServicesMap.containsKey(s.Parent_Milestone__r.id)){
                     if(s.Status__c){
                         if(stepSelectedServicesMap.get(s.Parent_Milestone__r.Id).containsKey('Selected')){
                             stepSelectedServicesMap.get(s.Parent_Milestone__r.Id).get('Selected').add(s);
                         }else{
                             List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                             tList.add(s);
                             stepSelectedServicesMap.get(s.Parent_Milestone__r.Id).put('Selected', tList);
                         }
                     }else{
                         if(stepSelectedServicesMap.get(s.Parent_Milestone__r.Id).containsKey('Unselected')){
                             stepSelectedServicesMap.get(s.Parent_Milestone__r.Id).get('Unselected').add(s);
                         }else{
                             List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                             tList.add(s);
                             stepSelectedServicesMap.get(s.Parent_Milestone__r.Id).put('Unselected', tList);
                         }
                     }
                 }else{
                     if(s.Status__c){
                         List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                         tList.add(s);
                         Map<String, List<SP_Transactional__c>> tMap = new Map<String, List<SP_Transactional__c>>();
                         tMap.put('Selected', tList);       
                         stepSelectedServicesMap.put(s.Parent_Milestone__r.Id, tMap);
                     }else{
                         List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                         tList.add(s);
                         Map<String, List<SP_Transactional__c>> tMap = new Map<String, List<SP_Transactional__c>>();
                         tMap.put('Unselected', tList);     
                         stepSelectedServicesMap.put(s.Parent_Milestone__r.Id, tMap);
                     }
                 }
             }    
         } 
         for(String s : stepSelectedServicesMap.keyset()){
             if(!stepSelectedServicesMap.get(s).containsKey('Selected')){
                 List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                 stepSelectedServicesMap.get(s).put('Selected', tList);
             }
             if(!stepSelectedServicesMap.get(s).containsKey('Unselected')){
                 List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                 stepSelectedServicesMap.get(s).put('Unselected', tList);
             }
         }
         for(String s : domainMileStoneMap.keyset()){
             for(SP_Transactional__c sp : domainMileStoneMap.get(s)){
                 if(!stepSelectedServicesMap.containsKey(sp.Id)){
                    List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                    Map<String, List<SP_Transactional__c>> tMap = new Map<String, List<SP_Transactional__c>>();
                    tMap.put('Unselected', tList);      
                    tMap.put('Selected', tList);    
                    stepSelectedServicesMap.put(sp.id, tMap); 
                 }
             }
         }
        system.debug('\n\n stepSelectedServicesMap:: > ' + stepSelectedServicesMap);  
         }catch(Exception e){}
         
         system.debug('end of fucntions:');
    }

    public PageReference dummyMethod() {
        //CitizenSuccessPlanCtrl();
        //return null;
        
        
        
            populateMilestonesAndRes();      
        displayServices();
        return null;
    }
    
    
    public PageReference EditDetails(){
        isEdit = true;
        return null;
    }
        
    public PageReference CancelSave(){
        System.debug('INSIDE CancelSAve ------------------------------------------->');
        isEdit = false;
        return null;
    }
    public PageReference SaveMilestone(){
        isEdit = false;
        system.debug('\n\n mStepId :: ' + mStepId);
        update milestoneMap.get(mStepId);
        Set<SP_Transactional__c> tSet =  new Set<SP_Transactional__c> ();
        for(SP_Transactional__c sp : milestoneItems.get(mStepId)){
            if(sp.id != mStepId){
                if(sp.Completed__c == true){
                    sp.Milestone_Status__c = 'Completed';
                    sp.End_Date__c = system.today();
                }else{
                    sp.Milestone_Status__c = 'In Progress';
                }
                tSet.add(sp);
            }
        }
        //tSet.add(milestoneMap.get(mStepId));
        List<SP_Transactional__c> tList =  new List<SP_Transactional__c> ();
        tList.addAll(tSet);
        update tList;
        if(toDeleteStep != '' && toDeleteStep != null){
            string sId = toDeleteStep.substring(2, toDeleteStep.length()); 
            delete [SELECT id FROM SP_Transactional__c WHERE id=: sId];         
        }
        //milestoneMap = new Map<Id, SP_Transactional__c>();
        //milestoneItems = new Map<id, List<SP_Transactional__c>>();
        //populateMilestonesAndRes();
        return null;
    } 
    
    public PageReference SaveResources(){
        isEdit = false;
        
        rscId = rscId.substring(2,rscId.length());
        List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
        goalResources.get(rscId);
        system.debug('\n\n tList:: ' + tList);
        update tList;   
        goalResources = new Map<Id, List<SP_Transactional__c>>();       
        populateMilestonesAndRes();
        return null;
    }
    
    public PageReference DeleteResource(){
        delete [SELECT id FROM SP_Transactional__c WHERE id=: toDeleteRsc];
        goalResources = new Map<Id, List<SP_Transactional__c>>();       
        populateMilestonesAndRes();
        return null;
    }
    
    public void populateMilestonesAndRes(){
     try{
       List<SP_Goal__c> Domains = [SELECT Name, Title__c, Case__c, Domain__c, 
                                                (Select id, Name__c, Start_Date__c,End_date__c, Type__c, Status__c , Goal__r.id, Goal__r.Domain__c, completed__C,
                                                        Notes_And_Recommendations__c,GoalDescription__c,How_It_Works__c
                                                    from SP_Transactional__r Where Type__c = 'Milestone' AND Status__c = true Order BY End_date__c)
                                               FROM SP_Goal__c
                                               WHERE case__c = :contactCase.id AND SuccessPlanId__r.Status__c = 'Active'];
        for(SP_Goal__c goal : Domains){
            for(SP_Transactional__c milestone: goal.SP_Transactional__r){
                if(goalsMap.containsKey(goal.Domain__c)){
                    goalsMap.get(goal.Domain__c).add(milestone.id);
                }else{
                    List<id> tempList = new List<id>();
                    tempList.add(milestone.id);
                    goalsMap.put(goal.Domain__c,tempList);
                }
                milestoneMap.put(milestone.id, milestone);
                if(goal.Domain__c == 'Shelter' || goal.Domain__c == 'Food'){
                    if(categoryGoalMap.get('Basic Needs').containsKey(goal.Domain__c)){
                        categoryGoalMap.get('Basic Needs').get(goal.Domain__c).add(milestone.id);    
                    }else{
                        List<id> tempList2 = new List<id>();
                        tempList2.add(milestone.id);
                        categoryGoalMap.get('Basic Needs').put(goal.Domain__c,tempList2);
                    }
                }
                if(goal.Domain__c == 'Health Care' || goal.Domain__c == 'Mental Health' || goal.Domain__c == 'Substance Abuse'){
                    if(categoryGoalMap.get('Health & Wellness').containsKey(goal.Domain__c)){
                        categoryGoalMap.get('Health & Wellness').get(goal.Domain__c).add(milestone.id);    
                    }else{
                        List<id> tempList2 = new List<id>();
                        tempList2.add(milestone.id);
                        categoryGoalMap.get('Health & Wellness').put(goal.Domain__c,tempList2);
                    }
                }
                if(goal.Domain__c == 'Income' || goal.Domain__c == 'Transportation' || goal.Domain__c == 'Childcare' || goal.Domain__c == 'Legal'){
                    if(categoryGoalMap.get('Living & Working').containsKey(goal.Domain__c)){
                        categoryGoalMap.get('Living & Working').get(goal.Domain__c).add(milestone.id);    
                    }else{
                        List<id> tempList2 = new List<id>();
                        tempList2.add(milestone.id);
                        categoryGoalMap.get('Living & Working').put(goal.Domain__c,tempList2);
                    }                    
                }
                if(goal.Domain__c == 'Child Education' || goal.Domain__c == 'Adult Education' || goal.Domain__c == 'Financial Management' 
                        || goal.Domain__c == 'Life Skills' || goal.Domain__c == 'Parenting'){
                    if(categoryGoalMap.get('Education').containsKey(goal.Domain__c)){
                        categoryGoalMap.get('Education').get(goal.Domain__c).add(milestone.id);    
                    }else{
                        List<id> tempList2 = new List<id>();
                        tempList2.add(milestone.id);
                        categoryGoalMap.get('Education').put(goal.Domain__c,tempList2);
                    }
                }
                if(goal.Domain__c == 'Support Network' || goal.Domain__c == 'Community Involvement'){
                    if(categoryGoalMap.get('Community').containsKey(goal.Domain__c)){
                        categoryGoalMap.get('Community').get(goal.Domain__c).add(milestone.id);    
                    }else{
                        List<id> tempList2 = new List<id>();
                        tempList2.add(milestone.id);
                        categoryGoalMap.get('Community').put(goal.Domain__c,tempList2);
                    }
                }  
                
            }
        }
         for(String s : categoryGoalMap.keySet()){
                    if(categoryGoalMap.get(s).keyset().size() == 0 ){
                        categoryGoalMap.remove(s);
                    }    
                }

        Map<id, SP_Transactional__c> milestoneStepMap = new Map<Id, SP_Transactional__c>([Select id, Name__c, Start_Date__c,End_date__c, Type__c, Status__c , Goal__r.id, GoalDescription__c,
                                                                    Goal__r.Domain__c, completed__C, Parent_Milestone__c,Notes_And_Recommendations__c,Notes_And_Recommendations2__c,
                                                                    Milestone_Status__c,How_It_Works__c,Selected_Step__c
                                                                FROM SP_Transactional__c 
                                                                WHERE (Type__c = 'MilestoneSteps' OR Type__c = 'MilestoneHeads')
                                                                    AND parent_Milestone__c IN : milestoneMap.keyset() 
                                                                        Order BY End_date__c]);
        system.debug('\n\n milestoneStepMap:: ' + milestoneStepMap);
        
        Map<id, SP_Transactional__c> milestonesMap = new Map<id, SP_Transactional__c> ([Select id, Name__c, Start_Date__c,End_date__c, Type__c, Status__c , Goal__r.id, GoalDescription__c,
                                                                    Goal__r.Domain__c, completed__C, Parent_Milestone__c,Notes_And_Recommendations__c,Notes_And_Recommendations2__c,
                                                                    Milestone_Status__c,How_It_Works__c
                                                                FROM SP_Transactional__c 
                                                                WHERE Type__c ='Milestone' AND ID In: milestoneMap.keyset()
                                                                Order BY End_date__c]);
                                                                
        for(SP_Transactional__c sp: milestoneStepMap.values()){
            if(milestoneItems.containsKey(sp.parent_Milestone__c)){
                milestoneItems.get(sp.parent_Milestone__c).add(sp);
            }else{
                List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                tempList.add(sp);
                milestoneItems.put(sp.parent_Milestone__c, tempList);
            } 
        }
        /*for(SP_Transactional__c sp: milestonesMap.values()){
            if(milestoneItems.containsKey(sp.id)){
                milestoneItems.get(sp.id).add(sp);
            }else{
                List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                tempList.add(sp);
                milestoneItems.put(sp.id, tempList);
            }
        }*/
        system.debug('\n\n milestoneItems:: ' + milestoneItems);
        
        List<SP_Transactional__c> Resources = [Select id, Resource_Comments__c, Resource_Description1__c, Resource_Description2__c, Name__c,Goal__c , Goal__r.Domain__c,GoalDescription__c 
                                                    FROM SP_Transactional__c 
                                                    WHERE Type__c ='Resource' AND Status__c = true AND Goal__c IN :Domains];
                                                    
        Map<String, List<SP_Transactional__c>>  DomainResources = new Map<String, List<SP_Transactional__c>>();
        
        for(SP_Transactional__c sp: Resources){
            if(DomainResources.containsKey(sp.Goal__r.Domain__c)){
                DomainResources.get(sp.Goal__r.Domain__c).add(sp);
            }else{
                List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                tList.add(sp);
                DomainResources.put(sp.Goal__r.Domain__c, tList);
            }               
        }
        for(String s : goalsMap.keyset()){
            for(Id mileId : goalsMap.get(s)){
                if(DomainResources.get(s) != null)
                    goalResources.put(mileId,DomainResources.get(s));
                else
                    goalResources.put(mileId, new List<SP_Transactional__c>() );
            }
        }        
        system.debug('\n\n categoryGoalMap:: ' + categoryGoalMap + '\n\n goalsMap:: ' + goalsMap);
     }catch(Exception e){}
        
    }
    
    public class MilestonesWrapper{
        public Date     StartDate           {get;set;}
        public Date     EndDate             {get;set;}
        public String   Description         {get;set;}
        public String   StepType            {get;set;}
        public String   StepId              {get;set;}
        public String   isComplete          {get;set;}
    }   

    public PageReference saveActiveTab(){       
        contactCase.Active_Goal__c = activeTab;
        update contactCase;
        return null;
    }
    
    public PageReference updateService(){
        string serviceId = ApexPages.currentPage().getParameters().get('ServId');
        String ServStat = ApexPages.currentPage().getParameters().get('ServStat');
        system.debug('\n\n serviceId: ' + serviceId + '\n\n ServStat - ' +  ServStat);
        SP_Transactional__c service = [SELECT id, Status__c,Parent_Milestone__c FROM SP_Transactional__c WHERE id = :serviceId ];          
        if(ServStat == 'true')
            service.Status__c = true;
        else
            service.Status__c = false;
        update service;
        List<SP_Transactional__c> otherServices = [SELECT id,Status__c 
                                                        FROM SP_Transactional__c 
                                                        WHERE type__c IN ('Service','Resource') 
                                                            AND Parent_Milestone__c = :service.Parent_Milestone__c 
                                                            AND id != :service.id];
        for(SP_Transactional__c os : otherServices){
            os.Status__c = false;
        }
        update otherServices;
        PageReference pr = new PageReference('/CitizenSuccessPlanProcess?SP_CaseId=' + SP_CaseId); 
        pr.setRedirect(true);
        return pr;
    }
   
   /* public void updateServiceStat(String servStat, String servId){
        system.debug('\n\n servStat :' + servStat + '\n servId : ' + servId);
        SP_Transactional__c service = [SELECT id,Service_Selected__c 
                                            FROM SP_Transactional__c
                                            WHERE id= : servId];
        service.Service_Selected__c = servStat;
        update service;
    }*/
    
    @RemoteAction
    global static void updateServiceStatRT(String servStat, String servId, String servType){
        
        SP_Transactional__c service = [SELECT id, Status__c,Parent_Milestone__c FROM SP_Transactional__c WHERE id = :servId ];          
        if(servStat == 'true')
            service.Status__c = true;
        else
            service.Status__c = false;
        update service;
        List<SP_Transactional__c> otherServices = [SELECT id,Status__c 
                                                        FROM SP_Transactional__c 
                                                        WHERE type__c IN ('Service','Resource') 
                                                            AND Parent_Milestone__c = :service.Parent_Milestone__c 
                                                            AND id != :service.id];
        for(SP_Transactional__c os : otherServices){
            os.Status__c = false;
        }
        update otherServices;
        
    }
    
    @RemoteAction
    global static void prePopulateCommonApp(String vCaseId, String vServices){
        
        system.debug('servStat :' + vServices);
        String vProgSelected = '';
        if(vServices.length() != 0)
        {
            List<String> vSrvArr = vServices.split('\\|');
            List<Id> vSrvId = new List<Id>();
            
            for(String s : vSrvArr)
            {
                Id tmpId = Id.valueOf(s);
                vSrvId.add(tmpId);
            }
            System.debug('vSrvId' + vSrvId);
            List<SP_Transactional__c> vSrvT = [SELECT id,Services_Type__c, Type__c
                                            FROM SP_Transactional__c
                                            WHERE id IN :vSrvId AND Type__c = 'MilestoneSteps'];
            system.debug('vSrvT :' + vSrvT);
            //Case vCurrCase = [select Id, CasePrograms__c from case where Id = :vCaseId ];
            SuccessPlan__c  vCurrSP = [Select Id,NeedsAssessmentId__c from SuccessPlan__c where CaseId__c = :vCaseId and Status__c = 'Active'];
           	List<CommonApplication__c> calst = [Select Id,CAPrograms__c,SPCAServices__c from CommonApplication__c where SuccessPlanId__c = :vCurrSp.Id];
            if(calst.size()==0){
                CommonApplication__c newca = new CommonApplication__c();
                newca.CaseId__c = vCaseId;
                newca.Status__c = 'Active';
                newca.SuccessPlanId__c = vCurrSp.Id;
                newca.NeedsAssessmentId__c = vCurrSp.NeedsAssessmentId__c;
                newca.CAPrograms__c = '';
                newca.SPCAServices__c = '';
                insert newca;
                calst.add(newca);
            }
            //String vProgramTmp = vCurrCase.CasePrograms__c;
            String vProgramTmp = calst[0].CAPrograms__c;
            
            if(vProgramTmp == null)
                vProgramTmp = '';
            if(vSrvT.size() != 0)
            {
                for(SP_Transactional__c st : vSrvT)
                {
                    if(st.Services_Type__c != '' && st.Services_Type__c != null)
                    {
                        if(st.Services_Type__c.contains('Housing Choice Voucher Program'))
                        {
                            if(!vProgramTmp.contains('HCVP'))                        
                                vProgSelected = vProgSelected + 'HCVP|';
                        }
                        else if(st.Services_Type__c.contains('State Emergency Relief'))
                        {
                            if(!vProgramTmp.contains('SER'))
                                vProgSelected = vProgSelected + 'SER|';
                        }
                        else if(st.Services_Type__c.contains('Food Assistance Program'))
                        {
                            if(!vProgramTmp.contains('FAP'))
                                vProgSelected = vProgSelected + 'FAP|';
                        }
                        else if(st.Services_Type__c.contains('Free or Reduced Lunch Program'))
                        {
                            if(!vProgramTmp.contains('FRLP'))
                                vProgSelected = vProgSelected + 'FRLP|';
                        }
                    }
                    
                }
                
                system.debug('vProgSelected :' + vProgSelected); 
                
                if(vProgSelected.length() != 0)
                {
                    vProgSelected = vProgSelected.substring(0, vProgSelected.length()-1);
                    //Case vCurrCase = [select Id, CasePrograms__c from case where Id = :SP_CaseId ];
                    //String vProgramTmp = vCurrCase.CasePrograms__c;
                    if(vProgramTmp.length() == 0 || vProgramTmp == null)
                    {
                        //vCurrCase.CasePrograms__c = vProgSelected;
                        calst[0].SPCAServices__c = vProgSelected;
                    }
                    else
                    {
                       //vCurrCase.CasePrograms__c =  vProgramTmp + '|' + vProgSelected;
                       calst[0].SPCAServices__c = vProgramTmp + '|' + vProgSelected;
                    }
                    
                    //update vCurrCase;
                    update calst[0];
                }
                
            }
            
        }
        
    }

    public void prePopulateCommonApp()
    {
        
    }
    
    @RemoteAction
    global static void markSPCompleteRt(String caseid)
    {
        Case vCase = [select id,SP_Completion__c from case where Id = :caseid];
            if(vCase.SP_Completion__c != true)
            {
                vCase.SP_Completion__c = true;
                update vCase;
            }
        SuccessPlan__c vSp = [select id,Completed__c from SuccessPlan__c where CaseId__c = :caseid and Status__c='Active'];
            if(vSp.Completed__c != true)
            {
                vSp.Completed__c = true;
                update vSp;
            }
    }
        
        public void UpdateServiceStAF(){
            
            
        }
    @RemoteAction
    public static void UpdateMilestoneIds(String Id1,String Id2, String CaseID){
        
        System.debug('CaseID' + CaseID);
        List<ID> midList = new List<ID>();
        List<SP_Transactional__c> UpdatedList = new List<SP_Transactional__c>();
        Map<String, SP_Transactional__c> allMid = new   Map<String, SP_Transactional__c>();
        
        for(SP_Transactional__c s : [Select id, Name__c,Selected_Step__c,Type__c, Status__c , Goal__r.id, Goal__r.Domain__c, completed__C,GoalDescription__c 
                                     
                                     from SP_Transactional__c Where Type__c = 'MilestoneSteps' and CaseId__c =:CaseID and SuccessPlanStatus__c='Active']){
                                         
                                         //midList.add(s.id);
                                         allMid.put(s.id,s);
                                         
                                     }
        
        System.debug('allMid' + allMid);
        List<String> CheckedList = new List<String>();
        List<String> UncheckedList = new List<String>();
        
        
        for(String key : Id1.split('\\|')){
            CheckedList.add(key);
        } 
        System.debug('CheckedList -->FO' + CheckedList);
        for(String key : Id2.split('\\|')){
            UncheckedList.add(key);
        } 
        System.debug('UncheckedList -->FO' + UncheckedList);
        
        
        for(String idd1 : CheckedList ){
            SP_Transactional__c s1 = allMid.get(idd1);
            System.debug(s1);
            if(s1 != null){
                s1.Selected_Step__c = true;
                System.debug('making field True');
                //update s1;
                UpdatedList.add(s1);                
            }
        }
        for(String idd2 :UncheckedList ){
            SP_Transactional__c s2 = allMid.get(idd2);
            if(s2 != null){
                s2.Selected_Step__c = false;
                System.debug('making field False');
                //update s2;
                UpdatedList.add(s2);                
            }
        }
        update UpdatedList;
    }
    
    @RemoteAction
    public static void Markcompletestep(String stpid, Boolean vchk, String vaddnotes){ 
        List<SP_Transactional__c> Markstep = [Select Id, Completed__c, Notes_And_Recommendations__c from SP_Transactional__c where Type__c = 'MilestoneSteps' and Id=:stpid];
        for(SP_Transactional__c tmpstp : Markstep)
        {
            tmpstp.Completed__c = vchk;
            tmpstp.Notes_And_Recommendations__c = vaddnotes;
        }
        update Markstep;
    }
}