@isTest

public class MI_CommonAppTest  {

  
 
   @IsTest(SeeAllData=true) 
public static void test_CreateMilestones(){


        Account acc=[Select id from Account where name='MIISD'];
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married',Zipcode__c ='49303');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
       
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);  
        insert cs;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
        
       
   MI_CommonApp Ca= new MI_CommonApp();
   ApexPages.currentPage().getParameters().put('CaseId',cs.id); 
   ApexPages.currentPage().getParameters().put('AssessmentConId',con.id); 
   
   MI_CommonApp.getAllContactDetails(cs.id);
   MI_CommonApp.getAllContactAvatar(cs.id);
   MI_CommonApp.getAllContactDetailsRemote(cs.id);
   
   
   ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
   PageReference pageRef = Page.MI_CommonApplication;
   pageRef = Page.MI_CommonApplication;
   Test.setCurrentPage(pageRef);
   ApexPages.currentPage().getParameters().put('ProgramSelected','CDC');
   ca.ShowAttachmentInfo();
   ApexPages.currentPage().getParameters().put('ProgramSelected','FAP');
   ca.ShowAttachmentInfo(); 
   ApexPages.currentPage().getParameters().put('ProgramSelected','SER');
   ca.ShowAttachmentInfo(); 
   ApexPages.currentPage().getParameters().put('ProgramSelected','Cash');
   ca.ShowAttachmentInfo(); 
   
   MI_CommonApp.saveAttachmentIntro('CommonAppIntro.txt','CDC', cs.id, con.id);
   MI_CommonApp.saveAttachmentIndividual('CommonAppAnswers.txt',cs.id, con.id);
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'Cash');
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'FAP');
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'CDC');
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'Medicaid');
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'SER');
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'Child Supp.');
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'WIC');
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'HCVP');
   MI_CommonApp.completeApplication('CommonAppAnswers.txt',cs.id, con.id,'FRLP');
   MI_CommonApp.retrieveattachment(cs.id);
   MI_CommonApp.GetQuestionsIntial('CommonAppAnswers.txt', cs.id);
   
   
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'Cash');
   
  
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'FAP');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'CDC');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'Medicaid');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'SER');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'Child Supp.');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'WIC');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'HCVP');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'FRLP');
  
   
  
   
   
  
          
 
 }
 }