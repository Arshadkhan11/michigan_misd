global with sharing class MI_CoachDashCont {

    
    //bugs
    public User CurrentChatUser {get;set;}
    
    public  Network Networkcom {get;set;}
    
    public String addcomment {get;set;}
    
    public List<User> CommUsers {get;set;}
     
    public String CommentMention { get; set; }
    
    public String selectedUser { get; set; }
    
    public String selectedAccout {get;set;}
    
    public String PostMention { get; set; }

    public String PostText { get; set; }
    
    public String CommentText {get;set;}
       
    public id FeedElementId {get;set;}
   
    public ConnectApi.FeedElementPage feedElementPage {get;set;}
    public ConnectApi.FeedItemInput feedItemInput {get;set;}
    public ConnectApi.MentionSegmentInput mentionSegmentInput {get;set;}
    public ConnectApi.MessageBodyInput messageBodyInput {get;set;}
    public ConnectApi.TextSegmentInput textSegmentInput {get;set;}  
    public ConnectApi.LinkSegmentinput linkSegmentInput {get;set;} 
    
    // tasks fixes
    public MI_Activity__c ALnewtask {get;set;}
    public String EditDate {get;set;}
    public String EditTime {get;set;}    

    //remoting TASKS
    public Id TaskId { get; set; }
    public static MI_Activity__c activity { get; set; }
    
    public Id delId {get;set;}
    public MI_Activity__c newTask {get;set;}
    public String newDate {get;set;}
    public String newTime {get;set;}
    public Id EditId {get;set;}
    public MI_Activity__c EditTask {get;set;}
    public boolean frontdiv {get;set;}
    public boolean newdiv {get;set;}
    public boolean editdiv {get;set;}
    public Integer overDueCount {get;set;}
    public String TaskSubject {get;set;}
    public String TaskPriority {get;set;}
    public String TaskStatus {get;set;}
    public String TaskType {get;set;}
    public string TaskNotes {get;set;}
    public String TaskClient {get;set;}
    
    public List<MI_Activity__c> taskListUserLogin {get;set;}
    public List<MI_Activity__c> overDueList {get;set;}
    public String taskIdChosen {get; set;}
    public Id TaskCompleteId {get;set;}
    
   public Integer counterOverdueTasks {get;set;}
   public Integer counterTodayTotalTasks {get;set;} 
   public Integer counterTodayCompletedTasks {get;set;}

   public Integer counterDueTodayTasks {get;set;}
    
// sampleCon - init - START
    public List<String> selectedCategory {get;set;}
    public List<News__c> UserList {get;set;}
    public date selectedDate {get;set;}
    public String selectedKeyword {get; set;}
    public boolean flag {get;set;}
    public List<SelectOption> categoryList {get;set;}
// sampleCon - init - END    

// query variables    
    public List<News__c> newsFeedItemslist {get;set;}
    public List<News__c> newsFeedItemslistCitizen {get;set;}
    public List<News__c> newsFeedItemslistCoach {get;set;}
    
    
// checkbox variables    
   public Boolean re1{get;set;}
   public Boolean re2{get;set;}
   public Boolean re3{get;set;}
   public Boolean re4{get;set;}
    
    public String UserCatPref1{get;set;}
    public Id RecTypeId{get;set;}
    
    public User currentUser {get;set;}
    
    public String latestcaseid {get;set;}
    
    public Object TextSegType { get{
     
      return ConnectApi.MessageSegmentType.Text;
      }
   }
   
   public Object LinkSegType { get{
     
      return ConnectApi.MessageSegmentType.Link;
      }
   }
   public Object MentionSegType {get{    
     
     return ConnectApi.MessageSegmentType.Mention;
     }
   }     
    
    public Integer Weeksinprog {get;set;}
    public List<citizendashinfo> dashinfo {get;set;}
    public Decimal goalsComp {get;set;}
    
    public MI_CoachDashCont()
    {
     
      //Removing hard coding of the CitizenCoach
    // latestcaseid=[SELECT ID, Name, Title, LatestCaseID__c from Contact where Name='Tamara Davis' and Title='Master'].LatestCaseID__c;  
   //List<Case> CaseIds = new List<Case> ();
     //  CaseIds =  [select id, contact.name from case where contact.id = : UserInfo.getUserId() order by CreatedDate desc limit 1]; 
     User loginuser = [select CreatedDate, contactid from user where id =:UserInfo.getUserId()];
     Id vUserContactId = loginuser.contactid;
        try{
     List<Case> CaseIds =  [select id from case where contact.id = :vUserContactId order by CreatedDate desc limit 1];
     if(!CaseIds.isEmpty())   
     latestcaseid = CaseIds[0].id;
        else
        latestcaseid = null;    
        }catch(Exception e){}    
        
    //newsFeedItemslist = queryNewsFeedList();// ARCHVIE
    newsFeedItemslistCitizen = queryNewsFeedListCitizen(); 
    newsFeedItemslistCoach = queryNewsFeedListCoach();
        
        
        RecTypeId = [select Id from RecordType where DeveloperName='Task'].Id;
        
       newTask = new MI_Activity__c();
       EditTask=null;        
       overDueList = [SELECT Id,Subject__c,Priority__c,Status__c,Activity_Date__c,Description__c,task_type__c from MI_Activity__c where ( Activity_Date__c < TODAY ) AND ( Owner.Id=:UserInfo.getUserId() ) ORDER BY Activity_Date__c desc];
       overDueCount = overDueList.size();
       taskListUserLogin = querytaskListUserLogin();
        
   		//Success Plan Widget -- Start
        Weeksinprog =  (loginuser.CreatedDate.Date().daysBetween(system.now().Date()))/7;
        goalsComp = 0;
        dashinfo = new List<citizendashinfo>();
        CitizenSuccessPlan();
        //Success Plan Widget -- End
    }

// Start -> news feed on Citizen dashboard
    public List<News__c> queryNewsFeedListCitizen()
    {
        Boolean Check = false;//checks whether All has been selected
        String AllVar = 'All';//checks for All, other values in multi-select
        User UNC = [SELECT Id, News_Category_Filter__c, News_Educational__c, News_FamilyEvents__c, News_JobRelated__c, News_Medical__c
                    FROM User where username=:UserInfo.getUsername()
                    LIMIT 1];
        String PrefConcat1;
        String PrefConcat2;
        String PrefConcat3;
        String PrefConcat4;
        
        if(UNC.News_Educational__c==true)
        {
            PrefConcat1 = 'Educational;';
        }
        else
        {
           PrefConcat1 = 'blank;';
        }
        
        if(UNC.News_FamilyEvents__c==true)
        {
            PrefConcat2 = 'Family Events;';
        }
        else
        {
           PrefConcat2 = 'blank;';
        }

        if(UNC.News_JobRelated__c==true)
        {
           PrefConcat3 = 'Job Related;';
        }
        else
        {
           PrefConcat3 = 'blank;';
        }

        if(UNC.News_Medical__c==true)
        {
           PrefConcat4 = 'Medical;';
        }
        else
        {
           PrefConcat4 = 'blank;';
        }
        
        String PrefConcat = PrefConcat1+PrefConcat2+PrefConcat3+PrefConcat4;
        
        String[] UserCatPref = (PrefConcat).split(';');     
//            String[] UserCatPref = ([SELECT News_Category_Filter__c FROM User where username=:UserInfo.getUsername() LIMIT 10].News_Category_Filter__c).split(';');     
                        // Check for All in user's preference            
            // No check for Category if "All" selected    


        List<News__c> feedItemslistCitizen = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c, NewsAge__c, Published_on__c, Event_Date_Time__c
                                            from News__c 
                                            WHERE ((Publish_Through__c >= TODAY) 
                                                   AND (Publish_from__c <= TODAY) 
                                                   AND (Intended_audience__c INCLUDES ('All Audiences','Citizens')) 
                                                   AND (Category__c IN :UserCatPref)) 
                                            ORDER BY publish_from__c DESC];
        return feedItemslistCitizen;

    }
// End -< news feed on Citizen dashboard

// Start -> news feed on COACH - FINAL
    public List<News__c> queryNewsFeedListCoach()
    {
        Boolean Check = false;//checks whether All has been selected
        String AllVar = 'All';//checks for All, other values in multi-select
        User UNC = [SELECT Id, News_Category_Filter__c, News_Educational__c, News_FamilyEvents__c, News_JobRelated__c, News_Medical__c
                    FROM User where username=:UserInfo.getUsername()
                    LIMIT 1];
        String PrefConcat1;
        String PrefConcat2;
        String PrefConcat3;
        String PrefConcat4;
        
        if(UNC.News_Educational__c==true)
        {
            PrefConcat1 = 'Educational;';
        }
        else
        {
           PrefConcat1 = 'blank;';
        }
        
        if(UNC.News_FamilyEvents__c==true)
        {
            PrefConcat2 = 'Family Events;';
        }
        else
        {
           PrefConcat2 = 'blank;';
        }

        if(UNC.News_JobRelated__c==true)
        {
           PrefConcat3 = 'Job Related;';
        }
        else
        {
           PrefConcat3 = 'blank;';
        }

        if(UNC.News_Medical__c==true)
        {
           PrefConcat4 = 'Medical;';
        }
        else
        {
           PrefConcat4 = 'blank;';
        }
        
        String PrefConcat = PrefConcat1+PrefConcat2+PrefConcat3+PrefConcat4;
        
        String[] UserCatPref = (PrefConcat).split(';');     
//            String[] UserCatPref = ([SELECT News_Category_Filter__c FROM User where username=:UserInfo.getUsername() LIMIT 10].News_Category_Filter__c).split(';');     
                        // Check for All in user's preference            
            // No check for Category if "All" selected    


        List<News__c> feedItemslistCoach = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c, NewsAge__c, Published_on__c, Event_Date_Time__c
                                            from News__c 
                                            WHERE ((Publish_Through__c >= TODAY) 
                                                   AND (Publish_from__c <= TODAY) 
                                                   AND (Intended_audience__c INCLUDES ('Success Coaches','All Audiences')) 
                                                   AND (Category__c IN :UserCatPref)) 
                                            ORDER BY publish_from__c DESC];
        return feedItemslistCoach;
        
    }
// End -> news feed on COACH - FINAL

    
    // START - sampleCon
  /*public PageReference Search() {
  
       Date checkDate = Date.Today().addDays(-7);
         
         
       if(String.isBlank(selectedKeyword))     
      {
                flag = true;
             }  
         else
           {
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter a valid date. News feed archives from the last one week are available.'));
           }
           
         getUserlist();  
         return null;
    }
    
  public void getUserlist() {
  
         if(flag==true)
         {
                   list<String> Criteria = new list<String>();
                   
                   if( selectedDate != null)
                       Criteria.add('publish_from__c <= :selectedDate AND publish_Through__c >= :selectedDate');
                                
                   if( selectedKeyword != null)
                       Criteria.add('Headline__c LIKE \'%' + string.escapeSingleQuotes(selectedKeyword) + '%\''); 
                    
                   if( selectedCategory.size() != 0)
                       Criteria.add('Category__c in :selectedCategory');  
                       
                  String whereClause = '';
                  
                   if (criteria.size()>0) {
                       whereClause = ' where ' + String.join(criteria, ' AND ');
                       }

                  String Query = 'select id,Name,category__c,publish_from__c,Headline__c  from News__c ' + whereClause  + ' AND (Intended_audience__c IN (\'All\',\'Coaches - Registered\',\'All Registered users\')) ORDER BY publish_from__C DESC';    
                  system.Debug('query'+query);
                  UserList = Database.Query(query); 
                
         }                     
         else
         {
                  String Query = 'Select id,Name,category__c, publish_from__c,Headline__c from News__c where Publish_from__c <= TODAY and publish_Through__c >= LAST_N_DAYS:7 AND (Intended_audience__c IN (\'All\',\'Coaches - Registered\',\'All Registered users\')) ORDER BY publish_from__C DESC';
                  UserList = Database.Query(query); 
         }    
    
     }
   
  public PageReference clear() {
  
                  flag = false;
                  getUserlist();  
                  SelectedKeyword = '';
                  if(selectedCategory != null){
                      selectedCategory.clear();
                         }
                  SelectedDate = null;
                  return null;
    }*/
    // END - sampleCon

    
    
        public List<MI_Activity__c> querytaskListUserLogin()
    {
        
        
        
        //Overdue tasks count
        List<MI_Activity__c> OverdueTasksList = [SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c<Today AND
                                          Status__c!='Completed' AND
                                          RecordTypeId =: RecTypeId)
                                          ORDER BY Activity_Date__c];
        counterOverdueTasks = OverdueTasksList.size();

// Today's Total tasks count
        List<MI_Activity__c> TasksTotalList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          RecordTypeId =: RecTypeId)
                                          ORDER BY Activity_Date__c];
        counterTodayTotalTasks = TasksTotalList.size();       

// Today's Completed tasks count        
        List<MI_Activity__c> TasksTodayCompletedList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c='Completed' AND
                                          RecordTypeId =: RecTypeId)
                                          ORDER BY Activity_Date__c];
        counterTodayCompletedTasks = TasksTodayCompletedList.size();      
        

// Tasks list to display
// COMPLETE
                             
        List<MI_Activity__c> TasksList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c!='Completed' AND
                                          RecordTypeId =: RecTypeId)
                                          ORDER BY Status__c];


        List<MI_Activity__c> TasksList1 = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c='Completed' AND
                                          RecordTypeId =: RecTypeId)
                                          ];
        TasksList.addall(TasksList1);
        return TasksList;       
    }
        
    
  public PageReference CompleteTaskMethod() {
                       EditTask.Status__c='Completed';
                       update EditTask;
                       taskListUserLogin = querytaskListUserLogin();
                       PageReference page = new Pagereference('/apex/CommunitiesLanding');
                       page.setRedirect(true);
                       return page;
     }        
    


   // Bhagyashree 
  public PageReference deleteId() {
          MI_Activity__c delTask;
                  try{
                      delTask = [SELECT  Id,Subject__c,Priority__c,Client_Name__c,Status__c,Activity_Date__c,Description__c,task_type__c from MI_Activity__c where id = :EditTask.Id];
                       delete delTask;
                 } catch(Exception ex){
                        delTask = null;
                  }
                      
                       taskListUserLogin = querytaskListUserLogin();
                       PageReference page = new Pagereference('/apex/CommunitiesLanding');
                       page.setRedirect(true);
                       return page;

     }   
     // Bhagyashree
     
  public PageReference EditId() {
         editdiv = true;
         frontdiv = false;
         newdiv = false;
        // editTask = new MI_Activity__c(); 
         EditTask = [SELECT  Id,Subject__c,Priority__c,Status__c,Client_Name__c,Activity_Date__c,Description__c,task_type__c from MI_Activity__c where id = :EditId];
         taskListUserLogin = querytaskListUserLogin();
        return null;
    }

   public PageReference backtofront() {
        frontdiv = true;
        editdiv = false;
        newdiv = false;
        taskListUserLogin = querytaskListUserLogin();
        return null;
    }
    
     public PageReference updateEditTask() {
         System.debug('ATO UpdateEditTask');
        update EditTask;
        taskListUserLogin = querytaskListUserLogin();
                       PageReference page = new Pagereference('/apex/CommunitiesLanding');
                       page.setRedirect(true);
                       return page;

    }
    
     public PageReference overDue() {
         taskListUserLogin = [SELECT Id,Subject__c,Client_Name__c,Priority__c,Status__c,Activity_Date__c,Description__c,task_type__c from MI_Activity__c 
                              where ( Activity_Date__c < TODAY ) AND ( Owner.Id=:UserInfo.getUserId() ) AND (RecordTypeId =: RecTypeId) 
                              ORDER BY Activity_Date__c desc];
         return null;
    }
    
    public PageReference UserTaskList() {
        newTask = new MI_Activity__c();
        taskListUserLogin = querytaskListUserLogin();
        return null;
    }
    
public PageReference saveRec(){
            //if(newTask !=null)
              newTask.OwnerId = UserInfo.getUserId();
              if(newDate != '' && newTime == null)
              {
                 String[] strDate = newDate.split('/');
                  Integer myIntDate = integer.valueOf(strDate[1]);
                  Integer myIntMonth = integer.valueOf(strDate[0]);
                  Integer myIntYear = integer.valueOf(strDate[2]);
                  Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
                 newTask.Activity_Date__c = DateTime.newInstance(d,Time.newInstance(0,0,0,0)); 
              
              }
              if(newDate!= '' && newTime !=null)
              {
                  newTask.Activity_Date__c = DateTime.parse(newDate+' '+newTime);
                //newTask.Activity_Date__c = DateTime.newInstance(newDate,newTime); 
              }
              if(String.isnotBlank(TaskSubject))
              newTask.Subject__C = TaskSubject;                                       
              newTask.Priority__C = TaskPriority;     
              newTask.Status__C = TaskStatus;    
              newTask.Task_Type__C = TaskType;  
              newTask.Description__c = TaskNotes;
              newTask.Client_Name__c = TaskClient;
              newTask.RecordTypeId=[select Id from RecordType where DeveloperName='Task'].Id;
            try{
                    insert newTask;   
                 }
                catch(DMLException e)
                  {
                        System.debug('The following exception has occurred: ' + e.getMessage());
                       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Task name.'));
                  }

                   if(String.isBlank(TaskSubject) || newDate == '')
                   {
                      return null;
                   }
                   else
                   {
                       PageReference page = new Pagereference('/apex/CommunitiesLanding');
                       page.setRedirect(true);
                       return page;
                    }

        }
    
    @RemoteAction
    global static MI_Activity__c getActivity(Id TaskId) {
        activity = [SELECT Id, Subject__c, Client_Name__c, Priority__c, Status__c, Activity_Date__c, Description__c, task_type__c  
                   FROM MI_Activity__c WHERE Id = :TaskId];
        return activity;
    }
    
    public PageReference ALNewTask()
    {
        TaskSubject = null;
        TaskClient = null;
        TaskNotes = null;
        newDate = null;
        newTime = null;
        TaskType = 'Personal';
        TaskStatus = 'Not Started';
        TaskPriority = 'Normal';
        //ALnewtask = new MI_Activity__c();
        return null;
    }
    public PageReference ALSaveNewTask()
    {
        ALnewtask = new MI_Activity__c();
        ALnewtask.OwnerId = UserInfo.getUserId();
        ALnewTask.RecordTypeId=[select Id from RecordType where DeveloperName='Task'].Id;
        ALnewtask.Subject__c = TaskSubject;
        ALnewTask.Priority__C = TaskPriority;     
        ALnewTask.Status__C = TaskStatus;    
        ALnewTask.Task_Type__C = TaskType;  
        ALnewTask.Description__c = TaskNotes;
        ALnewTask.Client_Name__c = TaskClient;
        System.debug('ATO - before Time value check. newTime value is:'+newTime+':');
        if(newDate != '' && ((newTime == null)||(newTime == '')))
            {
                System.debug('ATO - newTime is null');
                String[] strDate = newDate.split('/');
                Integer myIntDate = integer.valueOf(strDate[1]);
                Integer myIntMonth = integer.valueOf(strDate[0]);
                Integer myIntYear = integer.valueOf(strDate[2]);
                Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);

                ALnewtask.Activity_Date__c = DateTime.newInstance(d,Time.newInstance(0,0,0,0)); 
            }
        if(newDate!= '' && newTime !='')
            {
                System.debug('ATO - newTime is NOT null');
                ALnewtask.Activity_Date__c = DateTime.parse(newDate+' '+newTime);
               // ALnewtask.Activity_Date__c = DateTime.newInstance(newDate,newTime); 
            }
        try
            {
                insert ALnewtask;
            }
        catch(DMLException e)
            {
                System.debug('The following exception has occurred: ' + e.getMessage());
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Task name.'));
            }        
        taskListUserLogin = querytaskListUserLogin();
        return null;
    }    
    public PageReference ALEditTask()
    {
        EditTask = [SELECT  Id,Subject__c,Priority__c,Status__c,Client_Name__c,Activity_Date__c,Description__c,task_type__c from MI_Activity__c where id = :EditId];
        EditDate = EditTask.Activity_Date__c.format('MM/dd/YYYY');
        EditTime = EditTask.Activity_Date__c.format('h:mm a');
        return null;
    }
    public PageReference ALUpdateTask()
    {
        try
            {
                if(EditTask!=null)
                {
                System.debug('Edit Task !=null');
                if(EditDate != null && EditTime !=null)
                   {
                       //EditTask.Activity_Date__c = Datetime.newInstance(EditDate, EditTime);
                       EditTask.Activity_Date__c = Datetime.parse(EditDate+' '+EditTime);
                       EditTask.Start_Date__c = EditTask.Activity_Date__c;
                   }
                if(EditDate != null && EditTime ==null)
                   {
                       //EditTime = Time.newInstance(0,0,0,0);
                       EditTime = '12:00 AM';
                       //EditTask.Activity_Date__c = Datetime.newInstance(EditDate, EditTime);
                       EditTask.Activity_Date__c = Datetime.parse(EditDate+' '+EditTime);
                       EditTask.Start_Date__c = EditTask.Activity_Date__c;
                   }
                    
                    //EditTask.Activity_Date__c = Datetime.newInstance(EditDate, EditTime);
                    //EditTask.Start_Date__c = EditTask.Activity_Date__c;
                    update EditTask;
                    System.debug('After update Edit Task');
                    //EditTask=null;        
                }
            }
        catch(DMLException e)
            {
                System.debug('The following exception has occurred: ' + e.getMessage());
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Task name.'));
            }        

        taskListUserLogin = querytaskListUserLogin();
        return null;
    }
    public PageReference ALCompleteTask() 
      {
          EditTask.Status__c='Completed';
          if(EditDate != null && EditTime !=null)
            {          
              //EditTask.Activity_Date__c = Datetime.newInstance(EditDate, EditTime);
              EditTask.Activity_Date__c = Datetime.parse(EditDate+' '+EditTime);
              EditTask.Start_Date__c = EditTask.Activity_Date__c;
            }
        try
            {
                update EditTask;
            }
        catch(DMLException e)
            {
                System.debug('The following exception has occurred: ' + e.getMessage());
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter Task name.'));
            }        
          taskListUserLogin = querytaskListUserLogin();
          return null;
     }
    public PageReference ALDeleteTask()
    {
        MI_Activity__c delTask;
        try
        {
            delTask = [SELECT  Id,Subject__c,Priority__c,Client_Name__c,Status__c,Activity_Date__c,Description__c,task_type__c from MI_Activity__c where id = :EditTask.Id];
            delete delTask;
        }
        catch(Exception ex)
        {
            delTask = null;
        }
        taskListUserLogin = querytaskListUserLogin();
        return null;
     }  
       
      public void refresh()
    {
       
       
       
       Networkcom = [SELECT Id FROM Network WHERE Name ='State of Michigan'];
         
       feedElementPage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Networkcom.id, ConnectApi.FeedType.UserProfile,Userinfo.getUserId());
                  
       try{
           CommUsers = [SELECT id, Name FROM User WHERE User.Profile.UserLicense.Name LIKE 'Partner Community%' OR User.Profile.UserLicense.Name LIKE 'Customer Community%'];
       }Catch(exception e){}
        
        
        System.Debug('after refresh');
     
    }
   
   
    public void AddPost()
    {
     
      List<String> MentionUsers = PostMention.Split(';');
      system.debug('Mentions:'+MentionUsers);
      
      feedItemInput = new ConnectApi.FeedItemInput();

      mentionSegmentInput = new ConnectApi.MentionSegmentInput();

      messageBodyInput = new ConnectApi.MessageBodyInput();

      textSegmentInput = new ConnectApi.TextSegmentInput();

      messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
      
      User u;
      SendSMSForChatter sms;
      try{
           for( String s: MentionUsers){
            u = [select id,name,Chat_Text__c from User where name like :s];
           
            if(u.Chat_Text__c == true){
               system.debug('users option:'+u.Chat_Text__c);
               sms = new SendSMSForChatter();
               sms.SendSMSGlobal(u.name,PostText,u.id);
            }

            mentionSegmentInput.id = u.id;
            system.Debug('Mentions:'+ mentionSegmentInput.id);
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            mentionSegmentInput = new ConnectApi.MentionSegmentInput();

           }
      }catch(Exception e){
        u = null;
       }
       
     system.debug('PostText :'+PostText );
     if(PostText != null && MentionUsers != null)
     {
     
      textSegmentInput.text = PostText;
      messageBodyInput.messageSegments.add(textSegmentInput);

      feedItemInput.body = messageBodyInput;
      feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
      feedItemInput.subjectId = UserInfo.getUserId();

      System.Debug('Inside posting'+feedItemInput);
      ConnectApi.FeedElement feedElement; 
      
      try{
      feedElement = ConnectApi.ChatterFeeds.postFeedElement(Networkcom.id, feedItemInput);
      }catch(Exception e){}
      System.Debug('after posting:'+feedElement);
      
      
     }
       
      
       PostText =''; PostMention='';  
      refresh();
     
   }
   
   public void AddComment()
   {
         
     System.Debug('entring comment:');
     ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();

     mentionSegmentInput = new ConnectApi.MentionSegmentInput();

     messageBodyInput = new ConnectApi.MessageBodyInput();

     textSegmentInput = new ConnectApi.TextSegmentInput();

     messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
     
     system.Debug('comment:'+CommentText);
       
     
     textSegmentInput.text = CommentText;
     messageBodyInput.messageSegments.add(textSegmentInput);
     
           
     commentInput.body = messageBodyInput;
    
      
     if(feedElementId != null && CommentText != null)
     {
     
     ConnectApi.Comment commentRep = ConnectApi.ChatterFeeds.postCommentToFeedElement(Networkcom.id, feedElementId, commentInput, null);
     ConnectApi.FeedElement commentFeed = ConnectApi.ChatterFeeds.getFeedElement(Networkcom.id, feedElementId);
     
     List<User> mentionedUsers = new List<User>();
     List<String> mentionedNames = new List<String>();
     List<ConnectApi.MessageSegment> messageSegments = commentFeed.body.messageSegments;
        
                for(ConnectApi.MessageSegment m : messageSegments )
               {   
                    if (m instanceof ConnectApi.MentionSegment)
                    {  system.debug('Mention seg:'+m);
                       ConnectApi.MentionSegment mentionSegment = (ConnectApi.MentionSegment) m;
                       mentionedNames.add(mentionSegment.name);
                    }
                }
                 
      mentionedUsers = [SELECT Id, Name, Email,Chat_Text__c FROM User WHERE name IN :mentionedNames];
               
               for(User u : mentionedUsers)
               {     
                  if(u.Chat_Text__c == true)
                  {
                   system.debug('users option:'+u);
                   SendSMSForChatter sms = new SendSMSForChatter();
                   sms.SendSMSGlobal(u.name,CommentText,u.id);
                  }
               }    
     }
      CommentText = '';
      refresh();
   } 
    public void CitizenSuccessPlan(){
        if(latestcaseid != null){
            String domainarr = '';
            List<Domain__c> domainlst = [Select Id, Name__c, Important__c from Domain__c where Important__c = true];
                for(Domain__c dc : domainlst){
                    domainarr = domainarr+dc.Name__c+'|'; 
                }
            String substrcaseid = latestcaseid.substring(0,15);
            List<SP_Transactional__c> dashboardgoallst = [Select id, Parent__c, Name__c, Type__c, Goal__r.Domain__c, Status__c, Goal__r.CreatedDate, Goal__r.LastModifiedDate from SP_Transactional__c
                                                            where Goal__r.Show_Domain__c = true and Status__c = true and CaseId__c  = :substrcaseid AND Type__c IN ('Milestone') and SuccessPlanStatus__c='Active'];
            
            List<SP_Transactional__c> dashboardsteplst = [Select LastModifiedDate, id, Parent__c, Selected_Step__c, Completed__c, Name__c, Type__c, Goal__r.Domain__c from SP_Transactional__c
                                                            where Goal__r.Show_Domain__c = true and Selected_Step__c = true and CaseId__c  = :substrcaseid AND Type__c IN ('MilestoneSteps') and SuccessPlanStatus__c='Active'];
        	
            Map<String , List<SP_Transactional__c>> goalstep = new Map<String , List<SP_Transactional__c>>();
            for(SP_Transactional__c tmpobj : dashboardsteplst){
                if(goalstep.containsKey(tmpobj.Goal__r.Domain__c)){
                    goalstep.get(tmpobj.Goal__r.Domain__c).add(tmpobj);
                }
                else{
                    List<SP_Transactional__c> slst = new List<SP_Transactional__c>();
                    slst.add(tmpobj);
                    goalstep.put(tmpobj.Goal__r.Domain__c, slst);
                }
            }
            Decimal vcntgoals = 0;
            for(SP_Transactional__c tmpdashgoal : dashboardgoallst){
                citizendashinfo cdi = new citizendashinfo();
                cdi.strgoal = tmpdashgoal.Name__c;
                cdi.strdomain = tmpdashgoal.Goal__r.Domain__c;
                if(domainarr.contains(tmpdashgoal.Goal__r.Domain__c)){
                    cdi.strpriority = 'Primary';
                }
                else{
                    cdi.strpriority = 'Secondary';
                }
                cdi.dtcreated = tmpdashgoal.Goal__r.CreatedDate.Date();
                if(goalstep.containsKey(tmpdashgoal.Goal__r.Domain__c)){
                    DateTime vdate = tmpdashgoal.Goal__r.LastModifiedDate;
                    Boolean vflg = true;
                    for(SP_Transactional__c tobj : goalstep.get(tmpdashgoal.Goal__r.Domain__c)){
                        if(tobj.Completed__c != true){
                            vflg = false;
                        }
                        if(tobj.LastModifiedDate > vdate){
                            vdate = tobj.LastModifiedDate;
                        }
                    }
                    if(vflg){
                        cdi.strstatus = 'Complete';
                        vcntgoals = vcntgoals +1;
                    }    
                    else{
                       cdi.strstatus = 'In Progress'; 
                    }
                    cdi.dtupdated = vdate.Date();
                }
                else{
                    cdi.strstatus = 'Complete';
                    vcntgoals = vcntgoals +1;
                    cdi.dtupdated = tmpdashgoal.Goal__r.LastModifiedDate.Date();
                }
                dashinfo.add(cdi);
            }
            List<citizendashinfo> primarylst = new List<citizendashinfo>();
            for(citizendashinfo ci : dashinfo){
                if(ci.strpriority == 'Primary'){
                    primarylst.add(ci);
                }
            }
            for(citizendashinfo ci : dashinfo){
                if(ci.strpriority == 'Secondary'){
                    primarylst.add(ci);
                }
            }
            dashinfo.clear();
            Integer vgoalcnt = 0;
            for(citizendashinfo ci : primarylst){
                vgoalcnt = vgoalcnt +1;
                ci.strgoal = vgoalcnt+'. '+ci.strgoal;
                dashinfo.add(ci);
            }
            if(!dashboardgoallst.isEmpty()){
                goalsComp = vcntgoals.divide(dashboardgoallst.size(),2);
                goalsComp = (goalsComp*100).round();
            }
        }
    }
    
    public class citizendashinfo
    {
        public String strgoal{get; set;}
        public String strdomain {get; set;}
        public String strpriority {get; set;}
        public Date dtcreated {get; set;}
        public Date dtupdated {get; set;}
        public String strstatus {get; set;}
    }
}