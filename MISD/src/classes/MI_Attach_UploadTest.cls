@IsTest global with sharing class MI_Attach_UploadTest {
    @IsTest(SeeAllData=true) 
       
    global static void test1 () {
       
        Account acc=[Select id from Account where name='MIISD'];
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
       
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
       
     
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id);
        
        insert u;
        
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
        AttachmentDetail__C a1 = new AttachmentDetail__C(contact__C = con.id,sharedwith__C = 'lucy;sophie;tamara' );
        insert a1;
        system.debug('detail val:'+a1);
        Attachment a = new Attachment(ParentId = a1.id,name = 'trial.txt',body = blob.valueOf('example'));
        insert a;        
        system.debug('att val:'+a);
        
        System.runAs(u){
            ApexPages.currentPage().getParameters().put('citId', con.Id);
            ApexPages.currentPage().getParameters().put('coachId', u.Id);
            
            MI_Attach_Upload au = new MI_Attach_Upload();
            au.fileNamescont = a1.id+',';
            au.filename_c = 'trial';
            au.typ_c = 'txt';
            au.getDocs();
            au.upld();
            au.sendParamtoController();
            au.deletecontroller();
           
  
        }
         
     }
    @IsTest(SeeAllData=true)    
    public static void test2 () {
       
        Account acc=[Select id from Account where name='MIISD'];
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
       
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
       
     
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id);
        
        insert u;
        
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
        AttachmentDetail__C a1 = new AttachmentDetail__C(contact__C = con.id,sharedwith__C = 'lucy;sophie;tamara' );
        insert a1;
        system.debug('detail val:'+a1);
        Attachment a = new Attachment(ParentId = a1.id,name = 'trial.txt',body = blob.valueOf('example'));
        insert a;        
        system.debug('att val:'+a);
        
       
         System.runAs(u){
            ApexPages.currentPage().getParameters().put('citId', con.Id);
            
            MI_Attach_Upload au = new MI_Attach_Upload();
            au.fileNamescont = a1.id+',';
            
            au.ref_div();
            au.getDocs();
            au.filename_c = 'trial';
            au.typ_c = 'txt';
            au.upld();
            au.sendParamtoController();
            au.deletecontroller();
           
  
        }
       
       
    }
}