public class MI_CommonApp_CitizenController {
  
         public user u {get;set;}
    	public string currentContactCaseId {get;set;}
    	public string caseId {get;set;}
    	public string CAId {get;set;}
     	PageReference retURL{get; set;}
    	public string AppCaseID{get; set;}
     	public String MyProflieName{get; set;}
   		Public User CaseUser {get;set;}
		public String Assesconid {get;set;}
   
    public MI_CommonApp_CitizenController() {
     caseId = ApexPages.currentPage().getParameters().get('CaseId');  
     Assesconid = ApexPages.currentPage().getParameters().get('AssessmentConId');  
       CAId = '';
     List<CommonApplication__c> calst = [Select Id,Status__c from CommonApplication__c where CaseId__c=:caseId and Status__c='Active' LIMIT 1];
        if(!calst.isEmpty()){
            CAId = calst[0].Id;
        }
     }  
    public Pagereference checkCase() {
     	AppCaseID = apexpages.currentpage().getparameters().get('CaseId');
        
        if(AppCaseID == null || AppCaseID == '')
        {
            System.debug('New Case is Created');
            List<Profile> ProfileName= [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            MyProflieName = ProfileName[0].Name;
            CaseUser = [SELECT Id from User where Profile.Name = :System.Label.X_LABEL_189 and isactive = true Limit 1];
            Case Assessment = new Case();
            Assessment.Ownerid = CaseUser.Id;
            Assessment.Origin = 'Email';
            Assessment.Priority = 'Medium';
            if(MyProflieName == 'Success Coach Profile'){
                Assessment.SuccessCoach__c = UserInfo.getUserId();
            }
            insert Assessment;
            
            AppCaseID = Assessment.Id;
            
            CommonApplication__c newca = new CommonApplication__c();
            newca.CaseId__c = AppCaseID;
            newca.Status__c = 'Active';
            newca.CAPrograms__c = '';
            insert newca;
            
            System.debug('CaseId -> ' + AppCaseID + '|' + apexpages.currentpage().getparameters().get('CaseId'));
            System.debug('Calling Common App with Case Id : ' + AppCaseID);
             }
        	if (Assesconid != '' && Assesconid != null)
            {
             retURL = new PageReference('/apex/MI_CommonApplication?CaseId='+AppCaseID+'&AssessmentConId='+Assesconid);    
            }   
        else{
            if(CAId == ''){
                CommonApplication__c newca = new CommonApplication__c();
                newca.CaseId__c = AppCaseID;
                newca.Status__c = 'Active';
                newca.CAPrograms__c = '';
                insert newca;
            }
            retURL = new PageReference('/apex/MI_CommonApplication?CaseId='+AppCaseID);}
            //PageReference retURL = Page.MI_OpenCalendarView;
            //retURL.getParameters().put('CaseId',AppCaseID);
            retURL.setRedirect(true);
        return retURL;
    }   
}