@isTest
public class MI_PartnerDashTest {
  @isTest public static void generaltest(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
      MI_PartnerDash mpd = new MI_PartnerDash();
        mpd.counterDueTodayTasks = 0;
        mpd.UserCatPref1 = '';
        mpd.currentUser = new User();
        mpd.taskIdChosen = '';
        mpd.selectedKeyword = '';
        mpd.selectedDate = date.today();
        mpd.selectedCategory = new String[]{'Educational'};
        mpd.Search();
        mpd.clear();
        MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test', RecordTypeId = [select id from RecordType where Name='Task' and sObjectType='MI_Activity__c'].Id);
        insert miact;
        mpd.EditId = miact.id;
        mpd.TaskId = miact.id;
        mpd.delId = miact.id;
        mpd.TaskCompleteId = miact.id;
        mpd.EditTask = miact;
        mpd.GotoBulletinNew();
        mpd.BulletinNew();
        mpd.BulletinSaveNew();
        MI_PartnerDash.CurrentBulletinList();
        mpd.editdiv = true;
        mpd.newdiv = true;
        mpd.frontdiv = true;
         MI_PartnerDash.activity = new MI_Activity__c();
    }
    @isTest public static void queryNewsFeedListTest(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='testmpd@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testmpd@test.com', News_Educational__c=true,
            News_FamilyEvents__c=true, News_JobRelated__c=true, News_Medical__c=true);
        System.runAs(u) {
             Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
          insert con;
            MI_PartnerDash mpd = new MI_PartnerDash();
            MI_PartnerDash.MyBulletinList();
            MI_PartnerDash.PartnerBulletinList();
        }
    }
     @isTest public static void SearchTest(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
         MI_PartnerDash mpd = new MI_PartnerDash();
        mpd.selectedKeyword = 'Test';
        mpd.Search();
    }
    @isTest public static void BulletinSaveNewtest1(){
        MI_PartnerDash mpd = new MI_PartnerDash();
        mpd.BulletinName = '';
        mpd.BulletinSaveNew();
    }
    @isTest public static void BulletinSaveNewtest2(){
        MI_PartnerDash mpd = new MI_PartnerDash();
        mpd.BulletinDate = '01/01/2010';
        mpd.BulletinTime = '12:00 AM';
        mpd.BulletinSaveNew();
    }
    @isTest public static void CurrentBulletinListtest1(){
        Datetime myDate = Datetime.now();
    String dateOutput = myDate.format('MM/dd/yyyy');
        MI_PartnerDash mpd = new MI_PartnerDash();
        mpd.BulletinName = 'Testing';
        mpd.BulletinType = 'Event';
        mpd.BulletinDate = dateOutput;
        mpd.BulletinTime = '11:30 PM';
        mpd.BulletinSaveNew();
        MI_PartnerDash.CurrentBulletinList();
    }
    @isTest public static void CurrentBulletinListtest2(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test1', Email='testcbl1@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testingcbl1', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testcbl1@test.com');
        System.runAs(u) {
            Date myDate = System.now().date()+2;
            String dateOutput = myDate.format();
            MI_PartnerDash mpd = new MI_PartnerDash();
            mpd.BulletinName = 'Testing';
            mpd.BulletinType = 'Event';
            mpd.BulletinDate = dateOutput;
            mpd.BulletinTime = '11:30 PM';
            mpd.BulletinSaveNew();
        }
        User u1 = new User(Alias = 'test2', Email='testcbl2@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testingcbl2', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testcbl2@test.com');
       
         
        System.runAs(u1) {
            Bulletin__c b1 = new Bulletin__C(Calculated_End_Date__c=Date.today()+1,Event_Title__c='abc', Bulletin_Date_Time__c=Date.today(), Bulletin_Time__c='01:00 PM');
            insert b1;
            MI_PartnerDash.CurrentBulletinList();
            MI_PartnerDash.MyBulletinList();
            MI_PartnerDash.PartnerBulletinList();
        }
    }
    @isTest public static void chatterTest(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
       MI_PartnerDash pd = new MI_PartnerDash();
        // Build a simple feed item
      ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
      
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='testcdc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testcdc@test.com', News_Educational__c=true,
            News_FamilyEvents__c=true, News_JobRelated__c=true, News_Medical__c=true,Chat_Text__c = true);
 
       //MI_CoachDashCont1 cdc1 = new MI_CoachDashCont1();  
     
      Network Networkcom = new Network();      
      Networkcom = [SELECT Id FROM Network WHERE Name ='State of Michigan'];
 

    
       system.runAs(u){ 
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(Networkcom.id,ConnectApi.FeedType.UserProfile,UserInfo.getUserId(), testPage);
         Test.startTest();
         pd.refresh();
       
            
         pd.PostText = 'Hey all.';
         pd.PostMention = 'sophia;tamara;Tom';
         pd.AddPost();
         
        
         pd.CommentText = 'Hey, it is a pleasure';
         pd.AddComment();
            
        
       Test.stopTest();
     }
    }
}