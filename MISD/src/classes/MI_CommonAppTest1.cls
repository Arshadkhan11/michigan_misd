@isTest

public class MI_CommonAppTest1 {
    
    static testMethod void MI_CommonAppTest(){
 
 MI_CommonApp Ca= new MI_CommonApp();
   Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married',Zipcode__c ='49303');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
       
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);  
        insert cs;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
         List<Question__c> Qlst = new List<Question__c>();
   
   Question__c Q1 = new Question__c(Question_No__c='Qtest1',Question_Text__c='Test',Answers__c='Test',Members__c='',Question_Option__c='Test',OptionsDisplayed__c='Single Line',Category__c='Personal',PageName__c='Education',Program__c='Cash;FAP',PageSection__c='Education');
   insert Q1;
   Question__c Q2 = new Question__c(Question_No__c='Qtest2',Question_Text__c='Test',Answers__c='Test',Members__c='',Question_Option__c='Test',OptionsDisplayed__c='',ParentQuestion__c=Q1.id,Program__c='Cash;FAP',Category__c='Personal',PageName__c='Education',PageSection__c='Education');
   insert Q2;
   Question__c Q3 = new Question__c(Question_No__c='Qtest3',Question_Text__c='Test',Answers__c='Test',Members__c='',Question_Option__c='Test',OptionsDisplayed__c='Single Line',Category__c='Personal',PageName__c='Education',Program__c='Cash;FAP',PageSection__c='Education');
   insert Q3;
   Question__c Q4 = new Question__c(Question_No__c='Qtest4',Question_Text__c='Test',Answers__c='Test',Members__c='',Question_Option__c='Test',OptionsDisplayed__c='',ParentQuestion__c=Q2.id,Program__c='Cash;FAP',Category__c='Personal',PageName__c='Education',PageSection__c='Education');
   insert Q4;   
        
   CommonApp_Category__c cacat = new CommonApp_Category__c(Category__c='Personal',Order__c=1);
   insert cacat;
    
   CommonApp_Page__c capag = new CommonApp_Page__c(Page_Name__c='Education',Page_Seq__c=1);
    insert capag;   
   
   ApexPages.currentPage().getParameters().put('CaseId',cs.id); 
   ApexPages.currentPage().getParameters().put('AssessmentConId',con.id); 
        
   ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
   PageReference pageRef = Page.MI_CommonApplication;
   pageRef = Page.MI_CommonApplication;
   Test.setCurrentPage(pageRef);
   ApexPages.currentPage().getParameters().put('ProgramSelected','CDC');
   ca.ShowAttachmentInfo();
   ApexPages.currentPage().getParameters().put('ProgramSelected','FAP');
   ca.ShowAttachmentInfo(); 
   ApexPages.currentPage().getParameters().put('ProgramSelected','SER');
   ca.ShowAttachmentInfo(); 
   ApexPages.currentPage().getParameters().put('ProgramSelected','Cash');
   ca.ShowAttachmentInfo(); 
        
   MI_CommonApp.getQuestionsdetails('Cash|FAP',cs.id,'Y');     
   
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'Cash');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'FAP');
   //MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'CDC');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'Medicaid');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'SER');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'Child Supp.');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'WIC');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'HCVP');
   MI_CommonApp.saveAttachment('CommonAppAnswers.txt','CommonAppIntro.txt',cs.id, con.id,'FRLP');     

}
}