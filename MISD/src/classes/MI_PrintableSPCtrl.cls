public without sharing class MI_PrintableSPCtrl{

    public  List<GoalsWrapper> goalsList {get;set;} //Used by page to iterate over
    private string caseId = Apexpages.Currentpage().getparameters().get('caseId');
    public List <SP_Transactional__c> spTransList{get;set;}
    public Map<String, GoalsWrapper> goalsMap {get;set;}
    public Map<id, List<MilestonesWrapper>> goalMilestones {get;set;} // Key: goal id; pair: list of wrapper -- Final map to display in page
    public Map<id, String> goalNameMAp {get;set;} // key: goal id; Pair: name of the goal
    public Map<id, String> goalTitleMap {get;set;}
    public Map<Id, Integer> goalTimeLineHeight {get;set;}
    public String vConName{get;set;}
    
    public MI_PrintableSPCtrl(){
        
        Id vConId = [select ContactId from Case where Id = :CaseId].ContactId;
        //System.debug('Con Id is : ' + vConId);
        vConName = [select name from contact where Id = :vConId].name;
        //System.debug('Contact Name is : ' + vConName);
        
        List<SP_Goal__c> spGoalsList = new List<SP_Goal__c>([SELECT Name, Title__c, Case__c, Domain__c,
                                                                (Select id, Name__c, Start_Date__c, Type__c, Status__c , Goal__r.id, Goal__r.Domain__c
                                                                    from SP_Transactional__r Where Status__c = true Order BY Start_Date__c)
                                                               FROM SP_Goal__c
                                                               WHERE case__c = :CaseId]);  
        goalsList = new List<GoalsWrapper>();
        spTransList = new List<SP_Transactional__c>();
        goalsMap = new Map<String, GoalsWrapper>();
        
        Map<String, List<SP_Transactional__c>> domainMilestoneMap = new Map<String, List<SP_Transactional__c>>();
        Map<id, String> milestoneNameMap = new Map<id, String>();   // Key: milestoneId; pair: milestone Name 
        Map<id, SP_Transactional__c> milestoneMap = new Map<id, SP_Transactional__c>();    // Key: milestoneId; pair: milestone  
        Map<String, List<Success_Plan_Master__c>> DomainMilestonesMap = new Map<String, List<Success_Plan_Master__c>>();  // Key: domain name ; pair:List of master data 
        Map<Id, List<Success_Plan_Master__c>> milestoneTasksMap = new Map<Id, List<Success_Plan_Master__c>>();  // Key: milestoneId; pair:list of its master data 
        Map<id, List<MilestonesWrapper>> milestonesWrapperMap = new Map<id, List<MilestonesWrapper>>();  // Key: milestoneId; pair: list of wrapper 
        Map<Id, Set<Date>> findDuplicateDateMap = new Map<Id, Set<Date>>(); // Key: goalId; pair: set of its milestone dates
        goalMilestones = new Map<id, List<MilestonesWrapper>>();
        goalNameMap = new Map<id, String>();
        goalTitleMap = new Map<id, String>();
    
        
        for (SP_Goal__c spGoal: spGoalsList){
            for(SP_Transactional__c trans : spGoal.SP_Transactional__r){
                if(domainMilestoneMap.containsKey(trans.Goal__r.Domain__c)){
                    domainMilestoneMap.get(trans.Goal__r.Domain__c).add(trans);    
                }else{
                    List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                    tempList.add(trans);
                    domainMilestoneMap.put(trans.Goal__r.Domain__c, tempList);
                }
            }
            if(spGoal.SP_Transactional__r != null)
            for(SP_Transactional__c milestone: spGoal.SP_Transactional__r){
                milestoneNameMap.put(milestone.id, milestone.Name__c);            
                milestoneMap.put(milestone.id, milestone);
            }
            goalNameMap.put(spGoal.id, spGoal.Domain__c);
            if(spGoal.Title__c != null)
            {
            	goalTitleMap.put(spGoal.id, spGoal.Title__c);
            }
            else
            {
                goalTitleMap.put(spGoal.id, '');
            }
            Set<Date> tempSet = new Set<Date>();
            findDuplicateDateMap.put(spGoal.id, tempSet);
        } 
        
        for(String s : domainMilestoneMap.keyset()){
            string milestone = '';
            string resource = '';
            string service = '';
            Date startDate = domainMilestoneMap.get(s)[0].Start_Date__c;
            for(SP_Transactional__c trans: domainMilestoneMap.get(s)){
                if(trans.Type__c == 'Milestone' && trans.Name__c != null & trans.Name__c != ''){
                    milestone = milestone + trans.Name__c+ ',';  
                }
                else if(trans.Type__c == 'Resource' && trans.Name__c != null & trans.Name__c != ''){
                    resource = resource + trans.Name__c + ',';    
                }else if(trans.Type__c == 'Service' && trans.Name__c != null & trans.Name__c != ''){
                    service = service + trans.Name__c + ',';  
                }
            }
            if(milestone.endsWith(',')){
                milestone = milestone.substring(0, milestone.length() - 1);
            }
            if(resource.endsWith(',')){
                resource = resource.substring(0, resource.length() - 1);
            }
            if(service.endsWith(',')){
                service = service.substring(0, service.length() - 1);
            }
            milestone = milestone.replace('|Y|','');
            resource = resource.replace('|Y|','');
            service = service.replace('|Y|','');
            
            GoalsWrapper gw = new GoalsWrapper();
            gw.milestones = milestone;
            gw.resources = resource;
            gw.services = service;
            gw.goalDate =  startDate;
            goalsMap.put(s, gw);
        }
        for(Success_Plan_Master__c spm : [SELECT id, Parent__r.Value__c, value__c, Type__c, Duration__c, Sequence__c
                                            FROM Success_Plan_Master__c 
                                            WHERE ((Parent__r.Value__c IN : milestoneNameMap.values() and (Type__c = 'MilestoneSteps' OR Type__c = 'MilestoneHeads'))
                                                    OR(Value__c IN : milestoneNameMap.values() and Type__c = 'Milestone'))
                                                 ORDER by Sequence__c]){
            string key = '';
            if(spm.Type__c == 'Milestone')
                key = spm.Value__c;
            else
                key = spm.Parent__r.Value__c;
                
            if(DomainMilestonesMap.containsKey(key))                                                      
                DomainMilestonesMap.get(key).add(spm);
            else{
                List<Success_Plan_Master__c> tempList = new List<Success_Plan_Master__c>();
                tempList.add(spm);
                DomainMilestonesMap.put(key, tempList);
            }                                    
        }
        
        for(String s : milestoneNameMap.keyset()){
            milestoneTasksMap.put(s, DomainMilestonesMap.get(milestoneNameMap.get(s)));            
        }
                
        for(String milestoneId : milestoneTasksMap.keyset()){
            integer i = 1;
            Date taskDate = milestoneMap.get(milestoneId).Start_Date__c;
            List<MilestonesWrapper> mwList = new List<MilestonesWrapper>();
            if(milestoneTasksMap.get(milestoneId) != null)
            for(Success_Plan_Master__c spm : milestoneTasksMap.get(milestoneId)){
                if(spm.Sequence__c == i){
                    MilestonesWrapper mw = new MilestonesWrapper();    
                    mw.value = spm.Value__c;
                    if(spm.Type__c == 'MilestoneSteps')
                        mw.type = 'green-dot';
                    else if (spm.Type__c == 'MilestoneHeads')
                        mw.type = 'diamond';
                    else if (spm.Type__c == 'Milestone')
                        mw.type = 'someType';
                    mw.StartDate = taskDate.addDays(Integer.valueof(spm.Duration__c));
                    taskDate = mw.StartDate;
                    mw.ParentValue = spm.parent__r.Value__c;
                    mwList.add(mw);
                    i++;
                    if(findDuplicateDateMap.get(milestoneMap.get(milestoneId).Goal__r.id).contains(mw.StartDate)){
                        mw.DateDuplicate = 'Y';
                    }else{
                        mw.DateDuplicate = 'N';
                        findDuplicateDateMap.get(milestoneMap.get(milestoneId).Goal__r.id).add(mw.StartDate);                                                                
                    }
                }
            }
            milestonesWrapperMap.put(milestoneId, mwList);
        }
        for(SP_Goal__c goal: spGoalsList){
            for(SP_Transactional__c milestone: goal.SP_Transactional__r){
                if(goalMilestones.containsKey(goal.Id)){
                    goalMilestones.get(goal.Id).addAll(milestonesWrapperMap.get(milestone.id));
                }else{
                    List<MilestonesWrapper> tempList = new List<MilestonesWrapper>();
                    tempList.addAll(milestonesWrapperMap.get(milestone.id));
                    goalMilestones.put(goal.Id, tempList);
                }   
            }
        }
        system.debug('\n\n goalMilestones : ' + goalMilestones);
    }
    
    public class GoalsWrapper{
        public string     goalName      {get;set;}
        public string     goalTitle     {get;set;}
        public String     challenge     {get;set;}
        public string     service       {get;set;}
        public Date       goalDate      {get;set;}
        public string     goalDomain    {get;set;}
        public string     milestones    {get;set;}
        public string     resources     {get;set;}          
        public string     services      {get;set;}
        
    }
    
    public class MilestonesWrapper{
        public Date     StartDate       {get;set;}
        public String   Value           {get;set;}
        public String   type            {get;set;}
        public String   ParentValue     {get;set;}
        public String   DateDuplicate   {get;set;}
    }
    
}