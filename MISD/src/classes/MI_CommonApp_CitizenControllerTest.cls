@isTest

public with sharing class MI_CommonApp_CitizenControllerTest  {

 public static testMethod void MI_CommonApp_CitizenControllerTest(){
 
 
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         system.Debug('casecoach id:'+cs.SuccessCoach__c);
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
        
        system.runAs(u){
        PageReference pageRef = Page.MI_CommonAppCitizen;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('CaseId',cs.id);
        System.currentPageReference().getParameters().put('AssessmentConId',con.id);


        
        MI_CommonApp_CitizenController ctrl = new MI_CommonApp_CitizenController(); 
        ctrl.checkCase();
        System.currentPageReference().getParameters().put('CaseId','');
        ctrl.checkCase();
        ctrl.currentContactCaseId = cs.id;
 
 
 }
 
 
 
 
 }
 }