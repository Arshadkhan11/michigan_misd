@isTest

private class BatchEmailTest{
 @IsTest(SeeAllData=true)
 
 public static void test_BatchEmail(){
     
        Account acc = [select id from Account where name = 'MIISD'];  
       
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
                          Notifications_Email__c = true,
                          Notifications_Text__c = true,
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
     system.runAs(u){
        Notification__c n1 = new Notification__c(Notification_Date_Time__c=DateTime.newInstance(system.today(), Time.newInstance(11, 59, 59, 0)),Notification_Description__c='test',Unread__c = true,OwnerId = u.Id);
        insert n1;
         Notification__c n2 = new Notification__c(Notification_Date_Time__c=DateTime.newInstance(system.today(), Time.newInstance(11, 59, 59, 0)),Notification_Description__c='test',Unread__c = true,OwnerId = u.Id);
        insert n2;
 
    
         Test.startTest();
        
         Database.executeBatch(new BatchEmail());
     
         Test.stopTest();
     }
     
    
 }
}