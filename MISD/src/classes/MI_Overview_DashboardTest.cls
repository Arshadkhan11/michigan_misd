@isTest
public class MI_Overview_DashboardTest {
    
    @isTest
    public static void genTest(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='testcdc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testcdc@test.com', News_Educational__c=true,
            News_FamilyEvents__c=true, News_JobRelated__c=true, News_Medical__c=true);
        insert u;
        system.Debug('user id:'+u.id);
    //Data creation
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id);
        insert cs;
         system.Debug('casecoach id:'+cs.SuccessCoach__c);
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        //Education
        Decimal dur = 120.0;
        Decimal seq = 1.0;
        Decimal dur2 = 30.0;
        Decimal seq2 = 2.0;
        SP_Goal__c vEdu = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Education',Case__c = cs.id);
        insert vEdu;
        
        SP_Transactional__c sptEdu = new SP_Transactional__c(Name__c='College Degree', Status__c=true,Goal__c = vEdu.id, Start_Date__C = date.today(),Milestone_Status__c ='Not Completed',Parent__c = 'Education');
        insert sptEdu;
        sptEdu.Type__c='Milestone';
        update sptEdu;
        
        SP_Transactional__c sptEdu2 = new SP_Transactional__c(Name__c='High School', Status__c=true,Goal__c = vEdu.id, Start_Date__C = date.today().addDays(-150),Milestone_Status__c ='Not Completed',Parent__c = 'Education');
        insert sptEdu2;
        sptEdu2.Type__c='Milestone';
        update sptEdu2;
        
         SP_Transactional__c sptEdu3 = new SP_Transactional__c(Name__c='High School', Status__c=true,Goal__c = vEdu.id, Start_Date__C = date.today().addDays(-130),Milestone_Status__c ='Not Completed',Parent__c = 'Education');
        insert sptEdu3;
        sptEdu3.Type__c='Milestone';
        update sptEdu3;
        
        Success_Plan_Master__c spmDomEdu = new Success_Plan_Master__c(Value__c='Education',Type__C = 'Domain', duration__C = dur2, Sequence__c = seq2 );
        insert spmDomEdu;

        Success_Plan_Master__c spmMilEdu = new Success_Plan_Master__c(Value__c='College Degree',Type__C = 'Milestone', parent__C = spmDomEdu.id, duration__C = dur, Sequence__c = seq);
        insert spmMilEdu;
        
        Success_Plan_Master__c spmMilEdu2 = new Success_Plan_Master__c(Value__c='High School',Type__C = 'Milestone', parent__C = spmDomEdu.id, duration__C = dur, Sequence__c = seq);
        insert spmMilEdu2;
        
       
    
        System.runAs(u) {
            MI_Overview_Dashboard OD = new MI_Overview_Dashboard();
            OD.selectedDate = '06/30/2016';
            OD.selectedKeyword = 'Deg';
            OD.search();
            
            OD.clear();
            
            OD.selectedDate = '06/30/2016';
            OD.search();
            OD.clear();
            OD.overdue();
           
            
            OD.nameSort();
            OD.nameSort();
            
            OD.goalSort();
            OD.goalSort();
            
            OD.statusSort();
            OD.statusSort();
            
            
        }
    }

}