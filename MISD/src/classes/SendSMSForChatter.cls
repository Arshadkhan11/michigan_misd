public class SendSMSForChatter 
{
    public void SendSMSMethod(String sendrId, String Mob, String Ownr, String SMSText) //common method to send SMS
    {
        List<smagicinteract__smsmagic__c> smsObjectList = new List<smagicinteract__smsmagic__c>();
        smagicinteract__smsMagic__c smsObject = new smagicinteract__smsMagic__c();
        smsObject.smagicinteract__SenderId__c = sendrId;
        smsObject.smagicinteract__PhoneNumber__c = Mob;
        smsObject.smagicinteract__Name__c =Ownr;
        smsObject.smagicinteract__ObjectType__c = 'Contact';
        smsObject.smagicinteract__disableSMSOnTrigger__c =1;
        smsObject.smagicinteract__external_field__c = smagicinteract.ApexAPI.generateUniqueKey();
        smsObject.smagicinteract__SMSText__c = SMSText;
        smsObjectList.add(smsObject);
        Database.insert(smsObjectList,true);  
        system.debug('BB--3');
    }
    public void SendSMSGlobal(String MentionName,String MessageBody, String MentionId) //common method to send SMS
    {
        user us = [select Id,MobilePhone from user where Id=:MentionId LIMIT 1];
        if(us.MobilePhone <> '' && us.MobilePhone <> null)
        {
            system.debug('BB--1');
            SendSMSMethod('5172524697', us.MobilePhone, MentionName, MessageBody);
        }
    }
}