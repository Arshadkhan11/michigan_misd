public class SideNavigationLightningCtrl {
   
   @AuraEnabled
   public static List<SideNavigation__c> navigationdetails() {
       return [Select Id, Sequence__c, Title__c, PageName__c, SvgPath__c from SideNavigation__c Order by Sequence__c ASC LIMIT 200];
   }  
   
   @AuraEnabled
   public static List<String> getUserDetails() {
       List<String> retlst = new List<String>();
       retlst.add(userinfo.getName());
       Id pid = userinfo.getProfileId();
       String pname = [Select Id,Name from Profile where Id=:pid].Name;
       retlst.add(pname);
       String baseUrl = System.URL.getSalesforceBaseUrl().toExternalForm();
       retlst.add(baseUrl);
       return retlst;
   }
    
   @AuraEnabled
   public static String CaseDetails() {
       return [Select Id, CaseNumber from Case where Id='5002C000004KHnm' LIMIT 1].CaseNumber;
   }
    
   @AuraEnabled
   public static void Updatelanguage(String Lang) {
       User Pref=[select Id,LocaleSidKey from User where Id=:userinfo.getUserId()];
       Pref.LocaleSidKey = Lang;
       update Pref;
   }
}