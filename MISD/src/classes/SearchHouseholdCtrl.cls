public class SearchHouseholdCtrl {

    public List<ContactWrapper> contactsList 	{get;set;}   
    public Integer contactsCount				{get;set;}
    public Boolean showResults					{get;set;}
    public Boolean showText						{get;set;}
    public Boolean isAdvSearch					{get;set;}
    public String SeletedFilter 				{get;set;} {SeletedFilter = 'Search by';}
    public String fullURL						{get;set;}
    public String searchValue					{get;set;}
    
    public SearchHouseholdCtrl(){
        contactsCount = 0;
        showResults = false;
        showText = false;
        isAdvSearch = false;
        fullURL = URL.getSalesforceBaseUrl().toExternalForm() + '/Needs_Assessment_Process';
    }
    
    public List<SelectOption> getSearchOptions(){
        List<SelectOption> searchOptions = new List<SelectOption>();
        searchOptions.add(new SelectOption('Search by','Search by',true));
        searchOptions.add(new SelectOption('Date of Birth','Date of Birth'));
    	searchOptions.add(new SelectOption('SSN','SSN'));
        searchOptions.add(new SelectOption('Name','Name'));
        return searchOptions;
    }
    
    public Pagereference searchDB(){     
        String searchField 			= ApexPages.currentPage().getParameters().get('searchField');
        searchValue 			= ApexPages.currentPage().getParameters().get('searchValue');
        system.debug('\n searchField - ' + searchField + '\n searchValue - ' + searchValue);
        isAdvSearch = false;
        if(searchField =='Date of Birth' && !String.isEmpty(searchValue)){              
            string regex = '(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)';
            Matcher m1 = Pattern.compile(regex).matcher(searchValue);                 
            if(!m1.matches()) {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ' Enter valid Date');
                ApexPages.addMessage(msg);
                return null;
            }
        } 
        if(searchField == 'SSN' && !String.isEmpty(searchValue)){  
            if(searchValue.indexOf('-')!= -1)
                searchValue = searchValue.replace('-', '');
            string regex = '^\\d{9}?$';
            Matcher m1 = Pattern.compile(regex).matcher(searchValue);                 
            if(!m1.matches()) {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ' SSN must be of the format ***-**-****');
                ApexPages.addMessage(msg);
                return null; 
            }
        } 
        showText = true;
        contactsList 				= new List<ContactWrapper>();
        String searchQuery 			= 'SELECT id, FirstName, LastName,Social_Security_Num__c, Birthdate, ExternalId__c FROM Contact ';
        
        if(searchField == 'Date of Birth'){
            String todate = searchValue;
			Date dt = Date.parse(todate);
            searchQuery = searchQuery + ' WHERE  Birthdate = :dt';
        }else if(searchField == 'Name'){
            searchQuery = searchQuery + ' WHERE FirstName = :searchValue OR LastName = :searchValue';
        }
        List<Contact> cons = new List<Contact>();
        cons = Database.query(searchQuery);
        if(searchField == 'SSN'){
            List<Contact> consTemp = new List<Contact>();
            consTemp = cons;
            cons = new List<Contact>();
            for(Contact c: consTemp){
                if(c.Social_Security_Num__c != null && searchValue == c.Social_Security_Num__c){
                    cons.add(c);
                }
        	}
        }   
        Set<String> ExternalIdsList = populateContacts(cons);
        List<Document> refdata = [SELECT Id,Name,Body FROM Document WHERE Name = 'Enquiry Reference Data' limit 1];
        if(refdata.size()>0)
        {
            Document EnquiryRefData = refdata[0];
            Blob b = EnquiryRefData.Body;
            String data = b.toString();
            
            List<EnquiryReference> EnquiryReferenceData = (List<EnquiryReference>)JSON.deserialize(data, List<EnquiryReference>.class);
            
            for(EnquiryReference er : EnquiryReferenceData){
                if(!ExternalIdsList.contains(er.ExternalId) ){
                    if(searchField == 'Date of Birth' && er.contactDOB == searchValue ||
                            searchField == 'SSN' && er.contactSSN == searchValue ||
                            searchField == 'Name' && er.contactFName == searchValue || er.contactLName == searchValue){
                        ContactWrapper cw 			= new ContactWrapper();
                        cw.contactFullSSN			= er.contactSSN;
                        if(!String.isBlank(er.contactSSN) && er.contactSSN.length() >=9)
                            cw.contactSSN 			= '***-**-' + er.contactSSN.substring(er.contactSSN.length()-4, er.contactSSN.length());
                        cw.contactName 				= er.contactFName + ' ' + er.contactLName;
                        cw.contactDOB				= er.contactDOB;
                        cw.BenefitsStatus			= er.contactBenefits;
                        cw.extId					= er.ExternalId;
                        contactsList.add(cw);
                    }
                }
            }
        }    
        system.debug('\n contactsList : ' + contactsList);
        if(contactsList.size() > 0){
            showResults = true;
            if(contactsCount > 1000){
                List<ContactWrapper> contactsListTemp = contactsList;
                contactsList = new List<ContactWrapper>();
                for(Integer i = 0; i < 1000 ; i++){
                    contactsList.add(contactsListTemp[i]);
                }
            }
        }
        contactsCount = contactsList.size();
        return null;
    }
    
    public PageReference advancedSearch(){
       
        contactsList = new List<ContactWrapper>();
        isAdvSearch = true;
        String searchDate 			= '';
        String searchSSN 			= '';
        String searchFirstName 		= '';
        String searchLastName 		= '';
        
        searchDate 			= ApexPages.currentPage().getParameters().get('date');
        searchSSN 			= ApexPages.currentPage().getParameters().get('ssn');
        searchFirstName 	= ApexPages.currentPage().getParameters().get('fname');
        searchLastName 		= ApexPages.currentPage().getParameters().get('lname');
        
        if(!String.isEmpty(searchDate)){  
            string regex = '(0?[1-9]|1[012])/(0?[1-9]|[12][0-9]|3[01])/((19|20)\\d\\d)';
            Matcher m1 = Pattern.compile(regex).matcher(searchDate);                 
            if(!m1.matches()) {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ' Enter valid Date');
                ApexPages.addMessage(msg);
                return null; 
            }
        } 
        if(!String.isEmpty(searchSSN)){ 
            if(searchSSN.indexOf('-')!= -1)
                searchSSN = searchSSN.replace('-', '');
            string regex = '^\\d{9}?$';
            Matcher m1 = Pattern.compile(regex).matcher(searchSSN);                 
            if(!m1.matches()) {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, ' SSN must be of the format ***-**-****');
                ApexPages.addMessage(msg);
                return null; 
            }
        } 
        
        showText = true;
        
        String searchQuery = 'SELECT id, FirstName, LastName,Social_Security_Num__c, Birthdate, ExternalId__c FROM Contact ';
        if(!String.isBlank(searchDate) || !String.isBlank(searchSSN) || !String.isBlank(searchFirstName) || ! String.isBlank(searchLastName)){
       		searchQuery = searchQuery + ' WHERE';
        }
        if(!String.isBlank(searchDate)){
            String todate = searchDate;
			Date dt = Date.parse(todate);
            searchQuery = searchQuery + ' Birthdate = :dt AND';
        }        
        if(!String.isBlank(searchFirstName)){
            searchQuery = searchQuery + ' FirstName = :searchFirstName AND';
        }
        if(!String.isBlank(searchLastName)){
            searchQuery = searchQuery + ' LastName = :	searchLastName ';
        }
        if(searchQuery.endsWith('AND')){
            searchQuery = searchQuery.substring(0, searchQuery.length() -3);
        }
        if(searchQuery.endsWith('WHERE')){
            searchQuery = searchQuery.substring(0, searchQuery.length() -5);
        }
        system.debug('\n searchQuery : ' + searchQuery);
        List<Contact> cons = new List<Contact>();
        cons = Database.query(searchQuery);       
        if(!String.isBlank(searchSSN)){
            List<Contact> consTemp = new List<Contact>();
            consTemp = cons;
            cons = new List<Contact>();
            for(Contact c: consTemp){
                if(c.Social_Security_Num__c != null && searchSSN == c.Social_Security_Num__c){
                    cons.add(c);
                }
        	}
        } 
        Set<String> ExternalIdsList =  populateContacts(cons);
        
        List<Document> refdata = [SELECT Id,Name,Body FROM Document WHERE Name = 'Enquiry Reference Data' limit 1];
        if(refdata.size()>0)
        {
            Document EnquiryRefData = [SELECT Id,Name,Body FROM Document WHERE Name = 'Enquiry Reference Data' limit 1];
            Blob b = EnquiryRefData.Body;
            String data = b.toString();
            
            List<EnquiryReference> EnquiryReferenceData = (List<EnquiryReference>)JSON.deserialize(data, List<EnquiryReference>.class);
            
            for(EnquiryReference er : EnquiryReferenceData){    
                if(!ExternalIdsList.contains(er.ExternalId) ){
                    if((searchDate == '' || er.contactDOB == searchDate) && (searchSSN == '' || er.contactSSN == searchSSN) &&
                            (searchFirstName == '' || er.contactFName == searchFirstName) && (searchLastName == '' || er.contactLName == searchLastName)){
                        ContactWrapper cw 			= new ContactWrapper();
                        cw.contactFullSSN 			= er.contactSSN;
                        if(!String.isBlank(er.contactSSN) && er.contactSSN.length() >=9)
                            cw.contactSSN 			= '***-**-' + er.contactSSN.substring(er.contactSSN.length()-4, er.contactSSN.length());
                        cw.contactName 				= er.contactFName + ' ' + er.contactLName;
                        cw.contactDOB				= er.contactDOB;
                        cw.BenefitsStatus			= er.contactBenefits;
                        cw.extId					= er.ExternalId;
                        contactsList.add(cw);
                    }
                }
            }
        }
        
        if(contactsList.size() > 0){
            showResults = true;
            if(contactsCount > 1000){
                List<ContactWrapper> contactsListTemp = contactsList;
                contactsList = new List<ContactWrapper>();
                for(Integer i = 0; i < 1000 ; i++){
                    contactsList.add(contactsListTemp[i]);
                }
            }
        }
        contactsCount = contactsList.size();
        return null;
    }
    
    public Boolean stringContains(String s1, string s2){
        if(s1 != '' && s2 != '' && (s1.containsIgnoreCase(s2) || s2.containsIgnoreCase(s1))){
            return true;
        }
        else{
            return false;
        }
    }
    
    public Set<String> populateContacts(List<Contact> cons){
        List<Case> casesList = [SELECT id, ContactId, Success_Coach_Name__c, Goals_Count__c,CA_Completion__c, NA_Completion__c
                                	FROM Case
                               		WHERE ContactId IN :cons];
        List<MI_CommonAppService__c> services = [SELECT Contact__c,Processed__c,ServiveType__c,Status__c,Submitted__c,Id,RedeterminationDate__c,currentDate__c 
                                                    FROM MI_CommonAppService__c 
                                                    WHERE Contact__c IN : cons];
        
        Map<Id, Case> contactCaseMap = new Map <Id, Case>();
        Map<Id, MI_CommonAppService__c> contactServiceMap = new Map<ID, MI_CommonAppService__c>();
        for(Case c : casesList){
            contactCaseMap.put(c.ContactId, c);
        }
        for(MI_CommonAppService__c serv : services){
            contactServiceMap.put(serv.Contact__c, serv);
        }       
        
        Set<String> ExternalIdsList = new Set<String>();
        for(Contact con : cons){
            ContactWrapper cw 			= new ContactWrapper();
            cw.contactName 				= con.FirstName + ' ' + con.LastName;
            cw.contactFullSSN			= con.Social_Security_Num__c;
            if(!String.isBlank(con.Social_Security_Num__c) && con.Social_Security_Num__c.length() >=9)
            	cw.contactSSN 			= '***-**-' + con.Social_Security_Num__c.substring(con.Social_Security_Num__c.length()-4, con.Social_Security_Num__c.length());
            cw.contactDOB				= string.valueOf(con.Birthdate);
            if(contactCaseMap.containsKey(con.Id)){
            	cw.successCoachName 		= contactCaseMap.get(con.Id).Success_Coach_Name__c;
                if(contactCaseMap.get(con.Id).CA_Completion__c){
                    cw.isCADone				= true;
                }
                if(contactCaseMap.get(con.Id).NA_Completion__c){
                    cw.isNeedsDone			= true;
                }
                cw.caseId = contactCaseMap.get(con.Id).id;
            }
            
            if(contactServiceMap.containsKey(con.Id)){
                if(contactServiceMap.get(con.Id).currentDate__c > contactServiceMap.get(con.Id).RedeterminationDate__c){
                    cw.BenefitsStatus 	= 'Active';
                }
            }
            if(con.ExternalId__c == null || con.ExternalId__c == '')
            	cw.isMiPathUser 			= true;
            cw.contactId				= con.Id;
            contactsList.add(cw);
            if(con.ExternalId__c != null){
                ExternalIdsList.add(con.ExternalId__c);
            }
        }       
        return ExternalIdsList;
    }
    
    public class ContactWrapper{
        public string 		contactName 		{get;set;}        
        public string 		contactSSN 			{get;set;}
        public String 		contactFullSSN		{get;set;}
        public String 		contactDOB 			{get;set;}
        public String 		contactId			{get;set;}
        public String 		caseId				{get;set;}
        public String 		successCoachName 	{get;set;}                   
        public String 		BenefitsStatus 		{get;set;}
        public String 		extId				{get;set;}
        public Boolean 		isNeedsDone 		{get;set;}
        public Boolean 		isCADone 			{get;set;}       
        public Boolean 		isMiPathUser 		{get;set;}
        
        public ContactWrapper(){
            isCADone 			= false;
            isNeedsDone 		= false;
            isMiPathUser		= false;
            BenefitsStatus		= 'Inactive';
            successCoachName	= '';
        }
    }       
}