public class MI_TaskDetailsCont {

    public String retURLVar {get;set;}
    
    public Boolean RecordFound {get;set;}
    //public String TasksRecordIdCont {get;set;}    
    
    public Id TaskCompleteId {get;set;}
    public String EditDate {get;set;}
    public String EditTime {get;set;}
    
    private final MI_Activity__c TaskRecord;
    //private final MI_Activity__c TaskRecordDate;
   
    public MI_TaskDetailsCont(ApexPages.StandardController stdController)    
    {
        retURLVar=ApexPages.currentPage().getParameters().get('frompg');
        /*if((ApexPages.currentPage().getParameters().get('frompg'))==null)
        {z
            System.debug('ATO return URL - frompg param does not exist');
        }
        else
        {
            System.debug('ATO return URL FROMPG 1: '+ApexPages.currentPage().getParameters().get('frompg'));
            System.debug('ATO return URL FROMPG 2: '+retURLvar);
        }
        System.debug('ATO ACTUAL return URL 3: '+ApexPages.currentPage().getParameters().get('retURL'));*/
        
        MI_Activity__c TaskRecordDate;
        
        RecordFound=false;
        Id recid = null;
        EditDate=null;
        EditTime=null;
        try
        {
            //System.debug('ato std cont Start');
            recid = stdController.getId();
            TaskRecordDate = [SELECT Id, Subject__c, Client_Name__c, Priority__c, Status__c, Activity_Date__c, Description__c, task_type__c  
                              FROM MI_Activity__c WHERE Id = :recId];
            EditDate = TaskRecordDate.Activity_Date__C.format('MM/dd/YYYY');
            EditTime = TaskRecordDate.Activity_Date__C.format('h:mm a');
            if((MI_Activity__c)stdController.getRecord()!=null)
            {
                //System.debug('ato std cont not null'+(MI_Activity__c)stdController.getRecord());
                RecordFound=true;
                TaskRecord = (MI_Activity__c)stdController.getRecord();
            }
            //System.debug('ato std cont end');
        }
        catch(exception e)
        {
            System.debug('constructor exception'+e);
        }
    }

    public String getGreeting()
    {
        return null;
    }

    
    public PageReference CompleteTaskMethod()
    {
       if(TaskRecord.Subject__c == null || TaskRecord.Subject__c =='' || EditDate == null || EditDate == '' )
       {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter required Task Details.'));
         return null;
       }
       else{
       TaskRecord.Status__c='Completed';
       TaskRecord.Start_Date__c = TaskRecord.Activity_Date__c;
        update TaskRecord;
        if(retURLVar==null)
        {
            PageReference page = new Pagereference(ApexPages.currentPage().getParameters().get('retURL'));
            page.setRedirect(true);
            return page;
        }
        else
        {
            PageReference page = new Pagereference('/apex/'+retURLVar);
            page.setRedirect(true);
            return page;
        }
        }
    }        
    


    public PageReference UpdateSave()
    {
        
       if(TaskRecord.Subject__c == null || TaskRecord.Subject__c =='' || EditDate == null  || EditDate == '')
         {
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter required Task Details.'));
         return null;
         }
       else
        {
           TaskRecord.Activity_Date__c = Datetime.parse(EditDate+' '+EditTime);
           TaskRecord.Start_Date__c = TaskRecord.Activity_Date__c;
        }
        try{   
             update TaskRecord;
           }Catch(Exception e)
           {
              System.debug('Save Exception'+e);
           }
        
        if(retURLVar==null)
        {
            PageReference page = new Pagereference(ApexPages.currentPage().getParameters().get('retURL'));
            page.setRedirect(true);
            return page;
        }
        else
        {
            PageReference page = new Pagereference('/apex/'+retURLVar);
            page.setRedirect(true);
            return page;
        }
       
    }        

    public PageReference UpdateDelete()
    {
        
        try
          {
            delete TaskRecord;
          }Catch(Exception e)
          {
              System.debug('Delete Exception'+e);
          }
        
        if(retURLVar==null)
        {
            PageReference page = new Pagereference(ApexPages.currentPage().getParameters().get('retURL'));
            page.setRedirect(true);
            return page;
        }
        else
        {
            PageReference page = new Pagereference('/apex/'+retURLVar);
            page.setRedirect(true);
            return page;
        }
    }        

    
    
}