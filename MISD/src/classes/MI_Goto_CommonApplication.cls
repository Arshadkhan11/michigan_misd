public class MI_Goto_CommonApplication {
	
    public Id AppCaseID {get; set;}
    public String MyProflieName{get; set;}
    Public User CaseUser {get;set;}
    PageReference retURL{get; set;}
    
    public MI_Goto_CommonApplication()
    {
        
    }
    
    public Pagereference CreateNewCase()
    {
        AppCaseID = apexpages.currentpage().getparameters().get('CaseId');
        
        if(AppCaseID == null || AppCaseID == '')
        {
            System.debug('New Case is Created');
            List<Profile> ProfileName= [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
            MyProflieName = ProfileName[0].Name;
            CaseUser = [SELECT Id from User where Profile.Name = :System.Label.X_LABEL_189 and isactive = true Limit 1];
            Case Assessment = new Case();
            Assessment.Ownerid = CaseUser.Id;
            Assessment.Origin = 'Email';
            Assessment.Priority = 'Medium';
            if(MyProflieName == 'Success Coach Profile'){
                Assessment.SuccessCoach__c = UserInfo.getUserId();
            }
            insert Assessment;
            
            AppCaseID = Assessment.Id;
            
            CommonApplication__c newca = new CommonApplication__c();
            newca.CaseId__c = AppCaseID;
            newca.Status__c = 'Active';
            newca.CAPrograms__c = '';
            insert newca;
            
            System.debug('CaseId -> ' + AppCaseID + '|' + apexpages.currentpage().getparameters().get('CaseId'));
            System.debug('Calling Common App with Case Id : ' + AppCaseID);
            retURL = new PageReference('/apex/MI_CommonApplication?CaseId='+AppCaseID);
            //PageReference retURL = Page.MI_OpenCalendarView;
            //retURL.getParameters().put('CaseId',AppCaseID);
            retURL.setRedirect(true);
            
        }
        return retURL;
    }
    

    
}