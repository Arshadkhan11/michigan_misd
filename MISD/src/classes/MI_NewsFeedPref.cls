public with sharing class MI_NewsFeedPref {

    public User User{get;set;}

    public MI_NewsFeedPref() 
    {
        User = [SELECT Id, News_Category_Filter__c, News_Educational__c, News_FamilyEvents__c, News_JobRelated__c, News_Medical__c
                    FROM User where username=:UserInfo.getUsername()
                    LIMIT 1];
    }

    public PageReference SaveandClose()  
    {
        update User;   
        
        PageReference page = new Pagereference('/apex/MI_SuccessCoachDashboard');
        page.setRedirect(true);
        return page;
        
        
    }
}