@IsTest public class ClientFolioClassTest{
    @IsTest(SeeAllData=true) 
       
   public static void testClientFolioClass() {
      
       
        Account acc = [Select id from Account where name = 'MIISD'];  
        
        Contact con = new Contact( FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
       Contact con1 = new Contact( FirstName = 'Test1', LastName = 'Test1',AccountId = acc.id , Martial_Status__c = ' ');
        insert con1;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
       
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today()+10,Assess_Locked_By__c = u.id);
        insert cs;
        Case cs1 = new Case(Status= 'New', ContactId = con1.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today()+10,Assess_Locked_By__c = u.id);
        insert cs1;
        
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
       
       Domain__c d1 = new Domain__C(Name__c='Food');
       insert d1;
       AssessmentScoresMaster__c s1 = new AssessmentScoresMaster__c(score__c='1');
       insert s1;
       AssessmentScoresMaster__c s2 = new AssessmentScoresMaster__c(score__c='2');
       insert s2;
       AssessmentScoresMaster__c s3 = new AssessmentScoresMaster__c(score__c='3');
       insert s3;
       AssessmentScoresMaster__c s4 = new AssessmentScoresMaster__c(score__c='4');
       insert s4;
       AssessmentScoresMaster__c s5 = new AssessmentScoresMaster__c(score__c='5');
       insert s5;
       AssessmentScoresMaster__c s6 = new AssessmentScoresMaster__c(score__c=null);
       insert s6;
       AssessmentScores__c as1= new AssessmentScores__c(Score__c = s1.id, Domain__c = d1.id, Contact__c  = con.id);
       insert as1;  
       AssessmentScores__c as2= new AssessmentScores__c(Score__c = s2.id, Domain__c = d1.id, Contact__c  = con.id);
       insert as2;
       AssessmentScores__c as3= new AssessmentScores__c(Score__c = s3.id, Domain__c = d1.id, Contact__c  = con.id);
       insert as3;
       AssessmentScores__c as4= new AssessmentScores__c(Score__c = s4.id, Domain__c = d1.id, Contact__c  = con.id);
       insert as4;
       AssessmentScores__c as5= new AssessmentScores__c(Score__c = s5.id, Domain__c = d1.id, Contact__c  = con.id);
       insert as5;
       AssessmentScores__c as6= new AssessmentScores__c(Score__c = s6.id, Domain__c = d1.id, Contact__c  = con.id);
       insert as6;
       List<StaticResource> docs = new List<StaticResource>();
   //ref caseid: 50035000000dNjk
     docs = [select id, name, body from StaticResource where name = 'TestJSONCommonApplicationPDF'];
     if(docs.size()>0){
     String attbody = docs[0].body.toString();
     attbody = attbody.replace('00335000002jdIYAAY', con.id);
     attbody = attbody.replace('00335000002jdIiAAI', con.id);
     List<Attachment> atttemp = new List<Attachment>();
     Attachment atttemp1= new Attachment();
     atttemp1.Body = Blob.valueOf(attbody);
     atttemp1.Name = 'CommonAppAnswers.txt';
     atttemp1.ParentId=con.id; 
     atttemp.add(atttemp1);
         
     Attachment atttemp2= new Attachment();
     atttemp2.Body = Blob.valueOf('"Q3":"Branch","Q2":"No","Q4":"English","Q5":null,"Q6":"No","Q7A":null,"Q7B":null,"Q7C":null,"Q7D":null,"Q8":null,"Q9":null,"Q10":null,"Q11":null,"Q12":null}');
     atttemp2.Name = 'CommonAppIntro.txt';
     atttemp2.ParentId=con.id; 
     atttemp.add(atttemp2);
          
     insert atttemp;
     }
         
    
     if(docs.size()>0){
     String attbody = docs[0].body.toString();
     attbody = attbody.replace('00335000002jdIYAAY', con1.id);
     attbody = attbody.replace('00335000002jdIiAAI', con1.id);
     List<Attachment> atttemp = new List<Attachment>();
     Attachment atttemp1= new Attachment();
     atttemp1.Body = Blob.valueOf(attbody);
     atttemp1.Name = 'CommonAppAnswers.txt';
     atttemp1.ParentId=con.id; 
     atttemp.add(atttemp1);
         
     Attachment atttemp2= new Attachment();
     atttemp2.Body = Blob.valueOf('"Q3":"Branch","Q2":"No","Q4":"Sign Language","Q5":null,"Q6":"No","Q7A":null,"Q7B":null,"Q7C":null,"Q7D":null,"Q8":null,"Q9":null,"Q10":null,"Q11":null,"Q12":null}');
     atttemp2.Name = 'CommonAppIntro.txt';
     atttemp2.ParentId=con1.id; 
     atttemp.add(atttemp2);
          
     insert atttemp;
      
     }
       System.runAs(u){
           
       ClientFolioClass controller = new ClientFolioClass(con.id);
      
       ClientFolioClass.Wrapper_quickFacts qFacts = new ClientFolioClass.Wrapper_quickFacts();
      
       }
       System.runAs(u){
          
       ClientFolioClass controller = new ClientFolioClass(con1.id);
       
       ClientFolioClass.Wrapper_quickFacts qFacts = new ClientFolioClass.Wrapper_quickFacts();
       
       }
       
       
       
    }
}