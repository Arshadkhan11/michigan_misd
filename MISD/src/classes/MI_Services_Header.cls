public class MI_Services_Header 
{
    public Datetime submitteddate {get;set;}
	public Datetime processeddate {get;set;}
    public Date redetdate {get;set;}
    public Datetime currentdate {get;set;}
    public String citId {get;set;}
    public boolean status {get;set;}
    public String caseId {get;set;}
    public String ServiveType {get;set;}
    public MI_Services_Header()
    {
		MI_CommonAppService__c miser = [select currentDate__c,Processed__c,Submitted__c,RedeterminationDate__c,Status__c,ServiveType__c from MI_CommonAppService__c where Id=:ApexPages.currentPage().getParameters().get('ServiceId')];
    	submitteddate = miser.Submitted__c;
    	processeddate = miser.Processed__c;
    	redetdate = miser.RedeterminationDate__c;
    	currentdate = miser.currentDate__c;
        ServiveType = miser.ServiveType__c;
        citId=ApexPages.currentPage().getParameters().get('citId');
        status=miser.Status__c;
        caseId = [Select Id from Case where ContactId=:citId].Id;
    }
    public Id getCorrespondence1()
    {
        return [select id from Document where Name = 'Correspondence_1.pdf'].id;
    }
    public Id getCorrespondence2()
    {
        return [select id from Document where Name = 'Correspondence_2.pdf'].id;
    }
    public Id getCorrespondence3()
    {
        return [select id from Document where Name = 'Correspondence_3.pdf'].id;
    }
    

}