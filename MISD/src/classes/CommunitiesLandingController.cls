/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {
    
    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        //KD: Added custom settings to remove hard coding of the community name and profile names.
        CommunityDetails__c cdc = CommunityDetails__c.getOrgDefaults();
        String myCommunityName = cdc.CommunityName__c;
		 string spc = cdc.Success_Coach_Profile__c;
         string cp = cdc.Citizen_Profile__c;
         string pp = cdc.Partner_Profile__c;
        String sysadmin = cdc.System_Administrator_Profile__c;
        
        //KD: Added list to remove direct object declaration
        List<Network> netlst = [SELECT Id FROM Network WHERE Name = :myCommunityName and id <> null];
        if(netlst.size()>0){
            Network myCommunity = netlst[0];
            String communityUrl = Network.getLoginUrl(myCommunity.Id);
            String communityUrl1  = communityUrl;
            communityUrl = communityUrl.replace('login','');
           
            if (UserInfo.getUserType().equals('Guest')){
                return new PageReference(communityUrl + 'MI_LandingPage');
            }
            else{
            
                User usr = [Select Id, DualLogin__c, Profile.Id, Primary_Profile__c, AdditionalProfile__c from User where Id=:userinfo.getUserId() LIMIT 1];
                String AdditionalInfo= usr.AdditionalProfile__c;
                //usr.Profile.Id = usr.Primary_Profile__c;
                //update usr;
    
    
                List<Profile> profile = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() and id <> null LIMIT 1];
                String myProflieName = profile[0].Name;
                
               if(AdditionalInfo == spc && usr.DualLogin__c == true) {
                    update new User(Id = usr.Id, DualLogin__c=false);
                    return new PageReference(communityUrl + 'MI_ProfileSelection' );
                }
                else if(myProflieName == spc){
                    return new PageReference(communityUrl + 'MI_SuccessCoachDashboard' );
                }
                else if(MyProflieName == cp){
                    return new PageReference(communityUrl + 'MI_CitizenDashboard' );
                }
                else if(MyProflieName == pp){
                    return new PageReference(communityUrl + 'MI_PartnerDashboard' );
                }
                else if(MyProflieName == sysadmin){
                    return new PageReference(communityUrl + 'MI_AdminDashboard' );
                }
    
                else{
                   return null;
                }
        	}
            
        }
        else{
            return null;
        }
        
    }
    
    
}