public class MI_CreateNotification 
{
	public MI_CreateNotification()
    { }
    public static Id CreateNotification(String Description, Id ConId,String ParentId, String Source,String FeedbackId,String CaseId)
    {       
        Notification__c noti = new Notification__c();
        noti.Notification_Date_Time__c = system.now();
        noti.Notification_Description__c = Description;
        noti.OwnerId = ConId;//UserInfo.getUserId();
        noti.Source__c = Source;
        noti.Parent_Id__c = ParentId;
        noti.FeedbackURL__c = FeedbackId;
        noti.CaseId__c=CaseId;
		if(Source=='Milestone')
        {
            noti.GoalType__c=ParentId;
        }
        insert noti;
        return noti.Id;
    }
}