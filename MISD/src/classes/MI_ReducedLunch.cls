public class MI_ReducedLunch
{
	// page param
    public String currentContactId {get;set;}
    public String currentServiceId {get;set;}
    public Id currentContactCaseId {get;set;}
    
    // List var
    public List<CaseContactRole> SecondaryContactsOnCase {get;set;}
    public List<CaseContactRole> ReducedLunchKids {get;set;}
    public Integer ReducedLunchKidsCount {get;set;}    
	//BBANSAL
    public String ContactName {get;set;}
    
    public MI_ReducedLunch()
    {
        currentContactId = null;
        currentServiceId = null;
        currentContactCaseId = null;
        SecondaryContactsOnCase = new List<CaseContactRole>();
        ReducedLunchKids = new List<CaseContactRole>();
        ReducedLunchKidsCount = 0;
        
        currentContactId = (ApexPages.currentPage().getParameters().get('citId'));
        currentServiceId = (ApexPages.currentPage().getParameters().get('ServiceId'));
        //System.debug('Current Contact Id: '+currentContactId);
        //System.debug('Current Service Id: '+currentServiceId);
        currentContactCaseId = [Select Id from Case where ContactId=:currentContactId].Id;
        //System.debug('Current Case Id: '+currentContactCaseId);
                
        SecondaryContactsOnCase = QuerySecondaryContactsOnCase();
        ReducedLunchKids = QueryReducedLunchKids();
        ReducedLunchKidsCount = ReducedLunchKids.size();
       	//BBANSAL
        ContactName = [select Id,Name from Contact where Id=:currentContactId].Name;
    }
 
    Public List<CaseContactRole> QuerySecondaryContactsOnCase()
    {
        List<CaseContactRole> SecondaryContactsList = [SELECT Id, CasesId, ContactId, Role, Contact.ContactImageName__c, Contact.Name, Contact.Id, Contact.Children__c, Contact.Age__c, Contact.Birthdate 
                                                            FROM CaseContactRole
                                                            WHERE CasesId = :currentContactCaseId and ContactId != :currentContactId];
        return SecondaryContactsList;
    }

    Public List<CaseContactRole> QueryReducedLunchKids()
    {
        List<CaseContactRole> LunchContactsList = new List<CaseContactRole>();
        for(CaseContactRole ccr:SecondaryContactsOnCase)
        {
            if(ccr.Contact.Birthdate!=null)
            {
                if(ccr.Contact.Age__c<=18)
                {
		            LunchContactsList.add(ccr);
                }
            }
        }
        return LunchContactsList;
    }

    
    
}