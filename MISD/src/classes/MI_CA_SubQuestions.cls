public class MI_CA_SubQuestions {
    public list<MI_CA_SubSubQuestions> SubQuestions = new  list<MI_CA_SubSubQuestions>();
     //List<Question__c> SubQuestions = new List<Question__c>();
    public Question__c parentquestion = new Question__c();
    public String AccordianType;
    public MI_CA_SubQuestions() {}
    public MI_CA_SubQuestions(Question__c questionsobj,List<Question__c>  subquestionstemp,Map<String, List<String>> AnswerMap) {
       Map <String, String> subquestion = new Map <String, String> ();
        for(Question__c qtemp : subquestionstemp )
        {
            if(!(subquestion.containsKey(qtemp.ParentQuestion__r.id)))
            {
                subquestion.put(qtemp.ParentQuestion__r.id, qtemp.AccordianType__c);
            }
        }
        if(AnswerMap.isEmpty())
        {
            questionsobj.Answers__c=null;
            questionsobj.Members__c=null;
        }
        else if(AnswerMap.containsKey(questionsobj.id))
        {
            List<String> strtmp = AnswerMap.get(questionsobj.id);
            questionsobj.Answers__c=strtmp[0];
            questionsobj.Members__c=strtmp[1];
        }
        else
        {
            questionsobj.Answers__c=null;
            questionsobj.Members__c=null;
        }
        System.debug('QAnswer: '+questionsobj.Answers__c);
        if(questionsobj.Question_Option__c == null || questionsobj.Question_Option__c == '')
        {
            questionsobj.Question_Option__c = null;
        }
        if(questionsobj.OptionsDisplayed__c == null || questionsobj.OptionsDisplayed__c == '')
        {
            questionsobj.OptionsDisplayed__c = null;
        }
       parentquestion = questionsobj;
      if(subquestion.containsKey(questionsobj.id))
            {
                AccordianType = subquestion.get(questionsobj.id);
               // System.debug('Qacctype'+AccordianType);
            }
        for(Question__c subtemp : subquestionstemp)
        {
            if(subtemp.ParentQuestion__r.id == questionsobj.id)
            {
                if(AnswerMap.isEmpty())
                {
                    subtemp.Answers__c=null;
                    subtemp.Members__c = null;
                }
                else if(AnswerMap.containsKey(subtemp.id))
                {
                    List<String> strtmp = AnswerMap.get(subtemp.id);
                    subtemp.Answers__c = strtmp[0];
                    subtemp.Members__c = strtmp[1];
                }
                else
                {
                    subtemp.Answers__c=null;
                    subtemp.Members__c = null;
                }
                
                if(subtemp.Question_Option__c == null || subtemp.Question_Option__c == '')
                {
                    subtemp.Question_Option__c = null;
                }
                if(subtemp.OptionsDisplayed__c == null || subtemp.OptionsDisplayed__c == '')
                {
                    subtemp.OptionsDisplayed__c = null;
                }
                 MI_CA_SubSubQuestions subsubwrapper =  new MI_CA_SubSubQuestions(subtemp, subquestionstemp,AnswerMap); 
                SubQuestions.add(subsubwrapper);
             //   system.debug('SubQuestions'+ subtemp);
            }
       
        }
     //   system.debug(SubQuestions);   
    }
    
    
}