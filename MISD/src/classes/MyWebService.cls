global class MyWebService {
	webservice static Id makeContact(String lastName){
    	Contact[] c= [Select FirstName, LastName from Contact where LastName= :lastName];
    	return c[0].id;
  }
}