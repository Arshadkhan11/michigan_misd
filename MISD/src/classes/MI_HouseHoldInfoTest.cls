@isTest

public with sharing class MI_HouseHoldInfoTest  {

 public static testMethod void MI_HouseHoldInfoTest(){
 
 
  Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        
      
 Case C= new Case(ContactId=con.id,Status='New',Origin='Email');
 insert C;
 C = [SELECT id,Contact.FirstName, Contact.LastName FROM Case WHERE id = : c.id];
 CaseContactRole CaseRole = new CaseContactRole(CasesId = c.id,ContactId = con.id,Role = 'Primary Contact');
 insert CaseRole;
 List<StaticResource> docs = new List<StaticResource>();
 docs = [select id, name, body from StaticResource where name = 'TestJSONCommonApplicationPDF'];
     if(docs.size()>0){
     String attbody = docs[0].body.toString();
     attbody = attbody.replace('00335000002jdIYAAY', con.id);
     attbody = attbody.replace('00335000002jdIiAAI', con.id);
     List<Attachment> atttemp = new List<Attachment>();
     Attachment atttemp1= new Attachment();
     atttemp1.Body = Blob.valueOf(attbody);
     atttemp1.Name = 'CommonAppAnswers.txt';
     atttemp1.ParentId=c.id; 
     atttemp.add(atttemp1);
         
     Attachment atttemp2= new Attachment();
     atttemp2.Body = Blob.valueOf('"Q3":"Branch","Q2":"No","Q4":"Sign Language","Q5":null,"Q6":"No","Q7A":null,"Q7B":null,"Q7C":null,"Q7D":null,"Q8":null,"Q9":null,"Q10":null,"Q11":null,"Q12":null}');
     atttemp2.Name = 'CommonAppIntro.txt';
     atttemp2.ParentId=c.id; 
     atttemp.add(atttemp2);
          
     insert atttemp;
     
     List<Question__c> Qlst = new List<Question__c>();
   
   Question__c Q1 = new Question__c(Question_No__c='Qtest1',Question_Text__c='Test',Answers__c='Test',Members__c='',Question_Option__c='Test',OptionsDisplayed__c='Single Line',Category__c='Personal',PageName__c='Education',Program__c='Cash;FAP',PageSection__c='Education');
   insert Q1;
   Question__c Q2 = new Question__c(Question_No__c='Qtest2',Question_Text__c='Test',Answers__c='Test',Members__c='',Question_Option__c='Test',OptionsDisplayed__c='',ParentQuestion__c=Q1.id,Program__c='Cash;FAP',Category__c='Personal',PageName__c='Education',PageSection__c='Education');
   insert Q2;
   Question__c Q3 = new Question__c(Question_No__c='Qtest3',Question_Text__c='Test',Answers__c='Test',Members__c='',Question_Option__c='Test',OptionsDisplayed__c='Single Line',Category__c='Personal',PageName__c='Education',Program__c='Cash;FAP',PageSection__c='Education');
   insert Q3;
   Question__c Q4 = new Question__c(Question_No__c='Qtest4',Question_Text__c='Test',Answers__c='Test',Members__c='',Question_Option__c='Test',OptionsDisplayed__c='',ParentQuestion__c=Q2.id,Program__c='Cash;FAP',Category__c='Personal',PageName__c='Education',PageSection__c='Education');
   insert Q4;   
        
   CommonApp_Category__c cacat = new CommonApp_Category__c(Category__c='Personal',Order__c=1);
   insert cacat;
    
   CommonApp_Page__c capag = new CommonApp_Page__c(Page_Name__c='Education',Page_Seq__c=1);
   insert capag;   
   
     PageReference pageRef = Page.MI_HouseHoldDetails;
     Test.setCurrentPage(pageRef);
     System.currentPageReference().getParameters().put('contactId' , con.id);
     System.currentPageReference().getParameters().put('vcaseId' , c.id);
     
 
     MI_HouseHoldInfo ctrl = new MI_HouseHoldInfo();
     MI_HouseHoldInfo.getQuestionsdetails('Cash|FAP',c.id,'Y');
   
   
   }
 
 
 
 
 
 
 
 
 
 
 }
 }