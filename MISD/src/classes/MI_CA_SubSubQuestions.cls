public class MI_CA_SubSubQuestions {
    public List<Question__c> SubSubQuestions = new List<Question__c>();
   public  Question__c childquestion = new Question__c();
    public MI_CA_SubSubQuestions(Question__c subquestionsobj,List<Question__c>  subsubquestionstemp,Map<String, List<String>> AnswerMap)
    {
        childquestion = subquestionsobj;
        for(Question__c subtemp : subsubquestionstemp)
        {
            if(subtemp.ParentQuestion__r.id == subquestionsobj.id)
            {
                if(AnswerMap.isEmpty())
                {
                    subtemp.Answers__c=null;
                    subtemp.Members__c = null;
                }
                else if(AnswerMap.containsKey(subtemp.id))
                {
                    List<String> strtmp = AnswerMap.get(subtemp.id);
                    subtemp.Answers__c=strtmp[0];
                    subtemp.Members__c = strtmp[1];
                }
                else
                {
                    subtemp.Answers__c=null;
                    subtemp.Members__c = null;
                }
                if(subtemp.Question_Option__c == null || subtemp.Question_Option__c == '')
                {
                    subtemp.Question_Option__c = null;
                }
                if(subtemp.OptionsDisplayed__c == null || subtemp.OptionsDisplayed__c == '')
                {
                    subtemp.OptionsDisplayed__c = null;
                }
                SubSubQuestions.add(subtemp);
            }
       
        }
    }
}