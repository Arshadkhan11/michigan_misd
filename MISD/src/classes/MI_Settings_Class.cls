public class MI_Settings_Class {
    
    ////427
    ////428
    public String EnglishFlag {get;set;}
    public String SpanishFlag {get;set;}
    public String LanguageFlag {get;set;}
	public String FinalLanguage {get;set;}    
    public boolean NutritionFlag {get;set;}
    public boolean ShelterFlag {get;set;}
    public boolean EmploymentFlag {get;set;}
    public boolean TransportationFlag {get;set;}
    public boolean HealthFlag {get;set;}
    public boolean WellnessFlag {get;set;}
    public boolean EducationFlag {get;set;}
    public boolean FamilyLifeFlag {get;set;}
    public boolean FinancialsFlag {get;set;}
    public boolean JobSkillsFlag {get;set;}
    public boolean NoteEmailFlag {get;set;}
    public boolean NoteTextFlag {get;set;}
    public boolean ChatEmailFlag {get;set;}
    public boolean ChatTextFlag {get;set;}
    public boolean AppEmailFlag {get;set;}
    public boolean AppTextFlag {get;set;}
    
    
    public User Pref {get;set;}
    public String UserId{get;set;}
    public String sam {get;set;}
    public MI_Settings_Class()
    {       
        UserId=[select Id from user where id =:UserInfo.getUserId()].Id; 
        Pref=[select Notifications_Email__c, Notifications_Text__c,Chat_Email__c,Chat_Text__c,Appointment_Email__c,Appointment_Text__c,Financials__c,Nutrition__c,Family_Life__c,Employment__c,Health__c,Shelter__c,Job_Skills__c,Wellness__c,Education__c,Transportation__c,LocaleSidKey from User where Id=:UserId];
        NutritionFlag = Pref.Nutrition__c;
        ShelterFlag = Pref.Shelter__c;
        TransportationFlag = Pref.Transportation__c;
        EmploymentFlag = Pref.Employment__c;
        HealthFlag = Pref.Health__c;
        WellnessFlag = Pref.Wellness__c;
        EducationFlag = Pref.Education__c;
        FamilyLifeFlag = Pref.Family_Life__c;
        FinancialsFlag = Pref.Financials__c;
        JobSkillsFlag = Pref.Job_Skills__c;
        LanguageFlag= Pref.LocaleSidKey;
        
        system.debug('page load '+LanguageFlag);
    }
    public pagereference save1(){
    	save();
        return null;
    }
    public void save(){
        
        system.debug('Save trigger: '+LanguageFlag);
        
        if(EnglishFlag == 'true'){
            LanguageFlag = 'en_US';
            Pref.LocaleSidKey = LanguageFlag;
        }
        if(SpanishFlag == 'true'){
            LanguageFlag = 'es_US';
            Pref.LocaleSidKey = LanguageFlag;
        }
        
        
        Pref.Job_Skills__c = JobSkillsFlag;
        Pref.Financials__c = FinancialsFlag;
        Pref.Family_Life__c = FamilyLifeFlag;
        Pref.Education__c = EducationFlag;
        Pref.Wellness__c = WellnessFlag;
        Pref.Health__c =HealthFlag ;
        Pref.Nutrition__c = NutritionFlag;
        Pref.Shelter__c = ShelterFlag;
        Pref.Transportation__c = TransportationFlag;
        Pref.Employment__c= EmploymentFlag;
        //427
        Pref.Notifications_Email__c= NoteEmailFlag;
        Pref.Notifications_Text__c= NoteTextFlag;
        Pref.Chat_Email__c= ChatEmailFlag;
        Pref.Chat_Text__c= ChatTextFlag;
        Pref.Appointment_Email__c= AppEmailFlag;
        Pref.Appointment_Text__c= AppTextFlag;
        Update Pref;  
       system.debug('After save '+Pref.LocaleSidKey);
    } 
    public void setDomLanFunc()
    {
        system.debug('BBB'+LanguageFlag);
    }
    
    public PageReference refreshpg()
    {
        PageReference pageRef = new PageReference('apex/MI_Settings_page');
		pageRef.setRedirect(true);
		return pageRef;
    }   
}