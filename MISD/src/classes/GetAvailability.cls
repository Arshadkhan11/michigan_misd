public with sharing class GetAvailability {

    public List<MI_Activity__c> Ea_Str {get; set; }
    public List<MI_Activity__c> Ea {get; set; }
    public String BlockedTime {get; set; }
     
    public GetAvailability() {
        List<Event_Attendee__c> actList = [select id, Event__c from Event_Attendee__c where Attendee__c =:ApexPages.currentPage().getParameters().get('id')];
        set<Id> evIds = new set<Id> ();
        for(Event_Attendee__c act: actList ){
            evIds.add(act.Event__c);
        }
        Ea = [select start_date__c, end_date__c, subject__c from MI_Activity__c where (OwnerId= :ApexPages.currentPage().getParameters().get('id') OR Id IN:evIds) and start_date__c >= :Date.today().addDays(-1) order by start_date__c];
        BlockedTime = '';
        for(MI_Activity__c e: Ea)
        {
            BlockedTime = BlockedTime + e.start_date__c + ' - ' + e.end_date__c + '\n';
            //e.start_date__c = e.start_date__c.format();
        }
    }
}