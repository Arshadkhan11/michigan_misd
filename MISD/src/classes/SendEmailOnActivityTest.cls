@isTest
public class SendEmailOnActivityTest 
{
    @isTest public static void testConst3()
    {
		Account ac = new Account(name ='Grazitti') ;
        insert ac; 
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con;  
        Id p = [select id from profile where name='Standard User'].id;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing3', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com',MobilePhone='(315) 308-9954');
        insert user;

        //create MI_Activity__c
        MI_Activity__c m = new MI_Activity__c(Subject__c = 'Hello', RecordTypeId = [Select id,SobjectType,Name from RecordType where Name='Event' LIMIT 1].Id, Activity_Date__c=date.today(),Start_Date__c=date.today()+5,OwnerId=user.id);
        insert m;

        m.Description__c='hello';
        m.Email_Sent__c = true;
        //m.Email__c='bbansal@deloitte.com';
        update m;
    }
    @isTest public static void testConst()
    {
		Account ac = new Account(name ='Grazitti') ;
        insert ac; 
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con;  
        Id p = [select id from profile where name='Standard User'].id;
        User user = new User(alias = 'h3', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='h3r', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                timezonesidkey='America/Los_Angeles', username='tester3@noemail.com',MobilePhone='(315) 308-9954');
        insert user;
        User attendee = new User(alias = 'h4', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='h4r', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                timezonesidkey='America/Los_Angeles', username='tester4@noemail.com',MobilePhone='(315) 308-9954');
        insert attendee;

        //create MI_Activity__c
        MI_Activity__c m = new MI_Activity__c(Subject__c = 'Hello', RecordTypeId = [Select id,SobjectType,Name from RecordType where Name='Event' LIMIT 1].Id, Activity_Date__c=date.today(),Start_Date__c=date.today()+5,OwnerId=user.id);
        insert m;

		Event_Attendee__c at = new Event_Attendee__c(Attendee__c=attendee.Id,Event__c=m.Id,OwnerId=attendee.Id);
        insert at;

        m.Description__c='hello';
        m.Email_Sent__c = true;
        //m.Email__c='bbansal@deloitte.com';
        update m;
    }
    @isTest public static void testConst2()
    {
		Account ac2 = new Account(name ='Grazitti') ;
        insert ac2; 
        Contact con2 = new Contact(LastName ='testCon',AccountId = ac2.Id);
        insert con2;  
        Id p2 = [select id from profile where name='Standard User'].id;
        User user2 = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p2, country='United States',IsActive =true,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com',MobilePhone='(315) 308-9954');
        insert user2;
        //create MI_Activity__c
        MI_Activity__c m2 = new MI_Activity__c(Subject__c = 'Hello', RecordTypeId = [Select id,SobjectType,Name from RecordType where Name='Task' LIMIT 1].Id, Activity_Date__c=date.today(),Start_Date__c=date.today()+5,OwnerId=user2.Id);
        insert m2;
        m2.Description__c='hello';
        m2.Email_Sent__c = true;
        //m.Email__c='bbansal@deloitte.com';
        update m2;   
    }
    
    
    
/*    @isTest public static void testConst2()
    {
		Account ac = new Account(name ='Grazitti') ;
        insert ac; 
        Contact con = new Contact(LastName ='testCon',AccountId = ac.Id);
        insert con;  
        Id p = [select id from profile where name='Standard User'].id;
        User user = new User(alias = 'test123', email='test123@noemail.com',
                emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
                localesidkey='en_US', profileid = p, country='United States',IsActive =true,
                timezonesidkey='America/Los_Angeles', username='tester@noemail.com',MobilePhone='(315) 308-9954');
        insert user;
		SendSMSMethod('BBANSAL', user.MobilePhone, user.Id, 'Hello How are you');
    }*/
}