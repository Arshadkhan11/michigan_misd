@IsTest global with sharing class  MI_DocDetailsTest {
    @IsTest(SeeAllData=true) 
       
    global static void testMI_DocDetails() {
     Account acc=[Select id from Account where name='MIISD'];
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        Contact con1 = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con1;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
       
        List<User> u = new List<User>();
        
        User u1 = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
            Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
            Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        u.add(u1);
        
         User u2 = new User(Alias = 'tes', Email='clc@tes.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@tes.com', ContactId = con1.id,Security_Question_1__c = 'In what city were you born in?',
            Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
            Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        u.add(u2);
        
        insert u;
        
        
        
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u1.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
        AttachmentDetail__C doc = new AttachmentDetail__c( Attachment_Description__c = 'xyz', SharedWith__c ='lucy;tamara',Contact__c = con.id);
        insert doc;
        
        Attachment a1 = new Attachment(parentId = doc.id,name='trial.txt', body=blob.valueof('trial'));
        insert a1;
        
        ApexPages.currentPage().getParameters().put('citId',con.id);
        ApexPages.currentPage().getParameters().put('coachId',u1.id);
        ApexPages.currentPage().getParameters().put('docId',doc.id);
        
        MI_DocDetails ctrl = new MI_DocDetails();
        ctrl.allusers=u;
        MI_DocDetails.userWrapper uw = new MI_DocDetails.userWrapper(u1,u);
        ctrl.preselectedusersfunc();
        ctrl.editaccess();
        ctrl.saveaccess();
        ctrl.cancelsaveaccess();
        ctrl.clearsearchcontacts();
        ctrl.descclear();
        ctrl.descsave();
        ctrl.descshow();
        ctrl.deletedoc();
        ctrl.redirectback();
        ctrl.searchcontacts();
        ctrl.coachuser = false;
        ctrl.deletedoc();
     
         
      }
    }