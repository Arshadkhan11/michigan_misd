public class MI_WIC
{
	// page param
    public String currentContactId {get;set;}
    public String currentServiceId {get;set;}
    public Id currentContactCaseId {get;set;}
    
    // List var
    public List<CaseContactRole> SecondaryContactsOnCase {get;set;}
    public List<CaseContactRole> WICKids {get;set;}
    public Integer WICKidsCount {get;set;}    
    
    public Integer WICKidsRowCounterPage {get;set;}
    public List<Integer> WICKidsRowCounter {get;set;}
    public Integer i {get;set;}
    //BBANSAL
    public String ContactName {get;set;} 
    
    public MI_WIC()
    {
        currentContactId = null;
        currentServiceId = null;
        currentContactCaseId = null;
        SecondaryContactsOnCase = new List<CaseContactRole>();
        WICKids = new List<CaseContactRole>();
        WICKidsCount = 0;
        i = 0;
        WICKidsRowCounter = new List<Integer>();
                
        currentContactId = (ApexPages.currentPage().getParameters().get('citId'));
        currentServiceId = (ApexPages.currentPage().getParameters().get('ServiceId'));
        //System.debug('Current Contact Id: '+currentContactId);
        //System.debug('Current Service Id: '+currentServiceId);
        currentContactCaseId = [Select Id from Case where ContactId=:currentContactId].Id;
        //System.debug('Current Case Id: '+currentContactCaseId);
                
        SecondaryContactsOnCase = QuerySecondaryContactsOnCase();
        WICKids = QueryWICKids();
        WICKidsCount = WICKids.size();
        //system.debug('BBBB4---');
        for(Integer i=0;i<WICKidsCount;i++)
        {
            //system.debug('BBBB5---');
            //System.debug('i value: '+i);
            WICKidsRowCounter.add(i);
        }
		ContactName = [select Id,Name from Contact where Id=:currentContactId].Name;  
    }
 
    Public List<CaseContactRole> QuerySecondaryContactsOnCase()
    {
        List<CaseContactRole> SecondaryContactsList = [SELECT Id, CasesId, ContactId, Role, Contact.ContactImageName__c, Contact.Name, Contact.Id, Contact.Children__c, Contact.Age__c, Contact.Birthdate 
                                                            FROM CaseContactRole
                                                            WHERE CasesId = :currentContactCaseId and ContactId != :currentContactId];
        return SecondaryContactsList;
    }

    Public List<CaseContactRole> QueryWICKids()
    {
        List<CaseContactRole> WICContactsList = new List<CaseContactRole>();
        for(CaseContactRole ccr:SecondaryContactsOnCase)
        {
			//system.debug('BBB--1 --- '+ccr.Contact.Age__c);
            if(ccr.Contact.Birthdate!=null)
            {
                if(ccr.Contact.Age__c<=4)
                {
		            WICContactsList.add(ccr);
                }
            }
        }
        return WICContactsList;
    }

    
    
}