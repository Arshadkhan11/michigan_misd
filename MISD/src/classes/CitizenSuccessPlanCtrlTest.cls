@isTest

public with sharing class CitizenSuccessPlanCtrlTest   {

@IsTest(SeeAllData=true)
 
 public static void test1(){
     
        Account acc = [select id from Account where name = 'MIISD'];  
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         system.Debug('casecoach id:'+cs.SuccessCoach__c);
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
  //Case C= new Case(Status='New',Origin='Email');
  //insert C;
  
  List<SP_Goal__c> Spg = new List<SP_Goal__c>();
  SP_Goal__c spg1=new SP_Goal__c(Title__c='Test',Case__c=cs.id,Domain__c='Food',Show_Domain__c=true);
  SP_Goal__c spg2=new SP_Goal__c(Case__c=cs.id,Domain__c='Shelter',Show_Domain__c=true);
  //SP_Goal__c spg3=new SP_Goal__c(Case__c=cs.id,Domain__c='Income',Show_Domain__c=true);
  //SP_Goal__c spg4=new SP_Goal__c(Case__c=cs.id,Domain__c='Education',Show_Domain__c=true);
  spg.add(spg1);
  spg.add(spg2);
  //spg.add(spg3);
  //spg.add(spg4);
  
  insert spg;
  
  List<SP_Transactional__c> spt = new List<SP_Transactional__c>();
  SP_Transactional__c spt1;
  
  spt1= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='parentSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Services_Button_Text__c='test',Services_Type__c='test',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',Service_Selected__c='t',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today());
  insert spt1;
  
 
  
  SP_Transactional__c spt2= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='Service',Services_Button_Text__c='test',Services_Type__c='test',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',Service_Selected__c='t',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  insert spt2;
  
  
  SP_Transactional__c spt3= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='Service',Services_Button_Text__c='test',Services_Type__c='test',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',Service_Selected__c='t',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  insert spt3;
  
  
  SP_Transactional__c spt4= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='Service',Services_Button_Text__c='test',Services_Type__c='',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',Service_Selected__c='t',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  insert spt4;
  
  SP_Transactional__c spt5= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='MilestoneSteps',Services_Button_Text__c='test',Services_Type__c= NULL,Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',Service_Selected__c='t',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  insert spt5;
  
  Success_Plan_Master__c spm1 = new Success_Plan_Master__c(Type__c='MilestoneSteps', Value__c='Food', Duration__c=20);
  insert spm1;
  
  Success_Plan_Master__c spm2 = new Success_Plan_Master__c(Type__c='MilestoneSteps', Value__c='Food',parent__c=spm1.Id,Duration__c=20);
  insert spm2;
  
  Success_Plan_Master__c spm3 = new Success_Plan_Master__c(Type__c='MilestoneSteps', Value__c='Food',parent__c=spm2.Id,Duration__c=20);
  insert spm3;
  
  Success_Plan_Master__c spm4 = new Success_Plan_Master__c(Type__c='MilestoneSteps', Value__c='Food', Duration__c=20,parent__c=spm3.Id);
  insert spm4;
  
  
  
  
  /*SP_Transactional__c spt5= new SP_Transactional__c(Success_Plan_Master2__c = spm2.Id,Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='Milestone',Services_Button_Text__c='test',Services_Type__c='test',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',Service_Selected__c='t',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  insert spt5;*/
  
  
  insert spt;
  system.assert(spt1.Status__c == true);
  
  Service__c s=new Service__c(Case__c=cs.id);
  system.debug('spt1.Id '+spt1.Id );
  
  System.currentPageReference().getParameters().put('SP_CaseId', cs.id);
  
  CitizenSuccessPlanCtrl Csp = new CitizenSuccessPlanCtrl();
  
  
  System.currentPageReference().getParameters().put('serviceId',s.id);
  
  
  CitizenSuccessPlanCtrl.MilestonesWrapper Mw = new CitizenSuccessPlanCtrl.MilestonesWrapper(); 
  
  System.currentPageReference().getParameters().put('serviceId',s.id);
  
  
  String rscId = 'fjdlfjadklfj';
  PageReference pageRef = Page.CitizenSuccessPlan;
  Test.setCurrentPage(pageRef);
  csp.dummyMethod();
  csp.EditDetails();
  csp.CancelSave();
  csp.DeleteResource();
  csp.saveActiveTab();
  csp.prePopulateCommonApp();
  
  System.currentPageReference().getParameters().put('ServId',spt3.id);

  csp.updateService();
  System.currentPageReference().getParameters().put('mStepId','1234567');
  
  //csp.SaveMilestone();
  //csp.SaveResources();
 
  Csp.displayServices();
  Csp.populateMilestonesAndRes();
  Csp.updateServiceStat('t',spt3.id);
  CitizenSuccessPlanCtrl.markSPCompleteRt(cs.id);
  Csp.UpdateServiceStAF();
  CitizenSuccessPlanCtrl.prePopulateCommonApp(cs.id, spt5.id);
  CitizenSuccessPlanCtrl.updateServiceStatRT('true', spt3.id, '');
  CitizenSuccessPlanCtrl.UpdateMilestoneIds('','', cs.id);
 
        
        
        System.runAs(u){
        pageRef = Page.CitizenSuccessPlan;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
        ApexPages.currentPage().getParameters().put('SP_CaseId',cs.id); 
        csp = new CitizenSuccessPlanCtrl();
        
        

        
        Csp.populateMilestonesAndRes();
        Csp.displayServices();
        }
       
  System.currentPageReference().getParameters().put('SP_CaseId',null);
  
  CitizenSuccessPlanCtrl Csp2 = new CitizenSuccessPlanCtrl();
  Csp2.displayServices();
        
 
 }
 }