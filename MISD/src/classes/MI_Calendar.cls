public class MI_Calendar{
    //Property to display Event Name
    public String title {get;set;}
    //Property to indicate whether the event is full-day event
    public Boolean allDay {get;set;}
    //Property to display Start date
    public String startDate {get;set;}
    //Property to display End date
    public String endDate {get;set;}
    //Property to hold the URL so that user can navigate to the Detail page
    public String url {get;set;}
    //Property to display End date. To be used when we need to display the end date in the list view
    public String endDate2 {get;set;}
    //Property for background color of a event in a calendar
    public String backgroundColor {get;set;}
    //Property for border color of a event in a calendar
    public String borderColor {get;set;}
    //Property for text color of a event in a calendar
    public String textColor {get;set;}
    //Property to store style of a event in a calendar
    public String style {get;set;}
}