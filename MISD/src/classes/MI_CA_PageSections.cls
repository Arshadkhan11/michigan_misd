public class MI_CA_PageSections {
    public list<MI_CA_Questions> PageSection = new  list<MI_CA_Questions>();
   	public String name;
    public Integer PageNameSeq;
    
    public MI_CA_PageSections(string Category,string pagename,String vProgramStr,Map<String, List<String>> AnswerMap,Map<String, Integer> vPageSeq) {
        
        List<Question__c> allquestions = new List<Question__c>();
        String AllStr = 'SELECT PageSection__c FROM Question__c WHERE ParentQuestion__c = \'\' AND Program__c includes ( '+ vProgramStr+ ') AND PageName__c = :pagename AND Category__c  = :Category  GROUP BY PageSection__c';
        list<AggregateResult> pagesections = Database.query(AllStr); 
        //list<AggregateResult> pagesections = [SELECT PageSection__c FROM Question__c where category__c = :Category and PageName__c = :pagename and ParentQuestion__c = '' GROUP  BY PageSection__c];
        String AllQuestionStr = 'SELECT Program__c,id, Question_No__c, ParentQuestion__r.id, Question_Text__c, Question_Option__c, Data_Type__c, Category__c, PageName__c, PageSection__c, Columns__c, AccordianType__c,Answers__c,Members__c,OptionsDisplayed__c,SubSection__c,PDFName__c,SubTitle__c,SubSectionDetail__c,StyledDataType__c FROM Question__c WHERE category__c = :Category AND ParentQuestion__c = \'\' AND Program__c includes ( '+ vProgramStr+ ') order by Question_No__c';
        allquestions = Database.query(AllQuestionStr);
       // allquestions = [SELECT Program__c,id,Question_No__c,ParentQuestion__r.id, Question_Text__c, Question_Option__c, Data_Type__c, Category__c, PageName__c, PageSection__c, Columns__c, AccordianType__c,Answers__c FROM Question__c where category__c = :Category and ParentQuestion__c = '' order by Question_No__c]; 
        list<MI_CA_Questions> sections = new  list<MI_CA_Questions>();
        String pagesectionzzz;
        
        for(AggregateResult subtemp : pagesections)
        {
            name = pagename;
            //PageNameSeq = 1;
           	PageNameSeq = vPageSeq.get(pagename);
            if(PageNameSeq == null)
            {
                PageNameSeq = 0;
            }
            system.debug('subtempfrompagesections' + subtemp);
            string str = string.valueOf(subtemp.get('PageSection__c')) ;
            MI_CA_Questions Quessubwrapperz =  new MI_CA_Questions(str, allquestions,Category,vProgramStr, AnswerMap); 
            sections.add(Quessubwrapperz);
            PageSection.add(Quessubwrapperz);
			pagesectionzzz = str;
            // system.debug('vatsansubwrapperz' + vatsansubwrapperz);
        } 
    }   
}