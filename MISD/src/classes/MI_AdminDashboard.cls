global with sharing class MI_AdminDashboard {

    public Integer UsersCount {get; set; }
    public List<User> UserRecs {get; set; }
    public String SearchString {get;set;}
    public List<User> UsersLst {get; set; }
    public boolean SearchFlag {get; set; }
    public String AdditionalProfile {get; set; }

    public MI_AdminDashboard()
    {
        UsersCount = [Select Count() from User where (Profile.Name = 'Success Coach Profile' or Profile.Name = 'Partner Profile' or Profile.Name = 'Citizen Profile' )];
        SearchFlag = false;
        getUsersList();
    }
    
    
    public PageReference SearchUsers()
    { 
        try{
        if(SearchString.length()>0)
        { 
            SearchFlag = true;
            getUsersList();
        } 
        else
        {
            SearchFlag = false;
            getUsersList();
        }
        }
        catch(exception e)
        {
            //System.debug('ato const debug error 1'+e.getMessage());
        }
        return null;
    }
    
    public void getUsersList() 
    {
      if(SearchFlag)
        {
          if(SearchString.contains('\''))
            {
                UsersLst = new List<User>();
            }
            else
            {
                    UsersLst = [Select Id, Name, Profile.Name,  AdditionalProfile__c from User
                              where ((Name LIKE :('%'+string.escapeSingleQuotes(SearchString)+'%')) OR 
                              (Name LIKE :('%'+string.escapeSingleQuotes(SearchString))) OR
                              (Name LIKE :(string.escapeSingleQuotes(SearchString)+'%'))
                              ) AND (Profile.Name = 'Success Coach Profile' or Profile.Name = 'Partner Profile' or Profile.Name = 'Citizen Profile') ORDER BY Name];
            }
        }
        else
        {
                UsersLst = [Select Id, Name, Profile.Name,  AdditionalProfile__c from User WHERE (Profile.Name = 'Success Coach Profile' or Profile.Name = 'Partner Profile' or Profile.Name = 'Citizen Profile') ORDER BY Name];

                UsersCount = UsersLst.size();
        }

    }
    
    public PageReference saveAdditionalRole() {
        String usrrole = Apexpages.currentPage().getParameters().get('role');
        String usrid = Apexpages.currentPage().getParameters().get('usrid');
        System.debug('@Rameez- usrrole - ' + usrrole + '   usrid - ' + usrid);
        
        User usr = [Select Id, Name, Profile.Name,  AdditionalProfile__c from User where Id=:usrid];
        if(usrrole  != 'None') {
            usr.AdditionalProfile__c = usrrole;
        }
        else {
            usr.AdditionalProfile__c = null;
        }
        update usr;
        return null;
    }
    
}