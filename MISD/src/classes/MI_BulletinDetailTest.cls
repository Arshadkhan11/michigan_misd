@isTest
public class MI_BulletinDetailTest {
  @isTest public static void testMI_BulletinDetail(){
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id, Title='Master',BirthDate=date.today());
        insert con;
        
        Agency__c ag = new Agency__c(Agency_Name__c='TestAgency',Verification_Code__c=000);
        insert ag;
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile']; 
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, Agency_ID__c = ag.id,
            TimeZoneSidKey='America/Los_Angeles', UserName='testbul@test.com',ContactId = con.id);
        System.runAs(u) {
            Bulletin__c bul = new Bulletin__c(Event_Title__c ='Test',Bulletin_Date_Time__c=System.now(),Send_Event_To__c = u.FirstName+'!'+u.id);
            insert bul;
            PageReference pageRef = Page.MI_Bulletin_Detail;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(bul);
            ApexPages.currentPage().getParameters().put('bulletid',bul.id);
            MI_BulletinDetail bd = new MI_BulletinDetail();
            bd.uploadFile1();
            MI_BulletinDetail.vattBody = Blob.valueOf('Unit Test Attachment Body');
            MI_BulletinDetail.vattTitle = '';
            Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
            MI_BulletinDetail.uploadFile(bul.id, bodyBlob, 'Bulletintest.txt');
            MI_BulletinDetail.fetchDescription(bul.id);
            MI_BulletinDetail.fetchDescription('');
            Datetime myDate = Datetime.now();
            String dateOutput = myDate.format('MM/dd/yyyy');
            Date PastDate = System.now().date()-2;
            String PastdateOutput = PastDate.format();
             MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','Y', 'Health', u.LastName+'!'+u.id, 'TestAgency', 'Off', '0', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', '', '', '', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Off', '0', '','');
            MI_BulletinDetail.BulletinSave(bul.id, '', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Off', '0', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Off', '0', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', '', '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Daily', '0', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Daily', '0', PastdateOutput,'');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Daily', '0', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', PastdateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Off', '0', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '01:00 AM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Off', '0', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:00 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Off', '0', '','');
          MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Daily', '2', '','');
            MI_BulletinDetail.CreateCalevt(bul.id);
            
            bd.updsummarytxt();
            bd.Repeating='Daily';
            bd.Occurences=2;
            bd.updsummarytxt();
            bd.Repeating='Weekly';
            bd.updsummarytxt();
            bd.Repeating='Bimonthly';
            bd.updsummarytxt();
            bd.Repeating='Monthly';
            bd.updsummarytxt();
            bd.Repeating='Yearly';
            bd.updsummarytxt();
            bd.Occurences=0;
            bd.BulletinRepEnd=(System.now().date()+400).format();
            bd.updsummarytxt();
            bd.BulletinRepEnd=(System.now().date()+40).format();
            bd.Repeating='Daily';
            bd.updsummarytxt();
            bd.Repeating='Weekly';
            bd.updsummarytxt();
            bd.Repeating='Bimonthly';
            bd.updsummarytxt();
            bd.Repeating='Monthly';
            bd.updsummarytxt();
            
            bd.eventusersearch = 'est';
            bd.sendeventUserSearch();
            
           MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Weekly', '2', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Monthly', '2', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Bimonthly', '2', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Yearly', '2', '','');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Daily', '0', (System.now().date()+3).format(),'');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Weekly', '0', (System.now().date()+6).format(),'');
          MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Weekly', '0', (System.now().date()+7).format(),'');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Bimonthly', '0', (System.now().date()+14).format(),'');
          MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Monthly', '0', (System.now().date()+31).format(),'');
            MI_BulletinDetail.BulletinSave(bul.id, 'Event', 'Test', 'Michigan', 'James', '5677878788', 'www.google.com', 'Testing Bulletin', dateOutput, '11:30 PM', '11:45 PM', 'BulletinTest', 'Bulletintest.txt','N', 'Health', '', 'TestAgency', 'Yearly', '0', (System.now().date()+400).format(),'');
          MI_BulletinDetail.deletebul(bul.id);
            
        }   
    }
}