public class ClientFolioClass {

    public string   conId;
    public User     u {get;set;}
    public Case     contactCase {get;set;}
    public string   successCoach {get;set;}
    public String   skills {get;set;}
    public String   barrier {get;set;}
    public list<String> crisisList {get;set;}
    public list<String> vulnerableList {get;set;}
    public list<String> riskList {get;set;}
    public list<String> safeList {get;set;}
    public list<String> sufficientList {get;set;}
    public list<String> notAssessedList {get;set;}
    public List<Case> casesList {get;set;}
    public Wrapper_quickFacts qFacts {get;set;}
    
    //commonApp
    public String vCaseId {get; set;}
    public String vPrimConId {get; set;}
  
    public map<string, String> vFinalPDFMap {get; set;}
    
    public Case currentCase = new Case();
   
    public ClientFolioClass(Id contactId){
        
        //Accessing User Case details. 
       
        contactCase = new Case();
        conId = contactId;
        initialize();
      
       
     
    }
    public void initialize(){
         //Instantiating DomainList
        crisisList = new List<String>();
        vulnerableList = new List<String>();
        riskList = new List<String>();
        safeList = new List<String>();
        sufficientList = new List<String>();
        notAssessedList = new List<String>();
        qFacts = new Wrapper_quickFacts();
        
       
        
        //Marital status
        getMaritalAndCoachDetails();
        
        //segment Details()
        getSegmentDetails();
        
         //Needs Details
        getNeedsDetails();
        
        //Household Details
        getHouseholdDetails();
        
        //Goal Details
        getGoalDetails();
        
        //CommonAppData
        extractPDFdata();
        
        //gradeDetails()
        getGradeDetails();
        
        //incomeDetails()
        getIncomeDetails();
        
        //languageDetails
        qFacts.language = getLanguage();
        qFacts.language = qFacts.language.replace('"', '');
        qFacts.language = qFacts.language.replace('}', '');

        //living&EmploymentDetails
        getLivingAndEmpDetails();
    }
    public void getLivingAndEmpDetails(){
        
        qFacts.living = getResponse('QS-0015');
        if( getResponse('QS-0088') == 'Yes' )
        {
             qFacts.empStatus = getResponse('QS-0089'); 
        }
        else if( getResponse('QS-0088') == 'No' )
        {
            qFacts.empStatus = 'Unemployed';
        }
        else
        {
            qFacts.empStatus = '';
        }
    }
    public void getMaritalAndCoachDetails(){
        
         if(conId != null){
          Contact c = [select id,Martial_Status__c from Contact where id = :conId LIMIT 1];
          if( c.Martial_Status__c != null ){
           qFacts.maritalStatus = c.Martial_Status__C;
           system.debug('marital status:'+qFacts.maritalStatus);
             }
          else {
             qFacts.maritalStatus = '';
          }
        }
        else{
             qFacts.maritalStatus = '';
        }
        casesList = [SELECT id,Reassessment_Date__c,Assess_Locked_By__c,Assess_Locked_By__r.Name, contact.Name,SuccessCoach__r.name,SuccessCoach__r.phone,SuccessCoach__r.email,lastmodifiedDate, CreatedDate 
                     FROM Case
                     WHERE contactId = : conId];
        
        if(!casesList.isEmpty()){
            contactCase = casesList[0];
            qFacts.successCoach = contactCase.SuccessCoach__r.name;
            qFacts.phone = contactCase.SuccessCoach__r.phone;
            qFacts.email = contactCase.SuccessCoach__r.email;
            
            if(contactCase.Reassessment_Date__c != null )
            { Date d = contactCase.Reassessment_Date__c; 
              string []months = new string [] {'','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};
              qFacts.reAssessment = months[d.month()]+' '+d.day()+',  '+d.year();
            }
            else
            {
              qFacts.reAssessment = '';  
            }
            
            if(casesList[0].Assess_Locked_By__c != null)
            {
               
                
                qFacts.caselockedby = casesList[0].Assess_Locked_By__r.Name;
                if(UserInfo.getName() != qFacts.caselockedby ){
                     qFacts.caselocked = true;
                }
            }
        }
        
    }
    public void getSegmentDetails(){
        
         skills = 'Skills';
         barrier = 'Barrier';
        
         qFacts.segment = MI_FamilyManagement.FMSingleInit(contactCase.id);
         system.debug('qFacts.segment:'+qFacts.segment);
         
         if(qFacts.segment.contains('1'))
         {
           qFacts.skillDown = true;
           qFacts.barrierUp = true;
           qFacts.checkinFrequency = 'Weekly';
         }
        else if(qFacts.segment.contains('2'))
          {
           qFacts.skillUp = true;
           qFacts.barrierUp = true;
           qFacts.checkinFrequency = 'Bi-Weekly';
         }
        else if(qFacts.segment.contains('3'))
          {
           qFacts.skillDown = true;
           qFacts.barrierDown = true;
           qFacts.checkinFrequency = 'Monthly';
         }
        else if(qFacts.segment.contains('4'))
          {
           qFacts.skillUp = true;
           qFacts.barrierDown = true;
           qFacts.checkinFrequency = 'Monthly';
         }
        else 
          {
           
           skills = '';
           barrier = '';
           qFacts.checkinFrequency = '';
         }
         
    }
    public string getResponse( String qNo){
        try{
            //KD: Added list to remove direct object declaration
        	List<AssessmentResponses__c> alst = [select id,Question_Num__c, responses__c from AssessmentResponses__c where DomainScores__r.Contact__c  = :conId and Question_Num__c = :qNo];
            if(alst.size()>0){
                AssessmentResponses__c a = alst[0];
                return a.responses__c;
            }
            else{
                return '';
            }       
        }catch(Exception e){ return '';}
    }
    public string getLanguage(){
        string dataTmp = '';
        Blob bdy;
        try{
            string lng;
            string vStrTemp = null;
            //KD: Added list to remove direct object declaration
          	List<Attachment> attlst = [SELECT Id,Body FROM Attachment where ParentId = :vCaseId and Name = 'CommonAppIntro.txt' LIMIT 1];
            if(attlst.size()>0){
                Attachment Angdata = attlst[0];
                  bdy = Angdata.Body;
                  dataTmp = bdy.toString();     
                  List<String> vStr = dataTmp.split(',');       
                  for(string v : vStr)
                  {
                      if(v.contains('Q4'))
                          vStrTemp = v;
                      if(v.contains('Q5'))
                          lng = v;
                  }
            }
           
          if(vStrTemp == null)
          {
              return 'English';
          }
            else{
                    List<String> finalTemp = vStrTemp.split(':');                   
                    if(finalTemp[1].contains('Interpreter'))
                        {
                             List<String> language = lng.split(':');
                             return language[1];
                        }
                    else
                       {
                           return 'English';
                       }
                
            }
        }catch(Exception e){ return '';}
    }
    public string getMapValues(string tempKeyAns){
       if(tempKeyAns != null)
       {
        if(tempKeyAns.contains(conId))
                {
                    List<String> vStr = tempKeyAns.split('<>');
                    //system.debug('vStr:'+vStr);
                    string vStrTemp;
                    for(string v : vStr){
                        if(v.contains(conId))
                            vStrTemp = v;
                    }  
                    //system.debug('vstrTemp:'+vStrTemp);
                    
                    List<String> finalTemp = vStrTemp.split('\\|\\|');
                    //system.debug('finalTemp:'+finalTemp[1]);
                    return finalTemp[1];
                    
                }
                else
                {
                    return '';
                }
       }
        else{
            return '';
        }
            
    }
    public void getIncomeDetails(){
        
     string tempKeyAns1 = vFinalPDFMap.get('Income, Expenses and Assets~PayHours');
     string tempKeyAns2 = vFinalPDFMap.get('Income, Expenses and Assets~PayFrequency');  
     string tempKeyAns3 = vFinalPDFMap.get('Income, Expenses and Assets~Rate');
     string tempKeyAns4 = vFinalPDFMap.get('Income, Expenses and Assets~RateFrequency');
  
     system.debug('values map:'+getMapValues(tempKeyAns1));
     system.debug('values map:'+getMapValues(tempKeyAns2));
     system.debug('values map:'+getMapValues(tempKeyAns3));
     system.debug('values map:'+getMapValues(tempKeyAns4));
    if(tempKeyAns4 != null){   
     if( getMapValues(tempKeyAns4) != '' ){
        if( getMapValues(tempKeyAns4) == 'Salary' ){
            
             if( getMapValues(tempKeyAns2) == 'Weekly')
            {
                qFacts.income = String.valueOf(52 * Integer.valueOf(getMapValues(tempKeyAns3)));
            }
            else if( getMapValues(tempKeyAns2) == 'Every two weeks')
            {
                qFacts.income = String.valueOf(26 * Integer.valueOf(getMapValues(tempKeyAns3)));
            }
            else if( getMapValues(tempKeyAns2) == 'Monthly')
            {
                qFacts.income = String.valueOf(12 * Integer.valueOf(getMapValues(tempKeyAns3)));
            }
            else
            {
                qFacts.income = '';
            }
        }
        else {
         
            if( getMapValues(tempKeyAns2) == 'Weekly')
            {
                qFacts.income = String.valueOf(52 * Integer.valueOf(getMapValues(tempKeyAns1)) * Integer.valueOf(getMapValues(tempKeyAns3)));
            }
            else if( getMapValues(tempKeyAns2) == 'Every two weeks')
            {
                qFacts.income = String.valueOf(26 * Integer.valueOf(getMapValues(tempKeyAns1)) * Integer.valueOf(getMapValues(tempKeyAns3)));
            }
            else if( getMapValues(tempKeyAns2) == 'Monthly')
            {
                qFacts.income = String.valueOf(12 * Integer.valueOf(getMapValues(tempKeyAns1)) * Integer.valueOf(getMapValues(tempKeyAns3)));
            }
            else
            {
                qFacts.income = '';
            }
        }
     }
     else{
            qFacts.income = '';
        }
    }
    else{
            qFacts.income = '';
        }   
        if(qFacts.income != '')
        {
          qFacts.income = '$ '+ qFacts.income;
        }
        system.debug('income:'+qFacts.income);
    }
    public void getGradeDetails(){
        
     string tempKeyAns = vFinalPDFMap.get('Education~Grade');
           
            if(tempKeyAns!= null)
            {
                qFacts.educationLevel = getMapValues(tempkeyAns);
           }
           else
            {
                 qFacts.educationLevel = ''; 
            }
         System.debug('education level:' + qFacts.educationLevel);
    }
    public void extractPDFdata(){
        vCaseId = contactCase.id;
        vPrimConId = conId;
        currentCase = contactCase;
       
        
        string encodedContentsString,operation = '';
        string data = '';
        Blob b;
        
                
        vFinalPDFMap = new map<string, String>();
        List<Attachment> atttemp = new List<Attachment>();
        atttemp = [SELECT Id,Body FROM Attachment where ParentId = :vCaseId and Name = 'CommonAppAnswers.txt' LIMIT 1];
        if(atttemp.isEmpty())
        {
            //system.debug('newmap'+vFinalPDFMap);
            //return newmap;
        }
        else
        {
            Attachment Angdata = [SELECT Id,Body FROM Attachment where ParentId = :vCaseId and Name = 'CommonAppAnswers.txt' LIMIT 1];
            b = Angdata.Body;
            data = b.toString();
            //System.debug('data: '+data);
            if(data!='[{"Category":"Personal"}]')
            {
                list<MI_CA_PageName> ourdata = (list<MI_CA_PageName>) JSON.deserialize(data, list<MI_CA_PageName>.class);
               
                for(MI_CA_PageName ourdata1 :ourdata)
                {
                     
                    list<MI_CA_PageSections> pagesections = ourdata1.PageName;
                    for(MI_CA_PageSections pagesection :pagesections)
                    {
                       
                         list<MI_CA_Questions> pagesectionquestions = pagesection.PageSection;
                        for(MI_CA_Questions pagesectionquestion :pagesectionquestions)
                        {
                             
                             list<MI_CA_SubQuestions> pagesectionsubquestions = pagesectionquestion.Question;
                            for(MI_CA_SubQuestions pagesectionsubquestion :pagesectionsubquestions){
                                if(pagesectionsubquestion.parentquestion.PDFName__c != '' && pagesectionsubquestion.parentquestion.PDFName__c != null)
                                {
                                    vFinalPDFMap.put(pagesectionsubquestion.parentquestion.PageSection__c+'~'+pagesectionsubquestion.parentquestion.PDFName__c, pagesectionsubquestion.parentquestion.Answers__c);
                                }   
                             list<MI_CA_SubSubQuestions> pagesectionsubsubquestions = pagesectionsubquestion.SubQuestions;  
                                  for(MI_CA_SubSubQuestions pagesectionsubsubquestion :pagesectionsubsubquestions){
                                      if(pagesectionsubsubquestion.childquestion.PDFName__c != '' && pagesectionsubsubquestion.childquestion.PDFName__c != null)
                                        {
                                            vFinalPDFMap.put(pagesectionsubsubquestion.childquestion.PageSection__c+'~'+pagesectionsubsubquestion.childquestion.PDFName__c, pagesectionsubsubquestion.childquestion.Answers__c);
                                        }
                                        List<Question__c> subsublists = pagesectionsubsubquestion.SubSubQuestions;
                                      
                                      for(Question__c subsublist : subsublists){
                                          if(subsublist.PDFName__c != '' && subsublist.PDFName__c != null)
                                          {
                                              vFinalPDFMap.put(subsublist.PageSection__c+'~'+subsublist.PDFName__c, subsublist.Answers__c);
                                          }
                                      }
                                  }
                            }
                        }
                    }
                }
            }
           // for(String s : vFinalPDFMap.keyset()){
           //     system.debug('s -- ' + s);
           //}
        }
    }
    public void getNeedsDetails(){
        
        List<AssessmentScores__c> scoresList = [SELECT id, Score__c,Domain__c,Domain__r.Name__c,Score__r.score__c
                                                FROM AssessmentScores__c
                                                WHERE Contact__c  = :conId];
                
        if( !scoresList.isEmpty() ){
            
            for(AssessmentScores__c s : scoresList)
            {
                //system.debug('score with domain and score :'+s.Domain__r.Name__c+' '+s.Score__r.score__c);
                if( s.Score__r.score__c != null ){
                    
                        if ( s.Score__r.score__c.contains('1') )
                        {
                            crisisList.add(s.Domain__r.Name__c);
                        }
                        else if( s.Score__r.score__c.contains('2') )
                        {
                            vulnerableList.add(s.Domain__r.Name__c);
                        }
                        else if( s.Score__r.score__c.contains('3') )
                        {
                            riskList.add(s.Domain__r.Name__c);
                        }
                        else if( s.Score__r.score__c.contains('4') )
                        {
                            safeList.add(s.Domain__r.Name__c);
                        }
                        else
                        {
                            sufficientList.add(s.Domain__r.Name__c);
                        }
                   }
                else {
                    notAssessedList.add(s.Domain__r.Name__c);
                }
            }
         
        }
        System.debug('crisis:'+crisisList);
        System.debug('not assessed:'+notAssessedList);
     
    }
    public void getHouseholdDetails(){
        
        List<CaseContactRole> ContactDependents = [SELECT Id, CasesId, ContactId, Role, Contact.Name, Contact.Age__c 
                                                   FROM CaseContactRole 
                                                   WHERE CasesId = :contactCase.id];
        
        qFacts.houseHold = ContactDependents.size(); 
        qFacts.dependents = 0;
        for(CaseContactRole c : ContactDependents ){
            
            if(c.Role == 'Child' || c.Role == 'Step Child' || c.Role == 'Foster Child' || c.Role == 'Niece/Nephew'){
                qFacts.dependents++;
            }
        }
                    
    }
    public void getGoalDetails(){
        
         
         AggregateResult[] groupedResults = [Select SUM(CountMilestones__c)a,SUM(CountMilestonesComp__c)b 
                                             from sp_Goal__C 
                                             where case__C = :contactCase.Id];

         if(Integer.valueOf(groupedResults[0].get('a')) != null)
         { 
             qFacts.goals = Integer.valueOf(groupedResults[0].get('a'));
         }
         if(Integer.valueOf(groupedResults[0].get('b')) != null)
         {
             qFacts.goalsCompleted = Integer.valueOf(groupedResults[0].get('b'));
         }
         if(qFacts.goals != null && qFacts.goalsCompleted != null )
         {
             qFacts.goalsPending = Integer.ValueOf(qFacts.goals) - Integer.valueOf(qFacts.goalsCompleted);
         }
        
        
    }
    public class Wrapper_quickFacts {
        public String successCoach {get;set;}
        public String reAssessment {get;set;}
        public String phone {get;set;}
        public String email {get;set;}
        public String educationLevel {get;set;}
        public String segment {get;set;}
        public String empStatus {get;set;}
        public String language {get;set;}
        public String living {get;set;}
        public String maritalStatus {get;set;}
        public String checkinFrequency {get;set;}
        public String income {get;set;}
        public Boolean skillUp {get;set;}
        public Boolean barrierUp {get;set;}
        public Boolean skillDown {get;set;}
        public Boolean barrierDown {get;set;}
        public Integer household {get;set;}
        public Integer dependents {get;set;}
        public Integer goals {get;set;}
        public Integer goalsCompleted {get;set;}
        public Integer goalsPending {get;set;}
        public boolean caselocked {get;set;}
        public String caselockedby {get;set;}
        
        public Wrapper_quickFacts(){
            
            goals = 0;
            goalsCompleted = 0;
            goalsPending = 0;
            skillUp = skillDown = barrierUp = barrierDown = false;
        }
           
    }  
        
        
         
}