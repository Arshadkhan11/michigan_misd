@isTest
public class Testsample {

@IsTest(SeeAllData=true)
public static void testMI_NeedsAssessment(){
    	Account acc = [select id from Account where name = 'MIISD']; 
       Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id, Title='Master',BirthDate=date.today());
        insert con;
       Case cs = new Case(Status= 'New', ContactId = con.id, Subject = 'TestCase');
        insert cs;
        con.LatestCaseID__c = cs.id;
        update con;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile'];
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            	CitizenSuccessPlanCtrl abc = new CitizenSuccessPlanCtrl();
            	abc.SP_CaseId = '50035000000cbld';
            	abc.displayServices();
        }
}
}