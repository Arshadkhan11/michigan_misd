public class nextStepsClass {
    
    //Appointment Variables
    public MI_Activity__c appointmentEvent {get; set; }
    public DateTime evStartDate {get; set;}
    public DateTime evEndDate {get; set;}
    public Date evDate {get;set;}
    public Time startTime {get;set;}
    public Time endTime {get;set;}
    public String subject {get;set;}
    public String segment {get;set;}
    public String serviceMsg {get;set;}
    public String dateString {get;set;}
    public String timeString {get;set;}
    
    public Id caseId {get;set;}
    public list<SP_Transactional__c> serviceList {get;set;}
    
    public nextStepsClass(Id conId){
        
        
        if(conId != null)
        {
           caseId = [SELECT id FROM Case WHERE contactId = : conId LIMIT 1].id;
           system.debug('@Bhb - ' + caseId);
           segment = MI_FamilyManagement.FMSingleInit(caseId);
           
          if( !segment.contains('TBD') )
           {    
          
            Integer d = getDay();
            system.debug('d value:'+d);
            if( d != 0)
            {
               evDate = Date.today();
               evDate = evDate.addDays(d);
               dateString = evDate.format();
            }
          } 
            
          serviceList = [SELECT id,Services_Type__c,Services_Type_Description__c, goal__r.domain__c
                       FROM sp_transactional__c 
                       WHERE goal__r.case__c = :caseId 
                       AND type__c IN('Service','Resource')
                       AND Status__c = true
                       AND Parent_Milestone__r.Selected_Step__c = true
                       ];
        if( serviceList.isEmpty() )
        {
          serviceMsg = 'No Services Selected';
        }
        else
        {
            serviceMsg = null;
            system.debug('service list:'+serviceList);
        }
      }
        
    }
    public void getServiceList(){
        
        
         if( ! serviceList.isEmpty() )
        {
           serviceList.clear();
        }
        serviceList = [SELECT id,Services_Type__c,Services_Type_Description__c, goal__r.domain__c
                       FROM sp_transactional__c 
                       WHERE goal__r.case__c = :caseId 
                       AND type__c IN('Service','Resource')
                       AND Status__c = true
                       AND Parent_Milestone__r.Selected_Step__c = true
                       ];
        if( serviceList.isEmpty() )
        {
          serviceMsg = 'No Services Selected';
        }
        else{
            serviceMsg = null;
            system.debug('service list:'+serviceList);
        }
        system.debug('service list:'+serviceList);
    }
    
    public Integer getDay(){
         
         system.debug('qFacts.segment:'+segment);
         
         if(segment.contains('1'))
         {
           return 7;
         }
        else if(segment.contains('2'))
          {
           return 14;
         }
        else if(segment.contains('3'))
          {
           return 30;
         }
        else if(segment.contains('4'))
          {
           return 30;
         }
        else 
          {
           return 0;
          
         }
         
    }
    public boolean save() { 
  
        //Creating Appointment
        
        if(dateString == null  || timeString == null ||  subject == null || subject == '' || dateString == '' || timeString == '')
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'Please fill the required check-in details.'));
            return null;
        }
        
        
        
        evStartDate = DateTime.parse(dateString+' '+timeString);
        
        evDate = evStartDate.Date();
        startTime = evStartDate.Time();
        endTime = startTime.addHours(1);
        
        evEndDate = DateTime.newInstance(evDate,endTime);
        appointmentEvent = new MI_Activity__c();
        appointmentEvent.subject__c = subject;
        
        Id EvRecTypeId = [select id from RecordType where DeveloperName='Event' and sObjectType='MI_Activity__c'].Id;
       
        if(evStartDate < system.now()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'Check-in start date/time cannot be in the past'));
            return null;
        }
      
           
        
        appointmentEvent.RecordTypeId = EvRecTypeId ;
        appointmentEvent.start_date__c= evStartDate;
        appointmentEvent.End_date__c = evEndDate;
        insert appointmentEvent;
        
        subject = null;
        evDate = null;
        startTime = null;
        dateString = timeString = null;
        //Create Interaction Log
        Interaction_Log__c intrct       = new Interaction_Log__c();
        intrct.Interaction_Date__c      = evDate;
        intrct.LoggedBy__c              = UserInfo.getUserId();
        intrct.Case__c                  = caseId;
        intrct.Interaction_Type__c      = 'Checkin'; 
        intrct.Interaction_Subject__c   = 'Checkin';
        insert intrct;
        
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Info, 'Appointment is scheduled.'));
          
        return null;
      
    }    
}