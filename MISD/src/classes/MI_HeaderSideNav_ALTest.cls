@isTest
public class MI_HeaderSideNav_ALTest {
    
    @isTest(SeeAllData=true)
    public static void test1(){
        Account acc = [select id from Account where name = 'MIISD'];  
       
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        
        Case cs                  = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
                          Notifications_Email__c = true,
                          Notifications_Text__c = true,
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id);
        insert u;
        Notification__c n1 = new Notification__c(Notification_Description__c='abc',Notification_Date_Time__c=Date.today(),
                                                Source__c='ad', Unread__c=true); 
        insert n1;
        Notification__c n2 = new Notification__c(Notification_Description__c='abc1',Notification_Date_Time__c=Date.today(),
                                                Source__c='Required Document',CaseId__c = cs.id, Unread__c=false); 
        insert n2;
                           
        system.runAs(u){
            
            MI_HeaderSideNav_AL hs = new MI_HeaderSideNav_AL();
            hs.NotifEditId = n1.id;
            hs.MarkRead();
            hs.Drilldown();
            hs.NotifEditId = n2.id;
           
            hs.Drilldown();
            
        }
    }

}