global without sharing class MI_Citizen_RegistrationClass {

    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String UserId{get;set;}
    public User NewUser {get; set;}
    public Boolean isPartner {get; set;}
    public Boolean isCoach {get; set;}
    public List<User> userList {get; set;}
    public Profile P {get; set; }
    //Declaring for Case, Relation
    Public String CaseNo {get;set;}
    Public String AssessmentConId {get;set;}
    Public Contact ContactDetails {get;set;}
    Public Case Assessment {get;set;}
    Public String Role {get;set;}
    Public Relationship__c Relationship {get;set;}
   	public String param_value {get;set;}
    
    public MI_Citizen_RegistrationClass(){
       system.debug('Inside constructor');
       userList = [select id,email,username from user where id <> null];
       isPartner = false;
       isCoach = false;
       NewUser = new User();
       param_value = '';
       param_value =  System.currentPageReference().getParameters().get('name');
      
       if(param_value == 'partner')
        {
             p = [select id from profile where name='Partner Profile'];
             isPartner = true;
        }
        else if(param_value == 'coach')
        {
             p = [select id from profile where name='Success Coach Profile'];
             isCoach = true;
        }
        else
             p = [select id from profile where name='Citizen Profile'];
        
       
    }
    
   
    public pagereference Register(){
        
       
        String id = System.currentPageReference().getParameters().get('id');
        
        NewUser.ProfileId = p.Id;
        if(id != null || id!= '')
        NewUser.Agency_ID__c = id;
    
        system.debug('new user :'+ NewUser);
        NewUser.CommunityNickname = NewUser.Username;
        system.debug('New user val:'+NewUser.Username);
        //String accountId = '0016100000XM4hQ';
        String accountId = ''; 
        List<Account> acclst = [Select Id from Account where Name='MIISD'];
        if(acclst.isEmpty()){
            Account acc = new Account();
            acc.Name = 'MIISD';
            insert acc;
            accountId = acc.id;
        }
        else{
            accountId = acclst[0].Id;
        }
       

        try {
            
            userId = Site.createExternalUser(NewUser, accountId, password);
            system.debug('user created id val'+userId);
            if(isCoach)
            {
            Pagereference p = Page.SuccessCoachRegisterPage; 
            p.setredirect(true);
            return p;
            }
            else if(isPartner)
            {
                
                Pagereference p = Page.successRegisterPage; 
                p.getParameters().put('name','Community Partner');
                p.setredirect(true);
                return p;
            }
            else
            {
                //Code for Case , Relations
                system.debug('@Arshad check owner id'+NewUser.Id+'  '+ userId);
                Assessment = new Case();
                Assessment.Subject = NewUser.Contact.Name;
                String owner = NewUser.Id;
                //owner = owner.substring(0, 15);
                system.debug('@Arshad check owner id'+owner);
                Assessment.Ownerid = userId;
                Assessment.ContactId = NewUser.ContactId;
                Assessment.Origin = 'Email';
                Assessment.Priority = 'Medium';
                
                insert Assessment;
                CaseNo = Assessment.Id;
                CaseContactRole CaseRole = new CaseContactRole();
                CaseRole.CasesId = CaseNo;
                CaseRole.ContactId = NewUser.ContactId;
                CaseRole.Role = 'Primary Contact';
                insert CaseRole;
                
                Role = CaseRole.Role;
                Id CaseContactPrimaryId = CaseRole.ContactId;
                
                Relationship = new Relationship__c();
                Relationship.Case_Details__c = CaseNo;
                Relationship.Primary_Contact__c = CaseContactPrimaryId;
                Relationship.Secondary_Contact__c = CaseContactPrimaryId;
                Relationship.Relationship_Name__c = 'You';
                try{
                    insert Relationship; 
                }
                catch (DmlException e){
                    
                }
                
                //Till here
                Pagereference p = Page.successRegisterPage; 
                p.getParameters().put('name','Citizen');
                p.setredirect(true);
                return p;
                
            }
            
            
        } catch(Site.ExternalUserCreateException ex) {
            
            System.debug(ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, ex.getMessage()));
        }
        
      
        return null;
    }
    
    
    
        
    
}