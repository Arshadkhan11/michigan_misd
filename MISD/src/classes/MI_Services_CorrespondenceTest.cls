@isTest

public with sharing class MI_Services_CorrespondenceTest {

 public static testMethod void MI_Services_CorrespondenceTest(){
 
 
 
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
        
        
        
        
        PageReference pageRef = Page.MI_MyServices;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('citId',con.id);
        
        MI_CommonAppService__c cas = new MI_CommonAppService__c(Contact__c=con.id);
        
        list<Document> docs = new list<document>();
        Document doc1 = new Document(folderid='00lQ0000000EOuC',name='Correspondence_1.pdf');
        docs.add(doc1);
        Document doc2 = new Document(folderid='00lQ0000000EOuC',name='Correspondence_2.pdf');
        docs.add(doc2);
        Document doc3 = new Document(folderid='00lQ0000000EOuC',name='Correspondence_3.pdf');
        docs.add(doc3);
        insert docs;
        
        MI_Services_Correspondence  ctrl=new MI_Services_Correspondence();
        ctrl.getCorrespondence1();
        ctrl.getCorrespondence2();
        ctrl.getCorrespondence3();
 
 
 
 
 }
 }