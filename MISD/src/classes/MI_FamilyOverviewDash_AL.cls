global with sharing class MI_FamilyOverviewDash_AL {

// FAMILY MANAGEMENT DASH VAR - START --------------------------------------------------->

    // map -> 102
    public Map<String, String> inputFields { get; set; }
    public Map<Id, Integer> mPendingTasksPerCase {get;set;}
    public Map<Id, Integer> mAllTasksPerCase {get;set;}
    public Map<Id, Integer> mCompletedTasksPerCase {get;set;}
    public List<Integer> ListforMapTasksAll {get;set;}
    public List<Integer> ListforMapTasksCompleted {get;set;}    
    
  // goals widget - start
    public Integer GoalsTotal {get;set;}
    public String selectedSort { get; set; }
    public list<Wrapper_Goals> GoalsList {get;set;}
    public list<SP_Transactional__c> TransList {get;set;}
    public list<Success_Plan_Master__c> MasterList {get;set;}
    public Date StartDate {get;set;}
    public Date DueDate {get;set;}
    public Integer DurationDays {get;set;}
  // goals widget - end
    
    // search
    public String SearchString {get;set;}
    public boolean SearchFlag {get;set;}
    public List<String> ContactTypeAhead {get;set;}
    
  // client default
    public List<Case> FamilyManCases {get;set;}
    public Integer FamilyManCasesCount {get;set;}
    public String ClientNameListConcat {get;set;}
    public Integer TasksCompleted {get;set;}
    public Integer TasksTotal {get;set;}

    // Referrals
    public List<Referral__c> FamilyManRef {get;set;} 
    public Integer FamilyManRefCount {get;set;}
    
    // tasks applet:
    public String TasksSortBy {get;set;}
    public List<MI_Activity__c> TasksList {get;set;}
    public List<MI_Activity__c> AllTasksList {get;set;}
  public Id RecTypeId{get;set;}
/*    public List<MI_Activity__c> taskListUserLogin {get;set;}
    public MI_Activity__c ALnewtask {get;set;}
    public MI_Activity__c EditTask {get;set;}
    public Date newDate {get;set;}
    public Time newTime {get;set;}
    public Date EditDate {get;set;}
    public Time EditTime {get;set;} 
    public Id EditId {get;set;}
    public String TaskSubject {get;set;}
    public String TaskPriority {get;set;}
    public String TaskStatus {get;set;}
    public String TaskType {get;set;}
    public string TaskNotes {get;set;}
    public String TaskClient {get;set;}    */
    
    
    
// FAMILY MANAGEMENT DASH VAR - END --------------------------------------------------->

// FAMILY MANAGEMENT DASH CONTROLLER - START --------------------------------------------------->    
    public MI_FamilyOverviewDash_AL()
    {
        try
        {
            // goals widget - start
               StartDate = Date.today();
               GoalsList = new List<Wrapper_Goals>();
               
               
               TransList = new list<SP_Transactional__c>();
               DurationDays = 0;
              
         
              
               Translist = [Select id, Goal__r.Case__r.Contact.Name, Goal__r.Case__r.Primary_Contact_Name__c, Parent__c, Name__c,Start_Date__c 
                            from SP_Transactional__c 
                            where 
                            type__c='Milestone' and Goal__r.Case__r.SuccessCoach__c  = :UserInfo.getUserId()
                            and Milestone_Status__c != 'Completed' and Status__c = true];           
               //system.debug('trans:'+TransList); 
               
               try{
                 MasterList = [Select Duration__c, Parent__r.value__c, value__c, Sequence__c 
                                 from Success_Plan_Master__c
                                    where value__c!=null];
                 //system.debug('MasterList:'+MasterList); 
                }Catch(Exception e)
                 {
                 }
               for(SP_Transactional__c t : Translist )
               {
                  
                   StartDate = t.Start_Date__c;
                   
                   for(Success_Plan_Master__c m : MasterList )
                    {
                          if(m.value__c == t.Name__c  && m.Parent__r.value__c == t.Parent__c)
                           {
                             DurationDays = (Integer) m.Duration__c;
                             //system.debug('Start Date:'+StartDate);
                             //system.debug('Start Date:'+t.Goal__r.Case__r.Contact.Name); 
                             //system.debug('days:'+DurationDays); 
                             DueDate = StartDate.addDays(DurationDays);
                             //system.debug('duedate:'+DueDate); 
                            
                             GoalsList.add(new Wrapper_Goals(t,DueDate));
                           }
                    }
               }
               //system.debug('goalslist:'+GoalsList); 
            GoalsTotal=0;
            if(GoalsList.size()>0)
            {
                GoalsTotal = GoalsList.size();
            }
            
            // goals widget - end        
            
            // search init -> before default and referral query
            SearchFlag = false;
            ClientNameListConcat=null;
    
            // Clients default applet
            FamilyManCaseslist();
            
            // Referrals applet
            FamilyManRefFunc();
    
            // search init -> after default and referral query
            
    
            // tasks applet
            TasksSortBy = 'Due Date';
            RecTypeId = [select Id from RecordType where Name='Task'].Id;
            querytaskListUserLogin();
            TasksCompleted = TasksList.size();
            TasksTotal = 0;
            if(TasksList.size()>0)
            {
                TasksTotal = TasksList.size();
            }
            
                
            for(Case cc: FamilyManCases)
            {
                ClientNameListConcat = ClientNameListConcat + ';' + cc.Primary_Contact_Name__c;
            }
            //System.Debug('ato - clientnamelist -'+ClientNameListConcat);
            String[] ClientNameListConcatSplit = (ClientNameListConcat).split(';');
            
            
            // maps 102 ---------------------> ATO
            AllTasksList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                              where 
                                              (
                                              Owner.Id=:UserInfo.getUserId() AND
                                              //Activity_Date__c=Today AND
                                              //Status__c!='Completed' AND
                                              RecordTypeId =: RecTypeId AND
                                              Client_Name__c != null AND
                                              Client_Name__c IN :ClientNameListConcatSplit AND
                                              Activity_Date__c!=null    
                                              )
                                              ORDER BY Client_Name__c ASC];        
            Map<Id, Integer> mPendingTasksPerCase = new Map<Id, Integer>();
            Map<Id, Integer> mAllTasksPerCase = new Map<Id, Integer>();
            Map<Id, Integer> mCompletedTasksPerCase = new Map<Id, Integer>();
            for(Case cc1: FamilyManCases)
            {
                //System.debug('ato - case contact name'+cc1.Case.Primary_Contact_Name__c);
                //System.debug('ato - contact name'+cc1.Primary_Contact_Name__c);
                //System.debug('ato - contact Id'+cc1.ContactId);
                //System.debug('ato - contact LName'+cc1.Contact.LastName);
                
                Integer mPenTC=0;
                Integer mAllTC=0;
                Integer mCompTC=0;
                for (MI_Activity__c tt1:AllTasksList)
                {
                    if(tt1.Client_Name__c==cc1.Primary_Contact_Name__c)
                    {
                        if(tt1.Status__c=='Completed')
                        {
                            mCompTC++;
                        }
                        else
                        {
                            mPenTC++;                        
                        }
                        mAllTC++;
                    }
                }
                mAllTasksPerCase.put(cc1.Id,mAllTC);
                mPendingTasksPerCase.put(cc1.Id,mPenTC);
                mCompletedTasksPerCase.put(cc1.Id,mCompTC);
            }
            //System.debug('ato MAP of AllTasksperCase'+mAllTasksPerCase);
            //System.debug('ato MAP of PendingTasksperCase'+mPendingTasksPerCase);
            //System.debug('ato MAP of CompletedTasksperCase'+mCompletedTasksPerCase);
            ListforMapTasksAll=mAllTasksPerCase.values();
            ListforMapTasksCompleted=mCompletedTasksPerCase.values();
            //System.debug('ato -- ListforMapTasksAll'+ListforMapTasksAll+ListforMapTasksAll[0]+ListforMapTasksAll[1]); 
            //System.debug('ato -- ListforMapTasksCompleted'+ListforMapTasksCompleted); 
            
            //mapprint();       
                
        }
        catch(exception e)
        {
            //System.debug('ato const debug error 3'+e.getMessage());
            
        }

    }
// FAMILY MANAGEMENT DASH CONTROLLER - END --------------------------------------------------->

    
// FAMILY MANAGEMENT DASH functions - START --------------------------------------------------->

/*    public void mapprint()
    {
        System.debug('ato MAP of AllTasksperCase'+mAllTasksPerCase);
        System.debug('ato MAP of PendingTasksperCase'+mPendingTasksPerCase);
        System.debug('ato MAP of CompletedTasksperCase'+mCompletedTasksPerCase);

    }*/
    
    // goals widget - start
     public void SortList() {
     
               
                Wrapper_Goals temp = new  Wrapper_Goals();    
                Integer n = 10;
                
                 
                 if(selectedSort == 'Family')
                  {
                      for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].TS.Goal__r.Case__r.Primary_Contact_Name__c>= GoalsList[j+1].TS.Goal__r.Case__r.Primary_Contact_Name__c )
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }
                  }
                 else if(selectedSort == 'Due Date')
                  {
                        for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].DueDate <= GoalsList[j+1].DueDate)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }
                  }
                   else if(selectedSort == 'Goal')
                  {
                        for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].TS.Name__c >= GoalsList[j+1].TS.Name__c)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }
                  }
                   
       
    }

    public class Wrapper_Goals {
        
        public SP_Transactional__c TS {get;set;}
        public Date DueDate {get;set;}
        
        public Wrapper_Goals(SP_Transactional__c T,Date D){
            this.TS = T;
            this.DueDate = D;
            
        }  
        
        public Wrapper_Goals(){}
         
    }

  // goals widget - end
    
    
    public PageReference SearchClients()
    { 
        try{
        if(SearchString.length()>0)
        { 
            //System.debug('ato->NOT NULL search'+SearchString+SearchString.length());
            
            SearchFlag = true;
            FamilyManCaseslist();
            FamilyManRefFunc(); 
        } 
        else
        {
            //System.debug('ato->NULL search'+SearchString);
            
            SearchFlag = false;
            FamilyManCaseslist();
            FamilyManRefFunc();
        }
        }
        catch(exception e)
        {
            //System.debug('ato const debug error 1'+e.getMessage());
        }
        
        return null;
    }
    
  public void FamilyManRefFunc()
    {
        if(SearchFlag)
        {
        FamilyManRef = [Select Id, Name, Primary_Contact__c, Referral_Date__c, Referral_Source__c 
                        from Referral__c 
                          where ((Primary_Contact__c LIKE :('%'+string.escapeSingleQuotes(SearchString)+'%')) OR 
                          (Primary_Contact__c LIKE :('%'+string.escapeSingleQuotes(SearchString))) OR
                          (Primary_Contact__c LIKE :(string.escapeSingleQuotes(SearchString)+'%'))
                          )
                          ORDER BY Referral_Date__c DESC];
        }
        else
        {
        FamilyManRef = [Select Id, Name, Primary_Contact__c, Referral_Date__c, Referral_Source__c 
                        from Referral__c 
                        where Name !=null
                        ORDER BY Referral_Date__c DESC];
        }
        //System.debug('atoatoatoreferralsize'+FamilyManRef.size());
        FamilyManRefCount = FamilyManRef.size();
        //return FamilyManRef;
    }

    
    public void FamilyManCasesList() 
    {
        if(SearchFlag)
        {
        FamilyManCases = [Select Id, CaseNumber, Primary_Contact_Image__c, Contact.Name, Primary_Contact_Name__c, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c, LastUpdatedGoalCase__c from Case 
                          where (ContactId!=null) AND
                          Primary_Contact_Name__c!=null AND
                          ((Primary_Contact_Name__c LIKE :('%'+string.escapeSingleQuotes(SearchString)+'%')) OR 
                          (Primary_Contact_Name__c LIKE :('%'+string.escapeSingleQuotes(SearchString))) OR
                          (Primary_Contact_Name__c LIKE :(string.escapeSingleQuotes(SearchString)+'%'))
                          ) AND
                          SuccessCoach__c=:UserInfo.getUserId()
                          ORDER BY LastUpdatedGoalCase__c DESC];        
        }
        else
        {  
        FamilyManCases = [Select Id, CaseNumber, Primary_Contact_Image__c, Contact.Name, Primary_Contact_Name__c, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c, LastUpdatedGoalCase__c from Case 
                          where (ContactId!=null) AND
                          Primary_Contact_Name__c!=null AND
                          SuccessCoach__c=:UserInfo.getUserId()
                          ORDER BY LastUpdatedGoalCase__c DESC];        
        }

/*
        FamilyManCases = [Select Id, CaseNumber, Contact.Name, Primary_Contact_Name__c, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c from Case 
                          where (ContactId!=null) AND
                          Primary_Contact_Name__c!=null AND
                          SuccessCoach__c=:UserInfo.getUserId()
                          ORDER BY CreatedDate DESC LIMIT 10];        
        FamilyManCases = [Select Id, CaseNumber, ContactId, Contact.LastName, Contact.Name, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c from Case 
                          where 
                          (ContactId!='') AND
                          SuccessCoach__c=:UserInfo.getUserId()
                          ORDER BY CreatedDate DESC LIMIT 10];
*/
    FamilyManCasesCount = FamilyManCases.size();
        //System.Debug('atoatoclientdefaultsize'+FamilyManCasesCount);
        //return FamilyManCases;
    }


    public void querytaskListUserLogin()
    {
        for(Case cc: FamilyManCases)
        {
            ClientNameListConcat = ClientNameListConcat + ';' + cc.Primary_Contact_Name__c;
        }
        
        
        if(FamilyManCases.isEmpty())
        {
            ClientNameListConcat = '                    ;';
        }
        //System.Debug('ato - clientnamelist -'+ClientNameListConcat);
    	String[] ClientNameListConcatSplit = (ClientNameListConcat).split(';');

        if(TasksSortBy=='Family')
        { 
        TasksList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (
                                          Owner.Id=:UserInfo.getUserId() AND
                                          //Activity_Date__c=Today AND
                                          Status__c!='Completed' AND
                                          RecordTypeId =: RecTypeId AND
                                          Client_Name__c != null AND
                                          Client_Name__c IN :ClientNameListConcatSplit AND
                                          Activity_Date__c!=null    
                                          )
                                          ORDER BY Client_Name__c ASC];
            
        }
        else if(TasksSortBy=='Due Date')
        {
        TasksList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (
                                          Owner.Id=:UserInfo.getUserId() AND
                                          //Activity_Date__c=Today AND
                                          Status__c!='Completed' AND
                                          RecordTypeId =: RecTypeId AND
                                          Client_Name__c != null AND
                                          Client_Name__c IN :ClientNameListConcatSplit AND    
                                          Activity_Date__c!=null    
                                          )
                                          ORDER BY Activity_Date__c DESC];
        }
        else if(TasksSortBy=='Task')
        {
        TasksList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (
                                          Owner.Id=:UserInfo.getUserId() AND
                                          //Activity_Date__c=Today AND
                                          Status__c!='Completed' AND
                                          RecordTypeId =: RecTypeId AND
                                          Client_Name__c != null AND
                                          Client_Name__c IN :ClientNameListConcatSplit AND    
                                          Activity_Date__c!=null    
                                          )
                                          ORDER BY Subject__c ASC];
        }
        
    }    

    
}