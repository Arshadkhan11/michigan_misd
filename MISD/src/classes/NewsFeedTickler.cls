public class NewsFeedTickler {

// sampleCon - init - START
    public List<String> selectedCategory {get;set;}
    public List<News__c> UserList {get;set;}
    public date selectedDate {get;set;}
    public String selectedKeyword {get; set;}
    public boolean flag {get;set;}
    public List<SelectOption> categoryList {get;set;}
// sampleCon - init - END    

// query variables    
    public List<News__c> newsFeedItemslist {get;set;}
    public List<News__c> newsFeedItemslistCitizen {get;set;}
    public List<News__c> newsFeedItemslistCoach {get;set;}
    
    
// checkbox variables    
   public Boolean re1{get;set;}
   public Boolean re2{get;set;}
   public Boolean re3{get;set;}
   public Boolean re4{get;set;}
    
    public String UserCatPref1{get;set;}
    
    public User currentUser {get;set;}
    
    public NewsFeedTickler()
    {
     selectedCategory = new List<string>();
                  flag = false;
                  getUserlist(); 
                  categoryList = new List<SelectOption>();
                  CategoryList.add(new SelectOption('Job Related','Job Related'));
                  CategoryList.add(new SelectOption('Family Events','Family Events'));
                  CategoryList.add(new SelectOption('Educational','Educational'));
                  CategoryList.add(new SelectOption('Medical','Medical'));
        
       re1 = true;
       re2 = true;
       re3 = true;
       re4 = true;

                
        List<User> UserCatPref1 = [SELECT Id, Name, News_Category_Filter__c FROM User where username='aoommen@isd.com.isddev' LIMIT 100];        
        
    newsFeedItemslist = queryNewsFeedList();
    newsFeedItemslistCitizen = queryNewsFeedListCitizen(); 
    newsFeedItemslistCoach = queryNewsFeedListCoach();
    }


// Start -> news feed on WORKING dashboard
    public List<News__c> queryNewsFeedList()
    {
        List<String> DynCat = new List<String>{'','','',''};
        if(re1=true)
        {
            DynCat[0]='Job Related';
        }
        if(re2=true)
        {
            DynCat[1]='Family Events';
        }
        if(re3=true)
        {
            DynCat[2]='Educational';
        }
        if(re4=true)
        {
            DynCat[3]='Medical';
        }
//              List<String> DynCat = new List<String> {'Job Related','Family Events','Educational','Medical'};
        List<News__c> feedItemslist = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c from News__c WHERE ((Publish_Through__c > TODAY) AND (Publish_from__c <= TODAY) AND (Intended_audience__c IN ('All Audiences','Success Coaches')) AND (Category__c in :DynCat)) ORDER BY Publish_from__c DESC];
//        List<News__c> feedItemslist = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c from News__c WHERE ((Publish_Through__c > TODAY) AND (Publish_from__c <= TODAY) AND (Intended_audience__c IN ('All','Coaches - Registered','All Registered users'))) ORDER BY Publish_from__c DESC];
    return feedItemslist;
    }
// End -< news feed on WORKING dashboard  

// Start -> news feed on Citizen dashboard
    public List<News__c> queryNewsFeedListCitizen()
    {
        List<News__c> feedItemslistCitizen = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c from News__c WHERE ((Publish_Through__c > TODAY) AND (Publish_from__c <= TODAY) AND (Intended_audience__c IN ('All','Citizens - Registered','All Registered users'))) ORDER BY Publish_from__c DESC];

    return feedItemslistCitizen;
    }
// End -< news feed on Citizen dashboard

// Start -> news feed on COACH - FINAL
    public List<News__c> queryNewsFeedListCoach()
    {
        Boolean Check = false;//checks whether All has been selected
        String AllVar = 'All';//checks for All, other values in multi-select
        User UNC = [SELECT Id, News_Category_Filter__c, News_Educational__c, News_FamilyEvents__c, News_JobRelated__c, News_Medical__c
                    FROM User where username=:UserInfo.getUsername()
                    LIMIT 1];
        String PrefConcat1;
        String PrefConcat2;
        String PrefConcat3;
        String PrefConcat4;
        
        if(UNC.News_Educational__c==true)
        {
            PrefConcat1 = 'Educational;';
        }
        else
        {
           PrefConcat1 = 'blank;';
        }
        
        if(UNC.News_FamilyEvents__c==true)
        {
            PrefConcat2 = 'Family Events;';
        }
        else
        {
           PrefConcat2 = 'blank;';
        }

        if(UNC.News_JobRelated__c==true)
        {
           PrefConcat3 = 'Job Related;';
        }
        else
        {
           PrefConcat3 = 'blank;';
        }

        if(UNC.News_Medical__c==true)
        {
           PrefConcat4 = 'Medical;';
        }
        else
        {
           PrefConcat4 = 'blank;';
        }
        
        String PrefConcat = PrefConcat1+PrefConcat2+PrefConcat3+PrefConcat4;
        
        String[] UserCatPref = (PrefConcat).split(';');     
//            String[] UserCatPref = ([SELECT News_Category_Filter__c FROM User where username=:UserInfo.getUsername() LIMIT 10].News_Category_Filter__c).split(';');     
                        // Check for All in user's preference            
            // No check for Category if "All" selected    


        List<News__c> feedItemslistCoach = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c, NewsAge__c from News__c 
                                            WHERE ((Publish_Through__c >= TODAY) 
                                                   AND (Publish_from__c <= TODAY) 
                                                   AND (Intended_audience__c INCLUDES ('All Audiences','Success Coaches')) 
                                                   AND (Category__c IN :UserCatPref)) 
                                            ORDER BY Publish_from__c DESC];
        return feedItemslistCoach;
        
    }
// End -> news feed on COACH - FINAL

    
    // START - sampleCon
  public PageReference Search() {
  
       Date checkDate = Date.Today().addDays(-7);
         
         //if( (selectedDate >= checkDate && selectedDate <= Date.Today()) || selectedKeyword != null || selectedCategory.size() != 0)
       if(String.isBlank(selectedKeyword))     
      {
                flag = true;
             }  
         else
           {
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter a valid date. News feed archives from the last one week are available.'));
           }
           
         getUserlist();  
         return null;
    }
    
  public void getUserlist() {
  
         if(flag==true)
         {
                   list<String> Criteria = new list<String>();
                   
                   if( selectedDate != null)
                       Criteria.add('publish_from__c <= :selectedDate AND publish_Through__c >= :selectedDate');
                                
                   if( selectedKeyword != null)
                       Criteria.add('Headline__c LIKE \'%' + selectedKeyword +'%\'');
                    
                   if( selectedCategory.size() != 0)
                       Criteria.add('Category__c in :selectedCategory');  
                       
                  String whereClause = '';
                  
                   if (criteria.size()>0) {
                       whereClause = ' where ' + String.join(criteria, ' AND ');
                       }

                  String Query = 'select id,Name,category__c,publish_from__c,Headline__c  from News__c ' + whereClause  + ' AND (Intended_audience__c IN (\'All\',\'Coaches - Registered\',\'All Registered users\')) ORDER BY publish_from__C DESC';    
                  system.Debug('query'+query);
                  UserList = Database.Query(query); 
                
         }                     
         else
         {
                  String Query = 'Select id,Name,category__c, publish_from__c,Headline__c from News__c where Publish_from__c <= TODAY and publish_Through__c >= LAST_N_DAYS:7 AND (Intended_audience__c IN (\'All\',\'Coaches - Registered\',\'All Registered users\')) ORDER BY publish_from__C DESC';
                  UserList = Database.Query(query); 
         }    
    
     }
   
  public PageReference clear() {
  
                  flag = false;
                  getUserlist();  
                  SelectedKeyword = '';
                  if(selectedCategory != null){
                      selectedCategory.clear();
                         }
                  SelectedDate = null;
                  return null;
    }
    // END - sampleCon
    
    
    
}