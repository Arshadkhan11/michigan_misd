global with sharing class MI_FamilyManagement {

// FAMILY MANAGEMENT DASH VAR - START --------------------------------------------------->

    // map -> 102
    public Map<String, String> inputFields { get; set; }
    public Map<Id, Integer> mPendingTasksPerCase {get;set;}
    public Map<Id, Integer> mAllTasksPerCase {get;set;}
    public Map<Id, Integer> mCompletedTasksPerCase {get;set;}
    public List<Integer> ListforMapTasksAll {get;set;}
    public List<Integer> ListforMapTasksCompleted {get;set;}   
    public Integer vFamCntV {get; set;}
    
    // search
    public String SearchString {get;set;}
    public boolean SearchFlag {get;set;}
    public List<String> ContactTypeAhead {get;set;}
    
  // client default
    public List<Case> FamilyManCases {get;set;}
    public Integer FamilyManCasesCount {get;set;}
    public String ClientNameListConcat {get;set;}
    public Integer TasksCompleted {get;set;}
    public Integer TasksTotal {get;set;}

    // Referrals
    public List<Referral__c> FamilyManRef {get;set;} 
    public Integer FamilyManRefCount {get;set;}
    
    // added for new FM dashboard !!!!!!!!!!!!!!!!
    public List<CaseDomains__c> SegCaseDomains {get;set;}
    public Map<Id,String> CaseSegmentMap {get;set;}
    
    //BBANSAL
    public Map<Id,Integer> CaseRole {get;set;}
    //public List<CaseContactRole> C_R {get;set;}


    //BBANSAL - Added for Referrals
	public List<Referral__c> FamilyManRefrl {get;set;}     
    public Map<Id,Integer> CaseRoleRef {get;set;}
    public Map<Id,DateTime> CaseCheckinDate {get;set;}
    public Integer FamilyManRefrlCount {get;set;}
    
    public Integer countCheckinCoach {get;set;}
    public Integer countReassessCoach {get;set;}
    public Integer countCheckinPartner {get;set;}
    public Integer countReassessPartner {get;set;}
    
    //BBANSAL - End - Added for Referrals
    public Double vCurrAcceptRate  {get; set;}
    public Integer vFinalCurrAR {get; set;}
    public Integer vCurrAccept  {get; set;}
    public Double vCurrAcceptTot  {get; set;}
    public Double vPrevAcceptRate  {get; set;}
    public Integer vFinalPrevAR {get; set;}
    public Integer vPrevAccept  {get; set;}
    public Double vPrevAcceptTot  {get; set;}
    public Double vPrevPrevAcceptRate  {get; set;}
    public Integer vFinalPrevPrevAR {get; set;}
    public Integer vPrevPrevAccept  {get; set;}
    public Double vPrevPrevAcceptTot  {get; set;}
    public String vCurrMonth {get; set;}
	public String vPrevPrevMonth {get; set;}
	public String vPrevMonth {get; set;}
    public Integer vCPTotalFamSrv {get; set;}
    
    public List<SegWrapper> SegListWrapper {get;set;}
	public Map<Id,String> CaseSegMap {get;set;}
    public List<String> SegMapToList {get;set;}
    public Double SegCasesCount {get;set;}
    public Double Seg1Perc {get;set;}
    public Double Seg2Perc {get;set;}
    public Double Seg3Perc {get;set;}
    public Double Seg4Perc {get;set;}
    public Integer Seg1Count {get;set;}
    public Integer Seg2Count {get;set;}
    public Integer Seg3Count {get;set;}
    public Integer Seg4Count {get;set;}
    public string vff{get; set;}
// FAMILY MANAGEMENT DASH VAR - END --------------------------------------------------->

// FAMILY MANAGEMENT DASH CONTROLLER - START --------------------------------------------------->    
    public MI_FamilyManagement()
    {
        try
        {
    	        // search init -> before default and referral query
            SearchFlag = false;
            ClientNameListConcat=null;
            //FamilyManCasesCount = 0;
	            // Clients default applet
            FamilyManCasesList();
        	    // Referrals applet
            //FamilyManRefFunc();
            	// search init -> after default and referral query
            //CaseSegmentMap=new Map<Id,String>();
    		//NewFMDashInit();
            SegCasesCount = 0;
            Seg1Perc = 0;
            Seg2Perc = 0;
            Seg3Perc = 0;
            Seg4Perc = 0;
            Seg1Count = 0;
            Seg2Count = 0;
            Seg3Count = 0;
            Seg4Count = 0;
            SegCaseDomains=new List<CaseDomains__c>();
            SegListWrapper=new List<SegWrapper>();
            getContactRoles();
            CaseSegMap=new Map<Id,String>();
            if(!(FamilyManCases.isEmpty()))
            {
	    		FMListInit();
            }
            FamilyManR();
            calAcceptanceRate();
        }
        catch(exception e)
        {
            //System.debug('ato const debug error 3'+e.getMessage());
        }
    }
// FAMILY MANAGEMENT DASH CONTROLLER - END --------------------------------------------------->

    
//BBANSAL - Referrals Starts
	public Integer getCoachRefCount()
    {
       List<Referral__c> FMRef = new List<Referral__c>();
	   FMRef = [select Id from Referral__c where CreatedById=:userInfo.getUserId() and Introduction__c = false and ServiceCount__C>0 and CreatedDate = LAST_N_DAYS:30];
	   return FMRef.size();        
    }
    public Integer getPartnerRefC()
    {
        Integer count = 0;
		List<Referral__c> FamRef = new List<Referral__c>();
    	Id u_u= [Select Id,ContactId from User where Id = :userinfo.getUserId()].ContactId;
        FamRef = [select Id,PartnerName__c,CitizenName__c,Referral_Date__c,Referral_Source__c,Case__c,CaseId__c,CoachId__c,CitId__c,ServiceStatus__c,ReferralReason__c from Referral__c where PartnerAssociated__c =:u_u and Introduction__c = false and ServiceCount__C>0 and CreatedDate = LAST_N_DAYS:30] ;
        count =  FamRef.size();
        return count;
    }
    
    public Integer getvTranstionCnt()
    {
        Integer count = 0;
        Id u_u= [Select Id,ContactId from User where Id = :userinfo.getUserId()].ContactId;
		List<Interaction_Log__c> vInt = [select Id, LoggedBy__c, Interaction_Type__c, Interaction_Date__c from Interaction_Log__c where Interaction_Type__c = 'Transition' and LoggedBy__c =:UserInfo.getUserId() and Interaction_Date__c = LAST_N_DAYS:30]; 
        if(vInt != null)
        count = vInt.size();
        return count;
    }
    //Viral
    public void calAcceptanceRate()
    {
        Date dt = system.today();
        //Datetime dt = Datetime.newInstance(1960, 7, 17);
        Integer vMon = dt.month();
               
        System.debug('vMOn:' + vMon);
        
        Map<Integer, String> vMonthMap = new Map<Integer, String>();
        vMonthMap.put(1, 'JANUARY');
        vMonthMap.put(2, 'FEBRUARY');
        vMonthMap.put(3, 'MARCH');
        vMonthMap.put(4, 'APRIL');
        vMonthMap.put(5, 'MAY');
        vMonthMap.put(6, 'JUNE');
        vMonthMap.put(7, 'JULY');
        vMonthMap.put(8, 'AUGUST');
        vMonthMap.put(9, 'SEPTEMBER');
        vMonthMap.put(10, 'OCTOBER');
        vMonthMap.put(11, 'NOVEMBER');
        vMonthMap.put(12, 'DECEMEBER');
        
        vCurrMonth = vMonthMap.get(vMon);
        vPrevPrevMonth = '';
        vPrevMonth = '';
        Integer vPrevMonInt;
        Integer vPrevPrevMonInt; 
        if(vMon == 1)
        {
            vPrevMonth = vMonthMap.get(12);
            vPrevMonInt = 12;
            vPrevPrevMonth =  vMonthMap.get(11);
            vPrevPrevMonInt = 11;
        }
        else if(vMon == 2)
        {
            vPrevMonth = vMonthMap.get(vMon-1);
            vPrevMonInt = vMon-1;
            vPrevPrevMonth = vMonthMap.get(12);
            vPrevPrevMonInt = 11;
        }
        else
        {
            vPrevMonth = vMonthMap.get(vMon-1);
            vPrevMonInt = vMon-1;
            vPrevPrevMonth = vMonthMap.get(vMon-2);
            vPrevPrevMonInt = vMon-2;
        }
        
        System.debug('Curr : ' +vCurrMonth + '|Prev : ' + vPrevMonth + '|vPrevPrevMonth :' + vPrevPrevMonth);
        //Current month calculation
        Id vPartnerId = [Select Id,ContactId from User where Id = :userinfo.getUserId()].ContactId;
        //vPartnerId = '';
        List<Referral__c> vCurrMonRef = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vMon and PartnerAssociated__c =:vPartnerId]; // and PartnerAssociated__c =:vPartnerId]
        List<Referral__c> vCurrMonRefRej = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vMon and (Referral_Status__c = 'Rejected' OR Referral_Status__c = 'Transitioned') and Old_Partner_Associated__c = :vPartnerId];
        List<Referral__c> vCurrMonRefAcc = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vMon and Introduction__c = true  and PartnerAssociated__c =:vPartnerId];
        vCurrAcceptRate = 0;
        vCurrAccept = 0;
        vCurrAcceptTot = 0;
        if(vCurrMonRef != null)
         	vCurrAcceptTot = vCurrAcceptTot + vCurrMonRef.size();
        if(vCurrMonRefRej != null)
            vCurrAcceptTot = vCurrAcceptTot + vCurrMonRefRej.size();
        if(vCurrMonRefAcc != null)
            vCurrAccept = vCurrMonRefAcc.size();
        if(vCurrAccept == 0 || vCurrAcceptTot == 0)
            vFinalCurrAR = 0;
        else
        {
            vCurrAcceptRate = (vCurrAccept/vCurrAcceptTot)*100;
            vCurrAcceptRate = vCurrAcceptRate.round();
            vFinalCurrAR = vCurrAcceptRate.intValue();
        }
        	
        System.debug('vCurrMonRef:' + vCurrMonRef.size() + '|vCurrMonRefRej:' + vCurrMonRefRej.size() + '|vCurrMonRefAcc :' + vCurrMonRefAcc.size() + '|vCurrAcceptTot :' + vCurrAcceptTot+'|Acceptance:' + vCurrAcceptRate);
        
        //prev month calc
        List<Referral__c> vPrevMonRef = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vPrevMonInt and PartnerAssociated__c =:vPartnerId];
        List<Referral__c> vPrevMonRefRej = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vPrevMonInt and (Referral_Status__c = 'Rejected' OR Referral_Status__c = 'Transitioned') and Old_Partner_Associated__c = :vPartnerId];
        List<Referral__c> vPrevMonRefAcc = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vPrevMonInt and Introduction__c = true  and PartnerAssociated__c =:vPartnerId];
        vPrevAcceptRate = 0;
        vPrevAccept = 0;
        vPrevAcceptTot = 0;
        if(vPrevMonRef != null)
         	vPrevAcceptTot = vPrevAcceptTot + vPrevMonRef.size();
        if(vPrevMonRefRej != null)
            vPrevAcceptTot = vPrevAcceptTot + vPrevMonRefRej.size();
        if(vPrevMonRefAcc != null)
            vPrevAccept = vPrevMonRefAcc.size();
        if(vPrevAccept == 0 || vPrevAcceptTot == 0)
            vFinalPrevAR = 0;
        else
        {
            vPrevAcceptRate = (vPrevAccept/vPrevAcceptTot)*100;
            vPrevAcceptRate = vPrevAcceptRate.round();
            vFinalPrevAR = vPrevAcceptRate.intValue();
        }
        	
        System.debug('vPrevMonRef:' + vPrevMonRef.size() + '|vPrevMonRefRej:' + vPrevMonRefRej.size() + '|vPrevMonRefAcc :' + vPrevMonRefAcc.size() + '|vPrevAcceptTot :' + vPrevAcceptTot+'|PrevAcceptance:' + vPrevAcceptRate);
        
        //prev prev month calc
        List<Referral__c> vPrevPrevMonRef = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vPrevPrevMonInt and PartnerAssociated__c =:vPartnerId];
        List<Referral__c> vPrevPrevRefRej = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vPrevPrevMonInt and (Referral_Status__c = 'Rejected' OR Referral_Status__c = 'Transitioned') and Old_Partner_Associated__c = :vPartnerId];
        List<Referral__c> vPrevPrevRefAcc = [select Id,Referral_Date__c from Referral__c where CALENDAR_MONTH(Referral_Date__c) = :vPrevPrevMonInt and Introduction__c = true  and PartnerAssociated__c =:vPartnerId];
        vPrevPrevAcceptRate = 0;
        vPrevPrevAccept = 0;
        vPrevPrevAcceptTot = 0;
        if(vPrevPrevMonRef != null)
         	vPrevPrevAcceptTot = vPrevPrevAcceptTot + vPrevPrevMonRef.size();
        if(vPrevPrevRefRej != null)
            vPrevPrevAcceptTot = vPrevPrevAcceptTot + vPrevPrevRefRej.size();
        if(vPrevPrevRefAcc != null)
            vPrevPrevAccept = vPrevPrevRefAcc.size();
        if(vPrevPrevAccept == 0 || vPrevPrevAcceptTot == 0)
            vFinalPrevPrevAR = 0;
        else
        {
            vPrevPrevAcceptRate = (vPrevPrevAccept/vPrevPrevAcceptTot)*100;
            vPrevPrevAcceptRate = vPrevPrevAcceptRate.round();
            vFinalPrevPrevAR = vPrevPrevAcceptRate.intValue();
        }
        	
        System.debug('Prev Prev MonRef:' + vPrevPrevMonRef.size() + '|vPrevPrevRefRej:' + vPrevPrevRefRej.size() + '|vPrevPrevRefAcc :' + vPrevPrevRefAcc.size() + '|vPrevPrevAcceptTot :' + vPrevPrevAcceptTot+'|vPrevPrevAcceptRate:' + vPrevPrevAcceptRate);
        
        //System.debug('CurrCnt : ' + vCurrMonRef.size()+ '|PrevCnt : ' + vPrevMonRef.size()+ '|vPrevPrevMonRef : ' + vPrevPrevMonRef.size());        
        vCPTotalFamSrv = 0;
        List<Referral__c> vAllFam = [select Id,Referral_Date__c from Referral__c where PartnerAssociated__c =:vPartnerId];
        List<Referral__c> vAllRej = [select Id,Referral_Date__c from Referral__c where (Referral_Status__c = 'Rejected' OR Referral_Status__c = 'Transitioned') and Old_Partner_Associated__c = :vPartnerId];
        if(vAllFam != null)
            vCPTotalFamSrv = vCPTotalFamSrv + vAllFam.size();
        if(vAllRej != null)
            vCPTotalFamSrv = vCPTotalFamSrv + vAllRej.size();
    }
    
    public void FamilyManR()
    {
    	Id uu= [Select Id,ContactId from User where Id = :userinfo.getUserId()].ContactId;
		FamilyManRefrlCount = 0;    	
        FamilyManRefrl = new List<Referral__c>();
        //FamilyManRefrl = [select Id,PartnerName__c,CitizenName__c,Referral_Date__c,Referral_Source__c,Case__c,CaseId__c,CoachId__c,CitId__c,ServiceStatus__c,ReferralReason__c from Referral__c where PartnerAssociated__c =:uu and Introduction__c = false and ServiceStatus__c=true];
        FamilyManRefrl = [select Id,PartnerName__c,CitizenName__c,Referral_Date__c,Referral_Source__c,Case__c,CaseId__c,CoachId__c,CitId__c,ServiceStatus__c,ReferralReason__c, Referral_Status__c,(select Id, servicename__c from ReferralServices__r) from Referral__c where PartnerAssociated__c =:uu and Introduction__c = false and ServiceCount__C>0 order by Referral_Date__c];
		//vff = FamilyManRefrl[0].ReferralServices__r[0].servicename__c;
        List <Id> CsId = new List<Id>();
        
		for(Referral__c rffff: FamilyManRefrl)
        {
			FamilyManRefrlCount = FamilyManRefrl.size();            
            CsId.add(rffff.CaseId__c);
        }
        List <aggregateResult> roles1 = [select count(Id) total,CasesId  from CaseContactRole where CasesId IN :CsId GROUP BY CasesId];
        CaseRoleRef=new Map<Id,Integer>();
        for(AggregateResult rl1: roles1)
        {
            Id Case_Id1 = (Id)rl1.get('CasesId');
            Integer test1 = (Integer)rl1.get('total');
			CaseRoleRef.put(Case_Id1,test1);
        }
        
    }
//BBANSAL- End - Referrals Starts

//Contact Roles count - Family Count - Bhrigu
	public void getContactRoles()
    {
        List <aggregateResult> roles = [select count(Id) total,CasesId  from CaseContactRole where CasesId IN :FamilyManCases GROUP BY CasesId];
        CaseRole=new Map<Id,Integer>();
        for(AggregateResult rl: roles)
        {
			Id Case_Id = (Id)rl.get('CasesId');
            Integer test = (Integer)rl.get('total');
	        CaseRole.put(Case_Id,test);
        }
    }

    
// FAMILY MANAGEMENT DASH functions - START --------------------------------------------------->

    // ---------------------------------------------- START of Segmentation logic    
    public void FMListInit()
    {
        String TempSeg;
        SegCaseDomains = [Select Id, Name, 	Status__c, 	Domain__c, Case__c, Master_Domain_Name__c, Barrier__c, 	Skill__c, Assessment_Score_Value__c, Case__r.Contact.Name
                          from CaseDomains__c
                         where Case__c IN: FamilyManCases AND
                         Status__c=:true];
        for(Case fmc: FamilyManCases)
        {
            TempSeg=FMSegmentINIT(fmc.Id);
            SegListWrapper.add(new SegWrapper(fmc.Id,fmc.Primary_Contact_Name__c,TempSeg));
            CaseSegMap.put(fmc.Id,TempSeg);   
            //System.debug(TempSeg+' Temp seg for Case: '+fmc.Id);
            if(TempSeg=='1')
            {
                Seg1Count++;
            }
            else if(TempSeg=='2')
            {
                Seg2Count++;
            }
            else if(TempSeg=='3')
            {
                Seg3Count++;
            }
            else if(TempSeg=='4')
            {
                Seg4Count++;
            }
        }
        SegCasesCount=Seg1Count+Seg2Count+Seg3Count+Seg4Count;
        if(SegCasesCount == 0)
        {
            Seg1Perc = 0;
            Seg2Perc = 0;
            Seg3Perc = 0;
            Seg4Perc = 0;
        }
        else
        {
            Seg1Perc=(Seg1Count/SegCasesCount)*100;            
            Seg2Perc=(Seg2Count/SegCasesCount)*100;            
            Seg3Perc=(Seg3Count/SegCasesCount)*100;
            Seg4Perc=(Seg4Count/SegCasesCount)*100;            
        }
        
        /*System.debug('Seg1Count: '+Seg1Count);
        System.debug('Seg2Count: '+Seg2Count);
        System.debug('Seg3Count: '+Seg3Count);
        System.debug('Seg4Count: '+Seg4Count);
        System.debug('SegCasesCount: '+SegCasesCount);
        System.debug('Seg1Perc: '+Seg1Perc);
        System.debug('Seg2Perc: '+Seg2Perc);
        System.debug('Seg3Perc: '+Seg3Perc);
        System.debug('Seg4Perc: '+Seg4Perc);*/
    }
    
    public String FMSegmentINIT(Id CaseId)
    {
        Id FCaseId = CaseId;
        Decimal HighestBarrier=0;
        Decimal HighestSkill=0;
        String Segment;
        for(CaseDomains__c scd: SegCaseDomains)
        {
            if(FCaseId==scd.Case__c)
            {
                if(scd.Barrier__c>HighestBarrier)
                {
                    HighestBarrier=scd.Barrier__c;
                }
                if(scd.Skill__c>HighestSkill)
                {
					HighestSkill=scd.Skill__c;                    
                }            
            }
    	}
        if(HighestBarrier==2 && HighestSkill==1)
        {
			Segment='1';            
        }
        else if(HighestBarrier==2 && HighestSkill==2)
        {
			Segment='2';            
        }
        else if(HighestBarrier==1 && HighestSkill==1)
        {
			Segment='3';            
        }
        else if(HighestBarrier==1 && HighestSkill==2)
        {
			Segment='4';            
        }
        else
        {
            Segment='TBD';            
        }
        return Segment;
    }
    
    public class SegWrapper // wrapper definition
    {
        public Id cId {get;set;}
        public String cPrimConName {get;set;}
        public String cSegment {get;set;}
        public SegWrapper(Id SWcId, String SWcPrimConName, String SWcSegment)
        {
            cId=SWcId;
            cPrimConName=SWcPrimConName;
            cSegment=SWcSegment;
        }
    }
    
    public static String FMSingleInit(Id CasIdP)
    {
        Id sgFCaseId = CasIDP;
        Decimal sgHighestBarrier=0;
        Decimal sgHighestSkill=0;
        String sgSegment;
        List<CaseDomains__c> SegCaseDomainsSing= New List<CaseDomains__c>();
        if(sgFCaseId!=null)
        {
            SegCaseDomainsSing = [Select Id, Name, 	Status__c, 	Domain__c, Case__c, Master_Domain_Name__c, Barrier__c, 	Skill__c, Assessment_Score_Value__c, Case__r.Contact.Name
                              from CaseDomains__c
                             where Case__c =: sgFCaseId AND NeedsAssessmentId__r.Status__c='Active' AND
                             Status__c=:true];
            for(CaseDomains__c scds: SegCaseDomainsSing)
            {
                if(sgFCaseId==scds.Case__c)
                {
                    if(scds.Barrier__c>sgHighestBarrier)
                    {
                        sgHighestBarrier=scds.Barrier__c;
                    }
                    if(scds.Skill__c>sgHighestSkill)
                    {
                        sgHighestSkill=scds.Skill__c;                    
                    }            
                }
            }
            if(sgHighestBarrier==2 && sgHighestSkill==1)
            {
                sgSegment='1';            
            }
            else if(sgHighestBarrier==2 && sgHighestSkill==2)
            {
                sgSegment='2';            
            }
            else if(sgHighestBarrier==1 && sgHighestSkill==1)
            {
                sgSegment='3';            
            }
            else if(sgHighestBarrier==1 && sgHighestSkill==2)
            {
                sgSegment='4';            
            }
            else
            {
                sgSegment='TBD';            
            }            
        }
        return sgSegment;        
    }
    
    // ---------------------------------------------- END of Segmentation logic

    public PageReference SearchClients()
    { 
        try{
        if(SearchString.length()>0)
        { 
            //System.debug('ato->NOT NULL search'+SearchString+SearchString.length());
            
            SearchFlag = true;
            FamilyManCaseslist();
            //FamilyManRefFunc(); 
        } 
        else
        {
            //System.debug('ato->NULL search'+SearchString);
            
            SearchFlag = false;
            FamilyManCaseslist();
            //FamilyManRefFunc();
        }
        }
        catch(exception e)
        {
            //System.debug('ato const debug error 1'+e.getMessage());
        }
        return null;
    }
    
  public void FamilyManRefFunc()
    {
        if(SearchFlag)
        {
        FamilyManRef = [Select Id, Name, Primary_Contact__c, Referral_Date__c, Referral_Source__c 
                        from Referral__c 
                          where ((Primary_Contact__c LIKE :('%'+string.escapeSingleQuotes(SearchString)+'%')) OR 
                          (Primary_Contact__c LIKE :('%'+string.escapeSingleQuotes(SearchString))) OR
                          (Primary_Contact__c LIKE :(string.escapeSingleQuotes(SearchString)+'%'))
                          )
                          ORDER BY Referral_Date__c DESC];
        }
        else
        {
        FamilyManRef = [Select Id, Name, Primary_Contact__c, Referral_Date__c, Referral_Source__c 
                        from Referral__c 
                        where Name !=null
                        ORDER BY Referral_Date__c DESC];
        }
        //System.debug('atoatoatoreferralsize'+FamilyManRef.size());
        FamilyManRefCount = FamilyManRef.size();
        //return FamilyManRef;
    }
  
    public void FamilyManCasesList()     
    {
        FamilyManCases = new List<Case>();
        List<Case> FamilyManCasestemp = new List<Case>();
        vFamCntV = 0;
        String Pro_Name = [select Name from profile where id = :userinfo.getProfileId()].Name;    
		List<NeedsAssessment__c> ActiveAssessments = [Select Id,CaseId__c from NeedsAssessment__c where Status__c = 'Active'];
        Map<ID, NeedsAssessment__c> needsmap = new Map<ID, NeedsAssessment__c>();
        for(NeedsAssessment__c needstmp : ActiveAssessments){
            needsmap.put(needstmp.CaseId__c, needstmp);
        }
        List<CommonApplication__c> ActiveApp = [Select Id,CaseId__c from CommonApplication__c where Status__c = 'Active'];
        Map<ID, CommonApplication__c> camap = new Map<ID, CommonApplication__c>();
        for(CommonApplication__c catmp : ActiveApp){
            camap.put(catmp.CaseId__c, catmp);
        }        
        if(SearchFlag)
        {
            if(SearchString.contains('\''))
            {
                //do ntothing
                //FamilyManCases = new List<Case>();
            }
            else
            {
                if(Pro_Name == 'Success Coach Profile')	
                {
                 FamilyManCasestemp = [Select Id,Reassessment_Date__c,Reassessment_Alert__c, CaseNumber, Primary_Contact_Image__c, Contact.Name, Primary_Contact_Name__c, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c, LastUpdatedGoalCase__c,CA_Completion__c,NA_Completion__c,SP_Completion__c from Case 
                                      where (ContactId!=null) AND
                                      Primary_Contact_Name__c!=null AND
                                      ((Primary_Contact_Name__c LIKE :('%'+string.escapeSingleQuotes(SearchString)+'%')) OR 
                                       (Primary_Contact_Name__c LIKE :('%'+string.escapeSingleQuotes(SearchString))) OR
                                       (Primary_Contact_Name__c LIKE :(string.escapeSingleQuotes(SearchString)+'%'))
                                      ) AND
                                      SuccessCoach__c= :userInfo.getUserId()
                                      ORDER BY LastUpdatedGoalCase__c DESC];
                    

                    
                }
                else if(Pro_Name == 'Partner Profile')	
                {
                    //Viral-Need to change this as well
                    Id uu= [Select Id,ContactId from User where Id = :userinfo.getUserId()].ContactId;
                    //List<Referral__c> FamilyManRefrl2 = [select Id,PartnerName__c,CitizenName__c,Referral_Date__c,Referral_Source__c,Case__c,CaseId__c,CoachId__c,CitId__c,ServiceStatus__c from Referral__c where PartnerAssociated__c =:uu and Introduction__c = true and ServiceStatus__c=true];
                    List<Referral__c> FamilyManRefrl2 = [select Id,PartnerName__c,CitizenName__c,Referral_Date__c,Referral_Source__c,Case__c,CaseId__c,CoachId__c,CitId__c,ServiceStatus__c from Referral__c where PartnerAssociated__c =:uu and Introduction__c = true and ServiceCount__C>0];
                    List <Id> CsId = new List<Id>();
                    for(Referral__c rffff: FamilyManRefrl2)
                    {
                        CsId.add(rffff.CaseId__c);
                    }
                FamilyManCasestemp = [Select Id,Accepted_Date__c, Anticipated_Duration__c, Referral_Services__c, Reassessment_Date__c,Reassessment_Alert__c, CaseNumber, Primary_Contact_Image__c, Contact.Name, Primary_Contact_Name__c, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c, LastUpdatedGoalCase__c,CA_Completion__c,NA_Completion__c,SP_Completion__c from Case 
                                      where 
                                      ((Primary_Contact_Name__c LIKE :('%'+string.escapeSingleQuotes(SearchString)+'%')) OR 
                                       (Primary_Contact_Name__c LIKE :('%'+string.escapeSingleQuotes(SearchString))) OR
                                       (Primary_Contact_Name__c LIKE :(string.escapeSingleQuotes(SearchString)+'%'))
                                      ) AND
                                      Id IN :CsId
                                      ORDER BY LastUpdatedGoalCase__c DESC];
                }
                for(Case cstmp: FamilyManCasestemp){
                    if(needsmap.containsKey(cstmp.Id)){
                        FamilyManCases.add(cstmp);
                    }
                    else{
                        if(camap.containsKey(cstmp.Id)){
                            FamilyManCases.add(cstmp);
                        }
                    }
                }
            }
        }
        else
        {
            //if logged in user is success coach
            if(Pro_Name == 'Success Coach Profile')	
            {
                FamilyManCasestemp = [Select Id,Reassessment_Date__c,Reassessment_Alert__c, CaseNumber, Primary_Contact_Image__c, Contact.Name, Primary_Contact_Name__c, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c, LastUpdatedGoalCase__c,CA_Completion__c,NA_Completion__c,SP_Completion__c from Case 
                                  where (ContactId!=null) AND
                                  Primary_Contact_Name__c!=null AND
                                  SuccessCoach__c= :userInfo.getUserId()
                                  ORDER BY LastUpdatedGoalCase__c DESC];
                                    List<Integer> removeindex = new List<Integer>();
                
                for(Case cstmp: FamilyManCasestemp){
                    if(needsmap.containsKey(cstmp.Id)){
                       	FamilyManCases.add(cstmp);
                    }
                    else{
                        if(camap.containsKey(cstmp.Id)){
                            FamilyManCases.add(cstmp);
                        }
                    }
                }
                List<Interaction_Log__c> ILog = new List<Interaction_Log__c>();
                ILog = [Select Id from Interaction_Log__c where Case__c IN :FamilyManCases and Interaction_Type__c = 'Checkin' and CreatedDate = LAST_N_DAYS:30];
                countCheckinCoach = Ilog.size();
                ILog = [Select Id from Interaction_Log__c where Case__c IN :FamilyManCases and Interaction_Type__c = 'Re-Assessment' and CreatedDate = LAST_N_DAYS:30];
                system.debug('BBBBBB - 1');
                countReassessCoach = Ilog.size();
                
                List <aggregateResult> CheckinDate = [Select Case__c, min(Next_Schedule__c) checkdt from Interaction_Log__c where Case__c IN :FamilyManCases and Interaction_Type__c = 'Checkin' and CreatedDate >= TODAY group by Case__c];
                CaseCheckinDate=new Map<Id,DateTime>();
                for(AggregateResult CheckinDt: CheckinDate)
                {
                    system.debug('BBBBBB - 2');
                    Id Case_Id5 = (Id)CheckinDt.get('Case__c');
                    DateTime ChkDt = (DateTime)CheckinDt.get('checkdt');
                    CaseCheckinDate.put(Case_Id5,ChkDt);
                    system.debug('BBBBBB - 3');
                }
                List<Interaction_Log__c> ILog2= [Select Case__c, Next_Schedule__c,Id from Interaction_Log__c where Case__c IN :FamilyManCases and Interaction_Type__c = 'Checkin' and CreatedDate >= TODAY and Next_Schedule__c = null];
                for(Interaction_Log__c CheckinDt: ILog2)
                {
                    Id Case_Id5 = (Id)CheckinDt.get('Case__c');
                    DateTime ChkDt = (DateTime)CheckinDt.get('Next_Schedule__c');
                    CaseCheckinDate.put(Case_Id5,ChkDt);
                }
                
                for(Case cssssss: FamilyManCases)
                {
                    if(CaseCheckinDate.containsKey(cssssss.Id))
                    {
                    }
                    else
                    {
                        Id Case_Id5 = (Id)cssssss.Id;
                        DateTime ChkDt = null;
                        CaseCheckinDate.put(Case_Id5,ChkDt);   
                    }
                    
                }
                //system.debug('BBBBBB - '+CaseCheckinDate);
            }
            else if(Pro_Name == 'Partner Profile')	
            {
                Id uuuu= [Select Id,ContactId from User where Id = :userinfo.getUserId()].ContactId;
                List<Referral__c> Family_Ref = new List<Referral__c>();
                Family_Ref = [select Id,PartnerName__c,CitizenName__c,Referral_Date__c,Referral_Source__c,Case__c,CaseId__c,CoachId__c,CitId__c,ServiceStatus__c from Referral__c where PartnerAssociated__c =:uuuu and Introduction__c = true and ServiceCount__C>0];
                List <Id> Cs_Id2 = new List<Id>();
                for(Referral__c rf1: Family_Ref)
                {
                    Cs_Id2.add(rf1.CaseId__c);
                }
                FamilyManCasestemp = [Select Id,Accepted_Date__c, Anticipated_Duration__c, Referral_Services__c, Reassessment_Date__c,Reassessment_Alert__c, CaseNumber, Primary_Contact_Image__c, Contact.Name, Primary_Contact_Name__c, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c, LastUpdatedGoalCase__c,CA_Completion__c,NA_Completion__c,SP_Completion__c from Case 
                                  where Id IN :Cs_Id2
                                  ORDER BY LastUpdatedGoalCase__c DESC];
                                    List<Integer> removeindex = new List<Integer>();
                for(Case cstmp: FamilyManCasestemp){
                    if(needsmap.containsKey(cstmp.Id)){
                        FamilyManCases.add(cstmp);
                    }
                    else{
                        if(camap.containsKey(cstmp.Id)){
                            FamilyManCases.add(cstmp);
                        }
                    }
                }
                List<Interaction_Log__c> ILog = new List<Interaction_Log__c>();
                ILog = [Select Id from Interaction_Log__c where Case__c IN :Cs_Id2 and Interaction_Type__c = 'Checkin' and CreatedDate = LAST_N_DAYS:30];
                countCheckinPartner = Ilog.size();
                ILog = [Select Id from Interaction_Log__c where Case__c IN :Cs_Id2 and Interaction_Type__c = 'Re-Assessment' and CreatedDate = LAST_N_DAYS:30];
                countReassessPartner = Ilog.size();
                
                FamilyManR();
                vFamCntV = FamilyManCases.size();
            }
        }
        
        FamilyManCasesCount = FamilyManCases.size();
    }
    
    

}