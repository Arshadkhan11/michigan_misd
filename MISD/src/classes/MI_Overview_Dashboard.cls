public class MI_Overview_Dashboard {
  
  public Date TodayDate {get;set;}
  public String Status {get;set;}
  
  public String nameSortDirection {get;set;}
  public String goalSortDirection {get;set;}
  public String DateSortDirection {get;set;}
  public String StatusSortDirection {get;set;}

  public list<Wrapper_Goals> GoalsList {get;set;}
  public list<SP_Transactional__c> TransList {get;set;}
  public list<Success_Plan_Master__c> MasterList {get;set;}
  public Date StartDate {get;set;}
  public Date DueDate {get;set;}
  public Integer DurationDays {get;set;}
  
  public String selectedDate {get;set;}
  public String selectedKeyword {get;set;}
  public Integer overDueCount {get;set;}
    
  public Boolean Flag {get;set;}

  public List<Wrapper_Goals> overDueList {get;set;}
  public List<Wrapper_Goals> UserList {get;set;}
  
  public MI_Overview_Dashboard()
    {
               
              
              
               flag = false;
               nameSortDirection = '';
               goalSortDirection = '';
               DateSortDirection = 'glyphicon glyphicon-arrow-down';
               StatusSortDirection ='';
               
               
               DisplayList();
               
  
    }
    
    public PageReference search() {
   
         if ( selectedDate != null || selectedDate != '' || !String.isBlank(selectedKeyword)  )
          {
                flag = true; 
                
                displayList();
          }
         
       
         return null;
    }
    
    public PageReference clear() {
  
                 flag = false;
                
                 SelectedKeyword = null;
                 nameSortDirection = '';
                 goalSortDirection = '';
                 DateSortDirection = 'glyphicon glyphicon-arrow-down';
                 StatusSortDirection ='';
                 SelectedDate = null;
                  DisplayList();
                  return null;
    }
    
   
    
     public PageReference overDue() {
     
         nameSortDirection = '';
         goalSortDirection = '';
         DateSortDirection = 'glyphicon glyphicon-arrow-down';
         StatusSortDirection ='';
   
         GoalsList = overDueList;
         DateSort();
         
         return null;
    }
     public void displayList()
     {
              
               StartDate = Date.today();
                TodayDate = Date.Today();
               GoalsList = new List<Wrapper_Goals>();
               OverDueList = new List<Wrapper_Goals>();
               UserList = new List<Wrapper_Goals>();
               TransList = new list<SP_Transactional__c>();
               DurationDays = 0;
              
         
              
               Translist = [Select id, Goal__r.Case__r.Contact.Name,Goal__r.Case__r.Primary_Contact_Name__c, Parent__c, Name__c,Start_Date__c 
                            from SP_Transactional__c 
                            where 
                            type__c='Milestone' and Goal__r.Case__r.SuccessCoach__c  = :UserInfo.getUserId()
                            and Milestone_Status__c != 'Completed' and Status__c = true];    
         
               //system.debug('translist:'+TransList); 
               
               
                 MasterList = [Select Duration__c, Parent__r.value__c, value__c, Sequence__c 
                                 from Success_Plan_Master__c
                              		where value__c!=null];
                 //system.debug('MasterList:'+MasterList); 
               
         
               //system.debug('going inside tanslist :P');
               for(SP_Transactional__c t : Translist )
               {
                 
                   StartDate = t.Start_Date__c;
                   
                   for(Success_Plan_Master__c m : MasterList )
                    {
                          if(m.value__c == t.Name__c  && m.Parent__r.value__c == t.Parent__c)
                           { 
                             //system.debug('going inside loop :P');
                               
                             DurationDays = (Integer) m.Duration__c;
                             
                             //system.debug('Start Date:'+StartDate);
                             //system.debug('Start Date:'+t.Goal__r.Case__r.Contact.Name); 
                             //system.debug('days:'+DurationDays); 
                             
                             DueDate = StartDate.addDays(DurationDays);
                             //system.debug('duedate:'+DueDate); 
                             
                             if(DueDate < Date.Today())
                             {
                               Status = 'Overdue';
                               OverDueList.add(new Wrapper_Goals(t,DueDate,Status));
                             }
                             else if( DueDate.DaysBetween(TodayDate) <= 7 && ( DueDate > TodayDate) )
                               Status = 'Pending';
                             else if( DueDate.DaysBetween(TodayDate) > 7)
                               Status = 'Upcoming';
                             else {}
                             
                             GoalsList.add(new Wrapper_Goals(t,DueDate,Status));
                           }
                    }
               }
               //system.debug('goalslist:'+GoalsList); 
               //system.debug('overDuelist:'+overDueList);
               OverdueCount = overDueList.size(); 
               
               DateSort();
               
                
               if(flag == true)
               {
         
                   //system.debug('Inside true flat');
                   
                   if( selectedDate != null && selectedDate != '' && String.isnotBlank(selectedKeyword) )
                       {
                          for(Wrapper_Goals g : GoalsList)
                           {
                              if( Date.ValueOf(g.DueDate) == date.parse(selectedDate) && (g.TS.Goal__r.Case__r.Primary_Contact_Name__c.ContainsIgnoreCase(selectedKeyword) || g.TS.Name__C.containsIgnoreCase(selectedKeyword) ) )
                                UserList.add(new Wrapper_Goals(g.TS,g.DueDate,g.Status));
                           }
                       
                         //System.Debug('Inside date and key');
                         //System.Debug('UserList:'+UserList);
                       }
                  else if( String.isnotBlank(selectedKeyword) && selectedDate == null && selectedDate == '')
                      {
                         for(Wrapper_Goals g : GoalsList)
                           {
                              if( (g.TS.Goal__r.Case__r.Primary_Contact_Name__c.ContainsIgnoreCase(selectedKeyword) || g.TS.Name__C.containsIgnoreCase(selectedKeyword) ) )
                                 UserList.add(new Wrapper_Goals(g.TS,g.DueDate,g.Status));
                           }
                           
                          //System.Debug('Inside key:');
                         //System.Debug('UserList:'+UserList); 
                      }
                   else
                      {
                        for(Wrapper_Goals g : GoalsList)
                           {
                              //system.Debug('selected Date'+selectedDate);
                            
                              
                              if( Date.ValueOf(g.DueDate) == date.parse(selectedDate) )
                                 UserList.add(new Wrapper_Goals(g.TS,g.DueDate,g.Status));
                           }
                         //System.Debug('Inside date');
                         //System.Debug('UserList:'+UserList.size());  
                      }
                 
                 if(UserList.isEmpty())
                  {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Goals not available for the search.'));
                     
                     GoalsList = null;
                     
                  }
                 else
                  {
                     GoalsList = UserList;
                      nameSortDirection = '';
                      goalSortDirection = '';
                      DateSortDirection = 'glyphicon glyphicon-arrow-down';
                      StatusSortDirection ='';
                      DateSort();
                     
                      UserList = new List<Wrapper_Goals>();
                  }
       
       }
     
     }
     
     public void nameSort(){
     
      String sortVariableName;
      
      goalSortDirection = '';
      DateSortDirection = '';
      StatusSortDirection ='';
      
       if (nameSortDirection == 'glyphicon glyphicon-arrow-up' || nameSortDirection == '') {

            nameSortDirection = 'glyphicon glyphicon-arrow-down';

            sortVariableName = 'ASC';

        }

        else {

            nameSortDirection = 'glyphicon glyphicon-arrow-up';

            sortVariableName = 'DESC';

        }
        
        Wrapper_Goals temp = new  Wrapper_Goals();    
               
                
               if( sortVariableName == 'ASC')  
                      for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].TS.Goal__r.Case__r.Primary_Contact_Name__c>= GoalsList[j+1].TS.Goal__r.Case__r.Primary_Contact_Name__c)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }
               else
                 for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].TS.Goal__r.Case__r.Primary_Contact_Name__c<= GoalsList[j+1].TS.Goal__r.Case__r.Primary_Contact_Name__c)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }


     }
     public void goalSort(){
       String sortVariableName;
      
      nameSortDirection = '';
      DateSortDirection = '';
      StatusSortDirection ='';
      
       if (goalSortDirection == 'glyphicon glyphicon-arrow-up' || goalSortDirection == '') {

            goalSortDirection = 'glyphicon glyphicon-arrow-down';

            sortVariableName = 'ASC';

        }

        else {

            goalSortDirection = 'glyphicon glyphicon-arrow-up';

            sortVariableName = 'DESC';

        }
        
            Wrapper_Goals temp = new  Wrapper_Goals();    
                 
                if(sortVariableName == 'ASC')
                       for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].TS.Name__c >= GoalsList[j+1].TS.Name__c)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }
                else
                 for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].TS.Name__c <= GoalsList[j+1].TS.Name__c)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }
     
     
     }
     public void DateSort(){
        String sortVariableName;
      
       goalSortDirection = '';
      nameSortDirection = '';
      StatusSortDirection ='';
      
       if (DateSortDirection == 'glyphicon glyphicon-arrow-up' || DateSortDirection == '') {

            DateSortDirection = 'glyphicon glyphicon-arrow-down';

            sortVariableName = 'ASC';

        }

        else {

            DateSortDirection = 'glyphicon glyphicon-arrow-up';

            sortVariableName = 'DESC';

        }
        
         Wrapper_Goals temp = new  Wrapper_Goals();    
        
          if(sortVariableName == 'ASC')
                for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].DueDate >= GoalsList[j+1].DueDate)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }
           else
             for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].DueDate <= GoalsList[j+1].DueDate)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }   
     }
     public void StatusSort(){
        String sortVariableName;
      
       goalSortDirection = '';
      DateSortDirection = '';
      nameSortDirection ='';
      
       if (StatusSortDirection == 'glyphicon glyphicon-arrow-up' || StatusSortDirection == '') {

            StatusSortDirection = 'glyphicon glyphicon-arrow-down';

            sortVariableName = 'ASC';

        }

        else {

            StatusSortDirection = 'glyphicon glyphicon-arrow-up';

            sortVariableName = 'DESC';

        }
        
         Wrapper_Goals temp = new  Wrapper_Goals();    
        
         if(sortVariableName == 'ASC')
                for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].Status >= GoalsList[j+1].Status)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }
           else
             for(Integer i=0; i<GoalsList.size(); i++)
                       {
                          for(Integer j=0; j<GoalsList.size()-i-1; j++)     
                          {                       
                           if(GoalsList[j].Status <= GoalsList[j+1].Status)
                                {
                                   temp =GoalsList[j];
                                   GoalsList[j]=GoalsList[j+1];
                                   GoalsList[j+1]=temp;
                                }
                           } 
                       }   
     
     }
     
 
    public class Wrapper_Goals {
        
        public SP_Transactional__c TS {get;set;}
        public Date DueDate {get;set;}
        public String Status {get;set;}
        
        public Wrapper_Goals(SP_Transactional__c T,Date D,String S){
            this.TS = T;
            this.DueDate = D;
            this.Status = S;
            
        }  
        
        public Wrapper_Goals(){}
         
    }
   
}