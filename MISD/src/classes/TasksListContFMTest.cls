@isTest
public class TasksListContFMTest { 

    @isTest public static void testConst(){ 
        date d = date.today();
        
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey');
        insert con;        
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c= UserInfo.getUserId());
        insert cs;

        TasksListContFM t = new TasksListContFM();
		//New changes
		t.selectedDate = '11/11/2015';
        t.clear();
		t.HeaderSort();
        t.search();
        t.overDue();
        
        t.DateOrStatus = true;
        t.OverDueQueryFlag = false;
        t.DefaultQuery = null;
        t.OverdueQuery = null;
        t.LastSortColumn = null;
        t.SortColumn = 'Activity_Date__c';
        t.LastSortColumn = 'Activity_Date__c'; 
        t.SortOrder = 'ASC';
        t.SortOrder = 'DESC';
		
        t.RecTypeId = [select Id from RecordType where Name='Task'].Id;
        t.flag = false;
        t.overDueCount = 0; 

        t.selectedCategory = new List<string>();
        t.selectedDate = string.valueOfGmt(d);
        t.selectedKeyword = 'Test';
        List<String> st = new List<String> ();
        st.add('a');
        st.add('b'); 
        
        t.ClientNameListConcat='John;Jill;James';
        t.querytaskListUserLogin();
        t.selectedDate = '11/11/2015';
        t.search();
       
        t.overDue();
        t.HeaderSort();
        //t.HeaderSort();
        //t.clear();
        //t.overDue();
        //t.search();

        
        //t.SelectedCategory = st;
        //t.search();
        //t.clear();
        //t.overDue();
        List<MI_Activity__c> list1 = new List<MI_Activity__c>();
        MI_Activity__c  var1 = new MI_Activity__c(Activity_Date__c=system.today(),Description__c='test',Priority__c='test',Subject__c='test',Status__c='test',Task_Type__c='test',Client_Name__c='test');
        List1.add(var1);
        insert(List1);
        
        PageReference pageRef = Page.MI_TasksList_FM;
        Test.setCurrentPage(pageRef);
        TasksListContFM t1 = new TasksListContFM();
        t1.clear();
        t1.search();
         t1.overDue();
        Test.setCurrentPageReference(new PageReference('Page.MI_TasksList_FM'));
        
        
    }
    
     @isTest public static void testConst2(){
        Contact con1 = new Contact(FirstName = 'John1', LastName = 'Rey1');
        insert con1;        
        Case cs1 = new Case(Status= 'New', ContactId = con1.id , Subject = 'TestCase1',SuccessCoach__c= UserInfo.getUserId());
        insert cs1;
        
        TasksListContFM t = new TasksListContFM();
		List<MI_Activity__c> list1 = new List<MI_Activity__c>();
        MI_Activity__c  var1 = new MI_Activity__c(Activity_Date__c=system.today(),Description__c='test',Priority__c='test',Subject__c='test',Status__c='test',Task_Type__c='test',Client_Name__c='test');
        List1.add(var1);
        insert(List1);
        t.DateOrStatus = true;
        t.OverDueQueryFlag = false;
        t.DefaultQuery = null;
        t.OverdueQuery = null;
        t.LastSortColumn = null;
        t.SortColumn = 'Activity_Date__c';
        t.LastSortColumn = 'Activity_Date__c'; 
        t.SortOrder = 'ASC';
         t.SortOrder = 'DESC';
         
		
        t.RecTypeId = [select Id from RecordType where Name='Task'].Id;
        t.flag = false;
        t.overDueCount = 0; 

        t.selectedCategory = new List<string>();
         t.selectedDate = string.valueOfGmt(date.today());
        
        t.selectedKeyword = 'Test';
        List<String> st = new List<String> ();
        st.add('a');
        st.add('b'); 
        
        t.ClientNameListConcat='John;Jill;James';

        t.SortOrder = 'DESC';
        t.OverDueQueryFlag=true;  
         
         
        t.HeaderSort();
        t.DateOrStatus=true;
        //t.search();
        t.overDue();
        t.clear();
        PageReference pageRef = Page.MI_TasksList_FM;
        Test.setCurrentPage(pageRef);
         TasksListContFM t1 = new TasksListContFM();
        t1.clear();
        t1.search();
         t1.overDue();
         t1.HeaderSort();
         Test.setCurrentPageReference(new PageReference('Page.MI_TasksList_FM'));
        
 
         
    }
}