@isTest
public class MI_TaskDetailsContTest {
	
    @isTest static void testConst(){
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
       
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
       
     
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
            Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
            Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        
        MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Appointment_Type__c = 'New Family', Activity_Date__c = date.today(), RecordTypeId = [select id from RecordType where DeveloperName='Task' and sObjectType='MI_Activity__c'].id);
        insert miact; 
        
        PageReference pageRef = Page.MI_TaskDetails;
        PageReference pageRefRedir1 = Page.MI_TasksList;
        
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
        ApexPages.currentPage().getParameters().put('retURL','/apex/MI_TasksList');
        //MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc); 
        System.runas(u) {
        MI_TaskDetailsCont m = new MI_TaskDetailsCont(sc);
        m.getGreeting();
        m.TaskCompleteId = miact.Id;
        m.UpdateSave();
       // m.UpdateSave();
        m.CompleteTaskMethod();
        m.UpdateDelete();
       	m.UpdateDelete();
       
        }
        System.runas(u) {
        MI_TaskDetailsCont m = new MI_TaskDetailsCont(sc);
       
        m.retURLVar = 'xyz';
        m.getGreeting();
        m.TaskCompleteId = miact.Id;
        m.UpdateSave();
       // m.UpdateSave();
        miact.Subject__c = NULL;
        m.EditDate = NULL;
        m.CompleteTaskMethod();
        m.UpdateDelete();
       	m.UpdateDelete();
       
        }
    }
    
    
    
}