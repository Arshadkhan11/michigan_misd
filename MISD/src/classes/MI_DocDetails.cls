public class MI_DocDetails 
{
    
    // DECLARATIONS
    // URL parameters
    public String attrelatedrecordstr {get;set;}
    public String attrelatedrecordid {get;set;}
    public String contactId {get;set;}
    public String citId {get;set;}
    public String coachId {get;set;}
    public Boolean coachuser {get;set;}
    
    // Attachment record
    public Id attrecid {get;set;}
    public String attrecpreviewid {get;set;}
    public Attachment attrec {get;set;}
    //public Document attrec {get;set;}
    public User CreatedByUser {get;set;}
    // Attachment Details record
    public String attrecidstr {get;set;}
    public AttachmentDetail__c attrelrec {get;set;}
    public List<String> sharedusersplit {get;set;}
    public List<User> sharedusers {get;set;}
    public List<User> allusers {get;set;}
    // flip variables
    public Boolean showeditdesc {get;set;}
    public Boolean showeditaccess {get;set;}
    // EDIT access - search variables
    public String searchstring {get;set;}
    public Boolean searchflag {get;set;}
    // wrapper
    public List<userWrapper> userlistwrapper {get;set;}
    // post-selection processing
    public String userconcat {get;set;}
    // Others
    public Blob attrecbodypreview {get;set;}
    public Id conuserid {get;set;}
    public String contactimg {get;set;}

    // CONSTRUCTOR
    public MI_DocDetails()
    {
        System.debug('ATO BB --- CONSTRUCTOR ');
// 101 - Id to be replaced with URL param
		//attrecidstr = ApexPages.currentPage().getParameters().get('docid');
        //System.debug('ATO doc id: '+attrecidstr);
        //attrecid=attrecidstr;
        //attrecid='00P35000000JeSuEAK';
        //attrecidstr='00P35000000JeSuEAK';
        //attrelatedrecordstr = 'a0c35000000Ccdb';
        //attrelatedrecordid = attrelatedrecordstr;
        
        attrelatedrecordstr = ApexPages.currentPage().getParameters().get('docid');
        attrelatedrecordid = attrelatedrecordstr;

        contactId=null;
        citId=null;
        coachId=null;
        contactId = ApexPages.currentPage().getParameters().get('citId');
        citId = ApexPages.currentPage().getParameters().get('citId');
        coachId = ApexPages.currentPage().getParameters().get('coachId');
        System.debug('contactId :'+contactId);
        System.debug('citId :'+citId);
        System.debug('coachId :'+coachId);
		
		coachuser=false;        
		if(coachId!='')
        {
            coachuser=true;
            System.debug('Coach ID is NULL');
        }
        
        if(attrelatedrecordid!=null)
        {
            attrecid = [Select Id from Attachment where Parent.Id=:attrelatedrecordid].Id;
            attrecidstr=attrecid;
            System.debug('ato attrecid initialization'+attrecid+attrecidstr);
        }
           
                     
        showeditdesc=false;
        showeditaccess=false;
        searchflag=false;
        searchstring=null;
        sharedusers=new List<User>();
        
        if(attrecid!=null)
        {
            System.debug('ATO BB --- attrecid!=null ');
            System.debug('ATO BB --- attrecid is '+attrecid);
            //attrec = [SELECT Id, Name, parent.Id, parent.Name, parent.Type, CreatedById, LastModifiedDate  
            attrec = [SELECT Id, Name, CreatedById, LastModifiedDate  
                      FROM Attachment 
                      WHERE Id=:attrecid];
            attrecbodypreview = [Select Body from Attachment where Id=:attrecid].Body;
            //System.debug('ATO BB --- attrec Id from query '+attrec.Id);
            CreatedByUser = [Select Id, Name from User where Id=:attrec.CreatedById];
            attrecpreviewid='/servlet/servlet.FileDownload?file='+attrecid;
            //System.debug('ato attrecpreviewid'+attrecpreviewid);
            if(attrecidstr!=null)
            {
                System.debug('ATO BB --- attrecidstr!=null');
// 101 - update query criteria once done                
                allusers=[Select Id, Name, Title, Profile.UserLicense.Name, Contact.ContactImageName__c
                		  FROM User
                          WHERE Profile.UserLicense.Name = 'Customer Community Login'
                          AND Profile.Name != 'Citizen Profile'
                          //AND Id !=: CreatedByUser.Id
                          ORDER BY Name 
                          LIMIT 100];
                if(attrecidstr!=null)
                {
	                sharedusers=preselectedusersfunc();
                    System.debug('Shared users size: '+sharedusers.size());
                    System.debug('Shared users '+sharedusers);
                }
            }
        }
        
        userlistwrapper=new List<userWrapper>();
        loadwrapper();
    }

    // FUNCTIONS
	public List<User> preselectedusersfunc()
    {
        
        attrelrec=[SELECT Id, Name, Related_Attachment_Id__c, Attachment_Description__c, SharedWith__c, Contact__r.Id
                       FROM AttachmentDetail__c 
                       WHERE Id=:attrelatedrecordid];
        System.debug('ATO Contact Id'+attrelrec.Contact__r.Id);
        conuserid = [Select Id from User where ContactId=:attrelrec.Contact__r.Id].Id;
        System.debug('conuserid: '+conuserid);
        if(attrelrec.SharedWith__c!=null)
        {
            System.debug('ATO _ SharedWith__c: '+attrelrec.SharedWith__c);
            sharedusersplit=(attrelrec.SharedWith__c).split(';');
            System.debug('ATO _ sharedusersplit: '+sharedusersplit);
            if(sharedusersplit!=null)
            {
	            return [Select Id, Name, Title, Contact.ContactImageName__c from User where Id IN :sharedusersplit ORDER BY Name];
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }
    }
    
    public void loadwrapper()
    {
        userlistwrapper.clear();
        for(User u: allusers)
        {
            userlistwrapper.add(new userWrapper(u,sharedusers));
        }
    }

	// WRAPPER CLASS    
    public class userWrapper
    {
        public User usr {get;set;}
        public Boolean selected {get;set;}
        public List<User> preselected {get;set;}
        public Integer selectedcount {get;set;}
        public userWrapper(User u, List<User> sharedusers)
        {
            usr = u;
            preselected =  sharedusers;
            selectedcount=0;
            for(User sh: preselected)
            {
                if(usr.Id==sh.Id)
                {
                    selectedcount++;
                }
            }
            if(selectedcount>0)
            {
                selected = true;
            }
            else
            {
                selected = false;
            }
        }
    }
    

    public void editaccess()
    {
        searchflag=false;
        searchstring=null;
        showeditaccess=true;    
    }
    public void saveaccess()
    {
        userconcat=conuserid+';';
        //userconcat=CreatedByUser.Id+';';
        for(userWrapper usrsave: userlistwrapper)
        {
            if(usrsave.selected==true)
            {
                System.debug('ATO---Selected users to save: '+usrsave.usr.Id+' - '+usrsave.usr.Name);
                userconcat=userconcat+usrsave.usr.Id+';';
            }
        }
        System.debug('ATO --- concat string: '+userconcat);
        attrelrec.SharedWith__c = userconcat;
        update attrelrec;
        
        searchflag=false;
        searchstring=null;
        sharedusers=preselectedusersfunc();
        loadwrapper();
        showeditaccess=false;        
    }
    public void cancelsaveaccess()
    {
        searchflag=false;
        searchstring=null;
        sharedusers=preselectedusersfunc();
        loadwrapper();
        showeditaccess=false;        
    }
    public void descshow()
    {
        showeditdesc=true;
    }
    public void descsave()
    {
        update attrelrec;
        showeditdesc=false;
    }
    public void descclear()
    {
        attrelrec.Attachment_Description__c=null;
        update attrelrec;
    }
    public PageReference deletedoc()
    {
        //contactId = ApexPages.currentPage().getParameters().get('citId');
        //citId = ApexPages.currentPage().getParameters().get('citId');
        //coachId = ApexPages.currentPage().getParameters().get('coachId');
        try
          {
// 101 - include once integrated              
            delete attrelrec;
            delete attrec;
          }
        Catch(Exception e)
          {
          }
        if(coachuser==true)
        {
            PageReference Coach = new Pagereference('/apex/MI_ClientFolio');
            Coach.getParameters().put('citId',citId);
            Coach.getParameters().put('contactId',contactId);
            Coach.getParameters().put('coachId',coachId);
            Coach.setRedirect(true);
            return Coach;
        }
        else
        {
	        PageReference Citizen = new Pagereference('/apex/MI_CitizenServices');
            Citizen.getParameters().put('citId',citId);
            Citizen.setRedirect(true);
            return Citizen;
        }
    }
    public PageReference redirectback()
    {
        if(coachuser==true)
        {
            PageReference Coach = new Pagereference('/apex/MI_ClientFolio');
            Coach.getParameters().put('citId',citId);
            Coach.getParameters().put('contactId',contactId);
            Coach.getParameters().put('coachId',coachId);
            Coach.setRedirect(true);
            return Coach;
        }
        else
        {
	        PageReference Citizen = new Pagereference('/apex/MI_CitizenServices');
            Citizen.getParameters().put('citId',citId);
            Citizen.setRedirect(true);
            return Citizen;
        }
    }
    
    public void clearsearchcontacts()
    {
        searchstring=null;
        searchflag=false;
        System.debug('ATO - search flag: '+searchflag);
        System.debug('ATO - search string: '+searchstring);
    }
    
    public void searchcontacts()
    {
        if(searchstring!=null)
        {
	        searchflag=true;
        }
        else
        {
            searchflag=false;
        }
    }
    
}