@isTest
public class FeedV2Test {

    @isTest public static void testConst(){
        FeedV2 f = new FeedV2();
        f.selectedDate = date.today()-2;
        f.selectedKeyword = 'Viral';
        List<String> lst = new List<String>();
        lst.add('a');
        lst.add('b');
        f.selectedCategory = lst;
        f.Search();
        f.clear();
    }
    
     @isTest public static void testConst2(){
        FeedV2 f = new FeedV2();
        f.Search();
         List<SelectOption> so = new List<SelectOption>();
	     so.add(new SelectOption('Test Option', 'Test Option'));
         f.categoryList = so;
    }
    
     @isTest public static void testConst3(){
        FeedV2 f = new FeedV2();
        f.Search();
         f.selectedDate = date.today()-70;
        f.selectedKeyword = 'Viral';
         f.Search();
    }
}