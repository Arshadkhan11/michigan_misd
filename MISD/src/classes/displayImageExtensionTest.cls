@isTest
public class displayImageExtensionTest { 
    
   @isTest static void getFileIdTest() {
        
        //Lst<News__c> nw = [select Id from News__c limit 1];
        //List<Attachment> attachedFiles = [select Id from Attachment where parentId = 'a0L35000000CdNKEA0' order By LastModifiedDate DESC limit 1];
        //ApexPages.StandardController sc = new ApexPages.standardController(nw);
        News__c nw = new News__c (Headline__c = 'Testing1');
	    insert nw;
	    ApexPages.StandardController sc = new ApexPages.standardController(nw);
       
        displayImageExtension em = new displayImageExtension(sc);
        String test1 = em.getFileId();
        
        em.BackToDash();
       em.PrevItem();
       em.NextItem();
    }
    
    @isTest static void getFileIdTest2() {
        
       
        News__c nw = new News__c (Headline__c = 'Testing1');
	    insert nw;
        Id id1 = nw.id;
        Attachment attach=new Attachment();   
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=id1;
        insert attach;
	    ApexPages.StandardController sc = new ApexPages.standardController(nw);
       
        displayImageExtension em = new displayImageExtension(sc);
        String test1 = em.getFileId();
        
        em.BackToDash();
       em.PrevItem();
       em.NextItem();
        
    }
    
   
}