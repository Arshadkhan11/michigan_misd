public class MI_Avatar {
    
    public String imagename{get; set;}
    Public String user_locale {get;set;}
    
    public MI_Avatar()
    {
        user_locale = UserInfo.getLocale();
    }
    
    public PageReference saveimage(){
        PageReference oPageRef = ApexPages.currentPage();
        String caseid,contactid,fromneeds,pricontactid,fromintake,sValue;
        Map<string,string> mapQueryParameters =apexpages.currentpage().getparameters();
        for(String sQueryParameterName : mapQueryParameters.keySet()){
            sValue = mapQueryParameters.get(sQueryParameterName);
            System.debug(sQueryParameterName+'=='+sValue);
            if(sQueryParameterName =='CaseId')
            {
              caseid =   sValue;
            }
            if(sQueryParameterName =='SelectedContactID')
            {
              contactid =   sValue;
            }
            if(sQueryParameterName =='FromNeeds')
            {
              fromneeds =   sValue;
            }
            if(sQueryParameterName =='PrimaryContactId')
            {
              pricontactid =   sValue;
            }
            if(sQueryParameterName == 'FromIntake')
            {
                fromintake = sValue;
            }
            
        } 
        if(contactid != null && contactid != '' && imagename != null && imagename != '')
        {
            Contact con = [Select ContactImageName__c ,id from Contact where Id=:contactid Limit 1];
            con.ContactImageName__c = imagename;
            update con;
        }
        if(fromneeds == 'Yes')
        {
           PageReference pg = Page.Needs_Assessment_Process;
           pg.getParameters().put('AssessmentConId',pricontactid);
           pg.getParameters().put('SelectedContactID',contactid); 
           pg.getParameters().put('PrimaryContactId',pricontactid); 
           pg.getParameters().put('FromAvatar','Yes');
           pg.getParameters().put('ToProfile','Yes');
           return pg;
        }
        else if(fromintake == 'Yes')
        {
            //PageReference pg = Page.MI_Common_App;
            PageReference pg = Page.MI_CommonApplication;
            pg.getParameters().put('SelectedContactID',contactid); 
            pg.getParameters().put('PrimaryContactId',pricontactid); 
            pg.getParameters().put('FromAvatar','Yes');
            pg.getParameters().put('ToProfile','Yes');
            pg.getParameters().put('AssessmentConId',pricontactid);
            pg.getParameters().put('CaseId',caseid);
            return pg;
        }
        else
        {
           PageReference pg = Page.MI_ClientFolio;
           pg.getParameters().put('SelectedContactID',contactid); 
           pg.getParameters().put('contactId',pricontactid); 
           pg.getParameters().put('FromAvatar','Yes');
           pg.getParameters().put('ToProfile','Yes');
           pg.getParameters().put('citId',pricontactid);
           pg.getParameters().put('coachId',UserInfo.getUserId());
           return pg;
        }      
    }
}