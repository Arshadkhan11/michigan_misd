public class MI_CA_Questions {
    //  public MI_CA_SubQuestions_Vatsan vatsansubwrapper =  new MI_CA_SubQuestions_Vatsan();
    //
    public list<MI_CA_SubQuestions> Question = new  list<MI_CA_SubQuestions>();
   	public String name;
    public string AccordianType;
    public String PgSecMembers;
    public MI_CA_Questions(string sectionobj,List<Question__c>  questions,String Category, string vProgramStr,Map<String, List<String>> AnswerMap) {
    	
      Map <String, String> quest = new Map <String, String> ();  
      List<Question__c> allsubquestions = new List<Question__c>(); 
      String Allsubstr = 'SELECT Program__c,id, Question_No__c, Question_Text__c, Question_Option__c, Data_Type__c, ParentQuestion__r.id, Category__c, PageName__c, PageSection__c, ConditionalAnswer__c, Columns__c, AccordianType__c,Answers__c,Members__c,OptionsDisplayed__c,SubSection__c,PDFName__c,SubTitle__c,SubSectionDetail__c,StyledDataType__c FROM Question__c WHERE Category__c = :Category AND ParentQuestion__c != \'\' AND Program__c includes ( '+ vProgramStr+ ') order by Question_No__c';
        allsubquestions = Database.query(Allsubstr);
      //allsubquestions = [SELECT Program__c,id,ParentQuestion__r.id,Question_No__c, Question_Text__c, Question_Option__c, Data_Type__c, Category__c, PageName__c, PageSection__c, Columns__c, AccordianType__c,Answers__c, ConditionalAnswer__c FROM Question__c where category__c = :Category and ParentQuestion__c != '' order by Question_No__c];
    	for(Question__c qtemp : questions )
        {
            if(!(quest.containsKey(qtemp.PageSection__c)))
            {
                quest.put(qtemp.PageSection__c, qtemp.AccordianType__c);
            }
        }
        
        name = sectionobj;
         if(quest.containsKey(sectionobj))
            {
                AccordianType = quest.get(sectionobj);
            }
        if(AnswerMap.containsKey(sectionobj))
        {
            List<String> strtmp = AnswerMap.get(sectionobj);
            PgSecMembers=strtmp[0];
        }
        
      for(Question__c subtemp : questions)
        {
            //string str = string.valueOf(sectionobj.get('PageSection__c')) ;
           // system.debug('sectionobj'+sectionobj);
            //system.debug('subtempPageSection__c'+subtemp.PageSection__c);
            if(sectionobj == subtemp.PageSection__c)
            {   
            MI_CA_SubQuestions subwrapper =  new MI_CA_SubQuestions(subtemp, allsubquestions, AnswerMap); 
            Question.add(subwrapper);   
              //  vatsantest.put(subtemp, vatsansubwrapper);
            }
       
        }
  //       String JSONStringzzz = JSON.serialize(vatsantest);
    //    system.debug('JSONStringzzz' + JSONStringzzz);
      // system.debug('vatsantest'+ vatsantest);   
      }    
}