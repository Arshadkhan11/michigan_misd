/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
@IsTest public with sharing class CommunitiesLandingControllerTest {
    @IsTest(SeeAllData=true) public static void testCommunitiesLandingController() {
        // Instantiate a new controller with all parameters in the page
        CommunitiesLandingController controller = new CommunitiesLandingController();
        controller.forwardToStartPage();
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id);
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile']; 
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id);
        System.runAs(u) {
            controller.forwardToStartPage();
        }
        Contact con1 = new Contact(FirstName = 'Test1', LastName = 'Test1',AccountId = acc.id);
        insert con1;
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Citizen Profile']; 
        User u1 = new User(Alias = 'test', Email='clc1@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p1.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc1@test.com', ContactId = con1.id);
        System.runAs(u1) {
            controller.forwardToStartPage();
        }
        Contact con2 = new Contact(FirstName = 'Test1', LastName = 'Test1',AccountId = acc.id);
        insert con2;
        Profile p2 = [SELECT Id FROM Profile WHERE Name='Partner Profile'];
        User u2 = new User(Alias = 'test', Email='clc2@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p2.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc2@test.com',ContactId = con2.id);
        System.runAs(u2) {
            controller.forwardToStartPage();
        }
     Contact con3 = new Contact(FirstName = 'Test1', LastName = 'Test1',AccountId = acc.id);
        insert con3;
         Profile p3 = [SELECT Id FROM Profile WHERE UserType = 'Guest ' limit 1];
        User u3 = new User(Alias = 'test', Email='clc3@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p3.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc3@test.com',ContactId = con3.id);
        insert u3;
        System.runAs(u3) {
            controller.forwardToStartPage();
      }
}
}