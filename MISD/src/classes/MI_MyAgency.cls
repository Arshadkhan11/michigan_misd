public class MI_MyAgency{

// variables    
    Public Agency__c Agent{get;set;}
    public String UserId{get;set;}
    public List<User> vAgencyUsers {get; set;}
    
// constructor    
    public MI_MyAgency()
    {
// initialization
        getAgents();
    }
public void getAgents() {

        if(Agent == null) 
        {
            UserId=[select Agency_ID__c from user where id =:UserInfo.getUserId()].Agency_ID__c; 
            if(UserId != '' && UserId != null)
            Agent = [SELECT Id, Agency_Name__c, Address__c, Contact_No__c,Services_Provided__c,Agency_Description__c, services_rendered__C FROM Agency__c where Id=:UserId];
            
            //code for getting the Partner users belonging to the same Agency
            vAgencyUsers = [select Id, Profile_Name__c, name, Phone, IsActive, Email, contact.ContactImageName__c, Title from user where Agency_ID__c = :UserId];
        }
    
    	
        
    }
// functions    
    
}