public class MI_CommonAppSubmitCtrl {

    public Case currentCase{get;set;}
    public String vCaseId{get;set;}
    public String vPrimConId{get;set;}
    public String currContactName {get;set;}
    public String fullURL {get;set;}
    public String profileName {get;set;}
    
    public MI_CommonAppSubmitCtrl(){
        vCaseId = apexpages.currentpage().getparameters().get('CaseId');
        vPrimConId = apexpages.currentpage().getparameters().get('AssessmentConId');
        currentCase = [SELECT id,Contact.FirstName, Contact.LastName FROM Case WHERE id = : vCaseId];
        currContactName = currentCase.Contact.FirstName + ' ' + currentCase.Contact.LastName;
        fullURL = URL.getSalesforceBaseUrl().toExternalForm() + '/MI_CommonApplication_PDF?CaseId='+vCaseId+'&AssessmentConId='+vPrimConId;
        profileName = [select Name from profile where id = :userinfo.getProfileId()].Name;
    }
}