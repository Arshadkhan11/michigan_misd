@isTest
public class MI_NotificationsTest { 
    

    @isTest public static void testSearch(){
         
               
        
        MI_Notifications n = new MI_Notifications();
        List<Notification__c> NotList=new List<Notification__c> ();
        
        
        n.NotifRec=null;
        n.NotifEditId=null;
        n.RecordsFound= true;
        n.searchLatest=null;
        String toSearch='arya';
        
        n.UserId=[select Id from user where id =:UserInfo.getUserId()].Id; 
        
        n.search1();
        n.searchLatest='select Notification_Description__c,Notification_Date_Time__c,Source__c,FeedbackURL__c, GoalType2__c,Parent_Id__c from Notification__c  where ((Notification_Description__c LIKE \'%' + string.escapeSingleQuotes(toSearch) + '%\') OR (Notification_Description__c LIKE \'%' + string.escapeSingleQuotes(toSearch) + '\') OR (Notification_Description__c LIKE \'' + string.escapeSingleQuotes(toSearch) + '%\'))';
        NotList=[select id,Notification_Description__c,Notification_Date_Time__c,Source__c,FeedbackURL__c,GoalType2__c,Parent_Id__c, CaseId__c, OwnerId from Notification__c
                         where Owner.id=:UserInfo.getUserId() Order by Notification_Date_Time__c DESC];
        n.NotList= Database.query(n.searchLatest);  
        n.search1();
       
        
        
        
        
        
        
    }	
    @isTest public static void testDrilldown(){
        List<Notification__c> NotList=new List<Notification__c> ();
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
       
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
       
     
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
            Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
            Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        Notification__c notif = new Notification__c(Notification_Description__c = 'Test',Notification_Date_Time__c=date.today(),Source__c = 'Required Document', Unread__c=true , CaseId__c = cs.id);
        insert notif;
        
        Notification__c notif1 = new Notification__c(Notification_Description__c = 'Test',Notification_Date_Time__c=date.today(),Source__c = 'Milestone', Unread__c=false , CaseId__c = cs.id);
        insert notif1;
        
        Notification__c notif2 = new Notification__c(Notification_Description__c = 'Test',Notification_Date_Time__c=date.today(),Source__c = 'Resource Feedback', Unread__c=true , CaseId__c = cs.id);
        insert notif2;
        
        Notification__c notif3 = new Notification__c(Notification_Description__c = 'Test',Notification_Date_Time__c=date.today(),Source__c = 'Bulletin Sent', Unread__c=true , CaseId__c = cs.id);
        insert notif3;
        
         MI_Notifications n = new MI_Notifications();   
        
        
        //n.NotifRec=null;
        n.NotifEditId=notif.id;
        n.searchLatest='test';
        n.Drilldown();
        n.NotifEditId = notif1.id;
        n.Drilldown();
        n.NotifEditId = notif2.id;
        n.Drilldown();
        n.NotifEditId = notif3.id;
        n.Drilldown();
        n.search1();
            
        
    PageReference testPage = Page.MI_Notificationsfull;
        Test.setCurrentPage(testPage);

   	Test.StartTest();
        n.search1();
	
       
  		 n.Drilldown();
        n.NotifRec = [Select Id,Notification_Description__c,Notification_Date_Time__c,Source__c, Unread__c, GoalType2__c, FeedbackURL__c, Parent_Id__c 
                            from Notification__c 
                            where Id=:n.NotifEditId];
         n.pagename=null;
        if(n.NotifRec.Source__c=='Resource Feedback'){
            n.pagename = 'MI_Feedback';
        }
        
        PageReference testPage1 = new PageReference('/apex/'+n.pagename);
        testPage1.setRedirect(true);
        testPage1.getParameters().put('FeedbackId',n.NotifRec.FeedbackURL__c);
        testPage1.getParameters().put('SPGoalName',n.NotifRec.GoalType2__c);
        testPage1.getParameters().put('bulletid',n.NotifRec.Parent_Id__c);
        n.NotifEditId=null;
        //return testPage1;

        

   		Test.StopTest();
		

    
    }
    
    
}