Public Class displayImageExtension { 

// Previous and Next - START
    public List<News__c> UserList {get;set;}
    public Integer CurrentRecordIter {get;set;}
    public Id CurrentRecordId {get;set;}
    public Id PreviousRecordId {get;set;}
    public Id NextRecordId {get;set;}
    public Boolean ShowPrevRecordBtn {get;set;}
    public Boolean ShowNextRecordBtn {get;set;}
    public List<Profile> ProfileName {get;set;}
    public String MyProflieName {get;set;}
// Previous and Next - END
    
    String recId;
    
    public displayImageExtension(ApexPages.StandardController controller) {
        recId = controller.getId();
        
        
        // PREV AND NEXT
        ShowPrevRecordBtn=false;
        ShowNextRecordBtn=false;
        CurrentRecordId=RecId;
        CurrentRecordIter=0;
        PreviousRecordId=null;
        NextRecordId=null;
        PrevNextList();
        //System.debug('ato usetlist size: '+UserList.size());
        if(UserList.size()>0)
        {
            if(CurrentRecordIter>=1)
            {
                ShowPrevRecordBtn=true;
                PreviousRecordId=UserList[CurrentRecordIter-1].Id;
            }
            if(CurrentRecordIter!=(UserList.size()-1))
            {
                ShowNextRecordBtn=true;
                NextRecordId=UserList[CurrentRecordIter+1].Id;
            }
            
        }
    }


    public PageReference PrevNextList()
    {
        
        
        ProfileName = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        MyProflieName = ProfileName[0].Name;
        String Query = null;
        if(MyProflieName == 'Success Coach Profile')
        {
            Query = 'Select id,Name,category__c, publish_from__c,Headline__c,Img__C,Body__C,published_on__c,Event_Date_Time__c from News__c where Publish_from__c <= TODAY and publish_Through__c >= LAST_N_DAYS:60 AND (Intended_audience__c INCLUDES (\'All Audiences\',\'Success Coaches\')) ORDER BY publish_from__C DESC';
        }
        else if(MyProflieName == 'Citizen Profile')
        {
            Query = 'Select id,Name,category__c, publish_from__c,Headline__c,Img__C,Body__C,published_on__c,Event_Date_Time__c from News__c where Publish_from__c <= TODAY and publish_Through__c >= LAST_N_DAYS:60 AND (Intended_audience__c INCLUDES (\'All Audiences\',\'Citizens\')) ORDER BY publish_from__C DESC';
        }
        else
        {
            Query = 'Select id,Name,category__c, publish_from__c,Headline__c,Img__C,Body__C,published_on__c,Event_Date_Time__c from News__c where Publish_from__c <= TODAY and publish_Through__c >= LAST_N_DAYS:60 AND (Intended_audience__c INCLUDES (\'All Audiences\')) ORDER BY publish_from__C DESC';
        }
        UserList = Database.Query(Query);
        for(Integer i=0; i<UserList.size(); i++)
        {
            if(UserList.size()>0)
            {
                if(UserList[i].Id==CurrentRecordId)
                {
                    CurrentRecordIter=i;
                    //System.debug('ato CurrentRecordId: '+CurrentRecordId+' :ato IdInLoop: '+CurrentRecordIter+' :ato UserList[i].Id: '+UserList[i].Id+' :END');
                }
            }
            
        }
        return null;
    }
    public PageReference PrevItem()
    {
        PageReference page = new Pagereference('/apex/MI_NewsDetails?id='+PreviousRecordId);
        page.setRedirect(true);
        return page;
    }
    public PageReference NextItem()
    {
        PageReference page = new Pagereference('/apex/MI_NewsDetails?id='+NextRecordId);
        page.setRedirect(true);
        return page;
    }    
    

    
    public String getFileId() {
        String fileId = '';
        List<Attachment> attachedFiles = [select Id from Attachment where parentId =:recId order By LastModifiedDate DESC limit 1];
        if( attachedFiles != null && attachedFiles.size() > 0 ) {
            fileId = attachedFiles[0].Id;
        }
        else
        {
           fileId = '0'; 
        }
        return fileId;    
    }

    public PageReference BackToDash()  
    {
        PageReference page = new Pagereference('/apex/CommunitiesLanding');
        page.setRedirect(true);
        return page;
    }

    
}