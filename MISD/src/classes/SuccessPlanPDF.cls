public class SuccessPlanPDF {
    public String SP_CaseId {get;set;}
    public Case contactCase{get;set;}
    Public String MyProfileName {get;set;}
    Public Contact MyProfile {get;set;}
    public Map<String, List<SP_Transactional__c>> catMap {get;set;}
    public Map<String, List<SP_Transactional__c>> catMapordered {get;set;}
    public Map<String, Map<String, List<SP_Transactional__c>>> MileSteps {get;set;}
    
    public SuccessPlanPDF(){
        SP_CaseId = ApexPages.currentPage().getParameters().get('SP_CaseId');
        if(SP_CaseId != '' && SP_CaseId != null)
        {
            SP_CaseId = SP_CaseId.subString(0,15);
            contactCase = [SELECT id, contact.Name, Active_Goal__c FROM Case WHERE id = : SP_CaseId];
            MyProfile = [select FirstName, LastName from Contact where Id = :contactCase.contact.Id]; 
        	MyProfileName = MyProfile.FirstName + ' ' + MyProfile.LastName + ' Family';
            catMap = new Map<String, List<SP_Transactional__c>>();
            List<SP_Transactional__c> domainlist =  [Select id, Name__c, Type__c, Status__c , Goal__r.id, Goal__r.Domain__c from SP_Transactional__c Where Type__c = 'Milestone' AND Status__c = true and CaseId__c = :SP_CaseId and SuccessPlanStatus__c = 'Active'];
            for(SP_Transactional__c sptemp : domainlist)
            {
                if(sptemp.Goal__r.Domain__c == 'Shelter' || sptemp.Goal__r.Domain__c == 'Food'){
                    if(catMap.containsKey('Basic Needs')){
                       catMap.get('Basic Needs').add(sptemp);     
                    }
                    else{
                        List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                        tempList.add(sptemp);
                        catMap.put('Basic Needs', tempList);
                    }
                }
                if(sptemp.Goal__r.Domain__c == 'Health Care' || sptemp.Goal__r.Domain__c == 'Mental Health' || sptemp.Goal__r.Domain__c == 'Substance Abuse'){
                    if(catMap.containsKey('Health & Wellness')){
                       catMap.get('Health & Wellness').add(sptemp);     
                    }
                    else{
                        List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                        tempList.add(sptemp);
                        catMap.put('Health & Wellness', tempList);
                    }
                }
                if(sptemp.Goal__r.Domain__c == 'Income' || sptemp.Goal__r.Domain__c == 'Transportation' || sptemp.Goal__r.Domain__c == 'Childcare' || sptemp.Goal__r.Domain__c == 'Legal'){
                    if(catMap.containsKey('Living & Working')){
                       catMap.get('Living & Working').add(sptemp);     
                    }
                    else{
                        List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                        tempList.add(sptemp);
                        catMap.put('Living & Working', tempList);
                    }
                }
                if(sptemp.Goal__r.Domain__c == 'Child Education' || sptemp.Goal__r.Domain__c == 'Adult Education' || sptemp.Goal__r.Domain__c == 'Financial Management' || sptemp.Goal__r.Domain__c == 'Life Skills' || sptemp.Goal__r.Domain__c == 'Parenting'){
                    if(catMap.containsKey('Education')){
                       catMap.get('Education').add(sptemp);     
                    }
                    else{
                        List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                        tempList.add(sptemp);
                        catMap.put('Education', tempList);
                    }
                }
                if(sptemp.Goal__r.Domain__c == 'Support Network' || sptemp.Goal__r.Domain__c == 'Community Involvement'){
                    if(catMap.containsKey('Community')){
                       catMap.get('Community').add(sptemp);     
                    }
                    else{
                        List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                        tempList.add(sptemp);
                        catMap.put('Community', tempList);
                    }
                }
            }
            catMapordered = new Map<String, List<SP_Transactional__c>>();
            if(catMap.containsKey('Basic Needs')) 
            {
                catMapordered.put('Basic Needs', catMap.get('Basic Needs'));
            }
            if(catMap.containsKey('Living & Working')) 
            {
                catMapordered.put('Living & Working', catMap.get('Living & Working'));
            }
            if(catMap.containsKey('Health & Wellness')) 
            {
                catMapordered.put('Health & Wellness', catMap.get('Health & Wellness'));
            }
            if(catMap.containsKey('Education')) 
            {
                catMapordered.put('Education', catMap.get('Education'));
            }
            if(catMap.containsKey('Community')) 
            {
                catMapordered.put('Community', catMap.get('Community'));
            }
            
            List<SP_Transactional__c> servlst = [Select id, Name__c, Type__c, Parent_Milestone__r.Id, Services_About_Us__c FROM SP_Transactional__c WHERE (Type__c = 'Service' or Type__c = 'Resource') and Status__c = true and CaseId__c = :SP_CaseId and SuccessPlanStatus__c = 'Active'];
			Map<String, List<SP_Transactional__c>> ServicesMap = new Map<String, List<SP_Transactional__c>>();
            for(SP_Transactional__c tmpserv : servlst)
            {
                if(ServicesMap.containsKey(tmpserv.Parent_Milestone__r.Id)){
                  	ServicesMap.get(tmpserv.Parent_Milestone__r.Id).add(tmpserv);
                }
                else{
                    List<SP_Transactional__c> servselected = new List<SP_Transactional__c>();
                    servselected.add(tmpserv);
                    ServicesMap.put(tmpserv.Parent_Milestone__r.Id,servselected);
                }
            }
            List<SP_Transactional__c> msteps = [Select id, Name__c, Type__c, Goal__r.id, Goal__r.Domain__c, Selected_Step__c FROM SP_Transactional__c WHERE Type__c = 'MilestoneSteps' and Selected_Step__c = true and CaseId__c = :SP_CaseId and SuccessPlanStatus__c = 'Active' Order BY End_date__c]; 
       		MileSteps = new Map<String, Map<String, List<SP_Transactional__c>>>();
            for(SP_Transactional__c tmpsteps : msteps)
            {
                Map<String, List<SP_Transactional__c>> tmpmap = new Map<String, List<SP_Transactional__c>>();
                if(MileSteps.containsKey(tmpsteps.Goal__r.id)){
                   if(ServicesMap.containsKey(tmpsteps.Id)){
                       	MileSteps.get(tmpsteps.Goal__r.id).put(tmpsteps.Name__c,ServicesMap.get(tmpsteps.Id)); 
                    }
                    else{
                        List<SP_Transactional__c> tmplst = new List<SP_Transactional__c>();
                        MileSteps.get(tmpsteps.Goal__r.id).put(tmpsteps.Name__c,tmplst); 
                    }
                }
                else{
                    if(ServicesMap.containsKey(tmpsteps.Id)){
                        tmpmap.put(tmpsteps.Name__c,ServicesMap.get(tmpsteps.Id));
                       	MileSteps.put(tmpsteps.Goal__r.id,tmpmap); 
                    }
                    else{
                        List<SP_Transactional__c> tmplst = new List<SP_Transactional__c>();
                        tmpmap.put(tmpsteps.Name__c,tmplst);
                        MileSteps.put(tmpsteps.Goal__r.id,tmpmap); 
                    }
                }
            }
        }
    }
}