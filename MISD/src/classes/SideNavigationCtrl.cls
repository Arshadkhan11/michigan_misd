public class SideNavigationCtrl {
    public List<SideNav__c> navlist {get;set;}
    
    public SideNavigationCtrl(){
        try
        {
            navlist = [Select Id, Sequence__c, Title__c, PageName__c, Iconpath__c from SideNav__c Order by Sequence__c ASC LIMIT 200];
        }
        catch (Exception e){
            System.debug('The following exception has occurred: ' + e.getMessage());                
      	}
    }

   
}