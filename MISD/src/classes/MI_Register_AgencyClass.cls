public class MI_Register_AgencyClass {

    public Agency__c NewAgency {get;set;}
    public Integer VerifyCode { get; set; }
    public String noAgencyMsg { get; set; }
    public Integer r1 { get; set; }
    public String agencyName {get;set;}
    
    //value of selected servies from front end.
    public string education {get;set;}
    public string employment {get;set;}
    public string family {get;set;}
    public string health {get;set;}
    public string job {get;set;}
    public string nutrition {get;set;}
    public string finance {get;set;}
    public string shelter {get;set;}
    public string wellness {get;set;}
    public string transport {get;set;}
    //checkboxes lists
    public List<String> educationService {get; set;}
    public List<SelectOption> educationOptions {get; set;}
    public List<String> employmentService {get; set;}
    public List<SelectOption> employmentOptions {get; set;}
    public List<String> familyLifeService {get; set;}
    public List<SelectOption> familyLifeOptions {get; set;}
    public List<String> healthService {get; set;}
    public List<SelectOption> healthOptions {get; set;}
    public List<String> jobSkillsService {get; set;}
    public List<SelectOption> jobSkillsOptions {get; set;}
    public List<String> nutritionService {get; set;}
    public List<SelectOption> nutritionOptions {get; set;}
    public List<String> financialsService {get; set;}
    public List<SelectOption> financialsOptions {get; set;}
    public List<String> shelterService {get; set;}
    public List<SelectOption> shelterOptions {get; set;}
    public List<String> wellnessService {get; set;}
    public List<SelectOption> wellnessOptions {get; set;}
    public List<String> transportationService {get; set;}
    public List<SelectOption> transportationOptions {get; set;}
    
    public List <Agency__c> agencyList { get; set;}
    
    
    public MI_Register_AgencyClass () {
    
    NewAgency = new Agency__c();
    noAgencyMsg = '';
    agencyName = '';
   //Repeat for 9 services     
    educationOptions = new List<SelectOption>();
    educationService = new List<String>(); 
    educationOptions.add(new SelectOption('Health Education','Health Education'));
    educationOptions.add(new SelectOption('After School Programs','After School Programs'));
    educationOptions.add(new SelectOption('Nutrition Education','Nutrition Education'));
    educationOptions.add(new SelectOption('Interpersonal Skills','Interpersonal Skills'));
    educationOptions.add(new SelectOption('College Programs','College Programs'));
    educationOptions.add(new SelectOption('Parenting Counseling','Parenting Counseling'));
    educationOptions.add(new SelectOption('Personal Financial Courses','Personal Financial Courses'));
    educationOptions.add(new SelectOption('Domestic Violence','Domestic Violence'));
    educationOptions.add(new SelectOption('Life Skills','Life Skills'));
    educationOptions.add(new SelectOption('Disabled Student Education','Disabled Student Education'));
    educationOptions.add(new SelectOption('Child Development','Child Development'));
    educationOptions.add(new SelectOption('Newborn Resources','Newborn Resources'));
    educationOptions.add(new SelectOption('Dimentia Education','Dimentia Education'));
    educationOptions.add(new SelectOption('Job Skills','Job Skills'));
    educationOptions.add(new SelectOption('GED/High School Instruction','GED/High School Instruction'));
    educationOptions.add(new SelectOption('Preschool to Middle School Programs','Preschool to Middle School Programs'));
    educationOptions.add(new SelectOption('Literacy Programs','Literacy Programs'));
    educationOptions.add(new SelectOption('Budgeting Courses','Budgeting Courses'));
    educationOptions.add(new SelectOption('Scouting Meetings','Scouting Meetings'));
    educationOptions.add(new SelectOption('English as a Second Language','English as a Second Language'));
    educationOptions.add(new SelectOption('Tutoring','Tutoring'));
    educationOptions.add(new SelectOption('Computer Skills','Computer Skills'));
    educationOptions.add(new SelectOption('Character & Leadership Development','Character & Leadership Development'));
    educationOptions.add(new SelectOption('Homeless Education','Homeless Education'));
    educationOptions.add(new SelectOption('Adult Basic Education','Adult Basic Education'));
    educationOptions.add(new SelectOption('Book Club','Book Club'));
        
    employmentOptions = new List<SelectOption>();
    employmentService = new List<String>(); 
    employmentOptions.add(new SelectOption('Job Skills','Job Skills'));
    employmentOptions.add(new SelectOption('Job Placement','Job Placement'));
    employmentOptions.add(new SelectOption('Work Experience','Work Experience'));
    employmentOptions.add(new SelectOption('Job Training','Job Training'));
    employmentOptions.add(new SelectOption('Job Readiness','Job Readiness'));
        
    familyLifeOptions = new List<SelectOption>();
    familyLifeService = new List<String>(); 
    familyLifeOptions.add(new SelectOption('Family Store','Family Store'));
    familyLifeOptions.add(new SelectOption('Domestic Violence','Domestic Violence'));
    familyLifeOptions.add(new SelectOption('Discount Prescriptions','Discount Prescriptions'));
    familyLifeOptions.add(new SelectOption('Financial Assistance','Financial Assistance'));
    familyLifeOptions.add(new SelectOption('Child Development','Child Development'));
    familyLifeOptions.add(new SelectOption('Child Care','Child Care'));
        
    healthOptions = new List<SelectOption>();
    healthService = new List<String>(); 
    healthOptions.add(new SelectOption('Prenatal Care','Prenatal Care'));
    healthOptions.add(new SelectOption('Healthcare Counseling','Healthcare Counseling'));
    healthOptions.add(new SelectOption('Chronic Condition Management','Chronic Condition Management'));
    healthOptions.add(new SelectOption('Hearing & Assistive Devices','Hearing & Assistive Devices'));
    healthOptions.add(new SelectOption('Senior Support','Senior Support'));
    healthOptions.add(new SelectOption('Dental Care','Dental Care'));
    healthOptions.add(new SelectOption('Visual Services','Visual Services'));
    healthOptions.add(new SelectOption('Hospice','Hospice'));
    healthOptions.add(new SelectOption('Urgent Care','Urgent Care'));
    healthOptions.add(new SelectOption('Free Vaccine Service','Free Vaccine Service'));
    healthOptions.add(new SelectOption('Hearing Services','Hearing Services'));
    healthOptions.add(new SelectOption('Long-Term Care','Long-Term Care'));
    healthOptions.add(new SelectOption('Mental Illness','Mental Illness'));
    healthOptions.add(new SelectOption('General Medical Care','General Medical Care'));
    healthOptions.add(new SelectOption('Medical Equipment','Medical Equipment'));
        
    jobSkillsOptions = new List<SelectOption>();
    jobSkillsService = new List<String>(); 
    jobSkillsOptions.add(new SelectOption('Sewing','Sewing'));
    jobSkillsOptions.add(new SelectOption('Drivers Education','Drivers Education'));
    jobSkillsOptions.add(new SelectOption('Job Readiness','Job Readiness'));
    jobSkillsOptions.add(new SelectOption('Job Skills','Job Skills'));
    jobSkillsOptions.add(new SelectOption('Job Training','Job Training'));
        
    nutritionOptions = new List<SelectOption>();
    nutritionService = new List<String>(); 
    nutritionOptions.add(new SelectOption('Food Pantry','Food Pantry'));
    nutritionOptions.add(new SelectOption('Food Delivery','Food Delivery'));
    nutritionOptions.add(new SelectOption('Food Coupons','Food Coupons'));
    nutritionOptions.add(new SelectOption('Community Gardens','Community Gardens'));
    nutritionOptions.add(new SelectOption('Soup Kitchens','Soup Kitchens'));
    nutritionOptions.add(new SelectOption('Nutrition Programs','Nutrition Programs'));
        
    financialsOptions = new List<SelectOption>();
    financialsService = new List<String>(); 
    financialsOptions.add(new SelectOption('Financial Courses','Financial Courses'));
    financialsOptions.add(new SelectOption('Budgeting Courses','Budgeting Courses'));
    financialsOptions.add(new SelectOption('Social Security Benefits','Social Security Benefits'));
    financialsOptions.add(new SelectOption('Unemployment Benefits','Unemployment Benefits'));
    financialsOptions.add(new SelectOption('Senior Housing','Senior Housing'));
    financialsOptions.add(new SelectOption('Social Services','Social Services'));
    financialsOptions.add(new SelectOption('Utility/Rental Assistance','Utility/Rental Assistance'));
   
    shelterOptions = new List<SelectOption>();
    shelterService = new List<String>(); 
    shelterOptions.add(new SelectOption('Emergency Shelter','Emergency Shelter'));
    shelterOptions.add(new SelectOption('New Home Owners','New Home Owners'));
    shelterOptions.add(new SelectOption('Free Household Items','Free Household Items'));
    shelterOptions.add(new SelectOption('Senior Support','Senior Support'));
    shelterOptions.add(new SelectOption('Rental Assistance','Rental Assistance'));
    shelterOptions.add(new SelectOption('Assisted Living','Assisted Living'));
    shelterOptions.add(new SelectOption('Domestic Violence Shelter','Domestic Violence Shelter'));
    shelterOptions.add(new SelectOption('Sexual Abuse Shelter','Sexual Abuse Shelter'));
    shelterOptions.add(new SelectOption('Housing Assistance','Housing Assistance'));
        
    wellnessOptions = new List<SelectOption>();
    wellnessService = new List<String>(); 
    wellnessOptions.add(new SelectOption('Intellectual & Developmental Disabilities','Intellectual & Developmental Disabilities'));
    wellnessOptions.add(new SelectOption('Adult Day Services','Adult Day Services'));
    wellnessOptions.add(new SelectOption('Adult Homecare Services','Adult Homecare Services'));
    wellnessOptions.add(new SelectOption('Wellness Center','Wellness Center'));
    wellnessOptions.add(new SelectOption('Mental Health','Mental Health'));
    wellnessOptions.add(new SelectOption('Substance Abuse','Substance Abuse'));
    wellnessOptions.add(new SelectOption('Community Activities','Community Activities'));
    wellnessOptions.add(new SelectOption('Rehabilitation','Rehabilitation'));
    wellnessOptions.add(new SelectOption('Physical & Speech Therapy','Physical & Speech Therapy'));
    wellnessOptions.add(new SelectOption('After School Programs','After School Programs'));
    wellnessOptions.add(new SelectOption('Life Skills','Life Skills'));
    wellnessOptions.add(new SelectOption('Fitness Activities','Fitness Activities'));
        
    transportationOptions = new List<SelectOption>();
    transportationService = new List<String>(); 
    transportationOptions.add(new SelectOption('Senior Citizen Transportation','Senior Citizen Transportation'));
    transportationOptions.add(new SelectOption('Local Bus Transit Service','Local Bus Transit Service'));
    transportationOptions.add(new SelectOption('Low Cost Public Transit','Low Cost Public Transit'));
    transportationOptions.add(new SelectOption('Non-Emergency Medical Transport','Non-Emergency Medical Transport'));
    transportationOptions.add(new SelectOption('Public Transportation','Public Transportation'));
        
    //agency list 
    agencyList = new List <Agency__c>();
    agencylist= [select Agency_Name__c,Verification_Code__c from Agency__c where Agency_Name__c <> null];
    system.debug('list of codes :'+agencylist);
        
      
   
   }
   
    
    public pageReference createAgency()
    {
        system.debug('inside create agency');
        system.debug('education service:'+ educationService);
        list<String> servicesProvided = new list<string>();
        uniqueNumber();
       
        if( !educationService.isEmpty() && education == 'true')
        {
           servicesProvided.add('Education:'+ String.join( educationService, ',' ) );
           NewAgency.Education_Service__c = String.join( educationService, '; ' );
           
        }
        else if( educationService.isEmpty() && education == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for education.' ));
             return null;
        }
        else
            NewAgency.Education__c = false;
        
        if( !employmentService.isEmpty() && employment == 'true')
         {
           servicesProvided.add('Employment:'+ String.join( employmentService, ',' ));
           NewAgency.Employment_Service__c = String.join( employmentService, '; ' );
         }
        else if( employmentService.isEmpty() && employment == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for employment.' ));
             return null;
        }
        else
            NewAgency.Employment__c = false;
        
        if( !familyLifeService.isEmpty() && family == 'true')
         {
           servicesProvided.add('Family Life:'+ String.join( familyLifeService, ',' ));
           NewAgency.family_Life_Service__c = String.join( familyLifeService, '; ' );
         }
        else if( familyLifeService.isEmpty() && family == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for Family Life.' ));
             return null;
        }
        else
            NewAgency.Family_Life__c = false;
        
        if( !healthService.isEmpty() && health == 'true')
           {
           servicesProvided.add('Health:'+ String.join( healthService, ',' ));
           NewAgency.health_Service__c = String.join( healthService, '; ' );
           }
        else if( healthService.isEmpty() && health == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for Health.' ));
             return null;
        }
        else
            NewAgency.Health__c = false;
        
         if( !jobSkillsService.isEmpty() && job == 'true')
           {
            servicesProvided.add('Job Skills:'+ String.join( jobSkillsService, ',' ));
            NewAgency.job_Skills_Service__c = String.join( jobSkillsService, '; ' );
           }
        else if( jobSkillsService.isEmpty() && job == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for Job Skills.' ));
             return null;
        }
        else
            NewAgency.Job_Skills__c = false;
        
        if( !nutritionService.isEmpty() && nutrition == 'true')
           {
           servicesProvided.add('Nutrition:'+ String.join( nutritionService, ',' ));
           NewAgency.nutrition_Service__c = String.join( nutritionService, '; ' );
           }
        else if( nutritionService.isEmpty() && nutrition == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for Nutrition.' ));
             return null;
        }
        else
            NewAgency.Nutrition__c = false;
        
        if( !financialsService.isEmpty() && finance == 'true')
           {
           servicesProvided.add('Financials:'+ String.join( financialsService, ',' ));
           NewAgency.financials_Service__c = String.join( financialsService, '; ' );
           }
        else if( financialsService.isEmpty() && finance == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for Financials.' ));
             return null;
        }
        else
            NewAgency.Financials__c = false;
        
        if( !shelterService.isEmpty() && shelter == 'true')
           {
           servicesProvided.add('Shelter:'+ String.join( shelterService, ',' ));
           NewAgency.shelter_Service__c = String.join( shelterService, '; ' );
           }
        else if( shelterService.isEmpty() && shelter == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for Shelter.' ));
             return null;
        }
        else
            NewAgency.Shelter__c = false;
        
        if( !wellnessService.isEmpty() && wellness == 'true')
            {
           servicesProvided.add('Wellness:'+ String.join( wellnessService, ',' ));
           NewAgency.wellness_Service__c = String.join( wellnessService, '; ' );
            }
        else if( wellnessService.isEmpty() && wellness == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for Wellness.' ));
             return null;
        }
        else
            NewAgency.Wellness__c = false;
        
        if( !transportationService.isEmpty() && transport == 'true')
          {
           servicesProvided.add('Transportation:'+ String.join( transportationService, ',' ));
           NewAgency.transportation_Service__c = String.join( transportationService, '; ' );
          }
        else if( transportationService.isEmpty() && transport == 'true')
        {
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please select atleast one service for Transportation.' ));
             return null;
        }
        else
            NewAgency.Transportation__c = false;
       
        NewAgency.Services_Provided__c = String.join(servicesProvided,'|');
        
            insert NewAgency;
            system.debug('new agency:'+ NewAgency);
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            List<String> email = new List<String>();
            
            email.add(NewAgency.Primary_Contact_Email__c);
            
            if(NewAgency.Secondary_Contact_Email__c != null)
            {
                email.add(NewAgency.Secondary_Contact_Email__c);
            }
            //Mail to the partner user
             mail.setToAddresses(email);
             mail.setSubject('Verification Code for Agency');
             mail.setPlainTextBody('Hello '+NewAgency.Primary_Contact_Name__c+',\n\tYou have registered an Agency and following is the verification code to be used to register Community Partner.\n\tVerification Code : '+NewAgency.Verification_Code__c);
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            
       
            Pagereference p = Page.successAgencyPage; 
            p.setredirect(true);
            return p;
    }
    
    public pagereference displayAgency()
    {
        system.debug('inside partner reg verify code:'+verifyCode); 
        system.debug('list of codes :'+agencylist);
        agencyName = noAgencyMsg = '';
        for(Agency__c a : agencyList )
        {
            if(a.Verification_Code__c == verifyCode)
               {
                 system.debug('code matches:'+verifycode);
                 system.debug('existing code:'+a.Verification_Code__c); 
                 agencyName = a.Agency_Name__C;
                 return null;
               }
        }
        noAgencyMsg = 'No Agency Found';
        return null;        
    }
    public pageReference partnerReg()
    {
        system.debug('inside partner reg verify code:'+verifyCode); 
        system.debug('list of codes :'+agencylist);
        noAgencyMsg = '';
        for(Agency__c a : agencyList )
        {
            if(a.Verification_Code__c == verifyCode)
               {
                 system.debug('code matches:'+verifycode);
                 system.debug('existing code:'+a.Verification_Code__c); 
                 Pagereference p = Page.MI_Citizen_Registration;    
                 p.getParameters().put('name','partner'); 
                 p.getParameters().put('id',a.id);
                 return p;
               }
        }
         noAgencyMsg = 'No Agency Found';
        return null;
    }
    public Boolean VerifycodeCheck(Integer arg)
    {
       
        for(Agency__c a : agencyList )
        {
            if(a.Verification_Code__c == arg)
            {
                return false;
            }
        }
        return true;
      }
    
    public void uniqueNumber()
    {
        generateNumber(); 
                  
        while( !VerifycodeCheck(r1) ) {
            generateNumber();
        }
        
        system.debug('inside unique numberc r1:'+r1); 
        NewAgency.Verification_Code__c = r1;
        system.debug('random val:'+r1);
        system.debug('agency val:'+NewAgency.Verification_Code__c);
                     
                   
    }
    public void generateNumber()
    {
        r1=randomWithLimit();
        
        while( r1 < 1000000000 )
                {
                    r1=randomWithLimit(); 
                }
        system.debug('10 digit number:'+r1);
    }
    
    public Integer randomWithLimit(){
        Integer randomInt;
       
            randomInt = (Integer)(10000000000.0 * Math.random());
        
        return randomInt;
    }
    
   
}