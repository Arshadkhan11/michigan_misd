public class MFSI_FormatDateTime {
    public DateTime dateTimeValue { get; set; }
    public String defined_format { get; set; }
    
    public String getTimeZoneValue() {
        
        if( dateTimeValue != null ) {
          if(defined_format != null)
          {
            //String localeFormatDT = dateTimeValue.format(defined_format, 'America/New_York');
            String localeFormatDT = dateTimeValue.format('MM/dd/YYYY') + ' ' + dateTimeValue.format('h:mm a');
            return localeFormatDT;}
        }
        return null;
    }
}