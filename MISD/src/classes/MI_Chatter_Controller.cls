global with sharing class MI_Chatter_Controller {

    
    //bugs
    public User CurrentChatUser {get;set;}
    
    
    public String addcomment {get;set;}
    
    public List<User> CommUsers {get;set;}
     
    public String CommentMention { get; set; }
    
    public String selectedUser { get; set; }
    
    public String selectedAccout {get;set;}
    
    public String PostMention { get; set; }

    public String PostText { get; set; }
    
    public String CommentText {get;set;}
       
    public id FeedElementId {get;set;}
   
    public ConnectApi.FeedElementPage feedElementPage {get;set;}
    public ConnectApi.FeedItemInput feedItemInput {get;set;}
    public ConnectApi.MentionSegmentInput mentionSegmentInput {get;set;}
    public ConnectApi.MessageBodyInput messageBodyInput {get;set;}
    public ConnectApi.TextSegmentInput textSegmentInput {get;set;}  
    public ConnectApi.LinkSegmentinput linkSegmentInput {get;set;} 
    


    public Object TextSegType { get{
     
      return ConnectApi.MessageSegmentType.Text;
      }
   }
   
   public Object LinkSegType { get{
     
      return ConnectApi.MessageSegmentType.Link;
      }
   }
   public Object MentionSegType {get{    
     
     return ConnectApi.MessageSegmentType.Mention;
     }
   }     
    

    public MI_Chatter_Controller()
    {

    }

//
    
    
      public void refresh()
    {
       
       
       
       
       feedElementPage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(null, ConnectApi.FeedType.UserProfile,Userinfo.getUserId());
                  
       try{
           CommUsers = [SELECT id, Name FROM User WHERE User.Profile.UserLicense.Name LIKE '%Salesforce%'];
       }Catch(exception e){}
        
        
        System.Debug('after refresh');
     
    }
   
   
    public void AddPost()
    {
     
      List<String> MentionUsers = PostMention.Split(';');
      system.debug('Mentions:'+MentionUsers);
      
      feedItemInput = new ConnectApi.FeedItemInput();

      mentionSegmentInput = new ConnectApi.MentionSegmentInput();

      messageBodyInput = new ConnectApi.MessageBodyInput();

      textSegmentInput = new ConnectApi.TextSegmentInput();

      messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
      
      User u;
      SendSMSForChatter sms;
      try{
           for( String s: MentionUsers){
            u = [select id,name from User where name like :s];
            mentionSegmentInput.id = u.id;
            system.Debug('Mentions:'+ mentionSegmentInput.id);
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            mentionSegmentInput = new ConnectApi.MentionSegmentInput();

           }
      }catch(Exception e){
        u = null;
       }
       
     system.debug('PostText :'+PostText );
     if(PostText != null && MentionUsers != null)
     {
     
      textSegmentInput.text = PostText;
      messageBodyInput.messageSegments.add(textSegmentInput);

      feedItemInput.body = messageBodyInput;
      feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
      feedItemInput.subjectId = UserInfo.getUserId();

      System.Debug('Inside posting'+feedItemInput);
      ConnectApi.FeedElement feedElement; 
      
      try{
      feedElement = ConnectApi.ChatterFeeds.postFeedElement(null, feedItemInput);
      }catch(Exception e){}
      System.Debug('after posting:'+feedElement);
      
      
     }
       
      
       PostText =''; PostMention='';  
      refresh();
     
   }
   
   public void AddComment()
   {
         
     System.Debug('entring comment:');
     ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();

     mentionSegmentInput = new ConnectApi.MentionSegmentInput();

     messageBodyInput = new ConnectApi.MessageBodyInput();

     textSegmentInput = new ConnectApi.TextSegmentInput();

     messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
     
     system.Debug('comment:'+CommentText);
       
     
     textSegmentInput.text = CommentText;
     messageBodyInput.messageSegments.add(textSegmentInput);
     
           
     commentInput.body = messageBodyInput;
    
      
     if(feedElementId != null && CommentText != null)
     {
     
     ConnectApi.Comment commentRep = ConnectApi.ChatterFeeds.postCommentToFeedElement(null, feedElementId, commentInput, null);
     ConnectApi.FeedElement commentFeed = ConnectApi.ChatterFeeds.getFeedElement(null, feedElementId);

     List<ConnectApi.MessageSegment> messageSegments = commentFeed.body.messageSegments;
        
                for(ConnectApi.MessageSegment m : messageSegments )
               {   
                    if (m instanceof ConnectApi.MentionSegment)
                    {  system.debug('Mention seg:'+m);
                       ConnectApi.MentionSegment mentionSegment = (ConnectApi.MentionSegment) m;

                    }
                }

     }
      CommentText = '';
      refresh();
   } 
  
}