@isTest
public with sharing class SearchHouseholdCtrltest{
static testMethod void test_SearchHouseholdCtrl(){
Account acc = new Account (Name = 'newAcc');  
insert acc;
Contact con = new Contact(FirstName = 'Test', LastName = 'Johnson',AccountId = acc.id ,Birthdate=system.today(), Martial_Status__c = 'Married');
insert con;
SearchHouseholdCtrl c = new SearchHouseholdCtrl();
c.getSearchOptions();
Document document;
document = new Document();
document.Body = Blob.valueOf('[{"contactSSN" : "123456791","contactDOB" : "01/10/1970","contactFName" : "Scott","contactLName" : "Johnson","contactBenefits" : "Inactive","ExternalId" : "CON"}]');
document.ContentType = 'application/pdf';
document.DeveloperName = 'my_document';
document.IsPublic = true;
document.Name = 'Enquiry Reference Data';
document.FolderId = [select id from folder where name = 'SMS Magic'].id;
insert document;
PageReference pageRef = Page.SearchHousehold;
Test.setCurrentPage(pageRef);
String searchQuery = 'SELECT id, FirstName, LastName,Social_Security_Num__c, Birthdate, ExternalId__c FROM Contact ';
System.currentPageReference().getParameters().put('date','(19|20)\\d\\d)');
c.advancedSearch();
System.currentPageReference().getParameters().put('ssn','1234567890');
System.currentPageReference().getParameters().put('fname','Test');
System.currentPageReference().getParameters().put('lname','Test');
System.currentPageReference().getParameters().put('date','');
c.advancedSearch();
System.currentPageReference().getParameters().put('ssn','');
c.advancedSearch();
System.currentPageReference().getParameters().put('searchField','Date of Birth');
System.currentPageReference().getParameters().put('searchValue','Test');
c.searchDB();
System.currentPageReference().getParameters().put('searchField','SSN');
System.currentPageReference().getParameters().put('searchValue','Test');
c.searchDB();
System.currentPageReference().getParameters().put('searchField','Name');
System.currentPageReference().getParameters().put('searchValue','Test');
c.searchDB();
System.currentPageReference().getParameters().put('searchField','Date of Birth');
System.currentPageReference().getParameters().put('searchValue','system.today()');
c.searchDB();
System.currentPageReference().getParameters().put('searchField','SSN');
System.currentPageReference().getParameters().put('searchValue','');
c.searchDB();
System.currentPageReference().getParameters().put('ssn','');
System.currentPageReference().getParameters().put('fname','');
System.currentPageReference().getParameters().put('lname','');
System.currentPageReference().getParameters().put('date','');
c.advancedSearch();
}
}