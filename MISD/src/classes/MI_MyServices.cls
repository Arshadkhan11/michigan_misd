public class MI_MyServices 
{
    public List<MI_CommonAppService__c> service {get;set;}
    public Id citId {get;set;}
    public Id caseId {get;set;}
    public List<CaseContactRole> SecondaryContactsList{get;set;}
    public MI_MyServices()
    {
		service = [select Contact__c,Processed__c,ServiveType__c,Status__c,Submitted__c,Id,RedeterminationDate__c,currentDate__c from MI_CommonAppService__c where CommonApplicationId__r.Status__c='Active' AND Contact__c=:ApexPages.currentPage().getParameters().get('citId')];
        citId = ApexPages.currentPage().getParameters().get('citId');
        Id currentContactCaseId = [Select Id from Case where ContactId=:citId].Id;
        caseId=currentContactCaseId;
        SecondaryContactsList = [SELECT Id, CasesId, ContactId, Role, Contact.ContactImageName__c, Contact.Name, Contact.Id, Contact.Children__c, Contact.Age__c, Contact.Birthdate 
                                                            FROM CaseContactRole
                                                            WHERE CasesId = :currentContactCaseId and ContactId != :citId and Contact.Age__c !=null];
    }
}