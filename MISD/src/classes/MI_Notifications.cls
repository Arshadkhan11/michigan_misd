global with sharing class MI_Notifications {

   
    public String UserId{get;set;}
    public String searchLatest{get;set;}
    public Id NotifEditId {get;set;}
	public string searchNoti {get;set;}
    public string pagename {get;set;}
    public List<Notification__c> NotList {get;set;}
    //public String SearchString {get;set;}
    //public boolean SearchFlag {get;set;}
    //public List<Notification__c> searchQuery {get;set;}
    //public list <Notification__c> nf {get;set;}  
    public Notification__c NotifRec {get;set;}
    public Boolean RecordsFound {get;set;}

    public Id CaseContactId {get;set;}
	
    
    public MI_Notifications()
    {
        NotifEditId=null;
        RecordsFound=true;
        UserId=[select Id from user where id =:UserInfo.getUserId()].Id; 
            if(UserId != '' && UserId != null)
            {
                NotList=[select id,Notification_Description__c,Notification_Date_Time__c,Source__c,FeedbackURL__c,GoalType2__c,Parent_Id__c, CaseId__c, OwnerId from Notification__c
                         where Owner.id=:UserInfo.getUserId() Order by Notification_Date_Time__c DESC];
            }
        //System.debug('hello');
        //System.debug('size'+NotList.size()); 
        if(NotList.isEmpty())
        {
            RecordsFound=false;
        }
        else
        {
            RecordsFound=true;
        }
        
   }

    
    public void search1()
    {  
        /*        searchLatest='select Notification_Description__c,Notification_Date_Time__c,Source__c,FeedbackURL__c, GoalType2__c,Parent_Id__c from Notification__c where ((Notification_Description__c LIKE \'%' + string.escapeSingleQuotes(searchNoti) + '%\') OR (Notification_Description__c LIKE \'%' + string.escapeSingleQuotes(searchNoti) + '\') OR (Notification_Description__c LIKE \'' + string.escapeSingleQuotes(searchNoti) + '%\')) AND (Owner.id=:+UserInfo.getUserId()) Order by Notification_Date_Time__c DESC ';*/

        searchLatest='select Notification_Description__c,Notification_Date_Time__c,Source__c,FeedbackURL__c, GoalType2__c,Parent_Id__c, CaseId__c, OwnerId from Notification__c where Owner.id=\'' + UserInfo.getUserId() + '\' AND ((Notification_Description__c LIKE \'%' + string.escapeSingleQuotes(searchNoti) + '%\') OR (Notification_Description__c LIKE \'%' + string.escapeSingleQuotes(searchNoti) + '\') OR (Notification_Description__c LIKE \'' + string.escapeSingleQuotes(searchNoti) + '%\')) ORDER BY Notification_Date_Time__c DESC ';
        
		NotList= Database.query(searchLatest);  
        
        if(NotList.isEmpty())
        {
            RecordsFound=false;
        }
        else
        {
            RecordsFound=true;
        }
        //System.debug('Search has complted -- Records found?: '+RecordsFound+' -- Size of Not List  -- '+NotList.size());
   	}
//427
	public PageReference Drilldown()
    {
        try
        {
            //System.debug('ATO--- TRY');
            if((NotifEditId!=null))
            {
            //System.debug('ATO--- Inside if'+NotifEditId);                
                NotifRec = [Select Id,Notification_Description__c,Notification_Date_Time__c,Source__c, Unread__c, GoalType2__c, FeedbackURL__c, Parent_Id__c, CaseId__c, OwnerId
                            from Notification__c 
                            where Id=:NotifEditId];
            //System.debug('ATO--- TRY1'+NotifRec.Source__c);
                NotifRec.Unread__c=false;
                update NotifRec;
            	
            }
        }
        catch(Exception e)
        {
        	System.debug('Exception: '+e);    
       	}
        
        pagename = 'CommunitiesLanding';
        if(NotifRec.Source__c=='Resource Feedback')
        {
            pagename = 'MI_Feedback';
        }
        else if (NotifRec.Source__c=='Milestone')
        {
            pagename = 'MI_CitizenDashboard';
        }
        else if(NotifRec.Source__c=='Bulletin Sent')
        {
            pagename = 'MI_Bulletin_View';
        }
        else if(NotifRec.Source__c=='Required Document')
        {
            pagename = 'MI_CommonApplication';
        }
                
        PageReference page = new PageReference('/apex/'+pagename);
        page.setRedirect(true);
        page.getParameters().put('FeedbackId',NotifRec.FeedbackURL__c);
        page.getParameters().put('SPGoalName',NotifRec.GoalType2__c);
        page.getParameters().put('bulletid',NotifRec.Parent_Id__c);
        if(NotifRec.Source__c=='Required Document')
        {
        	CaseContactId=[Select Id,ContactId from Case where Id=:NotifRec.CaseId__c].ContactId;
            page.getParameters().put('CaseId',NotifRec.CaseId__c);
            page.getParameters().put('AssessmentConId',CaseContactId);
            page.setAnchor('tabs-0-pagetabs-summary');
            //page.getParameters().put('CaseId','50035000000cvdK');
            //page.getParameters().put('AssessmentConId','00335000002RKic');
            //page.getAnchor().put('#tabs-0-pagetabs-summary');
            //page.getParameters().put('DocNotif','true');
            //page.getParameters().put('CaseId',NotifRec.'50035000000cnybAAA');
            //page.getParameters().put('AssessmentConId','00335000002K9B7AAK');
        }
        NotifEditId=null;
        return page;
        //return null;
        
    }

   
    
}