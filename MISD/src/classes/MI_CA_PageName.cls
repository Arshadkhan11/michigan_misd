public class MI_CA_PageName {
	public string Category;
    public Integer seq;
    public list<MI_CA_PageSections> PageName = new  list<MI_CA_PageSections>();
    public List<Integer> currPageSeq;
    public Map<Integer, String> vPageSeqNew;
    public Map<Integer, String> vPageSeqTmp;
    
    public MI_CA_PageName(String vProgramStr,String eachcategory, Integer vSeq,Map<String, List<String>> AnswerMap, Map<String, Integer> vPageSeq){
        currPageSeq = new List<Integer>();
        vPageSeqTmp = new Map<Integer, String>();
        vPageSeqNew = new Map<Integer, String> ();
         Category = eachcategory;
        seq = vSeq;
        list<AggregateResult> pagenames;
         String AllStr = 'SELECT PageName__c FROM Question__c WHERE ParentQuestion__c = \'\' AND Program__c includes ( '+ vProgramStr+ ') AND Category__c  = :Category GROUP BY PageName__c';
        pagenames = Database.query(AllStr);        
        System.debug('vPageSeq' + vPageSeq);
        String pagenamezz;
        for(AggregateResult page : pagenames)
        {
            system.debug('PageName' + page.get('PageName__c'));
            string pagenameobj = string.valueOf(page.get('PageName__c')) ;
            Integer pageSq = vPageSeq.get(pagenameobj);
            System.debug('pageSq:' + pageSq + '|' + 'pagenameobj' + pagenameobj);
            currPageSeq.add(pageSq);
            vPageSeqTmp.put(pageSq, pagenameobj);
            //MI_CA_PageSections pagenamez =  new MI_CA_PageSections(Category,pagenameobj,vProgramStr,AnswerMap,vPageSeq); 
            //PageName.add(pagenamez);
			//pagenamezz = pagenameobj;        
        } 
        currPageSeq.sort();
        
        for(Integer i : currPageSeq)
        {
            vPageSeqNew.put(i, vPageSeqTmp.get(i));
        }
        
        for(Integer seq : vPageSeqNew.keyset())
        {
         	MI_CA_PageSections pagenamez =  new MI_CA_PageSections(Category,vPageSeqNew.get(seq),vProgramStr,AnswerMap,vPageSeq); 
            PageName.add(pagenamez);
			pagenamezz = vPageSeqNew.get(seq);    
        }
    }
}