/**
 * An apex page controller that supports self registration of users in communities that allow self registration
 */
public class CommunitiesSelfRegController {

    private static string secret = '6LelKAsTAAAAAEsqbcAOA6BCgjZl7SuRnd8x2Nd0';
    public string publicKey { get { return '6LelKAsTAAAAAHx0GwNEjHkrstk5x-zkBM6qebYf' ; }}
    private static string baseUrl = 'https://www.google.com/recaptcha/api/siteverify';
    public string challenge {get; set;} { challenge = null; }
    public string response {get; set; } { response = null; }
    public Boolean correctResponse { get; private set; } { correctResponse = false; }
    public String firstName {get; set;}
    public String lastName {get; set;}
    public String email {get; set;}
    public string userName {get;set;}
    public String password {get; set {password = value == null ? value : value.trim(); } }
    public String confirmPassword {get; set { confirmPassword = value == null ? value : value.trim(); } }
    public String communityNickname {get; set { communityNickname = value == null ? value : value.trim(); } }
    
    public CommunitiesSelfRegController() {}
    
    public pageReference captchaValidation() {
        system.debug('response'+response);
        if ( response == null ) {
            return null;
        }
        HttpResponse r = makeRequest( baseUrl ,'secret='+ secret +
                                      '&remoteip=' + remoteHost +
                                      '&response=' + response
                         );
        if ( r != null ) {
            correctResponse = ( r.getBody().contains('true') );
            system.debug('correctResponse:'+correctResponse);
        }
        if(correctResponse == false)
        {
            PageUtility.showError('Invalid Captcha. Please retry.');
        }
        return null;
    }
    
    public static HttpResponse makeRequest(string url, string body) {
        HttpRequest req = new HttpRequest();
        HttpResponse response = null;
        req.setEndpoint( url );
        req.setMethod('POST');
        req.setBody ( body);
        try {
            Http http = new Http();
            response = http.send(req);
        }catch( System.Exception e) {
            System.debug('ERROR: '+ e);
        }
        return response;
    }
    
    public string remoteHost {
        get{
            string ret = '127.0.0.1';
            // also could use x-original-remote-host
            map<string , string> hdrs = ApexPages.currentPage().getHeaders();
            if ( hdrs.get('x-original-remote-addr') != null)
                ret = hdrs.get('x-original-remote-addr');
            else if ( hdrs.get('X-Salesforce-SIP') != null)
                ret = hdrs.get('X-Salesforce-SIP');
            return ret;
        }
    }
    
    private boolean isValidPassword() {
        return password == confirmPassword;
    }
    
    public PageReference registerUser() {
    
           // it's okay if password is null - we'll send the user a random password in that case
        if (!isValidPassword()) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, Label.site.passwords_dont_match);
            ApexPages.addMessage(msg);
            return null;
        }    

        String profileId = null; // To be filled in by customer.
        String roleEnum = null; // To be filled in by customer.
        String accountId = ''; // To be filled in by customer.
        
        User u = new User();
        u.Username = userName;
        u.Email = email;
        u.FirstName = firstName;
        u.LastName = lastName;
        u.CommunityNickname = userName;
        u.ProfileId = profileId;
        
        String userId;

        try {
            userId = Site.createExternalUser(u, accountId, password);
        } catch(Site.ExternalUserCreateException ex) {
            List<String> errors = ex.getDisplayMessages();
            for (String error : errors)  {
                if(ex.getMessage().contains('User already exists'))
                    {
                        PageUtility.showError('User with the same User Name exists.Please try with a new User Name.');
                    }
                    else if(ex.getMessage().contains('portal user already exists for contact'))
                    {
                        PageUtility.showError('Portal user already exists for contact. Please login with the existing login details.');
                    }
                    else if(ex.getMessage().contains('That nickname already exists. Enter a unique nickname.'))
                    {
                        PageUtility.showError('User with the same User Name exists.Please try with a new User Name.');
                    }
                    else
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
                    }
              //    PageUtility.showError('User with the same User Name exists. Please try with a new User Name.');
            }
            
            // This message is used for debugging. Do not display this in the UI to the end user.
            // It has the information around why the user creation failed.
            System.debug(ex.getMessage());
        }
        
        if (userId != null) { 
            if (password != null && password.length() > 1) {
                return Site.login(userName, password, ApexPages.currentPage().getParameters().get('startURL'));
            }
            else {
                PageReference page = System.Page.CommunitiesSelfRegConfirm;
                page.setRedirect(true);
                return page;
            }
        }
        return null;
    }
}