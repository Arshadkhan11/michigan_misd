public class MI_Attach_Upload
{
    public boolean delFlag{get;set;}
    public transient String attBody_c{get;set;}
    public String typ_c{get;set;}
    public String filename_c{get;set;}
    public String fileNamescont {get;set;}

    public transient List<Attachment> DocList{get;set;}
    public transient List<aAttachment> attList {get;set;}

    public Id temp2{get;set;}
    public Id ConId{get;set;}
   	public Id CochId{get;set;}
	public Id caseId{get;set;}
	public MI_Attach_Upload()
    {
        system.debug('param --- '+ apexpages.currentpage().getparameters().get('citId'));
        
        delFlag=false;
        //DocList = getDocs();
        attList = getAttachments();
    }
    
	public void ref_div()
    {
        system.debug('BBB-Inside');
        //DocList = getDocs();
        attList = getAttachments();
    }
    public void deletecontroller()
    {
        List<Id> fileNamescontarray = fileNamescont.split(',');
        for(String fileN : fileNamescontarray)
        {
			List<AttachmentDetail__c> delOBjs_child = [SELECT Id FROM AttachmentDetail__c WHERE Id IN:fileNamescontarray];
			delete delOBjs_child;
			List<Attachment> delOBjs = [SELECT Id FROM Attachment WHERE ParentId IN:fileNamescontarray];
			delete delOBjs;
		}
      	//DocList = getDocs();
		attList = getAttachments();
    }

    public List<aAttachment> getAttachments()
    {
		ConId = apexpages.currentpage().getparameters().get('citId');
        CochId = apexpages.currentpage().getparameters().get('coachId');
        attList=new List<aAttachment>();       
        List <Attachment> attWrap = new List<Attachment>();
        List <AttachmentDetail__c> attDetWrap = new List<AttachmentDetail__c>();
		if(CochId!=null)
        {
            String qq = '%'+CochId+'%';
        	attDetWrap = [select Id,SharedWith__c,CreatedDate,CreatedDateFormattted__c,CreatedDateNew__c from AttachmentDetail__c where Contact__c=:apexpages.currentpage().getparameters().get('citId') and SharedWith__c like :qq order by CreatedDate];  
        }
        else
        {
        	attDetWrap = [select Id,SharedWith__c,CreatedDate,CreatedDateFormattted__c,CreatedDateNew__c from AttachmentDetail__c where Contact__c=:apexpages.currentpage().getparameters().get('citId') order by CreatedDate];
        }
		system.debug('BBB---'+attDetWrap.size());
        String[] IDSS = new List<String>();
        for(AttachmentDetail__c addd:attDetWrap)
        {
            system.debug('BBB1---'+IDSS.size());
			IDSS.add(addd.Id);
        }
       	system.debug('BBB2---'+IDSS.size());
        attWrap=[select id,name,ParentId from Attachment where ParentId IN:IDSS];

        for(AttachmentDetail__c addd:attDetWrap)
        {
            for(Attachment attt:attWrap)
            {
                if(attt.ParentId==addd.Id)
                {
                    String countppl = 'None';
                    List<String> cc = addd.SharedWith__c.split(';');
                    Integer c2 = cc.size()-1;
                    if(c2==1)
                    {
                       countppl = c2+' Person'; 
                       cc = null;
                       c2 = null;
                    }
                    if(c2>1)
                    {
                       countppl = c2+' People'; 
                       cc = null;
                       c2 = null;
                    }
					attList.add(new aAttachment(attt,addd,countppl));
                }
            		
            }
        }
        return attList;
    }
    
    public class aAttachment
    {
        public Attachment aa {get;set;}
        public AttachmentDetail__c ad {get;set;}
        public String cnt {get;set;}
        public aAttachment(Attachment a1,AttachmentDetail__c d1,String c1)
        {
			aa = a1;
            ad =d1;
            cnt=c1;
        }
    }

    public List<Attachment> getDocs()
    {
		ConId = apexpages.currentpage().getparameters().get('citId');
        CochId = apexpages.currentpage().getparameters().get('coachId');
        system.debug('CochId--'+CochId);
       	//caseId = [select id from Case where Contact.Id=:ConId LIMIT 1].id;
		//List<Attachment> Doc = [select Name, Id, OwnerId from Attachment where OwnerId=:temp2 and ParentId=:caseId order by CreatedDate];
		List<Attachment> Doc = [select Name, Id, OwnerId from Attachment where ParentId=:ConId order by CreatedDate];
        return Doc;
    }
    public void sendParamtoController()
    {
		//do nothing
    }
    public void upld()
    {
        	try
            {                
                transient Attachment toAdd = new Attachment();   
                toAdd.OwnerId = UserInfo.getUserId();
                toAdd.Body = EncodingUtil.base64Decode(attBody_c);
                toAdd.Name = filename_c;
                toAdd.ContentType = typ_c;
                //toAdd.FolderId='00D350000000Rzt'
                if(filename_c != '')
                {
/*                  insert toAdd;
                	AttachmentDetail__c toAddDet = new AttachmentDetail__c();
                    toAddDet.Related_Attachment_Id__c=toAdd.Id;
	                if(CochId==null)                    
                    {toAddDet.SharedWith__c='NONE';}
                    else							                  
                    {toAddDet.SharedWith__c=CochId+';';}
                    insert toAddDet;*/
                	AttachmentDetail__c toAddDet = new AttachmentDetail__c();
					String sharewith;
					if(ConId!=null)
                    {                    
	                    sharewith = [select id from user where ContactId=:ConId limit 1].Id+';';
                    }
                    if(CochId!=null)
                    {
                        sharewith = sharewith+UserInfo.getUserId()+';';
                    }
                    toAddDet.SharedWith__c=sharewith;
                	toAddDet.Contact__c=ConId;                    
                    insert toAddDet;
					toAdd.ParentId=toAddDet.Id;
	                insert toAdd;                    
                    
                    
					toAdd.Body = null;
                    toAdd=null;
                    attBody_c = null;
                } 
				//DocList = getDocs();
				attList = getAttachments();
            }
        	catch(Exception e)
            {
				System.debug('The following exception has occurred: ' + e.getMessage());                
            }

    }    
}