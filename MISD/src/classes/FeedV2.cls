public class FeedV2 {
    
    public List<String> selectedCategory {get;set;}
    
    public List<News__c> UserList {get;set;}
   
    public date selectedDate {get;set;}
    
    public String selectedKeyword {get; set;}
    
    public boolean flag {get;set;}
    
    public List<SelectOption> categoryList {get;set;}
    
   
  public FeedV2(){
     selectedCategory = new List<string>();
                 
                  flag = false;
                  getUserFeedList(); 
               
                 
    }
    
  public PageReference Search() {
  
       Date checkDate = Date.Today().addDays(-60);
       
       if( !(selectedDate >= checkDate && selectedDate <= Date.Today()) && selectedDate != null )
         {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter a valid date. News feed archives from the last 2 months are available.'));
          
          }
//       else if (selectedKeyword.contains('\''))
//           {
//               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please exclude special characters from your search.'));
//           }
       else if ( (selectedDate >= checkDate && selectedDate <= Date.Today()) || !String.isBlank(selectedKeyword) || selectedCategory.size() != 0 )
          {
                flag = true;  
                getUserFeedList();  
          }
      else 
      {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please select atleast one criteria.'));
       }
       
         return null;
    }
    
  public void getUserFeedList() {
     
        if(flag==true)
         {
                   list<String> Criteria = new list<String>();
                   
                   if( selectedDate != null)
                       Criteria.add('publish_from__c <= :selectedDate AND publish_Through__c >= :selectedDate');
                                
                   if( selectedKeyword != null)
                       Criteria.add('Headline__c LIKE \'%' + string.escapeSingleQuotes(selectedKeyword) + '%\''); 
                    
                   if( selectedCategory.size() != 0)
                       Criteria.add('Category__c in :selectedCategory');  
                       
                  String whereClause = '';
                  
                   if (criteria.size()>0) {
                       whereClause = ' where ' + String.join(criteria, ' AND ');
                       }
                  
                 String Query = 'select id,Name,category__c,publish_from__c,Headline__c, Img__C, Body__C,published_on__c,Event_Date_Time__c from News__c ' + whereClause  + ' AND (Intended_audience__c INCLUDES (\'All Audiences\',\'Success Coaches\')) ORDER BY publish_from__C DESC';    
                  UserList = Database.Query(query); 
                
         }                     
         else
         {
                String Query = 'Select id,Name,category__c, publish_from__c,Headline__c,Img__C,Body__C,published_on__c,Event_Date_Time__c from News__c where Publish_from__c <= TODAY and publish_Through__c >= LAST_N_DAYS:60 AND (Intended_audience__c INCLUDES (\'All Audiences\',\'Success Coaches\')) ORDER BY publish_from__C DESC';
                  UserList = Database.Query(query); 
         }   
      
        if(UserList.isEmpty())
        {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'News Feed not available for the search.'));
           
        }
    
     }
   
   
  public PageReference clear() {
  
                  flag = false;
                  getUserFeedList();  
                  SelectedKeyword = null;
                  if(selectedCategory != null){
                      selectedCategory.clear();
                         }
                  SelectedDate = null;
                  return null;
    }

 }