global class BatchEmail implements Database.Batchable<sObject>{
    
    global final String Query;
    global list<ID> pids {get;set;}
    global list<profile> profiles {get;set;}
    
    global BatchEmail()
     {
         //KD: Added custom settings to remove hard coding of the profile names in the SOQL below.
         //Sample Check for Jenkins.
         CommunityDetails__c cdc = CommunityDetails__c.getOrgDefaults();
		 string spc = cdc.Success_Coach_Profile__c;
         string cp = cdc.Citizen_Profile__c;
         string pp = cdc.Partner_Profile__c;
         
         profiles = new list<profile>();
         pids = new list<ID>();
         profiles = [select id from profile where name =:spc OR name =:cp OR name =:pp];
         system.debug('profiles'+profiles);
         for(profile p: profiles)
             pids.add(p.id);
         system.debug('profiles ids'+pids);
         query = 'select Id,name,email,profileId,Notifications_Email__c,Notifications_Text__c from user where ProfileId IN :pids';
         
     }
 
   global Database.QueryLocator start(Database.BatchableContext BC)
    {
       
        return Database.getQueryLocator(query);
    }
 
   global void execute(Database.BatchableContext BC,List<sObject> scope)
   {
     List <User> UserList = new list<User>();
     List <Notification__c> NotifList = new List <Notification__c>();
     Integer NewNotifCount = 0;
     
     for(Sobject s : scope)
     {
       User a = (User)s;
       UserList.add(a);
     }
    
     for(User u : UserList)
     {
        NotifList=[select Id,Notification_Description__c,Notification_Date_Time__c,Source__c, Unread__c, GoalType2__c, FeedbackURL__c, Parent_Id__c 
                 from Notification__c
                 where Owner.Id = :u.Id AND
                 Unread__c = true
                 Order by Notification_Date_Time__c DESC];
      
        NewNotifCount = NotifList.size(); 
         if(u.Notifications_Email__c == true)
         {
           if(NewNotifCount > 0)
           {
             String vURL = URL.getSalesforceBaseUrl().toExternalForm();  //KD: Added to remove the hardcoding of the environment details in the TextBody below     
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             List<String> emailID = new List<String>();
             emailID.add(u.email);
             mail.setToAddresses(emailID);
             mail.setSubject('You have '+NewNotifCount+' new notification[s] waiting');
             mail.setPlainTextBody('Hello '+u.name+',\n\tYou have '+NewNotifCount+' new notification[s] waiting.\n\tView more details here '+vURL );
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
         }
         
         if(NewNotifCount > 0)
         {
           string text ='You have '+NewNotifCount+' new notification[s] waiting';
           if(u.Notifications_Text__c == true)
           {
             SendSMSForChatter sms = new SendSMSForChatter();
             sms.SendSMSGlobal(u.name,text,u.id); 
           }
         }
       }
       
   }
   global void finish(Database.BatchableContext BC)
  {
       System.debug('inside finish');          
      //Send an email to the User after your batch completes
      
             Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
             string [] toAddress= new string[]{'bhb@deloitte.com'};
             mail.setToAddresses(toAddress);
             mail.setSubject('Notification Job is done');
             mail.setPlainTextBody('Notification job processed ');
             Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
  }
    
    
}