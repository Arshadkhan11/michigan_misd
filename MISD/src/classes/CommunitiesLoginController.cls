global without sharing class CommunitiesLoginController {

        public static final String PUBLIC_SITE_ROOT = 'https://isddryrun-isdpartner.cs3.force.com';
        public String profileId {get; set; }
        public String userId{get;set;}

        public String Option {get; set; }

        public boolean isLoginFailed {get; set;}
        public String password { get; set; }
        public String verifyPassword {get;set;}
        public String oldPassword {get; set;}
        public String userAnswer1 { get; set; }
        public String userAnswer2 { get; set; }
        
        public String username { get; set; }
        
        public User currentUser {get;set;}
        public Integer R1 {get;set;}
        public Integer R2 {get;set;}
        public String Question1 {get;set;}
        public String Question2 {get;set;}
        public String Answer1 {get;set;}
        public String Answer2 {get;set;}
        public String re {get;set;}
        public list<User> userList {get;set;}

        global CommunitiesLoginController () {
            system.debug('inside constructor');
            userList = [select username from user where username <> null ];
            
            String param_value =  System.currentPageReference().getParameters().get('name');
            //system.debug('param value'+param_value);
            if(param_value != null)
            {
              userId=[select Id from user where username = :param_value].Id; 
              //system.debug('User Id'+ userId);
               securityQuestions();
            }
           
         }
         
         public PageReference initialize() {
           profileId = ApexPages.currentPage().getParameters().get('profileId');
           return profileId == null ? intializeView() : initializeUpdate();
         }
         
         public PageReference intializeView()
         {
             return null;
         }
         
         public PageReference initializeUpdate() {
            String profileId = ApexPages.currentPage().getParameters().get('profileId');
            String userId = ApexPages.currentPage().getParameters().get('userId');
            
            System.debug('@dualrole - initializeUpdate - ' + profileId + '  ' + userId);
            
            update new User(Id = userId, ProfileId = profileId);
            
            System.debug('@dualrole - done - initializeUpdate - ' + profileId + '  ' + userId);
                         
            return null;
        }
             
        public void securityQuestions(){
           
            
            if(userId != '' && userId != null)
               currentUser=[Select Id,Username,Security_Question_1__c,Security_Question_2__c,Security_Question_3__c,Sq1_Answer__c,Sq2_Answer__c,Sq3_Answer__c from User
                            where Id = :userId];
            
            //system.debug('current user:'+currentUser);
                
            R1=randomWithLimit();
            R2=randomWithLimit();
            while(r2==r1)
            {
                            R2=randomWithLimit();
            }
            System.debug(R1);
            System.debug(R2);
            
            if(R1==1 && R2==2)
            {
                Question1=CurrentUser.Security_Question_1__c;
                Question2=CurrentUser.Security_Question_2__c;
                Answer1=CurrentUser.Sq1_Answer__c;
                Answer2=CurrentUser.Sq2_Answer__c;
                
            }
            else if(R1==2 && R2==3)
            {
                Question1=CurrentUser.Security_Question_2__c;
                Question2=CurrentUser.Security_Question_3__c;
                Answer1=CurrentUser.Sq2_Answer__c;
                Answer2=CurrentUser.Sq3_Answer__c;
            }
            else if(R1==3 && R2==1)
            {
                Question1=CurrentUser.Security_Question_3__c;
                Question2=CurrentUser.Security_Question_1__c;
                Answer1=CurrentUser.Sq3_Answer__c;
                Answer2=CurrentUser.Sq1_Answer__c;
            }
            else if(R1==1 && R2==3)
            {
                Question1=CurrentUser.Security_Question_1__c;
                Question2=CurrentUser.Security_Question_3__c;
                Answer1=CurrentUser.Sq1_Answer__c;
                Answer2=CurrentUser.Sq3_Answer__c;
            }
            else if(R1==2 && R2==1)
            {
                Question1=CurrentUser.Security_Question_2__c;
                Question2=CurrentUser.Security_Question_1__c;
                Answer1=CurrentUser.Sq2_Answer__c;
                Answer2=CurrentUser.Sq1_Answer__c;
            }
            else if(R1==3 && R2==2)
            {
                Question1=CurrentUser.Security_Question_3__c;
                Question2=CurrentUser.Security_Question_2__c;
                Answer1=CurrentUser.Sq3_Answer__c;
                Answer2=CurrentUser.Sq2_Answer__c;
            }

            //System.debug(Question1);
            //System.debug(Question2);
            //System.debug(Answer1);
            //System.debug(Answer2);
            
        }
        //To generate random security questions
         public Integer randomWithLimit(){
                    integer Random = (integer)(Math.random()*3+1);
               return Random;
          }
        // Code we will invoke on page load.
        global PageReference forwardToAuthPage() {
            String startUrl = System.currentPageReference().getParameters().get('startURL');
            String displayType = System.currentPageReference().getParameters().get('display');
            return Network.forwardToAuthPage(startUrl, displayType);
        }
        
        global PageReference login(){
        
            String startUrl = System.currentPageReference().getParameters().get('startURL'); 
            Pagereference p = Site.login(username, password, startUrl);
            if (p==null) {
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Incorrect Username and Password. Please try again.'));
                return null;
            }
            User usr = [Select Id, AdditionalProfile__c, DualLogin__c from User where username = :username];
            System.debug('@Rameez: param0 - ' + username + '   ' + usr.DualLogin__c);
            if(usr.AdditionalProfile__c == 'Success Coach Profile') {
                usr.DualLogin__c=true;
                update usr;
            }
            System.debug('@Rameez: param0 - ' + usr.Id + '   ' + usr.DualLogin__c);
            return p;
        }
        
       
           
        public PageReference forgotPasswordPage() {
        
            system.debug('going to forgot password page');
            Pagereference p = Page.MI_Forgot_Pass;    
            string checkuser = username;     
            p.getParameters().put('name',username);
            try{
                system.debug('checking for username in user.');
                user u = [select id from user where username = :checkuser];
            }catch(exception e){
              //if(username == null || username=='')
                system.debug('username failed.');
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please enter valid username to proceed.'));
                return null;
              }
          
              p.setredirect(true);
              return p;
            
          
        }
            
        public PageReference passwordReset() {
        
            system.debug('changing password');
           
            try{
            system.debug('changing password inside try');
            system.debug('new password for tamara before calling setpass:'+password);
            system.debug('user id :'+userid);
            System.setPassword(userid, password);  
            system.debug('new password for tamara after calling setpass:'+password);
            
            system.debug('PageReference');
            PageReference pr = Page.passwordSuccessPage;
            pr.setRedirect(true);
            return pr;
                
            }
             catch(Exception e) {
                 //System.debug('The following exception has occurred: ' + e.getMessage());
                 //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, e.getMessage()));
                 return null;
             }
           
        }
        
        public PageReference test() {
            
            String myCommunityName = 'State of Michigan';
            Network myCommunity = [SELECT Id FROM Network WHERE Name = :myCommunityName and id <> null];
            String communityUrl = Network.getLoginUrl(myCommunity.Id);
            String communityUrl1  = communityUrl;
            communityUrl = communityUrl.replace('login','');
            System.debug('@Rameez - Option ' + Option + '   ' + communityUrl + 'MI_SuccessCoachDashboard');
            
            userId = UserInfo.getUserId();
            
            if(Option == 'Success Coach'){
            
                    profileId = [Select Id from Profile where Name = 'Success Coach Profile'][0].Id;
                    
                }
                else if(Option == 'Partner'){
                    
                    profileId = [Select Id from Profile where Name = 'Partner Profile'][0].Id;
                }
                    
                    System.debug('@Rameez- public site - ');
                    String remoteURL = PUBLIC_SITE_ROOT + '/MI_ProfileSelection?profileId=' + profileId + '&userId=' + userId;
                    
                    HttpRequest http = new HttpRequest();
                    http.setMethod('GET');
                    http.setEndPoint(remoteURL);
                    HttpResponse httpr = new Http().send(http);
                    System.debug('@dualrole - ' + httpr);
                    
            PageReference finalpage = new PageReference('/apex/CommunitiesLanding');
            finalpage.setRedirect(true);
            return finalpage;
        }
        
        public List<SelectOption> getItems() {
            List<SelectOption> options = new List<SelectOption>(); 
            options.add(new SelectOption('Success Coach','Login as Success Coach')); 
            options.add(new SelectOption('Partner','Login as Partner')); 
            return options;
        }
       
        
        public String getOption() {
            return Option;
        }
        
        public void setOption(String Option) { this.Option = Option; }
       
    }