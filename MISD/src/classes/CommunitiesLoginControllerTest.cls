/**
 * An apex page controller that exposes the site login functionality
 */
@IsTest global with sharing class CommunitiesLoginControllerTest {
    @IsTest(SeeAllData=true) 
    global static void testCommunitiesLoginController () {
     	CommunitiesLoginController controller = new CommunitiesLoginController();
     	System.assertEquals(null, controller.forwardToAuthPage());  
       // controller.Register();
       controller.login();
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id);
        insert con;
         Profile p = [SELECT Id FROM Profile WHERE Name='Partner Profile'];
        Test.setCurrentPageReference(new PageReference('Page.MI_Citizen_Registration'));
         System.currentPageReference().getParameters().put('name','partner');

       //string param_value='partner'; 

       User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        System.runAs(u){
               controller.userid = u.id;
       controller.R1 = 1;
       controller.R2 = 1;
    
    
       controller.forgotPasswordPage();
       controller.passwordReset();
       controller.login();
      
       controller.securityQuestions();
    } 
         System.runAs(u){
               controller.userid = u.id;
       controller.R1 = 2;
       controller.R2 = 1;
    
    
       controller.forgotPasswordPage();
       controller.passwordReset();
       controller.login();
      
       controller.securityQuestions();
    } 
         System.runAs(u){
               controller.userid = u.id;
       controller.R1 = 1;
       controller.R2 = 3;
    
    
       controller.forgotPasswordPage();
       controller.passwordReset();
       controller.login();
      
       controller.securityQuestions();
    } 
         System.runAs(u){
               controller.userid = u.id;
       controller.R1 = 3;
       controller.R2 = 1;
    
    
       controller.forgotPasswordPage();
       controller.passwordReset();
       controller.login();
      
       controller.securityQuestions();
    } 
         System.runAs(u){
               controller.userid = u.id;
       controller.R1 = 2;
       controller.R2 = 3;
    
    
       controller.forgotPasswordPage();
       controller.passwordReset();
       controller.login();
      
       controller.securityQuestions();
    } 
         System.runAs(u){
               controller.userid = u.id;
       controller.R1 = 3;
       controller.R2 = 2;
    
    
       controller.forgotPasswordPage();
       controller.passwordReset();
       controller.login();
      
       controller.securityQuestions();
    } 
         System.runAs(u){
               controller.userid = u.id;
       controller.R1 = 2;
       controller.R2 = 2;
    
    
       controller.forgotPasswordPage();
       controller.passwordReset();
       controller.login();
      
       controller.securityQuestions();
    } 
    }
}