public class MI_Search_DetailsCtrl{

    public Boolean displayPopup                                     {get; set;}    
    public string ContactName                                       {get;set;}
    public String postText                                          {get;set;}
    public String postText1                                         {get;set;}
    public String postText2                                         {get;set;}
    
    
    public List<SearchDetailWrapper> swl                            {get;set;}
    public String vSearchKeyWord                                    {get;set;} 
    public List<List<sObject>> finalsearchResults                   = new List<List<sObject>>();
    public List<News__c> vNews                                      = new List<News__c>();
    public List<MI_Activity__c> vActivity                           = new List<MI_Activity__c>();
    public ConnectApi.FeedElementPage FeedItems;
    
    public Network Networkcom ;
    public ConnectApi.FeedItemInput feedItemInput                   {get;set;} 
    public ConnectApi.MentionSegmentInput mentionSegmentInput       {get;set;}
    public ConnectApi.MessageBodyInput messageBodyInput             {get;set;}
    public ConnectApi.TextSegmentInput textSegmentInput             {get;set;}
    public ConnectApi.LinkSegmentInput linkSegmentInput             {get;set;}//BBANSAL - added for URL sharing on chatter
    public ConnectApi.TextSegmentInput textSegmentInput2            {get;set;}//BBANSAL - added to split the message
    public string jsonString                                        {get;set;}
    public String topH                                              {get;set;}
    public String URLToDisplay                                      {get;set;}
    public String topost;

    public MI_Search_DetailsCtrl(){
         vSearchKeyWord              = apexpages.currentpage().getparameters().get('search');
         ContactName                 = 'Tamara Davis';
         topost = postText           = 'Hi Tamara Davis, \n\nPlease check the below link for more details, \n\n LINK HERE \n\n Thanks, \n' + UserInfo.getName();
         postText1           = 'Hi Tamara Davis, \n\nPlease check the below link for more details, \n\n';
         postText2                   =  '\n\n Thanks, \n' + UserInfo.getName();
         Networkcom                  = [SELECT Id FROM Network WHERE Name ='State of Michigan'];
    }
    
    public void sendMessage() { 
        
        List<user> lstUser = [SELECT id FROM User WHERE name like '%Tamara Davis%'];
        
        feedItemInput = new ConnectApi.FeedItemInput();
        mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        messageBodyInput = new ConnectApi.MessageBodyInput();
        textSegmentInput = new ConnectApi.TextSegmentInput();
        linkSegmentInput = new ConnectApi.LinkSegmentInput();
        textSegmentInput2 = new ConnectApi.TextSegmentInput();
        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
        
        mentionSegmentInput.id = lstUser[0].id;
        system.Debug('Mentions:'+ mentionSegmentInput.id);
        messageBodyInput.messageSegments.add(mentionSegmentInput);
        mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        
        if(PostText != null){
            system.debug('\n PostText:: ' + PostText);
            textSegmentInput.text = postText1;
            messageBodyInput.messageSegments.add(textSegmentInput);
            linkSegmentInput.url = URLToDisplay;
            messageBodyInput.messageSegments.add(linkSegmentInput);
            textSegmentInput2.text = postText2;
            messageBodyInput.messageSegments.add(textSegmentInput2);
            
            feedItemInput.body = messageBodyInput;
            feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
            feedItemInput.subjectId = UserInfo.getUserId();
            ConnectApi.FeedElement feedElement; 
            try{
                feedElement = ConnectApi.ChatterFeeds.postFeedElement(Networkcom.id, feedItemInput, null);
            }catch(Exception e){}
                System.Debug('after posting:'+feedElement);
            }
        
        displayPopup = false;    
    } 
    
    public void closePopup() {        
        displayPopup = false;  
        postText = '';
    }   
    
    public void showPopup() {        
        displayPopup = true;
       // linkSegmentInput.url = URLToDisplay;
        //system.debug('\n\n URLToDisplay:: ' + URLToDisplay);
        postText = topost.replace('LINK HERE' , URLToDisplay);
        
    }
    
    public void getResults(){
        
        swl                         = new List<SearchDetailWrapper>();
        //KD: Added custom settings to remove hard coding of the network in the SOQL below.
        CommunityDetails__c cdc = CommunityDetails__c.getOrgDefaults();
        String ntname = cdc.CommunityName__c;
        Networkcom                  = [SELECT Id FROM Network WHERE Name =:ntname];
        System.debug('$$$$$'+'vSearchKeyWord');
        finalsearchResults          = [FIND :vSearchKeyWord IN ALL FIELDS 
                                            RETURNING News__c(Id, Headline__c, Body__c), MI_Activity__c(Id, Subject__c, Description__c,RecordType.Name)];
        
        FeedItems                   = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Networkcom.id, ConnectApi.FeedType.UserProfile,UserInfo.getUserId());
        
        if(finalsearchResults.size() != 0){
            
            if(!finalsearchResults[0].isEmpty())
                vNews = ((List<News__C>)finalsearchResults[0]);
            if(!finalsearchResults[1].isEmpty())
                vActivity = ((List<MI_Activity__c>)finalsearchResults[1]);
            system.debug('\n\n vActivity:: ' + vActivity);
            for(News__C n : vNews){
                Boolean isAdded = false;
                SearchDetailWrapper sw = new SearchDetailWrapper();
                sw.vTitle = 'News - ' + n.Headline__c;
                if(n.Body__c != null && n.Body__c.containsIgnoreCase(vSearchKeyWord)){
                    Integer keyIndex = n.Body__c.IndexOfIgnoreCase(vSearchKeyWord);
                    Integer bodyLength = n.Body__c.length();
                    system.debug('\n keyIndex: ' + keyIndex + '\n bodyLength: ' + bodyLength);
                    if(keyIndex == 0){ 
                        if(bodyLength <= 500){
                            sw.vBody = n.Body__c;        
                        }else{
                            sw.vBody = n.Body__c.substring(0, 500) + '...';        
                        }    
                    }else if(keyIndex <= 500 && bodyLength <= 500){
                        sw.vBody = n.Body__c;    
                    }else if(keyIndex <= 500 && bodyLength > 500){
                        if(keyIndex > 250 && keyIndex < 500) {
                            if(bodyLength > 750){
                                sw.vBody = '...' + n.Body__c.substring(250, 750 ) + '...'; 
                            }else{
                                sw.vBody = '...' + n.Body__c.substring(bodyLength - 500, bodyLength);
                            }
                        }else{
                            sw.vBody = n.Body__c.substring(0, 500) + '...';    
                        }
                    }else if(keyIndex > 500 && bodyLength > keyIndex + 250){
                        sw.vBody = '...' + n.Body__c.substring(keyIndex- 250, keyIndex + 250) + '...';    
                    }else if(keyIndex > 500 && bodyLength <= keyIndex + 250){
                        if(vSearchKeyWord.length() + keyIndex +1 == bodyLength)
                            sw.vBody = '...' + n.Body__c.substring(keyIndex- 250, keyIndex+vSearchKeyWord.length()+1);
                        else
                            sw.vBody = '...' + n.Body__c.substring(keyIndex- 250, keyIndex+vSearchKeyWord.length()) + '...';
                    }  
                    sw.vUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/MI_NewsDetails?id=' + n.Id;
                    sw.vRecordType = 'News';
                    sw.resultType = 'Article';
                    swl.add(sw);
                    isAdded = true;
                }
                if(isAdded == false && n.Headline__c != null && n.Headline__c.containsIgnoreCase(vSearchKeyWord)){                      
                    if(n.Body__c != null && n.Body__c != ''){
                        if(n.Body__c.length() > 501)
                            sw.vBody = n.Body__c.substring(0,500) + '...';
                        else{
                            sw.vBody = n.Body__c;
                        }
                    }else{
                        sw.vBody = '';
                    }
                    sw.vUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/MI_NewsDetails?id=' + n.Id;
                    sw.vRecordType = 'News';
                    sw.resultType = 'Article';
                    swl.add(sw);
                }
            }
            for(MI_Activity__c m:vActivity){
                Boolean isAdded = false;
               if(m.RecordType.Name == 'Task'){
                    SearchDetailWrapper sw = new SearchDetailWrapper();
                    sw.vTitle = 'Task - ' + m.Subject__c;
                    if(m.Description__c != null && m.Description__c.containsIgnoreCase(vSearchKeyWord)){
                        Integer keyIndex = m.Description__c.IndexOfIgnoreCase(vSearchKeyWord);
                        Integer bodyLength = m.Description__c.length();
                        if(keyIndex == 0){ 
                            if(bodyLength <= 500){
                                sw.vBody = m.Description__c;        
                            }else{
                                sw.vBody = m.Description__c.substring(0, 500) + '...';        
                            }    
                        }else if(keyIndex <= 500 && bodyLength <= 500){
                            sw.vBody = m.Description__c;    
                        }else if(keyIndex <= 500 && bodyLength > 500){
                            if(keyIndex > 250 && keyIndex < 500) {
                                if(bodyLength > 750){
                                    sw.vBody = '...' + m.Description__c.substring(250, 750 ) + '...'; 
                                }else{
                                    sw.vBody = '...' + m.Description__c.substring(bodyLength - 500, bodyLength);
                                }
                            }else{
                                sw.vBody = m.Description__c.substring(0, 500) + '...';    
                            }    
                        }else if(keyIndex > 500 && bodyLength > keyIndex + 250){
                            sw.vBody = '...' + m.Description__c.substring(keyIndex- 250, keyIndex + 250) + '...';    
                        }else if(keyIndex > 500 && bodyLength <= keyIndex + 250){
                            if(vSearchKeyWord.length() + keyIndex + 1 == bodyLength)
                                sw.vBody = '...' + m.Description__c.substring(keyIndex- 250, keyIndex+vSearchKeyWord.length()+1);
                            else
                                sw.vBody = '...' + m.Description__c.substring(keyIndex- 250, keyIndex+vSearchKeyWord.length()) + '...';
                        }
                        sw.vUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/MI_TaskDetails?id=' + m.Id;
                        sw.vRecordType = 'Task';
                        sw.resultType = 'Article';
                        swl.add(sw);  
                        isAdded = true;                 
                    }  
                    if(isAdded == false && m.subject__c != null && m.subject__c.containsIgnoreCase(vSearchKeyWord)){                        
                        if(m.Description__c != null && m.Description__c != ''){
                            if(m.Description__c.length() > 501)
                                sw.vBody = m.Description__c.substring(0,500) + '...';
                            else{
                                sw.vBody = m.Description__c;
                            }
                        }else{
                            sw.vBody = '';
                        }
                        sw.vUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') + '/MI_TaskDetails?id=' + m.Id;
                        sw.vRecordType = 'Task';
                        sw.resultType = 'Article';
                        swl.add(sw);
                    }                       
                }   
                if(m.RecordType.Name == 'Event'){
                    SearchDetailWrapper sw = new SearchDetailWrapper();
                    sw.vTitle = 'Calendar - ' + m.Subject__c;
                    if(m.Description__c != null && m.Description__c.containsIgnoreCase(vSearchKeyWord)){
                        Integer keyIndex = m.Description__c.IndexOfIgnoreCase(vSearchKeyWord);
                        Integer bodyLength = m.Description__c.length();
                        if(keyIndex == 0){ 
                            if(bodyLength <= 500){
                                sw.vBody = m.Description__c;        
                            }else{
                                sw.vBody = m.Description__c.substring(0, 500) + '...';        
                            }    
                        }else if(keyIndex <= 500 && bodyLength <= 500){
                            sw.vBody = m.Description__c;    
                        }else if(keyIndex <= 500 && bodyLength > 500){
                            if(keyIndex > 250 && keyIndex < 500) {
                                if(bodyLength > 750){
                                    sw.vBody = '...' + m.Description__c.substring(250, 750 ) + '...'; 
                                }else{
                                    sw.vBody = '...' + m.Description__c.substring(bodyLength - 500, bodyLength);
                                }
                            }else{
                                sw.vBody = m.Description__c.substring(0, 500) + '...';    
                            }  
                        }else if(keyIndex > 500 && bodyLength > keyIndex + 250){
                            sw.vBody = '...' + m.Description__c.substring(keyIndex- 250, keyIndex + 250) + '...';    
                        }else if(keyIndex > 500 && bodyLength <= keyIndex + 250){
                            if(vSearchKeyWord.length() + keyIndex + 1 == bodyLength)
                                sw.vBody = '...' + m.Description__c.substring(keyIndex- 250, keyIndex+vSearchKeyWord.length()+1);
                            else
                                sw.vBody = '...' + m.Description__c.substring(keyIndex- 250, keyIndex+vSearchKeyWord.length()) + '...';
                        }
                        sw.vUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') +  '/EditViewAppointments?id=' + m.Id;
                        sw.vRecordType = 'Calendar';
                        sw.resultType = 'Article';
                        swl.add(sw); 
                        isAdded = true;                     
                    }
                    system.debug('\n m.Description__c - ' + m.Description__c + '\n sub - '+ m.subject__c);
                    if(isAdded == false && m.subject__c != null && m.subject__c.containsIgnoreCase(vSearchKeyWord)){                    
                        if(m.Description__c != null && m.Description__c != ''){
                            if(m.Description__c.length() > 501)
                                sw.vBody = m.Description__c.substring(0,500) + '...';
                            else{
                                sw.vBody = m.Description__c;
                            }
                        }else{
                            sw.vBody = '';
                        }
                        sw.vUrl = 'https://' + ApexPages.currentPage().getHeaders().get('Host') +  '/EditViewAppointments?id=' + m.Id;
                        sw.vRecordType = 'Calendar';
                        sw.resultType = 'Article';
                        swl.add(sw);
                    }                   
                }    
            }
        }
        
        for(ConnectApi.FeedElement f : FeedItems.elements){
            for(ConnectApi.MessageSegment m : f.body.messageSegments){
                if(m.text.ContainsIgnoreCase(vSearchKeyWord)){           
                    SearchDetailWrapper sw = new SearchDetailWrapper();
                    sw.vTitle = 'Message - '+ f.parent.name;
                    if(m.text != null){
                        string bodyToDisplay = '';
                        Integer keyIndex = m.text.IndexOfIgnoreCase(vSearchKeyWord);
                        Integer bodyLength = m.text.length();
                        if(keyIndex == 0){ 
                            if(bodyLength <= 500){
                                sw.vBody = m.text;        
                            }else{
                                sw.vBody = m.text.substring(0, 500) + '...';        
                            }    
                        }else if(keyIndex <= 500 && bodyLength <= 500){
                            sw.vBody = m.text;    
                        }else if(keyIndex <= 500 && bodyLength > 500){
                            if(keyIndex > 250 && keyIndex < 500) {
                                if(bodyLength > 750){
                                    sw.vBody = '...' + m.text.substring(250, 750 ) + '...'; 
                                }else{
                                    sw.vBody = '...' + m.text.substring(bodyLength - 500, bodyLength); 
                                }
                            }else{
                                sw.vBody = m.text.substring(0, 500) + '...';    
                            }  
                        }else if(keyIndex > 500 && bodyLength > keyIndex + 250){
                            sw.vBody = '...' + m.text.substring(keyIndex- 250, keyIndex + 250) + '...';    
                        }else if(keyIndex > 500 && bodyLength <= keyIndex + 250){
                            if(vSearchKeyWord.length() + keyIndex + 1 == bodyLength)
                                sw.vBody = '...' + m.text.substring(keyIndex- 250, keyIndex+vSearchKeyWord.length()+1);
                            else
                                sw.vBody = '...' + m.text.substring(keyIndex- 250, keyIndex+vSearchKeyWord.length()+1) + '...';
                        }
                    
                    }
                    sw.resultType = 'Article';
                    sw.vRecordType = 'Message';
                    swl.add(sw);
                }
            }
        }    
        //jsonString = '{"id": -1,"fieldErrors": [], "sError": "", "aaData": ' + JSON.serialize(swl) + '}';
        jsonString = '{ "aaData": ' + JSON.serialize(swl) + '}';
        System.debug('\n\n Virals JSON' + jsonString);
    }
       
    public class SearchDetailWrapper {
        public String vTitle {get; set;}
        public String vBody {get; set;}
        public String vUrl {get; set;}
        public String vRecordType {get; set;}
        public String resultType    {get; set;}
        public String shareIcon     {get; set;}
        
        public SearchDetailWrapper(){
            shareIcon   = '';  
            vBody       = '';
        }
    }
}