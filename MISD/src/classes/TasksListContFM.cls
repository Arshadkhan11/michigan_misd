public class TasksListContFM { 

// Tasks List page - VAR
    public String selectedDate {get;set;}
    public String selectedKeyword {get;set;}
    public List<String> SelectedCategory {get;set;}
    public Integer overDueCount {get;set;}
    public Boolean Flag {get;set;}
    public List<MI_Activity__c> taskListUserLogin {get;set;}
    public List<MI_Activity__c> OverdueTasksList {get;set;}
    public Id RecTypeId {get;set;}
    public List<Case> FamilyManCases {get;set;}
    public String[] ClientNameListConcatSplit {get;set;}    
	public String ClientNameListConcat {get;set;}

    public String SortColumn {get;set;}
    public String SortOrder {get;set;}
    public String LastSortColumn {get;set;}
    public String DefaultQuery {get;set;}
    public String OverdueQuery {get;set;}
    public Boolean OverDueQueryFlag {get;set;}
    public DateTime NextWeek {get;set;}
    public Boolean DateOrStatus {get;set;}
    
    public  TasksListContFM() 
    {
       
// Tasks List page - CONT
     try
     {
         	   NextWeek = DateTime.Now().AddDays(7);
	   DateOrStatus = true;
        
	   OverDueQueryFlag = false;
	   DefaultQuery = null;
       OverdueQuery = null;
       LastSortColumn = null;
	   SortColumn = 'Activity_Date__c';
       LastSortColumn = 'Activity_Date__c'; 
       SortOrder = 'ASC';
      
       selectedCategory = new List<string>();
       flag = false;
       overDueCount = 0; 
        
       RecTypeId = [select Id from RecordType where DeveloperName='Task'].Id; 
       FamilyManCases = [Select Id, CaseNumber, Contact.Name, Primary_Contact_Name__c, LastModifiedDate,CountMilestones__c, SuccessCoach__c, CountMilestonesComp__c,TransLstUpd__c, MinOverdueDate__c, LastUpdatedGoalCase__c from Case 
                          where (ContactId!=null) AND
                          Primary_Contact_Name__c!=null AND
                          SuccessCoach__c=:UserInfo.getUserId()
                          ORDER BY LastUpdatedGoalCase__c DESC];
        taskListUserLogin = querytaskListUserLogin();
     }
        catch(exception e)
        {
            System.debug('Constructor catch block'+e);
        }
     }

// FUNCTIONS START
    public PageReference HeaderSort()
    {
        if(LastSortColumn == SortColumn)
        {
            if(SortOrder == 'DESC')
            {
                SortOrder = 'ASC';
            }
            else
            {
                SortOrder = 'DESC';
            }
        }
        
        if(OverDueQueryFlag == false)
        { 
            taskListUserLogin = querytaskListUserLogin();
        }
        else
        {
            overDue();
        }
		LastSortColumn = SortColumn;        
        return null;
    }
    
    public List<MI_Activity__c> querytaskListUserLogin()
    {
        
        
        for(Case cc: FamilyManCases)
        {
            ClientNameListConcat = ClientNameListConcat + ';' + cc.Primary_Contact_Name__c;
        }
        String[] ClientNameListConcatSplit = (ClientNameListConcat).split(';');        
        
        List<MI_Activity__c> OverdueTasksList = [SELECT Id,Activity_Date__c, Description__c, Client_Name__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          Owner.Id=:UserInfo.getUserId() AND
                                          RecordTypeId =: RecTypeId AND
                                          Client_Name__c != null AND
                                          Client_Name__c IN :ClientNameListConcatSplit AND
                                          Activity_Date__c!=null AND
                                          Activity_Date__c<Today AND
                                          Status__c!='Completed'                                                 
                                          ORDER BY Activity_Date__c];
        overDueCount = 0;
        if(!OverdueTasksList.isEmpty())
        {
	        overDueCount = OverdueTasksList.size();
        }

   
     if(flag == true) 
       {
         
                   list<String> Criteria = new list<String>();
                   
                   if( selectedDate != '')
                   {
                       String[] strDate = selectedDate.split('/');
                        Integer myIntDate = integer.valueOf(strDate[1]);
                        Integer myIntMonth = integer.valueOf(strDate[0]);
                        Integer myIntYear = integer.valueOf(strDate[2]);
                        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
                       Criteria.add('DAY_ONLY(Activity_Date__c) = :d');
                   }            
                   if( String.isnotBlank(selectedKeyword))
                       Criteria.add('((Subject__c LIKE \'%' + string.escapeSingleQuotes(selectedKeyword) + '%\') OR (Subject__c LIKE \'%' + string.escapeSingleQuotes(selectedKeyword) + '\') OR (Subject__c LIKE \'' + string.escapeSingleQuotes(selectedKeyword) + '%\') OR (Client_Name__c LIKE \'%' + string.escapeSingleQuotes(selectedKeyword) + '%\') OR (Client_Name__c LIKE \'%' + string.escapeSingleQuotes(selectedKeyword) + '\') OR (Client_Name__c LIKE \'' + string.escapeSingleQuotes(selectedKeyword) + '%\'))');  

           		   Criteria.add('Client_Name__c IN :ClientNameListConcatSplit');
           
/*                   if( selectedCategory.size() != 0)
                       Criteria.add('Task_Type__c in :selectedCategory');  */
                       
                  String whereClause = '';
                  
                   if (!criteria.isEmpty()) {
                       whereClause = ' where Status__c!=\'' + 'Completed' +'\' AND '+ String.join(criteria, ' AND ');
                       }
                  
                 String Query = 'SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c ' + whereClause  + ' AND RecordTypeId =: RecTypeId AND Activity_Date__c!=null AND Client_Name__c != null AND Owner.Id =\'' + UserInfo.getUserId() +'\' ORDER BY '+SortColumn+' '+SortOrder;    
                 List<MI_Activity__c> UserList = new List<MI_Activity__C>();
                 system.Debug('Query'+query);
                 UserList = Database.Query(query); 
                 
                 
                 if(UserList.isEmpty())
                  {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Tasks not available for the search.'));
                     return null;
                  }
                 else
                  {
                     return UserList;
                  }
       
       }
     else 
     {
        DefaultQuery = 'SELECT Id, Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c where Client_Name__c != null AND Activity_Date__c!=null AND Owner.Id =\'' + UserInfo.getUserId() +'\''+' AND Status__c !=\'' + 'Completed' +'\''+' AND RecordTypeId=\'' + RecTypeId +'\''+' AND Client_Name__c IN: ClientNameListConcatSplit'+' ORDER BY '+SortColumn+' '+SortOrder;

// WORKING        DefaultQuery = 'SELECT Id, Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c where Client_Name__c != null AND Activity_Date__c!=null AND Owner.Id =\'' + UserInfo.getUserId() +'\''+' AND Status__c !=\'' + 'Completed' +'\''+' AND RecordTypeId=\'' + RecTypeId +'\''+' ORDER BY '+SortColumn+' '+SortOrder;
        //System.debug('ato - default query'+DefaultQuery);
        List<MI_Activity__c> TasksList = Database.Query(DefaultQuery); 
        return TasksList;
    }
        
   }
    public PageReference search() {
        OverDueQueryFlag = false;
   
         if ( selectedDate != '' || !String.isBlank(selectedKeyword) || !(selectedCategory.isEmpty()) )
          {
                SortColumn = 'Activity_Date__c';
                LastSortColumn = 'Activity_Date__c';
                SortOrder = 'ASC';	
                flag = true;  
                taskListUserLogin = querytaskListUserLogin();
              	
          }
      else 
      {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please select atleast one criteria.'));
       }
       
         return null;
    }     
   	public PageReference clear() {
        DateOrStatus = true;
        OverDueQueryFlag = false;
        SortColumn = 'Activity_Date__c';
        LastSortColumn = 'Activity_Date__c'; 
        SortOrder = 'ASC';

        flag = false;
        taskListUserLogin = querytaskListUserLogin();
        SelectedKeyword = null;
        if(selectedCategory != null){
            selectedCategory.clear();
        } 
        SelectedDate = '';
        return null;
    }
    public PageReference overDue() {

        OverDueQueryFlag = true; 
        
        for(Case cc: FamilyManCases)
        {
            ClientNameListConcat = ClientNameListConcat + ';' + cc.Primary_Contact_Name__c;
        }
        String[] ClientNameListConcatSplit = (ClientNameListConcat).split(';');                
        
        OverdueQuery = 'SELECT Id, Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c where Client_Name__c != null AND Activity_Date__c!=null AND Activity_Date__c<Today AND Owner.Id =\'' + UserInfo.getUserId() +'\''+' AND Status__c !=\'' + 'Completed' +'\''+' AND RecordTypeId=\'' + RecTypeId +'\''+' AND Client_Name__c IN: ClientNameListConcatSplit'+' ORDER BY '+SortColumn+' '+SortOrder;

        //System.debug('ato - default query'+OverdueQuery);
        List<MI_Activity__c> OverdueTasksList = Database.Query(OverdueQuery); 
        taskListUserLogin = OverdueTasksList;
        //System.debug('ato overdue tasks -> taskListUserLogin ->'+taskListUserLogin);
         
         return null;
    }
    
    
 
}