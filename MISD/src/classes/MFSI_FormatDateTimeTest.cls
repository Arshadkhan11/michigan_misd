@isTest
public class MFSI_FormatDateTimeTest {
    @isTest static void getTimeZoneValueTest1() {
        MFSI_FormatDateTime mfsi = new MFSI_FormatDateTime();
        mfsi.dateTimeValue = null;
        String outStr = mfsi.getTimeZoneValue();
    }
    @isTest static void getTimeZoneValueTest2() {
        MFSI_FormatDateTime mfsi = new MFSI_FormatDateTime();
        mfsi.dateTimeValue = datetime.newInstance(2015, 9, 15, 12, 30, 0);
        mfsi.defined_format = null;
        String outStr = mfsi.getTimeZoneValue();
    }
    @isTest static void getTimeZoneValueTest3() {
        MFSI_FormatDateTime mfsi = new MFSI_FormatDateTime();
        mfsi.dateTimeValue = datetime.newInstance(2015, 9, 15, 12, 30, 0);
        mfsi.defined_format = 'MM/dd/yyyy HH:mm:ss';
        String outStr = mfsi.getTimeZoneValue();
    }
}