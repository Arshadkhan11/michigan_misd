public class MI_Feedback 
{
    public Id FeedBackId{get;set;}
    public boolean ResourceUsed{get;set;}
    public decimal Rating{get;set;}
    public String Feeling{get;set;}  
    public boolean ResourceUsed2{get;set;}
    public decimal Rating2{get;set;}
    public String Feeling2{get;set;}    
    public String Comment1{get;set;}
    public String Comment2{get;set;}
    public String ResourceName{get;set;}
    Feedback__c ff = new Feedback__c();
    //PageReference returnPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));
    public MI_Feedback()
    {     
        if(apexpages.currentpage().getparameters().get('FeedbackId')!='' && apexpages.currentpage().getparameters().get('FeedbackId')!=null)
        {
            FeedBackId = apexpages.currentpage().getparameters().get('FeedbackId');
            getFeedbackDetails();
        }
        
    }
    public void getFeedbackDetails()
    {
       List<Feedback__c > ff = [select ResourceName__c,ResourceUsed__c,
                                           Rating__c,Feeling__c,Comment1__c,
                                                   Comment2__c from Feedback__c 
                                                       where Id = :FeedBackId limit 1];
       if(ff.size()>0){
        ResourceUsed = ff[0].ResourceUsed__c;
        Rating = ff[0].Rating__c;
        Feeling = ff[0].Feeling__c;
        Comment1 = ff[0].Comment1__c;
        Comment2 = ff[0].Comment2__c;
        ResourceName = ff[0].ResourceName__c;
       }
    }
    public void Feeling()
    {
        ff.Feeling__c = Feeling2;
        update ff;
    }
    public void Rating()
    {
        ff.Rating__c = Rating2;
        update ff;
    }
    public void ResourceUsed()
    {
        
        ff.ResourceUsed__c = ResourceUsed2;
        update ff;
    }
    public void saveFeedback()
    {

        //ff = [select ResourceUsed__c,Rating__c,Feeling__c,Comment1__c,Comment2__c from Feedback__c where Id = :FeedBackId];
        ff.Comment1__c= Comment1;
        ff.Comment2__c = Comment2;
        update ff;
        //PageReference page = new Pagereference(ApexPages.currentPage().getParameters().get('retURL'));
        //page.setRedirect(true);
        //return page; 
    }
}