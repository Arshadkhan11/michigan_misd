@isTest

public class CreateMilestonesTest   {
@IsTest(SeeAllData=true) 
public static void test_CreateMilestones(){


        Account acc=[Select id from Account where name='MIISD'];
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
       
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
  
  
  List<SP_Goal__c> Spg = new List<SP_Goal__c>();
  SP_Goal__c spg1=new SP_Goal__c(Title__c='Test',Case__c=cs.id,Domain__c='Food',Show_Domain__c=true);
  SP_Goal__c spg2=new SP_Goal__c(Case__c=cs.id,Domain__c='Shelter',Show_Domain__c=true);
  
  spg.add(spg1);
  spg.add(spg2);
  
  
  insert spg;
  
  List<SP_Transactional__c> spt = new List<SP_Transactional__c>();
  SP_Transactional__c spt1;
  
  spt1= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='parentSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Services_Button_Text__c='test',Services_Type__c='test',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today());
  insert spt1;
  
 
  
  SP_Transactional__c spt2= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='MilestoneSteps',Services_Button_Text__c='test',Services_Type__c='test',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  insert spt2;
  
  spt2.Type__c='Milestone';
  
  update spt2;
  
  
  SP_Transactional__c spt3= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='Service',Services_Button_Text__c='test',Services_Type__c='test',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  insert spt3;
  
  
  SP_Transactional__c spt4= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='Service',Services_Button_Text__c='test',Services_Type__c='',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  insert spt4;
  
  SP_Transactional__c spt5= new SP_Transactional__c(Parent__c='test',Selected_Step__c=true,Email__c='childSPT@gmail.com',Services_About_Us__c='test',
  Goal__c=spg1.id,Type__c='MilestoneSteps',Services_Button_Text__c='test',Services_Type__c='test',Name__c='test',
  Location__c='test',Phone_Number__c='1234567890',Website__c='abc@gmail.com',GoalDescription__c='test',
  How_It_Works__c='test',What_To_Do__c='test',Status__c=true,Start_Date__c=system.today(),End_Date__c=system.today(),Parent_Milestone__c=spt1.id);
  
  insert spt5;

  






}
}