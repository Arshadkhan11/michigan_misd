@isTest

public with sharing class MI_AllBulletinTest   {

@IsTest(SeeAllData=true)
 
 public static void test_MI_AllBulletinTest(){

 MI_AllBulletin ab = new MI_AllBulletin();
 
        Account acc = [select id from Account where name = 'MIISD'];
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         system.Debug('casecoach id:'+cs.SuccessCoach__c);
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        Bulletin__c b = new Bulletin__c(Name='test', Repeating__c = 'Daily', Occurences__c = 2 ,Event_Title__c='test', Bulletin_Time__c = '11:58 AM', Bulletin_End_Time__c = '11:59 AM', Calculated_End_Date__c = system.today().addDays(1),Bulletin_Date_Time__c= DateTime.newInstance(system.today(), Time.newInstance(11, 59, 59, 0)));
        insert b;
        Bulletin__c b1 = new Bulletin__c(Name='test',Event_Title__c='test', Repeating__c = 'Monthly', Occurences__c = 2 , Bulletin_Time__c = '11:57 AM', Bulletin_End_Time__c = '11:59 AM', Calculated_End_Date__c = system.today().addDays(1));
        insert b1;
        Attachment atttemp4= new Attachment();
        atttemp4.Body = Blob.valueOf('test');
        atttemp4.Name = 'CommonAppAnswers.txt';
         atttemp4.ParentId=cs.id; 
         insert atttemp4;
     
   ab.MyBulletinList(); 
   
   ab.AllBulletinList();
   ab.PartnerBulletinList(); 
   //ab.SearchBulletinList(); 
   MI_AllBulletin.CreateCalevt(b.id);
  
   
   
   MI_AllBulletin.BulletinsWrapper bw = new MI_AllBulletin.BulletinsWrapper(); 
   system.runas(u){
       ab.SearchStr = 'test';
       ab.SearchBulletinList();
   }
   

}
}