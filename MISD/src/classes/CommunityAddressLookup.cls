public class CommunityAddressLookup {    
    Public List<User> UserObjList {get; set;} 
    Public String AssessmentConId {get;set;}
    Public Contact C {get;set;}
    
    public CommunityAddressLookup() {    
        UserObjList = [SELECT Id, Address,city, ContactId FROM User WHERE Id=:UserInfo.getUserId() and id <> null LIMIT 1];
       AssessmentConId = UserObjList[0].ContactId; 
       
        if(AssessmentConId != null)
          c = [Select Address__c,Id from Contact where Id = :AssessmentConId];        
    }

     @RemoteAction
    public STATIC List<String> getaddress(String category) {
    
      List<CommunityResource__c> myCommunity;
      integer mycommunitysize;
      myCommunity = [SELECT Name,Address__c,Phone__c,Program_Name__c, Age_Group__c,Program_Website__c,Latitude__c,Longitude__c,Category__c FROM CommunityResource__c WHERE Category__c = :category];
      mycommunitysize = myCommunity.size();
      List<String> FinalOutput = new List<String>();
        
       for (integer i=0;i<mycommunitysize;i++)
       {
           FinalOutput.add(myCommunity[i].Name + '<>' + myCommunity[i].Address__c + '<>' + myCommunity[i].Latitude__c + '<>' + myCommunity[i].Longitude__c + '<>' + myCommunity[i].Phone__c + '<>' + myCommunity[i].Program_Name__c+ '<>' + myCommunity[i].Age_Group__c + '<>' + myCommunity[i].Program_Website__c+ '<>' + myCommunity[i].Category__c+ '<>' + 'no');
       }
       return FinalOutput;
    }
    
    
   @future (callout=true)  
   static public void getLocation(id recId){
        CommunityResource__c a =  [SELECT Name,Address__c,Phone__c,Provider_Type__c, Age_Group__c,Program_Website__c,Latitude__c,Longitude__c,Category__c FROM CommunityResource__c WHERE id =: recId];
        String address = a.Address__C;
        address = EncodingUtil.urlEncode(address, 'UTF-8');
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('http://maps.googleapis.com/maps/api/geocode/json?address='+address+'&amp;output=json&amp;sensor=false;key=AIzaSyC1JUjrrcH3eJHlW39aYNazfSDcA0cvF-c');
        req.setMethod('GET');
        HttpResponse res = h.send(req);
	    JSONParser parser = JSON.createParser(res.getBody());
        double lat,lon = null;

       while (parser.nextToken() != null) 
            {
                if ((parser.getCurrentToken() == JSONToken.FIELD_NAME) && (parser.getText() == 'location'))
                { parser.nextToken(); // object start
	      while (parser.nextToken() != JSONToken.END_OBJECT)
                       { String txt = parser.getText();
                        parser.nextToken();
                           if (txt == 'lat')
                               lat = parser.getDoubleValue();
                           else if (txt == 'lng')
                               lon = parser.getDoubleValue();
                      }
                 }
            }
            if (lat != null)//updating latitude and longitude back on the resource object.
            {
               a.Latitude__C = String.ValueOf(lat);
               a.Longitude__C = String.ValueOf(lon);
               update a;
            }

    }   
}