@isTest

private class MI_Search_DetailsCtrlTest  {

 @IsTest(SeeAllData=true)
 public static void test1(){
     
        Account acc = [select id from Account where name = 'MIISD']; 
 
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'Tamara', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Tamara Davis', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
     
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
     
        
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
     
        News__c N = new News__c(Headline__c='Test',Body__c='Test');
        insert N;
     
        MI_Activity__c M=new MI_Activity__c(Subject__c='Test',Description__c='Test');
        insert M;
     
        MI_Search_DetailsCtrl sd = new MI_Search_DetailsCtrl();
        MI_Search_DetailsCtrl.SearchDetailWrapper wrap = new MI_Search_DetailsCtrl.SearchDetailWrapper();
        
        sd.closePopup();
        
        PageReference pageRef = Page.MI_Search_DetailsL;
        Test.setCurrentPage(pageRef);
     

        
        
        sd.URLToDisplay = 'http://www.google.com';
        sd.showPopup();
        sd.closePopup();
        
        }
        
       
        
        
        @isTest(SeeAllData=true)
    public static void chatterTest(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
     
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='testcdc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Davis', firstname = 'Tamara', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testcdc@test.com', News_Educational__c=true,
            News_FamilyEvents__c=true, News_JobRelated__c=true, News_Medical__c=true,Chat_Text__c = true);
 
       
     
      Network Networkcom = new Network();      
      Networkcom = [SELECT Id FROM Network WHERE Name ='State of Michigan'];
      ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
      
        News__c N = new News__c(Headline__c='Test',Body__c='Test');
        insert N;
        MI_Activity__c M=new MI_Activity__c(Subject__c='Test',Description__c='Test');
        insert M;
       
         MI_Search_DetailsCtrl pd = new MI_Search_DetailsCtrl();
        
        system.runAs(u){ 
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(Networkcom.id,ConnectApi.FeedType.UserProfile,UserInfo.getUserId(), testPage);
         Test.startTest();
            
            pd.PostText = 'Hey all.';
            pd.postText1 = 'test 1';
            pd.postText2 = 'test 2';
            
            pd.sendMessage();
            pd.vSearchKeyWord  = 'Test'; 
            pd.getResults();
        
       Test.stopTest();
     } 
        
   
 
 }
 }