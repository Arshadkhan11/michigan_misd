@isTest
public class MI_CommonIntakeTest {
    @isTest public static void testMI_CommonIntake(){
       
        
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married',Is_Primary__c=True,BirthDate=system.today());
        insert con;
        Contact con1 = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married',Is_Primary__c=False,BirthDate=system.today());
        insert con1;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        Case cs = new Case(Status= 'New',Ownerid=u.id,ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
        system.Debug('casecoach id:'+cs.SuccessCoach__c);
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        CaseContactRole CaseRole1 = new CaseContactRole(CasesId = cs.id,ContactId = con1.id,Role = 'secondary Contact');
        insert CaseRole1;
     
        
               
        PageReference pageRef = Page.MI_CommonApp_Demographics;
        Test.setCurrentPage(pageRef);
        System.currentPageReference().getParameters().put('AssessmentConId',con.id);
        System.currentPageReference().getParameters().put('CaseId',cs.id);
        System.currentPageReference().getParameters().put('SelectedContactID',con.id);
        
        
     List<StaticResource> docs = new List<StaticResource>();
     docs = [select id, name, body from StaticResource where name = 'TestJSONCommonApplicationPDF'];
     if(docs.size()>0){
     String attbody = docs[0].body.toString();
     attbody = attbody.replace('00335000002jdIYAAY', con.id);
     attbody = attbody.replace('00335000002jdIiAAI', con.id);
     List<Attachment> atttemp = new List<Attachment>();
     Attachment atttemp1= new Attachment();
     atttemp1.Body = Blob.valueOf(attbody);
     atttemp1.Name = 'CommonAppAnswers.txt';
     atttemp1.ParentId=cs.id; 
     atttemp.add(atttemp1);
     
     insert atttemp;
     
     System.currentPageReference().getParameters().put('ParentAttId',atttemp1 .id);
     
     
         
     Attachment atttemp2= new Attachment();
     atttemp2.Body = Blob.valueOf('"Q3":"Branch","Q2":"No","Q4":"Sign Language","Q5":null,"Q6":"No","Q7A":null,"Q7B":null,"Q7C":null,"Q7D":null,"Q8":null,"Q9":null,"Q10":null,"Q11":null,"Q12":null}');
     atttemp2.Name = 'CommonAppIntro.txt';
     atttemp2.ParentId=cs.id; 
     atttemp.add(atttemp2);
     
     
         Boolean ErrorMsg;   
            
         MI_CommonIntake co = new MI_CommonIntake();
         co.EditAvatar='Yes';
         co.CaseNo = cs.id;                  
         co.SecConId=con1.id;
         co.SecContact();
         System.currentPageReference().getParameters().put('SelectedContactID',con1.id);
         co.saveimageCA();
         co.AddFamilyMember=true;
         co.AddFamilyContact();
         co.UpdateRelationships(cs.id,con.id,con1.id,'Child');
         co.UpdateReverseRelationships(cs.id,con.id,con1.id,'Child');
         co.getReverseRelationship('Child');
         co.ChangeRelPOV();
         co.ShowAttachmentInfoInitital('CDC');
         co.ShowAttachmentInfoInitital('FAP');
         co.ShowAttachmentInfoInitital('SER');
         co.ShowAttachmentInfoInitital('Cash');
         System.currentPageReference().getParameters().put('ProgramSelected','CDC');
         co.ShowAttachmentInfo();
         System.currentPageReference().getParameters().put('ProgramSelected','FAP');
         co.ShowAttachmentInfo();
         System.currentPageReference().getParameters().put('ProgramSelected','SER');
         co.ShowAttachmentInfo();
         System.currentPageReference().getParameters().put('ProgramSelected','Cash');
         co.ShowAttachmentInfo();
         co.UpdateAttachment();
         co.DeleteAttachment();
         co.RemoveCaseContact();
         MI_CommonIntake.getAttachmentsRemoteFIS(cs.id);
         MI_CommonIntake.getAttachmentsRemote(cs.id);
         co.ErrorMsg=false;
         
         co.Address='Test';
         co.SaveContact();
         System.currentPageReference().getParameters().put('SelectedContactID','');
         co.Address='Test1';
         co.AddFamilyMember = true;
         co.AddFamilyContact();
         co.saveimageCA();
         
         
         System.currentPageReference().getParameters().put('SelectedContactID',con.id);
         MI_CommonIntake co1 = new MI_CommonIntake();
         
         co1.SecContact();
         MI_CommonIntake.CreateAtt(cs.id,'atttemp1',atttemp1.id);
         MI_CommonIntake.CreateAttFIS(cs.id,'atttemp1',atttemp1.id);
         
         co.vSaveAllRelationships='';
         co.SaveAllRelationships();
         
        
         
        
        
            
        
        
    }
    }
}