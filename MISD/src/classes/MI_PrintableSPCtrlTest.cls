@isTest
public class MI_PrintableSPCtrlTest {
	
    @isTest static void testConst(){
        
        //Data creation
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        //Education
        Decimal dur = 120.0;
        Decimal seq = 1.0;
        Decimal dur2 = 30.0;
        Decimal seq2 = 2.0;
        SP_Goal__c vEdu = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Education',Case__c = cs.id);
        insert vEdu;

       
        Success_Plan_Master__c spmDomEdu = new Success_Plan_Master__c(Value__c='Education',Type__C = 'Domain', duration__C = dur2, Sequence__c = seq2 );
        insert spmDomEdu;

        Success_Plan_Master__c spmMilEdu = new Success_Plan_Master__c(Value__c='College Degree',Type__C = 'Milestone', parent__C = spmDomEdu.id, duration__C = dur, Sequence__c = seq);
        insert spmMilEdu;      
        
        Success_Plan_Master__c spmMilStepEdu = new Success_Plan_Master__c(Value__c='Submit application',Type__C = 'MilestoneSteps', parent__C = spmMilEdu.id, duration__C = dur2, Sequence__c = seq2);
        insert spmMilStepEdu;
        Success_Plan_Master__c spmMilStepEduHead = new Success_Plan_Master__c(Value__c='Submit application Head',Type__C = 'MilestoneHeads', parent__C = spmMilEdu.id, duration__C = dur2, Sequence__c = seq2);
        insert spmMilStepEduHead;

        SP_Transactional__c sptEdu = new SP_Transactional__c(Name__c='College Degree', Type__c='Milestone', Status__c=true,Goal__c = vEdu.id, Start_Date__C = date.today(),Success_Plan_Master2__c=spmMilEdu.Id);
        insert sptEdu;
         SP_Transactional__c sptEduRs = new SP_Transactional__c(Name__c='College Degree', Type__c='Resource', Status__c=true,Goal__c = vEdu.id, Start_Date__C = date.today(),Success_Plan_Master2__c=spmMilEdu.Id);
        insert sptEduRs;
         SP_Transactional__c sptEduSv = new SP_Transactional__c(Name__c='College Degree', Type__c='Service', Status__c=true,Goal__c = vEdu.id, Start_Date__C = date.today(),Success_Plan_Master2__c=spmMilEdu.Id);
        insert sptEduSv;

        
        PageReference pageRef = Page.MI_SP_PDF;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
        ApexPages.currentPage().getParameters().put('caseId',cs.id);
       // System.debug(cs.id + '|' + ApexPages.currentPage().getParameters().get('SP_CaseId'));
        MI_PrintableSPCtrl sp = new MI_PrintableSPCtrl();   
        
        sp.goalTimeLineHeight = new Map<Id, Integer>();
        //public Map<Id, Integer> goalTimeLineHeight {get;set;}
        //sp.GoalsWrapper gw = new sp.GoalsWrapper();
        //sp.GoalsWrapper = '';
        
    }
}