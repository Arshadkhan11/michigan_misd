public class MI_CA_CommonApp_DataExtracter {
    
    public MI_CA_CommonApp_DataExtracter(){
        
    }
    
    public static map<string, List<String>> extractjsondata(String Caseid){
        string encodedContentsString,operation = '';
        string data = '';
        Blob b;
        map<string, List<String>> newmap = new map<string, List<String>>();
        List<Attachment> atttemp = new List<Attachment>();
        List<CommonApplication__c> calst = [Select Id,CAPrograms__c,SPCAServices__c from CommonApplication__c where CaseId__c = :Caseid and Status__C = 'Active'];
        if(calst.size() != 0){
            String CAId = calst[0].Id;
            atttemp = [SELECT Id,Body FROM Attachment where ParentId = :CAId and Name = 'CommonAppAnswers.txt' LIMIT 1];
            if(atttemp.isEmpty())
            {
                system.debug('newmap'+newmap);
                return newmap;
            }
            else
            {
                Attachment Angdata = [SELECT Id,Body FROM Attachment where ParentId = :CAId and Name = 'CommonAppAnswers.txt' LIMIT 1];
                b = Angdata.Body;
                data = b.toString();
                System.debug('data: '+data);
                if(data!='[{"Category":"Personal"}]')
                {
                    list<MI_CA_PageName> ourdata = (list<MI_CA_PageName>) JSON.deserialize(data, list<MI_CA_PageName>.class);
                    system.debug('ourdata'+ ourdata.size());
                    for(MI_CA_PageName ourdata1 :ourdata)
                    {
                         system.debug('ourdata1PageCategory' + ourdata1.Category);
                         system.debug('ourdata1PageNames' + ourdata1.PageName.size()); 
                        list<MI_CA_PageSections> pagesections = ourdata1.PageName;
                        for(MI_CA_PageSections pagesection :pagesections)
                        {
                           // system.debug('pagesection'+ pagesection.name); 
                             list<MI_CA_Questions> pagesectionquestions = pagesection.PageSection;
                            for(MI_CA_Questions pagesectionquestion :pagesectionquestions)
                            {
                                List<String> sectiondetails = new List<String>();
                                sectiondetails.add(pagesectionquestion.PgSecMembers);
                                newmap.put(pagesectionquestion.name, sectiondetails);
                                // system.debug('PGMembers '+ pagesectionquestion.name +' '+ pagesectionquestion.PgSecMembers); 
                                // system.debug('Answer'+ pagesection.name); 
                                 list<MI_CA_SubQuestions> pagesectionsubquestions = pagesectionquestion.Question;
                                for(MI_CA_SubQuestions pagesectionsubquestion :pagesectionsubquestions){
                                    List<String> parentqlist = new List<String>();
                                    parentqlist.add(pagesectionsubquestion.parentquestion.Answers__c);
                                    parentqlist.add(pagesectionsubquestion.parentquestion.Members__c);
                                    newmap.put(pagesectionsubquestion.parentquestion.ID, parentqlist);
                                 list<MI_CA_SubSubQuestions> pagesectionsubsubquestions = pagesectionsubquestion.SubQuestions;  
                                      for(MI_CA_SubSubQuestions pagesectionsubsubquestion :pagesectionsubsubquestions){
                                          List<String> childqlist = new List<String>();
                                          childqlist.add(pagesectionsubsubquestion.childquestion.Answers__c);
                                          childqlist.add(pagesectionsubsubquestion.childquestion.Members__c);
                                           newmap.put(pagesectionsubsubquestion.childquestion.ID, childqlist);
                                            List<Question__c> subsublists = pagesectionsubsubquestion.SubSubQuestions;
                                          for(Question__c subsublist : subsublists){
                                              List<String> subsubqlist = new List<String>();
                                              subsubqlist.add(subsublist.Answers__c);
                                              subsubqlist.add(subsublist.Members__c);
                                              newmap.put(subsublist.Id, subsubqlist);
                                          }
                                      }
                                }
                            }
                        }
                    }
                }
                system.debug('newmap'+newmap);
                return newmap;
            }    
        }
        return newmap;
        //system.debug('particularvalue'+ newmap.get('a0Z350000008RBYEA2'));  
    }
    
    public static map<string, string> extractdatamodel(String Caseid){
        string encodedContentsString,operation = '';
        string data = '';
        Blob b;
        map<string, string> newmap = new map<string, string>();
        List<Attachment> atttemp = new List<Attachment>();
        List<CommonApplication__c> calst = [Select Id,CAPrograms__c,SPCAServices__c from CommonApplication__c where CaseId__c = :Caseid and Status__C = 'Active'];
        if(calst.size() != 0){
            String CAId = calst[0].Id;
            atttemp = [SELECT Id,Body FROM Attachment where ParentId = :CAId and Name = 'CommonAppAnswers.txt' LIMIT 1];
            if(atttemp.isEmpty())
            {
                return newmap;
            }
            else
            {
                Attachment Angdata = [SELECT Id,Body FROM Attachment where ParentId = :CAId and Name = 'CommonAppAnswers.txt' LIMIT 1];
                b = Angdata.Body;
                data = b.toString();
                if(data!='[{"Category":"Personal"}]')
                {
                    list<MI_CA_PageName> ourdata = (list<MI_CA_PageName>) JSON.deserialize(data, list<MI_CA_PageName>.class);
                    for(MI_CA_PageName ourdata1 :ourdata)
                    {
                        list<MI_CA_PageSections> pagesections = ourdata1.PageName;
                        for(MI_CA_PageSections pagesection :pagesections)
                        {
                            list<MI_CA_Questions> pagesectionquestions = pagesection.PageSection;
                            for(MI_CA_Questions pagesectionquestion :pagesectionquestions)
                            {
                                list<MI_CA_SubQuestions> pagesectionsubquestions = pagesectionquestion.Question;
                                for(MI_CA_SubQuestions pagesectionsubquestion :pagesectionsubquestions){
                                    newmap.put(pagesectionsubquestion.parentquestion.ID, pagesectionsubquestion.parentquestion.Answers__c);
                                 	list<MI_CA_SubSubQuestions> pagesectionsubsubquestions = pagesectionsubquestion.SubQuestions;  
                                      for(MI_CA_SubSubQuestions pagesectionsubsubquestion :pagesectionsubsubquestions){
                                          newmap.put(pagesectionsubsubquestion.childquestion.ID, pagesectionsubsubquestion.childquestion.Answers__c);
                                          List<Question__c> subsublists = pagesectionsubsubquestion.SubSubQuestions;
                                          for(Question__c subsublist : subsublists){
                                             // newmap.put(subsublist.Id, subsublist.Answers__c);
                                          }
                                      }
                                }
                            }
                        }
                    }
                }
                return newmap;
            }    
        }
        return newmap;
    }

}