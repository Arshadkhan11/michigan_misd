public class TasksListCont { 

// Tasks List page - VAR
    public String selectedDate {get;set;}
    public String selectedKeyword {get;set;}
    public List<String> SelectedCategory {get;set;}
    public Integer overDueCount {get;set;}
    public Boolean Flag {get;set;}
    public List<MI_Activity__c> taskListUserLogin {get;set;}
    public List<MI_Activity__c> overDueList {get;set;}
    public Integer counterOverdueTasks {get;set;}
    public Integer counterTodayTotalTasks {get;set;}
    public Integer counterTodayCompletedTasks {get;set;}

    
    public  TasksListCont()
    {
       
// Tasks List page - CONT

        
       selectedCategory = new List<string>();
       flag = false;
       taskListUserLogin = querytaskListUserLogin();
       overDueList = [SELECT Id,Subject__c,Priority__c,Status__c,Activity_Date__c,Description__c,task_type__c from MI_Activity__c 
                      where ( Activity_Date__c < TODAY ) AND ( Owner.Id=:UserInfo.getUserId() ) AND ( Status__c != 'Completed' ) 
                      ORDER BY Activity_Date__c desc];
        overDueCount = 0;
        if(!overDueList.isEmpty())
        {
	        overDueCount = overDueList.size();
        }
      

        
                
     }


    
// Tasks List page - FUNCTIONS      
    public PageReference search() {
   
         if ( selectedDate != '' || !String.isBlank(selectedKeyword) || !(selectedCategory.isEmpty()) )
          {
                flag = true;  
                taskListUserLogin = querytaskListUserLogin();
              	
          }
      else 
      {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Please select atleast one criteria.'));
       }
       
         return null;
    }
    public List<MI_Activity__c> querytaskListUserLogin()
    {
        //Overdue tasks count
        List<MI_Activity__c> OverdueTasksList = [SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c<Today)
                                          ORDER BY Activity_Date__c];
        counterOverdueTasks = OverdueTasksList.size();

// Today's Total tasks count
        List<MI_Activity__c> TasksTotalList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today)
                                          ORDER BY Activity_Date__c];
        counterTodayTotalTasks = TasksTotalList.size();       

// Today's Completed tasks count        
        List<MI_Activity__c> TasksTodayCompletedList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c='Completed')
                                          ORDER BY Activity_Date__c];
        counterTodayCompletedTasks = TasksTodayCompletedList.size();      
        

// Tasks list to display
// COMPLETE
     if(flag == true)
       {
         
                   list<String> Criteria = new list<String>();
                   
                   if( selectedDate != '')
                   {
                       String[] strDate = selectedDate.split('/');
                        Integer myIntDate = integer.valueOf(strDate[1]);
                        Integer myIntMonth = integer.valueOf(strDate[0]);
                        Integer myIntYear = integer.valueOf(strDate[2]);
                        Date d = Date.newInstance(myIntYear, myIntMonth, myIntDate);
                       Criteria.add('DAY_ONLY(Activity_Date__c) = :d');
                   }             
                   if( String.isnotBlank(selectedKeyword))
                       Criteria.add('((Subject__c LIKE \'%' + string.escapeSingleQuotes(selectedKeyword) + '%\') OR (Subject__c LIKE \'%' + string.escapeSingleQuotes(selectedKeyword) + '\') OR (Subject__c LIKE \'' + string.escapeSingleQuotes(selectedKeyword) + '%\'))');  
                    
                   if( !(selectedCategory.isEmpty()))
                       Criteria.add('Task_Type__c in :selectedCategory');  
                       
                  String whereClause = '';
                  
                   if (!criteria.isEmpty()) {
                       whereClause = ' where ' + String.join(criteria, ' AND ');
                       }
                  
                 String Query = 'SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c ' + whereClause  + ' AND Activity_Date__c!=null AND Owner.Id =\'' + UserInfo.getUserId() +'\' ORDER BY Activity_Date__c desc';    
                 List<MI_Activity__c> UserList = new List<MI_Activity__C>();
                 //system.Debug('Query'+query);
                 UserList = Database.Query(query); 
                 
                 
                 if(UserList.isEmpty())
                  {
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,'Tasks not available for the search.'));
                     return null;
                  }
                 else
                  {
                     return UserList;
                  }
       
       }
     else
     {                    
       List<MI_Activity__c> TasksList = [SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c!='Completed')
                                          ORDER BY Activity_Date__c desc];

        List<MI_Activity__c> TasksList1 = [SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c='Completed')
                                          ORDER BY Activity_Date__c desc];

        List<MI_Activity__c> TasksList2 = [SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c<Today AND
                                          Status__c!='Completed')
                                          ORDER BY Activity_Date__c desc];

        List<MI_Activity__c> TasksList3 = [SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c<Today AND
                                          Status__c='Completed') 
                                          ORDER BY Activity_Date__c desc];
        
        List<MI_Activity__c> TasksList4 = [SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c, Client_Name__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c>Today) 
                                          ORDER BY Activity_Date__c desc];

        
        TasksList.addall(TasksList1);
        TasksList.addall(TasksList2);
        TasksList.addall(TasksList3);
        TasksList.addall(TasksList4);
        return TasksList;  
    }
        
   }
   	public PageReference clear() {
  
                  flag = false;
                  taskListUserLogin = querytaskListUserLogin();
        		  
                  SelectedKeyword = null;
                 if(selectedCategory != null){
                      selectedCategory.clear();
                         }
                  
                  SelectedDate = null;
                  return null;
    }
    public PageReference overDue() {
   
         taskListUserLogin = overDueList;
         
         return null;
    }
    
    
 
}