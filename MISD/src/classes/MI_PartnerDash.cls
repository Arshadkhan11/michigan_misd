global with sharing class MI_PartnerDash {

     //Bugs
    public  Network Networkcom {get;set;}
    
    public String addcomment {get;set;}
    
    public List<User> CommUsers {get;set;}
     
    public String CommentMention { get; set; }
    
    public String selectedUser { get; set; }
    
    public String selectedAccout {get;set;}
    
    public String PostMention { get; set; }

    public String PostText { get; set; }
    
    public String CommentText {get;set;}
       
    public id FeedElementId {get;set;}
    
   
    public ConnectApi.FeedElementPage feedElementPage {get;set;}
    public ConnectApi.FeedItemInput feedItemInput {get;set;}
    public ConnectApi.MentionSegmentInput mentionSegmentInput {get;set;}
    public ConnectApi.MessageBodyInput messageBodyInput {get;set;}
    public ConnectApi.TextSegmentInput textSegmentInput {get;set;}  
    
    // tasks fixes
    public Bulletin__c Bulletintemp {get;set;}
    public Date EditDate {get;set;}
    public Time EditTime {get;set;}    

    //remoting TASKS
    public Id TaskId { get; set; }
    public static MI_Activity__c activity { get; set; }
    
    public Id delId {get;set;} // Bhagyashree
    public MI_Activity__c newTask {get;set;} // Bhagyashree
    public Date newDate {get;set;}
    public Time newTime {get;set;}
    public Id EditId {get;set;} // Bhagyashree
    public MI_Activity__c EditTask {get;set;} // Bhagyashree
    public boolean frontdiv {get;set;}
    public boolean newdiv {get;set;}
    public boolean editdiv {get;set;}
    public Integer overDueCount {get;set;}
    public String BulletinName {get;set;}
    public String BulletinType {get;set;}
    public String BulletinLocation {get;set;}
    public String BulletinDescription {get;set;}
    public String BulletinDate {get;set;}
    public String BulletinTime {get;set;}
    public Boolean BulletinSavedSucess {get; set;}
    public List<MI_Activity__c> taskListUserLogin {get;set;}
    public List<MI_Activity__c> overDueList {get;set;}
    public String taskIdChosen {get; set;}
    public Id TaskCompleteId {get;set;}
    
   public Integer counterOverdueTasks {get;set;}
   public Integer counterTodayTotalTasks {get;set;} 
   public Integer counterTodayCompletedTasks {get;set;}

   public Integer counterDueTodayTasks {get;set;}
    
// sampleCon - init - START
    public List<String> selectedCategory {get;set;}
    public List<News__c> UserList {get;set;}
    public date selectedDate {get;set;}
    public String selectedKeyword {get; set;}
    public boolean flag {get;set;}
    public List<SelectOption> categoryList {get;set;}
// sampleCon - init - END    

// query variables    
    public List<News__c> newsFeedItemslist {get;set;}
    public List<News__c> newsFeedItemslistCitizen {get;set;}
    public List<News__c> newsFeedItemslistCoach {get;set;}
    
    
// checkbox variables    
   public Boolean re1{get;set;}
   public Boolean re2{get;set;}
   public Boolean re3{get;set;}
   public Boolean re4{get;set;}
    
    public String UserCatPref1{get;set;}
    public Id RecTypeId{get;set;}
    
    public User currentUser {get;set;}
    
    public String latestcaseid {get;set;}
    
    public MI_PartnerDash()
    {

      //Removing hard coding of the CitizenCoach
    // latestcaseid=[SELECT ID, Name, Title, LatestCaseID__c from Contact where Name='Tamara Davis' and Title='Master'].LatestCaseID__c;  
   //List<Case> CaseIds = new List<Case> ();
     //  CaseIds =  [select id, contact.name from case where contact.id = : UserInfo.getUserId() order by CreatedDate desc limit 1]; 
     Id vUserContactId = [select contactid from user where id =:UserInfo.getUserId()].contactid;
     List<Case> CaseIds =  [select id from case where contact.id = :vUserContactId order by CreatedDate desc limit 1];
     if(!CaseIds.isEmpty())   
     latestcaseid = CaseIds[0].id;
        else
        latestcaseid = '';    

     selectedCategory = new List<string>();
                  flag = false;
                  getUserlist();
                  categoryList = new List<SelectOption>();
                  CategoryList.add(new SelectOption('Job Related','Job Related'));
                  CategoryList.add(new SelectOption('Family Events','Family Events'));
                  CategoryList.add(new SelectOption('Educational','Educational'));
                  CategoryList.add(new SelectOption('Medical','Medical'));
        
       re1 = true;
       re2 = true;
       re3 = true;
       re4 = true;

                
        //List<User> UserCatPref1 = [SELECT Id, Name, News_Category_Filter__c FROM User where username='aoommen@isd.com.isddev' LIMIT 100];        
        
    newsFeedItemslist = queryNewsFeedList();// ARCHVIE
    newsFeedItemslistCitizen = queryNewsFeedListCitizen(); 
    newsFeedItemslistCoach = queryNewsFeedListCoach();
        
        
        RecTypeId = [select Id from RecordType where Name='Task'].Id;
        
       newTask = new MI_Activity__c();
       EditTask=null;        
       overDueList = [SELECT Id,Subject__c,Priority__c,Status__c,Activity_Date__c,Description__c,task_type__c from MI_Activity__c where ( Activity_Date__c < TODAY ) AND ( Owner.Id=:UserInfo.getUserId() ) ORDER BY Activity_Date__c desc];
       overDueCount = overDueList.size();
       taskListUserLogin = querytaskListUserLogin();
        
    }


// Start -> news feed ARCHIVE
    public List<News__c> queryNewsFeedList()
    {
        List<String> DynCat = new List<String>{'','','',''};
        if(re1=true)
        {
            DynCat[0]='Job Related';
        }
        if(re2=true)
        {
            DynCat[1]='Family Events';
        }
        if(re3=true)
        {
            DynCat[2]='Educational';
        }
        if(re4=true)
        {
            DynCat[3]='Medical';
        }
//              List<String> DynCat = new List<String> {'Job Related','Family Events','Educational','Medical'};
        List<News__c> feedItemslist = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c, Published_on__c, Event_Date_Time__c from News__c WHERE ((Publish_Through__c > TODAY) AND (Publish_from__c <= TODAY) AND (Intended_audience__c INCLUDES ('All Audiences','Success Coaches')) AND (Category__c in :DynCat)) ORDER BY publish_from__c DESC];
//        List<News__c> feedItemslist = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c from News__c WHERE ((Publish_Through__c > TODAY) AND (Publish_from__c <= TODAY) AND (Intended_audience__c INCLUDES ('All Audiences','Success Coaches'))) ORDER BY Publish_from__c DESC];
    return feedItemslist;
    }
// End -<  news feed ARCHIVE

// Start -> news feed on Citizen dashboard
    public List<News__c> queryNewsFeedListCitizen()
    {
        Boolean Check = false;//checks whether All has been selected
        String AllVar = 'All';//checks for All, other values in multi-select
        User UNC = [SELECT Id, News_Category_Filter__c, News_Educational__c, News_FamilyEvents__c, News_JobRelated__c, News_Medical__c
                    FROM User where username=:UserInfo.getUsername()
                    LIMIT 1];
        String PrefConcat1;
        String PrefConcat2;
        String PrefConcat3;
        String PrefConcat4;
        
        if(UNC.News_Educational__c==true)
        {
            PrefConcat1 = 'Educational;';
        }
        else
        {
           PrefConcat1 = 'blank;';
        }
        
        if(UNC.News_FamilyEvents__c==true)
        {
            PrefConcat2 = 'Family Events;';
        }
        else
        {
           PrefConcat2 = 'blank;';
        }

        if(UNC.News_JobRelated__c==true)
        {
           PrefConcat3 = 'Job Related;';
        }
        else
        {
           PrefConcat3 = 'blank;';
        }

        if(UNC.News_Medical__c==true)
        {
           PrefConcat4 = 'Medical;';
        }
        else
        {
           PrefConcat4 = 'blank;';
        }
        
        String PrefConcat = PrefConcat1+PrefConcat2+PrefConcat3+PrefConcat4;
        
        String[] UserCatPref = (PrefConcat).split(';');     
//            String[] UserCatPref = ([SELECT News_Category_Filter__c FROM User where username=:UserInfo.getUsername() LIMIT 10].News_Category_Filter__c).split(';');     
                        // Check for All in user's preference            
            // No check for Category if "All" selected    


        List<News__c> feedItemslistCitizen = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c, NewsAge__c, Published_on__c, Event_Date_Time__c
                                            from News__c 
                                            WHERE ((Publish_Through__c >= TODAY) 
                                                   AND (Publish_from__c <= TODAY) 
                                                   AND (Intended_audience__c INCLUDES ('All Audiences','Citizens')) 
                                                   AND (Category__c IN :UserCatPref)) 
                                            ORDER BY publish_from__c DESC];
        return feedItemslistCitizen;

    }
// End -< news feed on Citizen dashboard

// Start -> news feed on COACH - FINAL
    public List<News__c> queryNewsFeedListCoach()
    {
        Boolean Check = false;//checks whether All has been selected
        String AllVar = 'All';//checks for All, other values in multi-select
        User UNC = [SELECT Id, News_Category_Filter__c, News_Educational__c, News_FamilyEvents__c, News_JobRelated__c, News_Medical__c
                    FROM User where username=:UserInfo.getUsername()
                    LIMIT 1];
        String PrefConcat1;
        String PrefConcat2;
        String PrefConcat3;
        String PrefConcat4;
        
        if(UNC.News_Educational__c==true)
        {
            PrefConcat1 = 'Educational;';
        }
        else
        {
           PrefConcat1 = 'blank;';
        }
        
        if(UNC.News_FamilyEvents__c==true)
        {
            PrefConcat2 = 'Family Events;';
        }
        else
        {
           PrefConcat2 = 'blank;';
        }

        if(UNC.News_JobRelated__c==true)
        {
           PrefConcat3 = 'Job Related;';
        }
        else
        {
           PrefConcat3 = 'blank;';
        }

        if(UNC.News_Medical__c==true)
        {
           PrefConcat4 = 'Medical;';
        }
        else
        {
           PrefConcat4 = 'blank;';
        }
        
        String PrefConcat = PrefConcat1+PrefConcat2+PrefConcat3+PrefConcat4;
        
        String[] UserCatPref = (PrefConcat).split(';');     
//            String[] UserCatPref = ([SELECT News_Category_Filter__c FROM User where username=:UserInfo.getUsername() LIMIT 10].News_Category_Filter__c).split(';');     
                        // Check for All in user's preference            
            // No check for Category if "All" selected    


        List<News__c> feedItemslistCoach = [SELECT Id, Body__c, Name, Headline__c, Link__c, Publish_from__c, Publish_Through__c, Category__c, NewsAge__c, Published_on__c, Event_Date_Time__c
                                            from News__c 
                                            WHERE ((Publish_Through__c >= TODAY) 
                                                   AND (Publish_from__c <= TODAY) 
                                                   AND (Intended_audience__c INCLUDES ('Community Partners','All Audiences')) 
                                                   AND (Category__c IN :UserCatPref)) 
                                            ORDER BY publish_from__c DESC];
        return feedItemslistCoach;
        
    }
// End -> news feed on COACH - FINAL

    
    // START - sampleCon
  public PageReference Search() {
  
       Date checkDate = Date.Today().addDays(-7);
         
         //if( (selectedDate >= checkDate && selectedDate <= Date.Today()) || selectedKeyword != null || selectedCategory.size() != 0)
       if(String.isBlank(selectedKeyword))     
      {
                flag = true;
             }  
         else
           {
               ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter a valid date. News feed archives from the last one week are available.'));
           }
           
         getUserlist();  
         return null;
    }
    
  public void getUserlist() {
  
         if(flag==true)
         {
                   list<String> Criteria = new list<String>();
                   
                   if( selectedDate != null)
                       Criteria.add('publish_from__c <= :selectedDate AND publish_Through__c >= :selectedDate');
                                
                   if( selectedKeyword != null)
                       Criteria.add('Headline__c LIKE \'%' + selectedKeyword +'%\'');
                    
                   if( selectedCategory.size() != 0)
                       Criteria.add('Category__c in :selectedCategory');  
                       
                  String whereClause = '';
                  
                   if (criteria.size()>0) {
                       whereClause = ' where ' + String.join(criteria, ' AND ');
                       }

                  String Query = 'select id,Name,category__c,publish_from__c,Headline__c  from News__c ' + whereClause  + ' AND (Intended_audience__c IN (\'All\',\'Coaches - Registered\',\'All Registered users\')) ORDER BY publish_from__C DESC';    
                  system.Debug('query'+query);
                  UserList = Database.Query(query); 
                
         }                     
         else
         {
                  String Query = 'Select id,Name,category__c, publish_from__c,Headline__c from News__c where Publish_from__c <= TODAY and publish_Through__c >= LAST_N_DAYS:7 AND (Intended_audience__c IN (\'All\',\'Coaches - Registered\',\'All Registered users\')) ORDER BY publish_from__C DESC';
                  UserList = Database.Query(query); 
         }    
    
     }
   
  public PageReference clear() {
  
                  flag = false;
                  getUserlist();  
                  SelectedKeyword = '';
                  if(selectedCategory != null){
                      selectedCategory.clear();
                         }
                  SelectedDate = null;
                  return null;
    }
    // END - sampleCon
    
    @RemoteAction
    Public Static List<String> CurrentBulletinList(){ 
        DateTime myDateTime = system.now();
        Date myDate = system.today();
        List<Bulletin__c> CurrentBulletin = [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime ORDER BY CreatedDate DESC LIMIT 5];
        List<String> displaydata = new String[]{};
        Integer j = 0;
        for(integer i=0; i<CurrentBulletin.size();i++)
        {
            if(CurrentBulletin[i].CreatedById == UserInfo.getUserID())
            {
               displaydata.add('<span class="bultitle"><a href="/apex/MI_Bulletin_View?bulletid='+CurrentBulletin[i].id+'">'+CurrentBulletin[i].Event_Title__c+'</a></span><span class="buledit"><a href="/apex/MI_Bulletin_Detail?bulletid='+CurrentBulletin[i].id+'" class="edit-custom">edit</a></span>'); 
            }
            else
            {
               displaydata.add('<span class="bultitle"><a href="/apex/MI_Bulletin_View?bulletid='+CurrentBulletin[i].id+'">'+CurrentBulletin[i].Event_Title__c+'</a></span>'); 
            }
            if(CurrentBulletin[i].Bulletin_Date_Time__c != null)
            {
                String Datetemp = String.valueOf(CurrentBulletin[i].Bulletin_Date_Time__c.month()) + '/' + String.valueOf(CurrentBulletin[i].Bulletin_Date_Time__c.day());
                if(myDate == (date.newinstance(CurrentBulletin[i].Bulletin_Date_Time__c.year(), CurrentBulletin[i].Bulletin_Date_Time__c.month(), CurrentBulletin[i].Bulletin_Date_Time__c.day())))
                {
                    displaydata.add(CurrentBulletin[i].Bulletin_Time__c);
                }
                else
                {
                    displaydata.add(Datetemp);
                } 
            }
            else
            {
                displaydata.add('');
            }
            
        }
        return displaydata;     
    }
    
    @RemoteAction
    Public Static List<String> MyBulletinList(){ 
        DateTime myDateTime = system.now();
        Date myDate = system.today();
        List<Bulletin__c> MyBulletin = [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime AND CreatedById = :UserInfo.getUserID() ORDER BY CreatedDate DESC LIMIT 5];
        List<String> displaydata = new String[]{};
        Integer j = 0;
        for(integer i=0; i<MyBulletin.size();i++)
        {
            displaydata.add('<span class="bultitle"><a href="/apex/MI_Bulletin_View?bulletid='+MyBulletin[i].id+'">'+MyBulletin[i].Event_Title__c+'</a></span><span class="buledit"><a href="/apex/MI_Bulletin_Detail?bulletid='+MyBulletin[i].id+'" class="edit-custom">edit</a></span>'); 
            if(MyBulletin[i].Bulletin_Date_Time__c != null)
            {
                String Datetemp = String.valueOf(MyBulletin[i].Bulletin_Date_Time__c.month()) + '/' + String.valueOf(MyBulletin[i].Bulletin_Date_Time__c.day());
                if(myDate == (date.newinstance(MyBulletin[i].Bulletin_Date_Time__c.year(), MyBulletin[i].Bulletin_Date_Time__c.month(), MyBulletin[i].Bulletin_Date_Time__c.day())))
                {
                    displaydata.add(MyBulletin[i].Bulletin_Time__c);
                }
                else
                {
                    displaydata.add(Datetemp);
                } 
            }
            else
            {
                displaydata.add('');
            }
            
        }
        return displaydata;     
    }
    
    @RemoteAction
    Public Static List<String> PartnerBulletinList(){ 
        DateTime myDateTime = system.now();
        Date myDate = system.today();
        User Userlst = [Select Agency_ID__c,id from User where Id=:UserInfo.getUserID() LIMIT 1];
        List<Bulletin__c> PartnerBulletin = [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime AND CreatedById IN (Select Id from User Where Agency_ID__c = :Userlst.Agency_ID__c) ORDER BY CreatedDate DESC LIMIT 5];
        List<String> displaydata = new String[]{};
        Integer j = 0;
        for(integer i=0; i<PartnerBulletin.size();i++)
        {
            if(PartnerBulletin[i].CreatedById == UserInfo.getUserID())
            {
               displaydata.add('<span class="bultitle"><a href="/apex/MI_Bulletin_View?bulletid='+PartnerBulletin[i].id+'">'+PartnerBulletin[i].Event_Title__c+'</a></span><span class="buledit"><a href="/apex/MI_Bulletin_Detail?bulletid='+PartnerBulletin[i].id+'" class="edit-custom">edit</a></span>'); 
            }
            else
            {
               displaydata.add('<span class="bultitle"><a href="/apex/MI_Bulletin_View?bulletid='+PartnerBulletin[i].id+'">'+PartnerBulletin[i].Event_Title__c+'</a></span>'); 
            }
            if(PartnerBulletin[i].Bulletin_Date_Time__c != null)
            {
                String Datetemp = String.valueOf(PartnerBulletin[i].Bulletin_Date_Time__c.month()) + '/' + String.valueOf(PartnerBulletin[i].Bulletin_Date_Time__c.day());
                if(myDate == (date.newinstance(PartnerBulletin[i].Bulletin_Date_Time__c.year(), PartnerBulletin[i].Bulletin_Date_Time__c.month(), PartnerBulletin[i].Bulletin_Date_Time__c.day())))
                {
                    displaydata.add(PartnerBulletin[i].Bulletin_Time__c);
                }
                else
                {
                    displaydata.add(Datetemp);
                } 
            }
            else
            {
                displaydata.add('');
            }
            
        }
        return displaydata;     
    }
    
    public PageReference GotoBulletinNew()
    {
        PageReference redirect = new PageReference('/apex/MI_Bulletin_Detail'); 
        return redirect;
    }
    
    public PageReference BulletinNew()
    {
        BulletinName = null;
        BulletinType = 'Event';
        BulletinDescription = null;
        BulletinDate = null;
        BulletinTime = null;
        BulletinLocation = null;
        return null;
    }
    
    public PageReference BulletinSaveNew()
    {
        Bulletintemp = new Bulletin__c();
        if(BulletinName == ''  || BulletinType == '' || BulletinDate == '' || BulletinTime == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please fill all the required fields'));
            BulletinSavedSucess = false;
            return null;
        }
        if(BulletinDate != null && BulletinTime != null)
        {
            DateTime ParsedDateTime = DateTime.parse(BulletinDate+' '+BulletinTime);
            Bulletintemp.Bulletin_Date_Time__c = ParsedDateTime;
            if(ParsedDateTime < system.now()){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Date can not be in the past'));
                BulletinSavedSucess = false;
                return null;
            } 
        } 
        Bulletintemp.OwnerId = UserInfo.getUserId();
        Bulletintemp.Event_Title__c = BulletinName;
        Bulletintemp.Type__c = BulletinType;
        Bulletintemp.Bulletin_Time__c = BulletinTime;
        Bulletintemp.Location__c = BulletinLocation;   
        Bulletintemp.Description__c = BulletinDescription;
        try
        {
            insert Bulletintemp;
        }
        catch(DMLException e)
        {
            BulletinSavedSucess = false;
            System.debug('The following exception has occurred: ' + e.getMessage());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please enter valid details.'));
        }
        BulletinSavedSucess = true;
        return null;
    } 
    
     public List<MI_Activity__c> querytaskListUserLogin()
    {
        
        
        
        //Overdue tasks count
        List<MI_Activity__c> OverdueTasksList = [SELECT Id,Activity_Date__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c<Today AND
                                          Status__c!='Completed' AND
                                          RecordTypeId =: RecTypeId)
                                          ORDER BY Activity_Date__c];
        counterOverdueTasks = OverdueTasksList.size();

// Today's Total tasks count
        List<MI_Activity__c> TasksTotalList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          RecordTypeId =: RecTypeId)
                                          ORDER BY Activity_Date__c];
        counterTodayTotalTasks = TasksTotalList.size();       

// Today's Completed tasks count        
        List<MI_Activity__c> TasksTodayCompletedList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c='Completed' AND
                                          RecordTypeId =: RecTypeId)
                                          ORDER BY Activity_Date__c];
        counterTodayCompletedTasks = TasksTodayCompletedList.size();      
        

// Tasks list to display
// COMPLETE
                             
        List<MI_Activity__c> TasksList = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c!='Completed' AND
                                          RecordTypeId =: RecTypeId)
                                          ORDER BY Status__c];


        List<MI_Activity__c> TasksList1 = [SELECT Id,Activity_Date__c,Client_Name__c, Description__c, Priority__c, Subject__c, Status__c, Task_Type__c from MI_Activity__c 
                                          where 
                                          (Owner.Id=:UserInfo.getUserId() AND
                                          Activity_Date__c=Today AND
                                          Status__c='Completed' AND
                                          RecordTypeId =: RecTypeId)
                                          ];
        TasksList.addall(TasksList1);
        return TasksList;       
    }
  
      public void refresh()
    {
       
       Networkcom = [SELECT Id FROM Network WHERE Name ='State of Michigan'];
         
       feedElementPage = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Networkcom.id, ConnectApi.FeedType.UserProfile,UserInfo.getUserId());
            
       try{
          
           CommUsers = [SELECT id, Name FROM User WHERE User.Profile.UserLicense.Name LIKE 'Partner Community%' OR User.Profile.UserLicense.Name LIKE 'Customer Community%'];
          
       }Catch(exception e){}
        
       
        System.Debug('after refresh');
     
    }
   
   
    public void AddPost()
    {
     
      List<String> MentionUsers = PostMention.Split(';');
      system.debug('Mentions:'+MentionUsers);
      
      feedItemInput = new ConnectApi.FeedItemInput();

      mentionSegmentInput = new ConnectApi.MentionSegmentInput();

      messageBodyInput = new ConnectApi.MessageBodyInput();

      textSegmentInput = new ConnectApi.TextSegmentInput();

      messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
      
      User u;
      try{
           for( String s: MentionUsers)
           {
            u = [select id,name from User where name like :s];
            SendSMSForChatter sms = new SendSMSForChatter();
            sms.SendSMSGlobal(u.name,PostText,u.id);

            mentionSegmentInput.id = u.id;
            system.Debug('Mentions:'+ mentionSegmentInput.id);
            messageBodyInput.messageSegments.add(mentionSegmentInput);
            mentionSegmentInput = new ConnectApi.MentionSegmentInput();

           }
      }catch(Exception e){
        u = null;
       }
       
     system.debug('PostText :'+PostText );
     if(PostText != null && MentionUsers != null)
     {
     
      textSegmentInput.text = PostText;
      
    

      messageBodyInput.messageSegments.add(textSegmentInput);

      feedItemInput.body = messageBodyInput;

      feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;

      feedItemInput.subjectId = UserInfo.getUserId();

      System.Debug('Inside posting'+feedItemInput);
        ConnectApi.FeedElement feedElement; 
      try{
      feedElement = ConnectApi.ChatterFeeds.postFeedElement(Networkcom.id, feedItemInput, null);
      }catch(Exception e){}
      System.Debug('after posting:'+feedElement);
     }
       
      
       PostText =''; PostMention='';  
      refresh();
     
   }
   
   public void AddComment()
   {
         
     System.Debug('entring comment:');
     ConnectApi.CommentInput commentInput = new ConnectApi.CommentInput();

     mentionSegmentInput = new ConnectApi.MentionSegmentInput();

     messageBodyInput = new ConnectApi.MessageBodyInput();

     textSegmentInput = new ConnectApi.TextSegmentInput();

     messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();
     
     system.Debug('commentVatsan:'+CommentText);
       
     
     textSegmentInput.text = CommentText;
     messageBodyInput.messageSegments.add(textSegmentInput);
     commentInput.body = messageBodyInput;
    
      
     if(feedElementId != null && CommentText != null)
     ConnectApi.Comment commentRep = ConnectApi.ChatterFeeds.postCommentToFeedElement(Networkcom.id, feedElementId, commentInput, null);
   
      CommentText = '';
      refresh();
   }
    
    
   public Object TextSegType { get{
        return ConnectApi.MessageSegmentType.Text;
      }
   }
   
   public Object MentionSegType {get{ 
     return ConnectApi.MessageSegmentType.Mention;
     }
   }   
   
   public Object LinkSegType { get{
      return ConnectApi.MessageSegmentType.Link;
      }
   }    
    
}