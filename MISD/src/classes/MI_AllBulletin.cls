public class MI_AllBulletin {
   public  List<BulletinsWrapper> bulsList {get;set;}
    public string profilepartner {get;set;}
    public String filtertype {get;set;}
    public string SearchStr {get;set;}
    
    public MI_AllBulletin(){
        bulsList = new List<BulletinsWrapper>();
    }
    
    //To calculate the day of the week
    public String readableDay(DateTime d) {
    Datetime dt = DateTime.newInstance(d.year(), d.month(), d.day());
    return dt.format('EEEE'); 
    }
    
    //To get the Month in a particular format
    public String readableMonth(DateTime d) {
    Datetime dt = DateTime.newInstance(d.year(), d.month(), d.day());
    return dt.format('MMM'); 
    }
    
    Public void AllBulletinList(){ 
        filtertype = 'All';
        List<Profile> ProfileName = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProflieName = ProfileName[0].Name;
        if(MyProflieName != 'Partner Profile')
        {
            profilepartner = 'NotEditable';
        }
        DateTime myDateTime = system.now();
        //List<Bulletin__c> AllBulletin = [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime ORDER BY CreatedDate DESC];
    	bulsList.clear();
        List<Attachment> bulletinatt = new List<Attachment>();
		List<MI_Activity__c> presentact = new List<MI_Activity__c>();
        for(Bulletin__c tmpbul : [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime ORDER BY CreatedDate DESC])
        {
            BulletinsWrapper bw = new BulletinsWrapper();
            bw.BulletinTitle = tmpbul.Event_Title__c;
            bw.BulletinDesc = tmpbul.Description__c;
            bw.BulletinURL = tmpbul.Website__c;
            bw.BulletinLoc = tmpbul.Location__c;
            if(tmpbul.Bulletin_Date_Time__c != null)
            {
            	bw.BulletinDate = readableDay(tmpbul.Bulletin_Date_Time__c) + ', '+readableMonth(tmpbul.Bulletin_Date_Time__c)+' '+String.valueOf(tmpbul.Bulletin_Date_Time__c.day());
         		bw.BulletinTime = tmpbul.Bulletin_Time__c + ' to ' + tmpbul.Bulletin_End_Time__c;
                bw.BulletinCal = 'Editable';
            }
            else
            {
                bw.BulletinDate = '';
                bw.BulletinTime = '';
                bw.BulletinCal = 'NotEditable';
            }
            bw.ContactPerson = tmpbul.Contact_Person__c;
            bw.PhoneNo = tmpbul.Phone_Number__c;
            bw.BulletinId = tmpbul.Id;
            if(tmpbul.CreatedById == UserInfo.getUserId())
            {
                bw.BulletinEdit = 'Editable';
            }
            else
            {
               bw.BulletinEdit = 'NotEditable'; 
            }
            bulletinatt = [Select Name,id from Attachment where parentid=:tmpbul.Id LIMIT 1]; 
            if(bulletinatt.size()>0)
            {
                bw.AttachmentId = '/servlet/servlet.FileDownload?file='+bulletinatt[0].id;        
            	bw.AttachmentName = bulletinatt[0].Name;
                bw.atticon = 'Editable';
            }
            else
            {
                bw.AttachmentId = '';        
            	bw.AttachmentName = '';
                bw.atticon = 'NotEditable';
            }
            presentact = [Select id from MI_Activity__c where BulletinId__c = :tmpbul.id AND OwnerId=:UserInfo.getUserId()];
            if(presentact.size()>0)
            {
                bw.CalendarPresent = 'present-calendar';
            }
            else
            {
               bw.CalendarPresent = 'add-to-calendar'; 
            }
            bulsList.add(bw);
        }    
    }
  
    Public void MyBulletinList(){ 
        filtertype = 'My';
        List<Profile> ProfileName = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProflieName = ProfileName[0].Name;
        if(MyProflieName != 'Partner Profile')
        {
            profilepartner = 'NotEditable';
        }
		DateTime myDateTime = system.now();
       // List<Bulletin__c> MyBulletin = [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime AND CreatedbyId=:UserInfo.getUserId() ORDER BY CreatedDate DESC];
        
    	bulsList.clear();
        List<Attachment> bulletinatt = new List<Attachment>();
		List<MI_Activity__c> presentact = new List<MI_Activity__c>();
        for(Bulletin__c tmpbul : [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime AND CreatedbyId=:UserInfo.getUserId() ORDER BY CreatedDate DESC])
        {
            BulletinsWrapper bw = new BulletinsWrapper();
            bw.BulletinTitle = tmpbul.Event_Title__c;
            bw.BulletinDesc = tmpbul.Description__c;
            bw.BulletinURL = tmpbul.Website__c;
            bw.BulletinLoc = tmpbul.Location__c;
            if(tmpbul.Bulletin_Date_Time__c != null)
            {
            	bw.BulletinDate = readableDay(tmpbul.Bulletin_Date_Time__c) + ', '+readableMonth(tmpbul.Bulletin_Date_Time__c)+' '+String.valueOf(tmpbul.Bulletin_Date_Time__c.day());
         		bw.BulletinTime = tmpbul.Bulletin_Time__c + ' to ' + tmpbul.Bulletin_End_Time__c;
                bw.BulletinCal = 'Editable';
            }
            else
            {
                bw.BulletinDate = '';
                bw.BulletinTime = '';
                bw.BulletinCal = 'NotEditable';
            }
            bw.ContactPerson = tmpbul.Contact_Person__c;
            bw.PhoneNo = tmpbul.Phone_Number__c;
            bw.BulletinId = tmpbul.Id;
            if(tmpbul.CreatedById == UserInfo.getUserId())
            {
                bw.BulletinEdit = 'Editable';
            }
            else
            {
               bw.BulletinEdit = 'NotEditable'; 
            }
            bulletinatt = [Select Name,id from Attachment where parentid=:tmpbul.Id LIMIT 1]; 
            if(bulletinatt.size()>0)
            {
                bw.AttachmentId = '/servlet/servlet.FileDownload?file='+bulletinatt[0].id;        
            	bw.AttachmentName = bulletinatt[0].Name;
                bw.atticon = 'Editable';
            }
            else
            {
                bw.AttachmentId = '';        
            	bw.AttachmentName = '';
                bw.atticon = 'NotEditable';
            }
            presentact = [Select id from MI_Activity__c where BulletinId__c = :tmpbul.id AND OwnerId=:UserInfo.getUserId()];
            if(presentact.size()>0)
            {
                bw.CalendarPresent = 'present-calendar';
            }
            else
            {
               bw.CalendarPresent = 'add-to-calendar'; 
            }
            bulsList.add(bw);
        }
    }
    
    Public void SearchBulletinList(){ 
        List<Profile> ProfileName = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProflieName = ProfileName[0].Name;
        if(MyProflieName != 'Partner Profile')
        {
            profilepartner = 'NotEditable';
        }
        DateTime myDateTime = system.now();
        String newSearchText = '%'+SearchStr+'%';
        //List<Bulletin__c> SearchBulletin = [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime 
          //                                  AND (Event_Title__c LIKE :newSearchText OR Location__c LIKE :newSearchText OR Contact_Person__c LIKE :newSearchText OR Website__c LIKE :newSearchText) ORDER BY CreatedDate DESC];
    	if(SearchStr !='')
        {
        List<List<sObject>> finalsearchResults = new List<List<sObject>>(); 
        bulsList.clear();
        finalsearchResults = [FIND :SearchStr IN ALL FIELDS 
                                            RETURNING Bulletin__c (Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, OwnerId, CreatedById
                              				Where Calculated_End_Date__c>=:myDateTime )];
        List<Bulletin__c> SearchBulletin = new List<Bulletin__c>();
        if(finalsearchResults.size() > 0)
        {
             if(!finalsearchResults[0].isEmpty()) 
             {
                 SearchBulletin =  ((List<Bulletin__c>)finalsearchResults[0]); 
             }
        }
        List<Attachment> bulletinatt = new List<Attachment>();
		List<MI_Activity__c> presentact = new List<MI_Activity__c>();
        for(Bulletin__c tmpbul : SearchBulletin)
        {
            BulletinsWrapper bw = new BulletinsWrapper();
            bw.BulletinTitle = tmpbul.Event_Title__c;
            bw.BulletinDesc = tmpbul.Description__c;
            bw.BulletinURL = tmpbul.Website__c;
            bw.BulletinLoc = tmpbul.Location__c;
            if(tmpbul.Bulletin_Date_Time__c != null)
            {
            	bw.BulletinDate = readableDay(tmpbul.Bulletin_Date_Time__c) + ', '+readableMonth(tmpbul.Bulletin_Date_Time__c)+' '+String.valueOf(tmpbul.Bulletin_Date_Time__c.day());
         		bw.BulletinTime = tmpbul.Bulletin_Time__c + ' to ' + tmpbul.Bulletin_End_Time__c;
                bw.BulletinCal = 'Editable';
            }
            else
            {
                bw.BulletinDate = '';
                bw.BulletinTime = '';
                bw.BulletinCal = 'NotEditable';
            }
            bw.ContactPerson = tmpbul.Contact_Person__c;
            bw.PhoneNo = tmpbul.Phone_Number__c;
            bw.BulletinId = tmpbul.Id;
            if(tmpbul.CreatedById == UserInfo.getUserId())
            {
                bw.BulletinEdit = 'Editable';
            }
            else
            {
               bw.BulletinEdit = 'NotEditable'; 
            }
            bulletinatt = [Select Name,id from Attachment where parentid=:tmpbul.Id LIMIT 1]; 
            if(bulletinatt.size()>0)
            {
                bw.AttachmentId = '/servlet/servlet.FileDownload?file='+bulletinatt[0].id;        
            	bw.AttachmentName = bulletinatt[0].Name;
                bw.atticon = 'Editable';
            }
            else
            {
                bw.AttachmentId = '';        
            	bw.AttachmentName = '';
                bw.atticon = 'NotEditable';
            }
            presentact = [Select id from MI_Activity__c where BulletinId__c = :tmpbul.id AND OwnerId=:UserInfo.getUserId()];
            if(presentact.size()>0)
            {
                bw.CalendarPresent = 'present-calendar';
            }
            else
            {
               bw.CalendarPresent = 'add-to-calendar'; 
            }
            bulsList.add(bw);
        }
        }
        else
        {
            AllBulletinList();
        }
    }
    
    Public void PartnerBulletinList(){ 
        filtertype = 'Agency';
        List<Profile> ProfileName = [SELECT Id, Name FROM Profile WHERE Id=:userinfo.getProfileId() LIMIT 1];
        String MyProflieName = ProfileName[0].Name;
        if(MyProflieName != 'Partner Profile')
        {
            profilepartner = 'NotEditable';
        }
        DateTime myDateTime = system.now();
        User Userlst = [Select Agency_ID__c,id from User where Id=:UserInfo.getUserID() LIMIT 1];
        //List<Bulletin__c> AllBulletin = [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime AND CreatedById IN (Select Id from User Where Agency_ID__c = :Userlst.Agency_ID__c) ORDER BY CreatedDate DESC];
    	bulsList.clear();
        List<Attachment> bulletinatt = new List<Attachment>();
		List<MI_Activity__c> presentact = new List<MI_Activity__c>();
        for(Bulletin__c tmpbul : [SELECT Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, OwnerId, CreatedById FROM Bulletin__c WHERE Calculated_End_Date__c>=:myDateTime AND CreatedById IN (Select Id from User Where Agency_ID__c = :Userlst.Agency_ID__c) ORDER BY CreatedDate DESC])
        {
            BulletinsWrapper bw = new BulletinsWrapper();
            bw.BulletinTitle = tmpbul.Event_Title__c;
            bw.BulletinDesc = tmpbul.Description__c;
            bw.BulletinURL = tmpbul.Website__c;
            bw.BulletinLoc = tmpbul.Location__c;
            if(tmpbul.Bulletin_Date_Time__c != null)
            {
            	bw.BulletinDate = readableDay(tmpbul.Bulletin_Date_Time__c) + ', '+readableMonth(tmpbul.Bulletin_Date_Time__c)+' '+String.valueOf(tmpbul.Bulletin_Date_Time__c.day());
         		bw.BulletinTime = tmpbul.Bulletin_Time__c + ' to ' + tmpbul.Bulletin_End_Time__c;
                bw.BulletinCal = 'Editable';
            }
            else
            {
                bw.BulletinDate = '';
                bw.BulletinTime = '';
                bw.BulletinCal = 'NotEditable';
            }
            bw.ContactPerson = tmpbul.Contact_Person__c;
            bw.PhoneNo = tmpbul.Phone_Number__c;
            bw.BulletinId = tmpbul.Id;
            if(tmpbul.CreatedById == UserInfo.getUserId())
            {
                bw.BulletinEdit = 'Editable';
            }
            else
            {
               bw.BulletinEdit = 'NotEditable'; 
            }
            bulletinatt = [Select Name,id from Attachment where parentid=:tmpbul.Id LIMIT 1]; 
            if(bulletinatt.size()>0)
            {
                bw.AttachmentId = '/servlet/servlet.FileDownload?file='+bulletinatt[0].id;        
            	bw.AttachmentName = bulletinatt[0].Name;
                bw.atticon = 'Editable';
            }
            else
            {
                bw.AttachmentId = '';        
            	bw.AttachmentName = '';
                bw.atticon = 'NotEditable';
            }
            presentact = [Select id from MI_Activity__c where BulletinId__c = :tmpbul.id AND OwnerId=:UserInfo.getUserId()];
            if(presentact.size()>0)
            {
                bw.CalendarPresent = 'present-calendar';
            }
            else
            {
               bw.CalendarPresent = 'add-to-calendar'; 
            }
            bulsList.add(bw);
        }    
    }
    
    public class BulletinsWrapper{
        public string     BulletinTitle      {get;set;}
        public string     BulletinDesc     {get;set;}
        public String     BulletinURL     {get;set;}
        public string     BulletinLoc       {get;set;}
        public string     BulletinDate      {get;set;}
        public string     BulletinTime    {get;set;}
        public string     ContactPerson    {get;set;}
        public string     PhoneNo     {get;set;}          
        public string     AttachmentId      {get;set;}
        public string     AttachmentName      {get;set;}
        public string     BulletinId     {get;set;}
        public string	 BulletinEdit {get;set;}
        public string	 BulletinCal {get;set;}
        public string  	atticon {get;set;}
        public String 	CalendarPresent {get;set;}
    }

    @RemoteAction
    public static boolean CreateCalevt(String vBulletinId){
        Bulletin__c tmpbul = [Select Id, Event_Title__c, Bulletin_Date_Time__c, Bulletin_Time__c, Bulletin_End_Time__c, Type__c, Category__c, Publishing_Agency__c, Description__c, Location__c, Contact_Person__c, Phone_Number__c, Website__c, Repeating__c, Occurences__c from Bulletin__c where Id=:vBulletinId];
        Event_Attendee__c evRel = new Event_Attendee__c();
        List<MI_Activity__c> SendEventpresent = [Select id from MI_Activity__c where BulletinId__c = :vBulletinId AND OwnerId=:UserInfo.getUserId()];
        if(SendEventpresent.size()==0) 
        {
            //Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].Id;
            Id EvRecTypeId = Schema.SObjectType.MI_Activity__c.getRecordTypeInfosByName().get('Event').getRecordTypeId();
            MI_Activity__c SendEventVar = new MI_Activity__c(); 
            SendEventVar.RecordTypeId = EvRecTypeId;
            SendEventVar.start_date__c= tmpbul.Bulletin_Date_Time__c;
            String tmpDate = tmpbul.Bulletin_Date_Time__c.month()+'/'+ tmpbul.Bulletin_Date_Time__c.day()+'/'+tmpbul.Bulletin_Date_Time__c.year();
            DateTime ParsedEndDateTime = DateTime.parse(tmpDate+' '+tmpbul.Bulletin_End_Time__c);
            SendEventVar.End_date__c = ParsedEndDateTime;
            SendEventVar.Location__c = tmpbul.Location__c;
            SendEventVar.Subject__c = tmpbul.Event_Title__c;
            SendEventVar.Description__c = tmpbul.Description__c + '\nContact: \n'+tmpbul.Contact_Person__c+ '\n'+tmpbul.Phone_Number__c+'\n\n'+tmpbul.Website__c+'\nCategory:'+tmpbul.Category__c+'\nPublishing Agency:'+tmpbul.Publishing_Agency__c;
            SendEventVar.BUlletinId__c = vBulletinId;
            insert SendEventVar;
            evRel.Event__c = SendEventVar.Id;
            evRel.Attendee__c = UserInfo.getUserId();
        	insert evRel;
            if(tmpbul.Repeating__c != '' && tmpbul.Repeating__c != 'Off' && tmpbul.Occurences__c > 1)
            {
                List <MI_Activity__c> repeatbul = new List <MI_Activity__c>();
                List <Event_Attendee__c> repRel = new List <Event_Attendee__c>();
				String vrep = tmpbul.Repeating__c;
                Decimal vocc = tmpbul.Occurences__c;
                Integer vweekly = 7;
                Integer vbimon = 14;
                DateTime t;
                String repStartDate ='';
                MI_Activity__c repevt;
                for(Integer i=1;i<vocc;i++)
                { 
                    repevt = new MI_Activity__c();
                    repevt.RecordTypeId = EvRecTypeId;
                    repevt.Location__c = tmpbul.Location__c;
                    repevt.Subject__c = tmpbul.Event_Title__c;
                    repevt.Description__c = tmpbul.Description__c + '\nContact: \n'+tmpbul.Contact_Person__c+ '\n'+tmpbul.Phone_Number__c+'\n\n'+tmpbul.Website__c+'\nCategory:'+tmpbul.Category__c+'\nPublishing Agency:'+tmpbul.Publishing_Agency__c;
                    repevt.BUlletinId__c = vBulletinId;
                    if(vrep == 'Daily')
                	{
                        t = tmpbul.Bulletin_Date_Time__c.AddDays(i);
                    }
                    if(vrep == 'Weekly')
                	{
                        t = tmpbul.Bulletin_Date_Time__c.AddDays(vweekly);
                        vweekly = vweekly+7;
                    }
                    if(vrep == 'Monthly')
                	{
                        t = tmpbul.Bulletin_Date_Time__c.AddMonths(i);
                    }
                    if(vrep == 'Bimonthly')
                	{
                        t = tmpbul.Bulletin_Date_Time__c.AddDays(vbimon);
                        vbimon = vbimon+14;
                    }
                    if(vrep == 'Yearly')
                	{
                        t = tmpbul.Bulletin_Date_Time__c.AddYears(i);
                    }
                    repStartDate = t.month()+'/'+ t.day()+'/'+t.year();
                    DateTime repParsedStartDateTime = DateTime.parse(repStartDate+' '+tmpbul.Bulletin_Time__c);
                    repevt.start_date__c = repParsedStartDateTime;
                    DateTime repParsedEndDateTime = DateTime.parse(repStartDate+' '+tmpbul.Bulletin_End_Time__c);
                    repevt.End_date__c = repParsedEndDateTime;
                    repeatbul.add(repevt);
                }
                insert repeatbul;
                Event_Attendee__c Repeatevtrel;
                for(MI_Activity__c act : repeatbul)
                {
                    Repeatevtrel = new Event_Attendee__c();
                    Repeatevtrel.Event__c = act.id;
                    Repeatevtrel.Attendee__c = UserInfo.getUserId();
                    repRel.add(Repeatevtrel);
                }
                insert repRel;
            }   
            return true;
        }
        else
        {
            return false;
        }
        
    }
}