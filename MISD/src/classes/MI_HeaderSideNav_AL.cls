public class MI_HeaderSideNav_AL 
{


	// Notifications top bar display     
    public String UserId{get;set;}
    public List<Notification__c> NotifList {get;set;}
    public Integer NewNotifCount {get;set;}
    
    // Notificaations top bar READ
    public Id NotifEditId {get;set;}
    public Notification__c NotifRec {get;set;}
    public Id CaseContactId {get;set;}

    public Id ProfId {get;set;}
    public String ProfileName {get; set; }
    public contact ContactDetails {get;set;}
    
    public MI_HeaderSideNav_AL()
    {
        ProfId = userinfo.getProfileId();
        ProfileName = [select Name from profile where id = :ProfId].Name;
        
        NotifEditId=null;
        UserId=[select Id,contactId from user where id =:UserInfo.getUserId()].Id; 
        user u =[select Id,contactId from user where id =:UserInfo.getUserId()];
        try{
        ContactDetails = [select Id, FirstName, LastName,ContactImageName__c from Contact where Id=: u.ContactId Limit 1];
        system.debug('details:'+ContactDetails);
        }catch(Exception e){ ContactDetails = null;}
        
        if(UserId!=null && UserId!='')
        {
            NotificationsList();
        }
        
    }
    
    
    public PageReference NotificationsList()
    {
        NotifList=[select Id,Notification_Description__c,Notification_Date_Time__c,Source__c, Unread__c, GoalType2__c, FeedbackURL__c, Parent_Id__c, CaseId__c, OwnerId
                   from Notification__c
                 	where Owner.Id=:UserInfo.getUserId() AND
                   	Unread__c = true
                 	Order by Notification_Date_Time__c DESC];
        NewNotifCount = NotifList.size();
        return null;
    }


    public void MarkRead()
    {
        //System.debug('ATO -> MarkRead - START');
        try
        {
            if((NotifEditId!=null))
            {
		//System.debug('ATO -> MarkRead - IF loop');                
                NotifRec = [Select Id,Notification_Description__c,Notification_Date_Time__c,Source__c, Unread__c, GoalType2__c, FeedbackURL__c, Parent_Id__c, CaseId__c, OwnerId 
                            from Notification__c 
                            where Id=:NotifEditId
                           	LIMIT 999];
        //System.debug('ATO -> MarkRead - 1'+NotifRec.Unread__c);
                
                NotifRec.Unread__c=false;
       // System.debug('ATO -> MarkRead - 2'+NotifRec.Unread__c);
                
                update NotifRec;
            }
        }
        catch(Exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, '');
            //System.debug('Exception found: '+e);
        }
        NotifEditId=null;
        NotificationsList();
        //System.debug('ATO -> MarkRead - END');
        //return null;
    }
    

    
    public PageReference Drilldown()
    {
        try
        {
            if((NotifEditId!=null))
            {
                NotifRec = [Select Id,Notification_Description__c,Notification_Date_Time__c,Source__c, Unread__c, GoalType2__c, FeedbackURL__c, Parent_Id__c, CaseId__c, OwnerId 
                            from Notification__c 
                            where Id=:NotifEditId
                           LIMIT 999];
                NotifRec.Unread__c=false;
                update NotifRec;
            }
        }
        catch(Exception e)
        {
            return null;
            //System.debug('Exception found: '+e);
        }

        String pagename = 'CommunitiesLanding';

        if(NotifRec.Source__c=='Resource Feedback')
        {
            pagename = 'MI_Feedback';
        }
        else if (NotifRec.Source__c=='Milestone')
        {
            pagename = 'MI_CitizenDashboard';
        }
        else if(NotifRec.Source__c=='Bulletin Sent')
        {
            pagename = 'MI_Bulletin_View';
        }
        else if(NotifRec.Source__c=='Required Document')
        {
            pagename = 'MI_CommonApplication';
        }
                
        PageReference page = new PageReference('/apex/'+pagename);
        page.setRedirect(true);
        page.getParameters().put('FeedbackId',NotifRec.FeedbackURL__c);
        page.getParameters().put('SPGoalName',NotifRec.GoalType2__c);
        page.getParameters().put('bulletid',NotifRec.Parent_Id__c);
        if(NotifRec.Source__c=='Required Document')
        {
        	CaseContactId=[Select Id,ContactId from Case where Id=:NotifRec.CaseId__c].ContactId;
            page.getParameters().put('CaseId',NotifRec.CaseId__c);
            page.getParameters().put('AssessmentConId',CaseContactId);
            page.setAnchor('tabs-0-pagetabs-summary');
            //page.getParameters().put('CaseId',NotifRec.'50035000000cnybAAA');
            //page.getParameters().put('AssessmentConId','00335000002K9B7AAK');
        }
        NotifEditId=null;
        return page;
        //return null;

        
    }

}