public class MI_MultiSelectController{
 
    // SelectOption lists for public consumption
    public SelectOption[] leftOptions { get; set; }
    public SelectOption[] rightOptions { get; set; }
    
     /***********
        * Method name : setOptions
        * Description : Parse &-separated values and labels from value and 
                        put them in option
        * Return Type : void
        * Parameters  : SelectOption[] options, String value.
     ***********/
    public void setOptions(SelectOption[] options, String value) {
        if(options!=null)
        options.clear();
        String[] parts = value.split('&');
        for (Integer i=0; i<parts.size()/2; i++) {
            options.add(new SelectOption(EncodingUtil.urlDecode(parts[i*2], 'UTF-8'), 
              EncodingUtil.urlDecode(parts[(i*2)+1], 'UTF-8')));
        }
    }
    /***********
        * Method name : leftOptionsHidden
        * Description : Backing for hidden text field containing the options from the
                        left list
        * Return Type : String
        * Parameters  : none
     ***********/
     public String leftOptionsHidden { get; set {
           leftOptionsHidden = value;
           setOptions(leftOptions, value);
        }
    }
     /***********
        * Method name : rightOptionsHidden
        * Description : Backing for hidden text field containing the options from the
                        right list
        * Return Type : String
        * Parameters  : none
     ***********/
    public String rightOptionsHidden { get; set {
           rightOptionsHidden = value;
           setOptions(rightOptions, value);
        }
    }
}