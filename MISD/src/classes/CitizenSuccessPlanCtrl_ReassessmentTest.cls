@IsTest public class CitizenSuccessPlanCtrl_ReassessmentTest{
    @IsTest(SeeAllData=true) 
       
   public static void testCitizenSuccessPlanCtrl_Reassessment() {
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        
       Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile'];
       
        
       User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                         LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
                  Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
                        Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
       insert u;
       
       Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',Active_Goal__c = 'Test');
       insert cs;
       
       System.debug('Hello' + cs.id);
       
       CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
       
       

     system.debug('hello123'+apexpages.currentpage().getparameters());
       System.runAs(u){
           PageReference pageRef = Page.CitizenSuccessPlanProcess_Reassessment;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
        ApexPages.currentPage().getParameters().put('SP_CaseId',cs.id); 
        CitizenSuccessPlanCtrl_Reassessment controller = new CitizenSuccessPlanCtrl_Reassessment();
        
       }
   }
}