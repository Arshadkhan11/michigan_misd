@isTest public class MI_Register_AgencyClassTest {
    @isTest(SeeAllData = True)
    public static void test1(){
        
        MI_Register_AgencyClass ag = new MI_Register_AgencyClass();
        ag.NewAgency.Agency_Name__c = 'Trial';
        ag.NewAgency.Contact_No__c = '1234567890'; 
        ag.NewAgency.Address__c = 'Muskegon';
        ag.NewAgency.Agency_Description__c = 'xyz';
        ag.NewAgency.Primary_Contact_Name__C = 'sophie';
        ag.NewAgency.Primary_Contact_Email__c = 'bhb@deloitte.com';
        ag.NewAgency.Secondary_Contact_Name__c = 'Turner';
        ag.NewAgency.Secondary_Contact_Email__c = 'abc@deloitte.com';
        ag.education = 'true';
        ag.educationService.add('a');
        ag.employment = 'true';
        ag.employmentService.add('a');
        ag.family = 'true';
        ag.familyLifeService.add('a');
        ag.job = 'true';
        ag.jobSkillsService.add('a');
        ag.shelter = 'true';
        ag.shelterService.add('a');
        ag.wellness = 'true';
        ag.wellnessService.add('a');
        ag.finance ='true';
        ag.financialsService.add('a');
        ag.health = 'true';
        ag.healthService.add('a');
        ag.nutrition = 'true';
        ag.nutritionService.add('a');
        ag.transport = 'true';
        ag.transportationService.add('a');
        
        ag.createAgency();
        }
    @isTest(SeeAllData = True)
    public static void test2(){
        
        MI_Register_AgencyClass ag = new MI_Register_AgencyClass();
        ag.NewAgency.Agency_Name__c = 'Trial';
        ag.NewAgency.Contact_No__c = '1234567890'; 
        ag.NewAgency.Address__c = 'Muskegon';
        ag.NewAgency.Agency_Description__c = 'xyz';
        ag.NewAgency.Primary_Contact_Name__C = 'sophie';
        ag.NewAgency.Primary_Contact_Email__c = 'bhb@deloitte.com';
        ag.NewAgency.Secondary_Contact_Name__c = 'Turner';
        ag.NewAgency.Secondary_Contact_Email__c = 'abc@deloitte.com';
        
        ag.education = 'true';
        ag.educationService.clear();
        ag.employment = 'true';
        ag.employmentService.clear();
        ag.family = 'true';
        ag.familyLifeService.clear();
        ag.job = 'true';
        ag.jobSkillsService.clear();
        ag.shelter = 'true';
        ag.shelterService.clear();
        ag.wellness = 'true';
        ag.wellnessService.clear();
        ag.finance ='true';
        ag.financialsService.clear();
        ag.health = 'true';
        ag.healthService.clear();
        ag.nutrition = 'true';
        ag.nutritionService.clear();
        ag.transport = 'true';
        ag.transportationService.clear();
        ag.createAgency();
        
        }
      @isTest(SeeAllData = True)
    public static void test3(){
        
        MI_Register_AgencyClass ag = new MI_Register_AgencyClass();
        ag.NewAgency.Agency_Name__c = 'Trial';
        ag.NewAgency.Contact_No__c = '1234567890'; 
        ag.NewAgency.Address__c = 'Muskegon';
        ag.NewAgency.Agency_Description__c = 'xyz';
        ag.NewAgency.Primary_Contact_Name__C = 'sophie';
        ag.NewAgency.Primary_Contact_Email__c = 'bhb@deloitte.com';
        ag.NewAgency.Secondary_Contact_Name__c = 'Turner';
        ag.NewAgency.Secondary_Contact_Email__c = 'abc@deloitte.com';
        ag.education = 'false';
    
        ag.employment = 'false';
    
        ag.family = 'false';
       
        ag.job = 'false';
      
        ag.shelter = 'false';
      
        ag.wellness = 'false';
  
        ag.finance ='false';
        
        ag.health = 'false';
        
        ag.nutrition = 'false';
        
        ag.transport = 'false';
        ag.createAgency();
        
        }
      @isTest(SeeAllData = True)
    public static void test4(){
        
        MI_Register_AgencyClass ag = new MI_Register_AgencyClass();
        ag.VerifyCode = 1234567890;
        Agency__C a = new Agency__C(Agency_Name__c = 'Trial1' ,Verification_Code__c = 1234567890);
        insert a;
        ag.displayAgency();
        ag.partnerReg();
        }
    @isTest(SeeAllData = True)
    public static void test5(){
        
        MI_Register_AgencyClass ag = new MI_Register_AgencyClass();
        ag.VerifyCode = 123;
        Agency__C a = new Agency__C(Agency_Name__c = 'Trial1' ,Verification_Code__c = 1234567891);
        insert a;
        ag.displayAgency();
        ag.partnerReg();
        }


    
        
}