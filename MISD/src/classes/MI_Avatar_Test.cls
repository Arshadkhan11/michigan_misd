@isTest
public class MI_Avatar_Test {
	@isTest public static void generaltest(){
        MI_Avatar miavt = new MI_Avatar();
        miavt.saveimage();
    }
    @isTest public static void saveimagetest(){
        MI_Avatar miavt = new MI_Avatar();
        miavt.imagename = 'avatar1.jpg';
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test');
        insert con;
        Case cs = new Case(Subject = 'TestCase', Status = 'New');
        insert cs;
        PageReference pageRef = Page.MI_EditAvatar;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
        ApexPages.currentPage().getParameters().put('CaseId',cs.id);
        ApexPages.currentPage().getParameters().put('SelectedContactID',con.id);
        ApexPages.currentPage().getParameters().put('FromNeeds','Yes');
        ApexPages.currentPage().getParameters().put('PrimaryContactId',con.id);
        miavt.saveimage();
    }
}