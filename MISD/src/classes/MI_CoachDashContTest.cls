@isTest
public class MI_CoachDashContTest {
    
    @isTest public static void generaltest(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
        MI_CoachDashCont cdc = new MI_CoachDashCont();
        cdc.counterDueTodayTasks = 0;
        cdc.UserCatPref1 = '';
        cdc.currentUser = new User();
        cdc.taskIdChosen = '';
        cdc.selectedKeyword = '';
        cdc.selectedDate = date.today();
        //cdc.selectedCategory = new String[]{'Educational'};
        //cdc.Search();
        //cdc.clear();
        cdc.deleteId();
        MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Activity_Date__c = Date.today(), RecordTypeId = [select id from RecordType where Name='Task' and sObjectType='MI_Activity__c'].Id);
        insert miact;
        cdc.EditId = miact.id;
        cdc.TaskId = miact.id;
        cdc.delId = miact.id;
        cdc.TaskCompleteId = miact.id;
        cdc.EditId();
        cdc.backtofront();
        cdc.overDue();
        cdc.UserTaskList();
        MI_CoachDashCont.getActivity(miact.id);
        //cdc.saveRec();
        cdc.updateEditTask();
        cdc.EditTask = miact;
        cdc.CompleteTaskMethod();
        cdc.ALNewTask();
        cdc.AlEditTask();
        cdc.AlupdateTask();
        cdc.ALCompleteTask();
        cdc.ALDeleteTask();
        cdc.TaskNotes = 'aaa';
        cdc.TaskSubject = 'aaa';
        cdc.TaskStatus = 'In Progress';
        cdc.TaskPriority = 'High';
        cdc.TaskType = 'sff';
        cdc.TaskClient = 'sophia';
        cdc.newDate = '06/20/2016';
        cdc.newTime = '12:00 PM';
        cdc.ALSaveNewTask();
        cdc.deleteId();     
    }
    @isTest public static void queryNewsFeedListTest(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='testcdc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testcdc@test.com', News_Educational__c=true,
            News_FamilyEvents__c=true, News_JobRelated__c=true, News_Medical__c=true);
        System.runAs(u) {
             Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
            insert con;
            
            MI_CoachDashCont cdc = new MI_CoachDashCont();
             cdc.TaskNotes = 'aaa';
        cdc.TaskSubject = 'aaa';
        cdc.TaskStatus = 'In Progress';
        cdc.TaskPriority = 'High';
        cdc.TaskType = 'sff';
        cdc.TaskClient = 'sophia';
        cdc.newDate = '06/20/2016';
        cdc.newTime = '';
        cdc.ALSaveNewTask();
        }
    }
    @isTest public static void SearchTest(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
        MI_CoachDashCont cdc = new MI_CoachDashCont();
        cdc.selectedKeyword = 'Test';
        //cdc.Search();
    }
    @isTest public static void savRecTest1(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
        MI_CoachDashCont cdc = new MI_CoachDashCont();
        cdc.newDate = '06/14/2016';
        cdc.TaskSubject= 'Test';
        cdc.saveRec();
    }
    /*
    @isTest public static void chatterTest(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
        MI_CoachDashCont cdc = new MI_CoachDashCont();
        // Build a simple feed item
      ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
      List<ConnectApi.FeedItemInput> testItemList = new List<ConnectApi.FeedItemInput>();
       testItemList.add(new ConnectApi.FeedItemInput());
       testItemList.add(new ConnectApi.FeedItemInput());
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='testcdc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testcdc@test.com', News_Educational__c=true,
            News_FamilyEvents__c=true, News_JobRelated__c=true, News_Medical__c=true);
        System.runAs(u) { 
        
     //Network n = new Network();
     //insert n;
     community c = new community();
    // Set the test data
    //ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(c.id,ConnectApi.FeedType.UserProfile, UserInfo.getUserId());
    
 

        // The method returns the test page, which we know has two items in it.

        Test.startTest();
       cdc.refresh();
       Test.stopTest();
       
     }
       
       
        }
    }
    */
      @isTest public static void chatterTest(){
        Contact con = new Contact(FirstName = 'Tamara', LastName = 'Davis', Title= 'Master');
        insert con;
       MI_PartnerDash pd = new MI_PartnerDash();
        // Build a simple feed item
      ConnectApi.FeedElementPage testPage = new ConnectApi.FeedElementPage();
      
       Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User u = new User(Alias = 'test', Email='testcdc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='testcdc@test.com', News_Educational__c=true,
            News_FamilyEvents__c=true, News_JobRelated__c=true, News_Medical__c=true,Chat_Text__c = true);
 
       //MI_CoachDashCont1 cdc1 = new MI_CoachDashCont1();  
     
      Network Networkcom = new Network();      
      Networkcom = [SELECT Id FROM Network WHERE Name ='State of Michigan'];
      
      

    
       system.runAs(u){ 
       
         MI_CoachDashCont ctrl = new MI_CoachDashCont();
        ConnectApi.ChatterFeeds.setTestGetFeedElementsFromFeed(Networkcom.id,ConnectApi.FeedType.UserProfile,UserInfo.getUserId(), testPage);
         Test.startTest();
         ctrl.refresh();
       
            
         ctrl.PostText = 'Hey all.';
         ctrl.PostMention = 'sophia;tamara;Tom';
         ctrl.AddPost();
         
        
         ctrl.CommentText = 'Hey, it is a pleasure';
         ctrl.AddComment();
         
        
 
         
         
       
         
            
        
       Test.stopTest();
       }
       }
    
}