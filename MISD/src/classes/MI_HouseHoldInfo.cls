public class MI_HouseHoldInfo {
    
    public String vconId {get;set;}
    public String vcaseId {get; set;}
    public String vCasePrograms{get; set;}
    static list<MI_CA_PageName> PageCategory = new  list<MI_CA_PageName>();
        public static List<CaseContactRole>  vAllContactInfo {get; set;}
    public static String vAllContactStrInfo {get; set;}
    public static String vAllContactStrName {get; set;}
    public static String vAllContactStrImg {get; set;}
    
    public List<HHContact> vHHContact {get; set;}
    public map<string, String> vFinalPDFMap {get; set;}
    public String vCommAppData {get; set;}
    public String vSerHHCon {get; set;}
    public String vTaxFilingStat {get; set;}
    public Integer vNoDependents {get; set;}
    public String vDrugFelony {get; set;}
    public String vParoleViolation {get; set;}
    public String vPregnant {get; set;}
    public String vPPMotherBF {get; set;}
    public String vPPMontherNotBF {get; set;}
    public String vServicesNeeded {get; set;}
    public String vServices {get; set;}
    public String vChildName {get; set;}
    public String vHours {get; set;}
    public String vWork {get; set;}
    public String vMealPlan {get; set;}
    public String vAllergy {get; set;}
    public String vBlind {get; set;}
    public String vDisabled {get; set;}
    public String vAlc {get; set;}
    public String vDV {get; set;}
    public String MedicalJSon {get; set;}
    public String vCaseLastUpdDt{get; set;}

public MI_HouseHoldInfo()
{
    String vconId1  = ApexPages.currentPage().getParameters().get('contactId');
    String vconId2  = ApexPages.currentPage().getParameters().get('citId');
    
    if(vconId1 != '' && vconId1 != null)
    {
        vconId = vconId1;
    }
    else if(vconId2 != '' && vconId2 != null)
    {
        vconId = vconId2;
    }
    //vconId = '00335000002RKicAAG';
    //vconId = '00335000002jLN9';
    Case contactCase = [SELECT id, contact.Name, Reassessment_Date__c,  LastModifiedDate, Assess_Locked_By__c,NA_Completion__c,SP_Completion__c,CasePrograms__c
                            FROM Case
                        WHERE contactId = : vconId];
    vcaseId = contactCase.id;
     vCasePrograms = contactCase.CasePrograms__c;
    vCaseLastUpdDt = contactCase.LastModifiedDate.format('MMMMM dd, yyyy');
    //System.debug('vcaseId' + vcaseId + '|50035000000dLyPAAU');
    //new Household
    getHouseHoldInfo();
    extractPDFdata();
    getPrimaryConDetails();
    getAllOtherDetails();    
}
    
    public void getPrimaryConDetails()
    {
        //Tax
        vTaxFilingStat = '';
        
        String tmpTaxReturn = vFinalPDFMap.get('Tax Info~TaxInfo');  
        String tmpJoint = vFinalPDFMap.get('Tax Info~TaxJointFill'); 
        String tmpDep = vFinalPDFMap.get('Tax Info~TaxDependant'); 
        
        if(tmpTaxReturn == null || tmpTaxReturn == ' ' || tmpTaxReturn == '' || tmpTaxReturn == 'No')
        {
            vTaxFilingStat = 'Not Filing Tax';
        }
        else
        {
            if(tmpTaxReturn == 'Yes' && (tmpJoint == ' ' || tmpJoint == null || tmpJoint == ''))
            {
                vTaxFilingStat = 'Filing Tax, Independently';
            }
            else if(tmpTaxReturn == 'Yes' && tmpJoint != ' ' && tmpJoint != null)
            {
                vTaxFilingStat = 'Filing Tax, Filing Jointly';
            }
        }
       /* if(tmpDep != ' ' && tmpDep != null && tmpDep != ''){
            List<String> tmpDepArr = tmpDep.split('\\|');
            vNoDependents = tmpDepArr.size();
        }
        else{
            vNoDependents = 0;
        }*/
            
        String tempServices = vFinalPDFMap.get('Child Care~CCServices');
        String tempChildName = vFinalPDFMap.get('Child Care~CCChildren');
        String tempHours = vFinalPDFMap.get('Child Care~NoofHours');
        String tempWord = vFinalPDFMap.get('Child Care~WorkSchedule');
        
        vServicesNeeded = 'No';
        vServices = '';
        vChildName = '';
        vHours = '';
        vWork = '';
        if(tempServices != null && tempServices != ' ' && tempServices != ''){
            vServicesNeeded = 'Yes';
            if(tempServices.contains('|')){
                tempServices = tempServices.replace('|', ', ');
            } 
			vServices = tempServices;           
        }
        
        String tmpChildStr = '';
        if(tempChildName != '' && tempChildName != null && tempChildName != ' '){
            for(HHContact con : vHHContact){
                if(tempChildName.contains(con.vConId)){
                    tmpChildStr = tmpChildStr + con.vFirstName + ' ' + con.vLastName + ', ';
                }
            }
            tmpChildStr = tmpChildStr.substring(0,tmpChildStr.length()-2);
        }
        vChildName = tmpChildStr;
        if(tempHours != null && tempHours != ' ' && tempHours != ''){
            vHours = tempHours;
        }
        if(tempWord != null && tempWord != ' ' && tempWord != ''){
            if(tempWord.contains('|')){
                tempWord = tempWord.replace('|', ', ');
            } 
			vWork = tempWord;           
        }
    }
    
    public void getAllOtherDetails()
    {
        //Drug and Felony
        vDrugFelony = '';
        vParoleViolation = '';
        
        vDrugFelony = vFinalPDFMap.get('Criminal Record~DrugFelony');
        vParoleViolation = vFinalPDFMap.get('Criminal Record~ParoleViolation');
        
        //Pregnancy
        vPregnant = '';
        vPPMotherBF = '';
        vPPMontherNotBF = '';
        
		String vPregTmp = vFinalPDFMap.get('Pregnancy~KindOfPreg');
        if(vPregTmp != null && vPregTmp != '')
        {
            if(vPregTmp == 'Pregnant')
            {
                vPregnant = vFinalPDFMap.get('Pregnancy~Member');
            }
            else if(vPregTmp == 'Postpartum Mother Breastfeeding')
            {
                vPPMotherBF = vFinalPDFMap.get('Pregnancy~Member');
            }
            else if(vPregTmp == 'Postpartum Mother Not Breastfeeding')
            {
                vPPMontherNotBF = vFinalPDFMap.get('Pregnancy~Member');
            }
        }
        
        String vConNameSt = vFinalPDFMap.get('Medical Information~DisabledCon');
        String vDrugAlc =  vFinalPDFMap.get('Medical Information~AlcoDrug');
        String vDomVioSt =  vFinalPDFMap.get('Medical Information~DomesticViolence');
        String dDorm = vFinalPDFMap.get('Nutrition~Dorm');
        String dAllergy = vFinalPDFMap.get('Nutrition~Allergy');
        if(vConNameSt != '' && vConNameSt != ' ' && vConNameSt != null){
            vBlind = vConNameSt;
            vDisabled = vConNameSt;
        }
        else{
            vBlind = '';
            vDisabled = '';
        }
        if(vDrugAlc != '' && vDrugAlc != ' ' && vDrugAlc != null){
            vAlc = vDrugAlc;
        }
        else{
            vAlc = '';
        }
        if(vDomVioSt != '' && vDomVioSt != ' ' && vDomVioSt != null){
            vDV = vDomVioSt;
        }
        else{
            vDV = '';
        }
        if(dDorm != '' && dDorm != ' ' && dDorm != null){
            vMealPlan = dDorm;
        }
        else{
            vMealPlan = '';
        }
        if(dAllergy != '' && dAllergy != ' ' && dAllergy != null){
            vAllergy = dAllergy;
        }
        else{
            vAllergy = '';
        }
        
        MedicalJSon = '';
        List<HHContactMedical> hhcm = new List<HHContactMedical>();
        String MedicaidM = vFinalPDFMap.get('Health Coverage~MedicaidMember');
        String MedicaidI = vFinalPDFMap.get('Health Coverage~MedicaidInsurance');
        String MedicaidE = vFinalPDFMap.get('Health Coverage~MedicaidEffDt');
        if(MedicaidM != '' && MedicaidM != null && MedicaidM != ' '){
            List<String> memtmp = MedicaidM.split('\\|\\|');
            List<String> providertmp = MedicaidI.split('\\|\\|');
            List<String> Datetmp = MedicaidE.split('\\|\\|');
            for(integer i=0; i<memtmp.size(); i++){
                HHContactMedical hhcmtmp = new HHContactMedical();
                hhcmtmp.vConId = memtmp[i];
                hhcmtmp.vType = 'Medicaid';
                hhcmtmp.vProvider = providertmp[i];
                hhcmtmp.vStart = Datetmp[i];
                hhcm.add(hhcmtmp);
            }
        }
        String CHIPM = vFinalPDFMap.get('Health Coverage~CHIPMember');
        String CHIPI = vFinalPDFMap.get('Health Coverage~CHIPInsurance');
        String CHIPE = vFinalPDFMap.get('Health Coverage~CHIPEffDt');
        if(CHIPM != '' && CHIPM != null && CHIPM != ' '){
            List<String> memtmp = CHIPM.split('\\|\\|');
            List<String> providertmp = CHIPI.split('\\|\\|');
            List<String> Datetmp = CHIPE.split('\\|\\|');
            for(integer i=0; i<memtmp.size(); i++){
                HHContactMedical hhcmtmp = new HHContactMedical();
                hhcmtmp.vConId = memtmp[i];
                hhcmtmp.vType = 'CHIP/MI Child';
                hhcmtmp.vProvider = providertmp[i];
                hhcmtmp.vStart = Datetmp[i];
                hhcm.add(hhcmtmp);
            }
        }
        String MedicarM = vFinalPDFMap.get('Health Coverage~MedicareMember');
        String MedicarI = vFinalPDFMap.get('Health Coverage~MedicareInsurance');
        String MedicarE = vFinalPDFMap.get('Health Coverage~MedicareEffDt');
        if(MedicarM != '' && MedicarM != null && MedicarM != ' '){
            List<String> memtmp = MedicarM.split('\\|\\|');
            List<String> providertmp = MedicarI.split('\\|\\|');
            List<String> Datetmp = MedicarE.split('\\|\\|');
            for(integer i=0; i<memtmp.size(); i++){
                HHContactMedical hhcmtmp = new HHContactMedical();
                hhcmtmp.vConId = memtmp[i];
                hhcmtmp.vType = 'Medicare';
                hhcmtmp.vProvider = providertmp[i];
                hhcmtmp.vStart = Datetmp[i];
                hhcm.add(hhcmtmp);
            }
        }
        String TRM = vFinalPDFMap.get('Health Coverage~TRIMember');
        String TRI = vFinalPDFMap.get('Health Coverage~TRIInsurance');
        String TRE = vFinalPDFMap.get('Health Coverage~TRIEffDt');
        if(TRM != '' && TRM != null && TRM != ' '){
            List<String> memtmp = TRM.split('\\|\\|');
            List<String> providertmp = TRI.split('\\|\\|');
            List<String> Datetmp = TRE.split('\\|\\|');
            for(integer i=0; i<memtmp.size(); i++){
                HHContactMedical hhcmtmp = new HHContactMedical();
                hhcmtmp.vConId = memtmp[i];
                hhcmtmp.vType = 'TRICARE';
                hhcmtmp.vProvider = providertmp[i];
                hhcmtmp.vStart = Datetmp[i];
                hhcm.add(hhcmtmp);
            }
        }
        String VAM = vFinalPDFMap.get('Health Coverage~VAMember');
        String VAI = vFinalPDFMap.get('Health Coverage~VAInsurance');
        String VAE = vFinalPDFMap.get('Health Coverage~VAEffDt');
        if(VAM != '' && VAM != null && VAM != ' '){
            List<String> memtmp = VAM.split('\\|\\|');
            List<String> providertmp = VAI.split('\\|\\|');
            List<String> Datetmp = VAE.split('\\|\\|');
            for(integer i=0; i<memtmp.size(); i++){
                HHContactMedical hhcmtmp = new HHContactMedical();
                hhcmtmp.vConId = memtmp[i];
                hhcmtmp.vType = 'VA health care programs';
                hhcmtmp.vProvider = providertmp[i];
                hhcmtmp.vStart = Datetmp[i];
                hhcm.add(hhcmtmp);
            }
        }
        String PeaceM = vFinalPDFMap.get('Health Coverage~PeaceMember');
        String PeaceI = vFinalPDFMap.get('Health Coverage~PeaceInsurance');
        String PeaceE = vFinalPDFMap.get('Health Coverage~PeaceEffDt');
        if(PeaceM != '' && PeaceM != null && PeaceM != ' '){
            List<String> memtmp = PeaceM.split('\\|\\|');
            List<String> providertmp = PeaceI.split('\\|\\|');
            List<String> Datetmp = PeaceE.split('\\|\\|');
            for(integer i=0; i<memtmp.size(); i++){
                HHContactMedical hhcmtmp = new HHContactMedical();
                hhcmtmp.vConId = memtmp[i];
                hhcmtmp.vType = 'Peace Corps';
                hhcmtmp.vProvider = providertmp[i];
                hhcmtmp.vStart = Datetmp[i];
                hhcm.add(hhcmtmp);
            }
        }
        String EIM = vFinalPDFMap.get('Health Coverage~EIMember');
        String EII = vFinalPDFMap.get('Health Coverage~EIInsurance');
        String EIE = vFinalPDFMap.get('Health Coverage~EIEffDt');
        if(EIM != '' && EIM != null && EIM != ' '){
            List<String> memtmp = EIM.split('\\|\\|');
            List<String> providertmp = EII.split('\\|\\|');
            List<String> Datetmp = EIE.split('\\|\\|');
            for(integer i=0; i<memtmp.size(); i++){
                HHContactMedical hhcmtmp = new HHContactMedical();
                hhcmtmp.vConId = memtmp[i];
                hhcmtmp.vType = 'Employer insurance';
                hhcmtmp.vProvider = providertmp[i];
                hhcmtmp.vStart = Datetmp[i];
                hhcm.add(hhcmtmp);
            }
        }
        MedicalJSon = JSON.serialize(hhcm);
        //System.debug('MedicalJSon '+ MedicalJSon);
    }
    
    public void extractPDFdata()
    {
        vFinalPDFMap = new map<string, String>();
        List<Attachment> atttemp = new List<Attachment>();
        List<CommonApplication__c> calst = [Select Id,CAPrograms__c,SPCAServices__c from CommonApplication__c where CaseId__c = :vcaseId and Status__C = 'Active'];
        if(calst.size() != 0){
            String CAId = calst[0].Id;
            atttemp = [SELECT Id,Body FROM Attachment where ParentId = :CAId and Name = 'CommonAppAnswers.txt' LIMIT 1];
            if(atttemp.isEmpty())
            {
                system.debug('newmap is empty');
                //return newmap;
            }
            else
            {
                string data = '';
                Blob b;
                Attachment Angdata = [SELECT Id,Body FROM Attachment where ParentId = :CAId and Name = 'CommonAppAnswers.txt' LIMIT 1];
                b = Angdata.Body;
                data = b.toString();
                
                //System.debug('data: '+data);
                if(data!='[{"Category":"Personal"}]')
                {
                    list<MI_CA_PageName> ourdata = (list<MI_CA_PageName>) JSON.deserialize(data, list<MI_CA_PageName>.class);
                   
                    for(MI_CA_PageName ourdata1 :ourdata)
                    {
                         
                        list<MI_CA_PageSections> pagesections = ourdata1.PageName;
                        for(MI_CA_PageSections pagesection :pagesections)
                        {
                           
                             list<MI_CA_Questions> pagesectionquestions = pagesection.PageSection;
                            for(MI_CA_Questions pagesectionquestion :pagesectionquestions)
                            {
                                 
                                 list<MI_CA_SubQuestions> pagesectionsubquestions = pagesectionquestion.Question;
                                for(MI_CA_SubQuestions pagesectionsubquestion :pagesectionsubquestions){
                                    if(pagesectionsubquestion.parentquestion.PDFName__c != '' && pagesectionsubquestion.parentquestion.PDFName__c != null)
                                    {
                                        vFinalPDFMap.put(pagesectionsubquestion.parentquestion.PageSection__c+'~'+pagesectionsubquestion.parentquestion.PDFName__c, pagesectionsubquestion.parentquestion.Answers__c);
                                    }   
                                 list<MI_CA_SubSubQuestions> pagesectionsubsubquestions = pagesectionsubquestion.SubQuestions;  
                                      for(MI_CA_SubSubQuestions pagesectionsubsubquestion :pagesectionsubsubquestions){
                                          if(pagesectionsubsubquestion.childquestion.PDFName__c != '' && pagesectionsubsubquestion.childquestion.PDFName__c != null)
                                            {
                                                vFinalPDFMap.put(pagesectionsubsubquestion.childquestion.PageSection__c+'~'+pagesectionsubsubquestion.childquestion.PDFName__c, pagesectionsubsubquestion.childquestion.Answers__c);
                                            }
                                            List<Question__c> subsublists = pagesectionsubsubquestion.SubSubQuestions;
                                          for(Question__c subsublist : subsublists){
                                              if(subsublist.PDFName__c != '' && subsublist.PDFName__c != null)
                                              {
                                                  vFinalPDFMap.put(subsublist.PageSection__c+'~'+subsublist.PDFName__c, subsublist.Answers__c);
                                              }
                                          }
                                      }
                                }
                            }
                        }
                    }
                }
               
                system.debug('newmap'+vFinalPDFMap);
            }
        }
    }
    
    
    public void getHouseHoldInfo()
    {
        vHHContact = new List<HHContact>();
        List<CaseContactRole> vAllCon = [SELECT Id,CasesId, ContactId, Role,Contact.FirstName, Contact.LastName, Contact.Birthdate, Contact.HomePhone, Contact.MobilePhone, Contact.Email, Contact.Address__c, Contact.Social_Security_Num__c, Contact.Gender__c, Contact.Zipcode__c, Contact.ContactImageName__c, Contact.Martial_Status__c, Contact.Citizenship__c, Contact.Ethnicity__c, Contact.Race__c FROM CaseContactRole WHERE CasesId = :vcaseId]; 
        if(vAllCon.size() != 0)
        {
            vNoDependents = vAllCon.size()-1;
            for(CaseContactRole c : vAllCon)
            {
                HHContact contmp = new HHContact();
                contmp.vFirstName = c.Contact.FirstName;
                contmp.vLastName = c.Contact.LastName;
                contmp.vRel = c.Role;
                contmp.vTel = c.Contact.HomePhone;
                contmp.vMail = c.Contact.Email;
                contmp.vSSN = c.Contact.Social_Security_Num__c;
                contmp.vDOB = c.Contact.Birthdate;
                contmp.vCitizenship = c.Contact.Citizenship__c;
                contmp.vRace = c.Contact.Race__c;
                contmp.vEthnicity = c.Contact.Ethnicity__c;
                contmp.vMarital = c.Contact.Martial_Status__c;
                contmp.vconImg = c.Contact.ContactImageName__c;
                contmp.vConId = c.ContactId;
                vHHContact.add(contmp);
            }   
        }
        //System.debug('vHHContact' + vHHContact);
        vSerHHCon = JSON.serialize(vHHContact);
        //System.debug('vSerHHCon' + vSerHHCon);
        
    }
    
    
    public class HHContact
    {
        public String vConId {get;set;}
        public String vFirstName {get;set;}
        public String vLastName {get;set;}
        public String vRel {get;set;}
        public String vTel {get;set;}
        public String vMail {get;set;}
        public String vSSN {get;set;}
        public Date vDOB {get;set;}
        public String vCitizenship {get;set;}
        public String vRace {get;set;}
        public String vEthnicity {get;set;}
        public String vMarital {get;set;}
        public String vconImg {get;set;}
        
    }
    
    public class HHContactMedical
    {
        public String vConId {get; set;}
        public String vType {get; set;}
        public String vProvider {get; set;}
        public String vStart {get; set;}
    }
    
        @RemoteAction
    public static List<String> getQuestionsdetails(string vPrograms, String vCaseId, String Initial){
        //String vProgramStr = 'Cash|FAP';
        String vProgramStr = '';
        List<String> vProgramtemp = vPrograms.split('\\|');
        list<AggregateResult> category;
       for(integer i=0;i<vProgramtemp.size();i++)
        {
            vProgramStr=vProgramStr+ '\'' + String.escapeSingleQuotes(vProgramtemp[i]) + '\'' + ',';
        }
        vProgramStr = vProgramStr.substring(0, vProgramStr.length()-1);
        System.debug('Programs:'+vProgramStr);
       
        Map<String, List<String>> AnswerMap = new Map<String, List<String>>();
        AnswerMap = MI_CA_CommonApp_DataExtracter.extractjsondata(vCaseId);
        
        String AllStr = 'SELECT Category__c FROM Question__c WHERE ParentQuestion__c = \'\' AND Program__c includes ( '+ vProgramStr+') GROUP BY Category__c';
        category = Database.query(AllStr);
        List<CommonApp_Category__c> CatOrder = new List<CommonApp_Category__c>();
        CatOrder = [Select Category__c, Order__c, Id from CommonApp_Category__c Where Category__c != ''];
        Map<String, Integer> CatOrderMap = new Map<String, Integer>();
        Map<Integer, String> CatOrderMapSeq = new Map<Integer, String>();
        Map<Integer, String> CatOrderTempMap = new Map<Integer, String>();
        List<Integer> vSeqInt = new List<Integer>();
        for(CommonApp_Category__c cattemp : CatOrder)
        {
            if(!(CatOrderMap.containsKey(cattemp.Category__c)))
            {
                CatOrderMap.put(cattemp.Category__c, integer.valueOf(cattemp.Order__c));
                vSeqInt.add(integer.valueOf(cattemp.Order__c));
                CatOrderTempMap.put(integer.valueOf(cattemp.Order__c), cattemp.Category__c);
            }
        }
        vSeqInt.sort();
        
        for(Integer tmpInt : vSeqInt)
        {
            CatOrderMapSeq.put(tmpInt, CatOrderTempMap.get(tmpInt));
        }
        system.debug(' \n CatOrderMapSeq - ' + CatOrderMapSeq);
        
        String pagecategoriezz;
        String JSONString = '';
        String vConName = '';
        if(vCaseId != '' && vCaseId != null)
        {
            vConName = [select Contact.name from case where id = :vCaseId].contact.name;
        }
        
        if(category.size() == 0 || vConName == '' || vConName == null)
        {
            JSONString = '[{"Category":"Personal"}]';
        }
        else
        {
            Integer vSeq = 0;
            String Personalpresent = 'N';
            Map<String, Integer> vPageSeqMap = new  Map<String, Integer>();
            List<CommonApp_Page__c> vPageSeq = [select Page_Name__c, Page_Seq__c from CommonApp_Page__c where Page_Name__c != ''];
            for(CommonApp_Page__c seq : vPageSeq)
            {
                vPageSeqMap.put(seq.Page_Name__c, integer.valueOf(seq.Page_Seq__c));
            }
            
            /*for(AggregateResult c : category)
            {
                system.debug('category - ::' + c.get('Category__c'));
                string eachcategory = string.valueOf(c.get('Category__c')) ; 
                if(CatOrderMap.containsKey(eachcategory))
                {
                    vSeq = CatOrderMap.get(eachcategory);
                }
                if(eachcategory == 'Personal')
                {
                    Personalpresent = 'Y';
                }
                
                
                MI_CA_PageName pagecategoriez =  new MI_CA_PageName(vProgramStr,eachcategory,vSeq,AnswerMap, vPageSeqMap); 
                 PageCategory.add(pagecategoriez);
                //pagecategoriezz = category;        
            }*/
            /*for(Integer seq : CatOrderMapSeq.keyset()){
                
                if(CatOrderMapSeq.get(seq) == 'Personal')
                {
                    Personalpresent = 'Y';
                }
                MI_CA_PageName pagecategoriez =  new MI_CA_PageName(vProgramStr,CatOrderMapSeq.get(seq),seq,AnswerMap, vPageSeqMap); 
                PageCategory.add(pagecategoriez);
            }
            if(Personalpresent == 'N')
            {
                if(CatOrderMap.containsKey('Personal'))
                {
                    vSeq = CatOrderMap.get('Personal');
                }
                MI_CA_PageName pagecategoriez =  new MI_CA_PageName(vProgramStr,'Personal',vSeq,AnswerMap, vPageSeqMap); 
                PageCategory.add(pagecategoriez);
            }*/
			Map<Integer, String> catMapTemp = new Map<Integer, String>();
			Map<Integer, String> catMap = new Map<Integer, String>();
			List<Integer> tabSeqList = new List<Integer>();
			Boolean isPersonalExists = false;
            for(AggregateResult c : category)
            {
				if(string.valueOf(c.get('Category__c')) == 'Personal'){
					isPersonalExists = true;
				}
				tabSeqList.add(CatOrderMap.get(string.valueOf(c.get('Category__c'))));
                catMapTemp.put(CatOrderMap.get(string.valueOf(c.get('Category__c'))), string.valueOf(c.get('Category__c')));               
                
            }
			tabSeqList.sort();
			for(Integer seq : tabSeqList){
				catMap.put(seq, catMapTemp.get(seq));
			}
			if(isPersonalExists){
				for(Integer seq : catMap.keyset()){
					MI_CA_PageName pagecategoriez =  new MI_CA_PageName(vProgramStr,CatOrderMapSeq.get(seq),seq,AnswerMap, vPageSeqMap); 
					PageCategory.add(pagecategoriez);
				}
			}else{
				catMap.put(CatOrderMap.get('Personal'), 'Personal');
                List<Integer> tempList = new List<Integer>();
				for(Integer seq : catMap.keyset()){
                    tempList.add(seq);
					//MI_CA_PageName pagecategoriez =  new MI_CA_PageName(vProgramStr,CatOrderMapSeq.get(seq),seq,AnswerMap, vPageSeqMap); 
					//PageCategory.add(pagecategoriez);
				}
                tempList.sort();
                for(Integer seq : tempList){
                    MI_CA_PageName pagecategoriez =  new MI_CA_PageName(vProgramStr,CatOrderMapSeq.get(seq),seq,AnswerMap, vPageSeqMap); 
					PageCategory.add(pagecategoriez);
                }
			}    
            JSONString = JSON.serialize(PageCategory);
        }

      
        system.debug('JSONString' + JSONString);
       // savedata(JSONString);
        List <String> retStr = new List<String>();
        retStr.add(JSONString);
        if(Initial == 'Y')
        {
            String vContactStr = getAllContactDetails(vCaseId)[0];
            String vContactNm = getAllContactDetails(vCaseId)[1];
            String vatt = retrieveattachment(vCaseId);
            String vContactImg = getAllContactAvatar(vCaseId);
            retStr.add(vatt);
            retStr.add(vContactStr);
            retStr.add(vContactImg);
            retStr.add(vContactNm);
        }
        return retStr;
      //return JSONString;  
    }
	
    public static List<String> getAllContactDetails(String vCaseId)
    {
            //String vCaseId = apexpages.currentpage().getparameters().get('CaseId');
            vAllContactInfo = [SELECT Id, CasesId, ContactId, Role, Contact.Name, Contact.ContactImageName__c, Contact.FirstName, Contact.LastName, Contact.Birthdate, Contact.Gender__c FROM CaseContactRole WHERE CasesId = :vCaseId];
            List<String> detailist = new List<String>();
            vAllContactStrInfo = '';
            vAllContactStrName = '';
            if(vAllContactInfo.size() != 0)
            {
                for(CaseContactRole vcon : vAllContactInfo)
                {
                   vAllContactStrName = vAllContactStrName + '|' + vcon.Contact.FirstName + ' ' + vcon.Contact.LastName;
                   vAllContactStrInfo = vAllContactStrInfo + '|' + vcon.Contact.Id;
                }
                vAllContactStrInfo = vAllContactStrInfo.substring(1, vAllContactStrInfo.length());
                vAllContactStrName = vAllContactStrName.substring(1, vAllContactStrName.length());
            }
            detailist.add(vAllContactStrInfo);
            detailist.add(vAllContactStrName);
        return detailist;
    }
    
    public static String  retrieveattachment(String caseid){
        string encodedContentsString,operation = '';
        string data = '';
        Blob b;
        string firsttime = 'N';
        Attachment Angdata = new attachment();
        encodedContentsString = 'FileCreatedfor the firsttime';
        List<CommonApplication__c> calst = [Select Id,CAPrograms__c,SPCAServices__c from CommonApplication__c where CaseId__c = :caseid and Status__C = 'Active'];
        if(calst.size() != 0){
            String CAId = calst[0].Id;
            try{
                Angdata = [SELECT Id,Body FROM Attachment where ParentId = :CAId and Name = 'CommonAppIntro.txt'];
                operation = 'UPDATE'; 
            }
            catch(exception e){
                operation = 'INSERT'; 
            }
            if(operation == 'INSERT'){
                Attachment attach=new Attachment();
                attach.Body = Blob.valueOf(encodedContentsString);
                attach.Name = 'CommonAppIntro.txt';
                attach.ContentType='txt';
                attach.ParentID= CAId; 
                insert attach;
                firsttime = 'Y';
                b = attach.Body;
                data = b.toString();
                
            }
            else{
                b = Angdata.Body;
                data = b.toString();
            }
        }
        else{
            data = encodedContentsString;
        }
        
        return   data;       
    }
    
	public static String getAllContactAvatar(String vCaseId)
    {
            vAllContactInfo = [SELECT Id, CasesId, ContactId, Role, Contact.Name, Contact.ContactImageName__c, Contact.FirstName, Contact.LastName, Contact.Birthdate, Contact.Gender__c FROM CaseContactRole WHERE CasesId = :vCaseId];
            vAllContactStrImg = '';
            if(vAllContactInfo.size() != 0)
            {
                for(CaseContactRole vcon : vAllContactInfo)
                {
                    if(vcon.Contact.ContactImageName__c != null && vcon.Contact.ContactImageName__c != '')
                    {
                        vAllContactStrImg = vAllContactStrImg + '|' + vcon.Contact.ContactImageName__c;
                    }
                    else
                    {
                        vAllContactStrImg = vAllContactStrImg + '|avatar2.jpg';
                    }
                }
                vAllContactStrImg = vAllContactStrImg.substring(1, vAllContactStrImg.length());
            }
        return vAllContactStrImg;
    }
}