@isTest
global class CommunityAddressLookupTest {
    @isTest global static void Test1()
    {
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id, Title='Master',BirthDate=date.today(),Address__c = 'Chennai');
        insert con;
            CommunityAddressLookup obj1 = new CommunityAddressLookup();
            CommunityResource__c cr1 = new CommunityResource__c(Address__C = 'Chennai,India', Category__c='Education');
		    CommunityAddressLookup.getaddress('Education');
            Test.startTest();
            Test.setMock(HttpCalloutMock.class, new CommunityAddressLookupMockTest());
          	insert cr1;
		    Test.stopTest();
    }
    @isTest global static void Test2()
    {
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id, Title='Master',BirthDate=date.today(),Address__c = 'Chennai');
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];                    
          CommunityAddressLookup obj1 = new CommunityAddressLookup();            
           CommunityResource__c cr2 = new CommunityResource__c(Address__C = 'Muskegon,Michigan', Category__c='Transportation'); 
           
            CommunityAddressLookup.getaddress('Transportation');
             Test.startTest();
             Test.setMock(HttpCalloutMock.class, new CommunityAddressLookupMockTest());
          	  insert cr2;
            Test.stopTest();
    }
}