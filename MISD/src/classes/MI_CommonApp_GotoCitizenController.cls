public class MI_CommonApp_GotoCitizenController {
  
        public user u {get;set;}
    	public string currentContactCaseId {get;set;}
    	public string caseId {get;set;}
     	PageReference retURL{get; set;}
   
    public MI_CommonApp_GotoCitizenController() {
     }  
    public Pagereference checkCase() {
    system.debug('User' + UserInfo.getUserId());
    u =[select Id,contactId from user where id =:UserInfo.getUserId()];
    if (u.contactId != null)    
    {        
  	Id currentContactCaseId = [Select Id from Case where ContactId=:u.contactId LIMIT 1].Id;
    caseId=currentContactCaseId;
    } 
    else
    {
        caseId = '';
    }
    retURL = new PageReference('/apex/MI_CommonAppCitizen?CaseId='+caseId+'&AssessmentConId=' +u.contactId);
    retURL.setRedirect(true);
    return retURL;
    }   
}