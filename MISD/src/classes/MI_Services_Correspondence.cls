public class MI_Services_Correspondence 
{
	public String ContactName {get;set;}
	public MI_Services_Correspondence()
    {
		ContactName = [select Id,Name from Contact where Id=:ApexPages.currentPage().getParameters().get('citId')].Name;    
    }
    public Id getCorrespondence1()
    {
        return [select id from Document where Name = 'Correspondence_1.pdf'].id;
    }
    public Id getCorrespondence2()
    {
        return [select id from Document where Name = 'Correspondence_2.pdf'].id;
    }
    public Id getCorrespondence3()
    {
        return [select id from Document where Name = 'Correspondence_3.pdf'].id;
    }
}