@isTest

public with sharing class MI_CommonApp_Cont_PDFTest  {

 public static testMethod void MI_CommonApp_Cont_PDFTest(){
 
 MI_CommonApp_Cont_PDF ca=new MI_CommonApp_Cont_PDF();
 MI_CommonApp_Cont_PDF.AbsentParent Ap = new MI_CommonApp_Cont_PDF.AbsentParent();    
     
  Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
        
      
 Case C= new Case(ContactId=con.id,Status='New',Origin='Email');
 insert C;
 C = [SELECT id,Contact.FirstName, Contact.LastName FROM Case WHERE id = : c.id];
 CaseContactRole CaseRole = new CaseContactRole(CasesId = c.id,ContactId = con.id,Role = 'Primary Contact');
 insert CaseRole;
 List<StaticResource> docs = new List<StaticResource>();
 docs = [select id, name, body from StaticResource where name = 'TestJSONCommonApplicationPDF'];
     if(docs.size()>0){
     String attbody = docs[0].body.toString();
     attbody = attbody.replace('00335000002jdIYAAY', con.id);
     attbody = attbody.replace('00335000002jdIiAAI', con.id);
     List<Attachment> atttemp = new List<Attachment>();
     Attachment atttemp1= new Attachment();
     atttemp1.Body = Blob.valueOf(attbody);
     atttemp1.Name = 'CommonAppAnswers.txt';
     atttemp1.ParentId=c.id; 
     atttemp.add(atttemp1);
         
     Attachment atttemp2= new Attachment();
     atttemp2.Body = Blob.valueOf('"Q3":"Branch","Q2":"No","Q4":"Sign Language","Q5":null,"Q6":"No","Q7A":null,"Q7B":null,"Q7C":null,"Q7D":null,"Q8":null,"Q9":null,"Q10":null,"Q11":null,"Q12":null}');
     atttemp2.Name = 'CommonAppIntro.txt';
     atttemp2.ParentId=c.id; 
     atttemp.add(atttemp2);
          
     insert atttemp;
     System.currentPageReference().getParameters().put('CaseId' , c.id);
     ca.extractPDFdata();
     
     }
 
 }
 }