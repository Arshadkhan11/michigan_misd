@IsTest global with sharing class MI_Settings_ClassTest {
    @IsTest(SeeAllData=true) 
       
    global static void testMI_Settings_Class () {
       
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
       
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
       
     
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
            Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
            Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        
        Contact con1 = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con1;
       
        Profile p1 = [SELECT Id FROM Profile WHERE Name='Citizen Profile'];
       
     
        User u1 = new User(Alias = 'test1', Email='clc1@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p1.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc1@test.com', ContactId = con1.id,Security_Question_1__c = 'In what city were you born in?',
            Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
            Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u1;
        
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
   
        System.runAs(u){
        MI_Settings_Class controller = new MI_Settings_Class();
            controller.EnglishFlag = 'True';
            
            controller.save1();
         
            
               
            PageReference pageRef = Page.MI_Settings_page;
            Test.setCurrentPage(pageref);
            System.assertNotEquals(null,controller.refreshpg());
            
            
        
        }
        System.runAs(u1){
        MI_Settings_Class controller = new MI_Settings_Class();
            
            controller.SpanishFlag = 'True';
            controller.save1();

         
            controller.refreshpg();
            
        }

    }
       
        }