@IsTest global with sharing class SuccessPlanPDFTest {
    
    static SP_Goal__c                          goal1, goal2, goal3, goal4, goal5, goal6, goal7,goal8, goal9, goal10;
    static List<SP_Goal__c>                    goalsList;
    static SP_Transactional__c                 mile1, mile2, mile3, mile4, mile5, mile6, mile7, mile8, mile9, mile10;
    static SP_Transactional__c                 serv1, serv2, serv3, serv4, serv5, serv6, serv7, serv8, serv9, serv10;
    static List<SP_Transactional__c>           milestonesList;
    static List<SP_Transactional__c>           servicesList;
    static Success_Plan_Master__c              spm1, spm2, spm3, spm4, spm5, spm6, spm7,spm8, spm9, spm10, spm11, spm12, spm13, spm14, spm15, spm16, spm17, spm18, spm19, spm20;
    static List<Success_Plan_Master__c>        successPlanList;
    static MI_Activity__c                      activity, activity2;
    @IsTest(SeeAllData=true) 
       
    global static void testSuccessPlanPDF () {
       
        Account acc=[Select id from Account where name='MIISD'];
        
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
       
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
       
     
        User u = new User(Alias = 'test', Email='clc@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id,Security_Question_1__c = 'In what city were you born in?',
            Security_Question_2__c = 'What is your favorite team?',Security_Question_3__c = 'What is your mother’s maiden name?',
            Sq1_Answer__c='xyz',Sq2_Answer__c='xyz',Sq3_Answer__c='xyz');
        insert u;
        
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
     goalsList           = new List<SP_Goal__c>();
       milestoneslist      = new List<SP_Transactional__c>();
    servicesList        = new List<SP_Transactional__c>();
     successPlanList     = new List<Success_Plan_Master__c>();
        
        goal1               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Support Network',Case__c = cs.id);
        goalsList.add(goal1);
        
        goal2               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Education',Case__c = cs.id);
        goalsList.add(goal2);
        
        goal3               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Food',Case__c = cs.id);
        goalsList.add(goal3);
        
        goal4               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Health Care',Case__c = cs.id);
        goalsList.add(goal4);
        
        goal5               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Income',Case__c = cs.id);
        goalsList.add(goal5);
        
        goal6               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Child Education',Case__c = cs.id);
        goalsList.add(goal6);
        
        goal7               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Financial',Case__c = cs.id);
        goalsList.add(goal7);
        
        goal8               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Nutrition',Case__c = cs.id);
        goalsList.add(goal8);
        
        goal9               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Shelter',Case__c = cs.id);
        goalsList.add(goal9);
        
        goal10              = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Transport',Case__c = cs.id);
        goalsList.add(goal10);
        
        insert goalsList;
        
        spm1                = new Success_Plan_Master__c(Value__c='Health',Type__C = 'Domain' );
        successPlanList.add(spm1);
        
        spm2                = new Success_Plan_Master__c(Value__c='Education',Type__C = 'Domain' );
        successPlanList.add(spm2);
        
        spm3                = new Success_Plan_Master__c(Value__c='Job Skills',Type__C = 'Domain' );
        successPlanList.add(spm3);
        
        spm4                = new Success_Plan_Master__c(Value__c='Employment',Type__C = 'Domain' );
        successPlanList.add(spm4);
        
        spm5                = new Success_Plan_Master__c(Value__c='Transport',Type__C = 'Domain' );
        successPlanList.add(spm5);
        
        spm6                = new Success_Plan_Master__c(Value__c='Financial',Type__C = 'Domain' );
        successPlanList.add(spm6);
        
        spm7                = new Success_Plan_Master__c(Value__c='Family Life',Type__C = 'Domain' );
        successPlanList.add(spm7);
        
        spm8                = new Success_Plan_Master__c(Value__c='Shelter',Type__C = 'Domain' );
        successPlanList.add(spm8);
        
        spm9                = new Success_Plan_Master__c(Value__c='Wellness',Type__C = 'Domain' );
        successPlanList.add(spm9);
        
        spm10               = new Success_Plan_Master__c(Value__c='Nutrition',Type__C = 'Domain' );
        successPlanList.add(spm10);
        
        insert successPlanList;
        
        spm11               = new Success_Plan_Master__c(Value__c='Health Coverage',Type__C = 'Milestone', Sequence__c = 5, Duration__c = 10, Parent__c = spm1.id);
        insert spm11;
        
        spm12               = new Success_Plan_Master__c(Value__c='Apply for health coverage',Type__C = 'MilestoneSteps', Sequence__c = 1, Duration__c = 10, Parent__c = spm11.id);
        insert spm12;
        
        spm13               = new Success_Plan_Master__c(Value__c='Provide verifications',Type__C = 'MilestoneSteps', Sequence__c = 2, Duration__c = 10, Parent__c = spm11.id);
        insert spm13;
        
        spm14               = new Success_Plan_Master__c(Value__c='Establish health coverage',Type__C = 'MilestoneHeads', Sequence__c = 3, Duration__c = 10, Parent__c = spm11.id);
        insert spm14;
        
        spm15               = new Success_Plan_Master__c(Value__c='Select your health coverage plan',Type__C = 'MilestoneHeads', Sequence__c = 4, Duration__c = 10, Parent__c = spm11.id);
        insert spm15;
        
        
        mile1               = new SP_Transactional__c(Name__c='Health Coverage',  Status__c=true,Goal__c = goal1.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm1.id);
        milestonesList.add(mile1);
        
        mile2               = new SP_Transactional__c(Name__c='College Degree', Status__c=true,Goal__c = goal2.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm2.id);
        milestonesList.add(mile2);
        
        mile3               = new SP_Transactional__c(Name__c='Language',  Status__c=true,Goal__c = goal3.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm3.id);
        milestonesList.add(mile3);
        
        mile4               = new SP_Transactional__c(Name__c='Full Time Work',  Status__c=true,Goal__c = goal4.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm4.id);
        milestonesList.add(mile4);
        
        mile5               = new SP_Transactional__c(Name__c='Wellness Health Coverage',  Status__c=true,Goal__c = goal5.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm5.id);
        milestonesList.add(mile5);
        
        mile6               = new SP_Transactional__c(Name__c='Day Care',  Status__c=true,Goal__c = goal6.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm6.id);
        milestonesList.add(mile6);
        
        mile7               = new SP_Transactional__c(Name__c='Debt Management',  Status__c=true,Goal__c = goal7.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm7.id);
        milestonesList.add(mile7);
        
        mile8               = new SP_Transactional__c(Name__c='Healthy Foods',  Status__c=true,Goal__c = goal8.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm8.id,Selected_Step__c = true );
        milestonesList.add(mile8);
        
        mile9               = new SP_Transactional__c(Name__c=' Immediate Housing Need',  Status__c=true,Goal__c = goal9.id, Start_Date__C = date.today(),Duration__c = 3,Success_Plan_Master2__c = spm9.id,Selected_Step__c = true );
        milestonesList.add(mile9);
        
        mile10              = new SP_Transactional__c(Name__c='Medical Transportation',  Status__c=true,Goal__c = goal10.id, Start_Date__C = date.today(),Duration__c = 3,Success_Plan_Master2__c = spm10.id,Selected_Step__c = true );
        milestonesList.add(mile10);
       
        insert milestonesList;
        
        mile10.Type__c = 'MilestoneSteps';
        update mile10;
        mile9.Type__c = 'MilestoneSteps';
        update mile9;
        
        mile8.Type__c = 'MilestoneSteps';
        update mile8;
        mile7.Type__c = 'Milestone';
        update mile7;
        mile6.Type__c = 'Milestone';
        update mile6;
        mile5.Type__c = 'Milestone';
        update mile5;
        mile4.Type__c = 'Milestone';
        update mile4;
        mile3.Type__c = 'Milestone';
        update mile3;
        mile2.Type__c = 'Milestone';
        update mile2;
        mile1.Type__c = 'Milestone';
        update mile1;
        
        serv1               = new SP_Transactional__c(Name__c='Medical Aid', Type__c='Service', Status__c=true,Goal__c = goal1.id, Start_Date__C = date.today());
        servicesList.add(serv1);
        
        serv2               = new SP_Transactional__c(Name__c='Degree', Type__c='Service', Status__c=true,Goal__c = goal2.id, Start_Date__C = date.today());
        servicesList.add(serv2);
        
        serv3               = new SP_Transactional__c(Name__c='Language', Type__c='Service', Status__c=true,Goal__c = goal3.id, Start_Date__C = date.today());
        servicesList.add(serv3);
        
        serv4               = new SP_Transactional__c(Name__c='Learning', Type__c='Service', Status__c=true,Goal__c = goal4.id, Start_Date__C = date.today());
        servicesList.add(serv4);
        
        serv5               = new SP_Transactional__c(Name__c='healthy', Type__c='Service', Status__c=true,Goal__c = goal5.id, Start_Date__C = date.today());
        servicesList.add(serv5);
        
        serv6               = new SP_Transactional__c(Name__c='child care', Type__c='Service', Status__c=true,Goal__c = goal6.id, Start_Date__C = date.today());
        servicesList.add(serv6);
        
        serv7               = new SP_Transactional__c(Name__c='Earning', Type__c='Service', Status__c=true,Goal__c = goal7.id, Start_Date__C = date.today());
        servicesList.add(serv7);
        
        serv8               = new SP_Transactional__c(Name__c='Food', Type__c='Service', Status__c=true,Goal__c = goal8.id, Start_Date__C = date.today());
        servicesList.add(serv8);
        
        serv9               = new SP_Transactional__c(Name__c='House', Type__c='Service', Status__c=true,Goal__c = goal8.id, Start_Date__C = date.today());
        servicesList.add(serv9);
        
        serv10              = new SP_Transactional__c(Name__c='Medical transportation', Type__c='Service', Status__c=true,Goal__c = goal10.id, Start_Date__C = date.today());
        servicesList.add(serv10);
        
        insert servicesList;
        
        
        
        
        System.runAs(u){
        PageReference pageRef = Page.SuccessPlan_PDF;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
        ApexPages.currentPage().getParameters().put('SP_CaseId',cs.id); 
            
            SuccessPlanPDF controller = new SuccessPlanPDF();
            
        }
    }
}