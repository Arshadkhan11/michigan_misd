@isTest
public class MI_NeedsAssessmentTest {
    
    @IsTest(SeeAllData=true)
    public static void testMI_NeedsAssessment(){
       //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
        Account acc = [select id from Account where name = 'MIISD'];
        Contact c = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert c;
        ApexPages.CurrentPage().getparameters().put('ContactId',c.id);           
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        mina.getDomainStatus();
        mina.ErrorMsg=true;
        mina.AddFamilyContact();
        mina.SecContact();
        
        mina.ShowRelationship=true;
        MI_NeedsAssessment.Resources = new String[]{};
        mina.EducationLst = new String[]{};
        mina.HealthLst = new String[]{};
        mina.TransportationLst = new String[]{};
        mina.JobSkillsLst = new String[]{};
        mina.EmploymentLst = new String[]{};
        mina.WellnessLst = new String[]{};
        mina.FamilyLifeLst = new String[]{};
        mina.FinancialLst = new String[]{};
        mina.NutritionLst = new String[]{};
        mina.ShelterLst = new String[]{};
        mina.contactimg = '';
        mina.PrimaryContactDetails = new Contact();
        mina.SSN='';
        mina.PrimaryContactUserList = new List<User>();
        mina.PrimaryContactUser = new USer();
        mina.PrimaryContactName= '';
        mina.PrimaryContactAge=0.0;
        mina.PrimaryContactUserName='';
        mina.PrimaryContactHome='';
        mina.PrimaryContactCell='';
        mina.PrimaryContactEmail='';
        mina.PrimaryContactPreference='';
        mina.UserObjList = new List<User>();
        mina.SalutationOptions = new List<SelectOption>();
        mina.RoleOptions = new List<SelectOption>();
        mina.endTime=Time.newInstance(1, 2, 3, 4);
        mina.getItems();
        mina.ReassessConId='00535000000Ia5QAAS';
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile'];
        //p.id='00e35000000Lr43AAC';
        
        //SELECT id,ContactId FROM User WHERE ContactId ='00335000002JrhEAAS';
        //List<User> scConId = new List<User>();
        //scConId = [SELECT id,ContactId FROM User WHERE id = '00335000002JrhEAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SecContactTest1(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        
        /*Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase',SuccessCoach__c = u.id,Reassessment_Date__c = date.today(),Origin='Email',SP_Completion__c=false);
        insert cs;
         
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
        */
        
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey');
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.SecCasId = cs.id;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id);
        insert CaseRole;
        mina.SecConId = con.id;
        mina.CaseNo = cs.id;
        mina.SecContact();
        mina.ReassessConId=cs.id;
    }
    @IsTest(SeeAllData=true)
    public static void SecContactTest2(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        mina.ReassessConId='00535000000Ia5QAAS';
        mina.SecConId = con.id;
        mina.CaseNo = cs.id;
        mina.SecContact();
    }
    @IsTest(SeeAllData=true)
    public static void SecContactTest3(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Son');
        insert CaseRole;
        mina.SecConId = con.id;
        mina.CaseNo = cs.id;
        mina.SecContact();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SavePrimaryContactTest(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        mina.SavePrimaryContact();
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest1(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        mina.AssessmentConId = con.id;
        mina.ContactDetails = con;
        mina.BirthDateFormat = 'May 3,1987';
        mina.ContactName = 'John';
        mina.Address = '';
        mina.Email = 'jcom';
        mina.ReassessConId='00535000000Ia5QAAS';
        mina.SaveContact();
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest2(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        mina.AssessmentConId = con.id;
        mina.ContactDetails = con;
        mina.CaseRole = CaseRole;
        mina.BirthDateFormat = '05/03/1987';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.Role = 'Primary Contact';
        mina.SaveContact();
        mina.ReassessConId='00535000000Ia5QAAS';
        mina.AddFamilyContact();
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest3(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = true;
        mina.ContactDetails = con;
        mina.BirthDateFormat = 'May 3,1987';
        mina.ContactName = 'John';
        mina.Address = '';
        mina.Email = 'jcom';
        mina.SaveContact();
        mina.AddFamilyContact();
    }
   @IsTest(SeeAllData=true)
    public static void SaveContactTest4(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = true;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '05/03/1987';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.Role = 'Primary Contact';
        mina.CaseNo = cs.id; 
        mina.SaveContact();
       mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest5(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact();
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = true;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '05/03/1987';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.Role = 'Primary Contact';
        mina.CaseNo = cs.id; 
        mina.SaveContact();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest6(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact();
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = true;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.Role = 'Primary Contact';
        mina.CaseNo = cs.id; 
        mina.SaveContact();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest7(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = false;
        mina.ContactDetails = con;
        mina.BirthDateFormat = 'May 3,1987';
        mina.ContactName = 'John';
        mina.Address = '';
        mina.Email = 'jcom';
        mina.SaveContact();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest8(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = false;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '05/03/1987';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.Role = 'Primary Contact';
        mina.CaseNo = cs.CaseNumber; 
        mina.SaveContact();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest9(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact();
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.Assessment = cs;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = false;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '05/03/1987';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.CaseNo = cs.id; 
        mina.SaveContact();
        mina.getDomainStatus();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest10(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact();
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.Assessment = cs;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = false;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.CaseNo = cs.id; 
        mina.SaveContact();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest11(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', BirthDate=date.today());
        insert con;
        mina.AssessmentConId = con.id;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '05/03/2020';
        mina.ContactName = 'John Rey';
        mina.Address = '';
        mina.SaveContact();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest12(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact();
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = true;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '05/03/2020';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.Role = 'Primary Contact';
        mina.CaseNo = cs.id; 
        mina.SaveContact();
        mina.ReassessConId='00535000000Ia5QAAS';
    }
    @IsTest(SeeAllData=true)
    public static void SaveContactTest13(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Contact con = new Contact();
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        mina.Assessment = cs;
        mina.AssessmentConId = '';
        mina.AddFamilyMember = false;
        mina.ContactDetails = con;
        mina.BirthDateFormat = '05/03/2020';
        mina.ContactName = 'John Rey';
        mina.Address = '98 TestStreet City';
        mina.CaseNo = cs.id; 
        mina.SaveContact();
    }
    @IsTest(SeeAllData=true)
    public static void ContactDependentListTest(){
        Case cs = new Case(Status= 'New', Subject = 'TestCase');
        insert cs;
        MI_NeedsAssessment.ContactDependentList(cs.id);
    }
    @IsTest(SeeAllData=true)
    public static void saveAttachmentTest1(){
        Case cs = new Case(Status= 'New', Subject = 'TestCase');
        insert cs;
        MI_NeedsAssessment.saveAttachment('TestData',cs.id);
    }
    @IsTest(SeeAllData=true)
    public static void saveAttachmentTest2(){
        Case cs = new Case(Status= 'New', Subject = 'TestCase');
        insert cs;
        Attachment attach=new Attachment();    
        attach.Name='NeedsAssesment.txt';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cs.id;
        insert attach;
        MI_NeedsAssessment.saveAttachment('TestData',cs.id);
    }
    @IsTest(SeeAllData=true)
    public static void successPlantTest1(){
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', Title = 'Master');
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id, Subject = 'TestCase');
        insert cs;
        Success_Plan_Master__c spm1 = new Success_Plan_Master__c(Type__c = 'Domain',Value__c = 'Shelter');
        insert spm1;
        Success_Plan_Master__c spm2 = new Success_Plan_Master__c(Type__c = 'Milestone',Parent__c = spm1.id);
        insert spm2;
        String [] goals=new String[]{'Shelter','Health'};
        String [] service=new String[]{''};
        String [] resource=new String[]{''};
        String [] berrienResource=new String[]{'027'};
        String [] muskegonResource=new String[]{''};
        String [] wayneResource=new String[]{''};
       // MI_NeedsAssessment.successPlan(cs.id, goals, service, resource, berrienResource, muskegonResource, wayneResource);
    }
    @IsTest(SeeAllData=true)
    public static void successPlantTest2(){
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', Title = 'Master', Zipcode__c='49079');
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id, Subject = 'TestCase');
        insert cs;
        Success_Plan_Master__c spm1 = new Success_Plan_Master__c(Type__c = 'Domain',Value__c = 'Shelter');
        insert spm1;
        Success_Plan_Master__c spm2 = new Success_Plan_Master__c(Type__c = 'Milestone',Parent__c = spm1.id);
        insert spm2;
        Success_Plan_Master__c spm3 = new Success_Plan_Master__c(Type__c = 'Resource',RID__c = '027',Parent__c=spm1.Id);
        insert spm3;
        SP_Goal__c spg = new SP_Goal__c(Domain__c = 'Shelter',Case__c = cs.id);
        insert spg;
        String [] goals=new String[]{'Shelter','Health'};
        String [] service=new String[]{''};
        String [] resource=new String[]{''};
        String [] berrienResource=new String[]{'027'};
        String [] muskegonResource=new String[]{''};
        String [] wayneResource=new String[]{''};
        //MI_NeedsAssessment.successPlan(cs.id, goals, service, resource, berrienResource, muskegonResource, wayneResource);
    }
    @IsTest(SeeAllData=true)
    public static void successPlantTest3(){
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', Title = 'Master', Zipcode__c='49303');
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id, Subject = 'TestCase');
        insert cs;
        Success_Plan_Master__c spm1 = new Success_Plan_Master__c(Type__c = 'Domain',Value__c = 'Shelter');
        insert spm1;
        Success_Plan_Master__c spm2 = new Success_Plan_Master__c(Type__c = 'Milestone',Parent__c = spm1.id);
        insert spm2;
        Success_Plan_Master__c spm3 = new Success_Plan_Master__c(Type__c = 'Resource',RID__c = '027',Parent__c=spm1.Id);
        insert spm3;
        SP_Goal__c spg = new SP_Goal__c(Domain__c = 'Shelter',Case__c = cs.id);
        insert spg;
        String [] goals=new String[]{'Shelter','Health'};
        String [] service=new String[]{''};
        String [] resource=new String[]{''};
        String [] berrienResource=new String[]{''};
        String [] muskegonResource=new String[]{'027'};
        String [] wayneResource=new String[]{''};
        //MI_NeedsAssessment.successPlan(cs.id, goals, service, resource, berrienResource, muskegonResource, wayneResource);
    }
        @IsTest(SeeAllData=true)
        public static void successPlantTest4(){
        Contact con = new Contact(FirstName = 'John', LastName = 'Rey', Title = 'Master', Zipcode__c='48101');
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id, Subject = 'TestCase');
        insert cs;
        Success_Plan_Master__c spm1 = new Success_Plan_Master__c(Type__c = 'Domain',Value__c = 'Shelter');
        insert spm1;
        Success_Plan_Master__c spm2 = new Success_Plan_Master__c(Type__c = 'Milestone',Parent__c = spm1.id);
        insert spm2;
        Success_Plan_Master__c spm3 = new Success_Plan_Master__c(Type__c = 'Resource',RID__c = '027',Parent__c=spm1.Id);
        insert spm3;
        SP_Goal__c spg = new SP_Goal__c(Domain__c = 'Shelter',Case__c = cs.id);
        insert spg;
        String [] goals=new String[]{'Shelter','Health'};
        String [] service=new String[]{''};
        String [] resource=new String[]{''};
        String [] berrienResource=new String[]{''};
        String [] muskegonResource=new String[]{''};
        String [] wayneResource=new String[]{'027'};
        //MI_NeedsAssessment.successPlan(cs.id, goals, service, resource, berrienResource, muskegonResource, wayneResource);
    }
    @IsTest(SeeAllData=true)
        public static void SetupTransactionalTest(){
        MI_NeedsAssessment.Milestones = new String[]{'Shelter','Health'};
        Case cs = new Case(Status= 'New', Subject = 'TestCase');
        insert cs;
        MI_NeedsAssessment.SP_CaseId = cs.Id;
        SP_Goal__c spg = new SP_Goal__c(Case__c = cs.id, Title__c='Shelter');
        insert spg;
        Success_Plan_Master__c spm1 = new Success_Plan_Master__c(Type__c = 'Domain',Value__c = 'Shelter');
        insert spm1;
        Success_Plan_Master__c spm2 = new Success_Plan_Master__c(Type__c = 'MilestoneSteps',Parent__c = spm1.id);
        insert spm2;
        spm2.Type__c='Milestone';
        update spm2;    
        Success_Plan_Master__c spm3 = new Success_Plan_Master__c(Type__c = 'Resource',RID__c = '027',Parent__c=spm1.Id);
        insert spm3;
        SP_Transactional__c spt = new SP_Transactional__c(Type__c = 'MilestoneSteps', Goal__c = spg.id, Name__c ='Shelter');
        insert spt;
        spt.Type__c='Milestone';
        update spt;    
        MI_NeedsAssessment.SetupTransactional();
    }
    @IsTest(SeeAllData=true)
        public static void retrieveattachmentTest1(){
        Case cs = new Case(Status= 'New', Subject = 'TestCase');
        insert cs;
        MI_NeedsAssessment.retrieveattachment(cs.id);
    }
    @IsTest(SeeAllData=true)
        public static void retrieveattachmentTest2(){
        Case cs = new Case(Status= 'New', Subject = 'TestCase');
        insert cs;
        Attachment attach=new Attachment();    
        attach.Name='NeedsAssesment.txt';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=cs.id;
        insert attach;
        MI_NeedsAssessment.retrieveattachment(cs.id);
    }
    @IsTest(SeeAllData=true)
        public static void saveEventTest1(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        mina.evDate = date.today(); 
        mina.startTime = Time.newInstance(1, 2, 3, 4);
        mina.saveEvent();
    }
        @IsTest(SeeAllData=true)
        public static void saveEventTest2(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        mina.evDate = date.today()-1; 
        mina.startTime = Time.newInstance(1, 2, 3, 4);
        mina.obj.subject__c='Test';
        mina.saveEvent();
    }
    @IsTest(SeeAllData=true)
        public static void saveEventTest3(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        mina.evDate = date.today()+1; 
        mina.startTime = Time.newInstance(1, 2, 3, 4);
        mina.obj.subject__c='Test';
        mina.saveEvent();
    }
    @IsTest(SeeAllData=true)
    public static void saveEventTest4(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        mina.evDate = date.today()+1; 
        mina.startTime = Time.newInstance(1, 2, 3, 4);
        mina.obj.subject__c='Test';
        insert mina.obj;
        mina.saveEvent();
    }
   @IsTest(SeeAllData=true)
    public static void QueryCaseTest1(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment(); 
        
        //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
       Account acc = [select id from Account where name = 'MIISD']; 
       Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id, Title='Master',BirthDate=date.today());
        insert con;
       Case cs = new Case(Status= 'New', ContactId = con.id, Subject = 'TestCase');
        insert cs;
        con.LatestCaseID__c = cs.id;
        update con;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile'];
        //p.id='00e35000000Lr43AAC';
        //string scConId = '00335000002JrhEAAS';
       //mina.ReassessConId='00535000000Ia5QAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            mina.QueryCase();
        }

        /*User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
       //insert u;
        System.runAs(u) {
            mina.QueryCase();
        }*/
    }
    @IsTest(SeeAllData=true)
    public static void QueryCaseTest2(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
        Account acc = [select id from Account where name = 'MIISD']; 
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',Title='Master',AccountId = acc.id);
        insert con;
        Case cs = new Case(Status= 'New', ContactId = con.id, Subject = 'TestCase');
        insert cs;
        con.LatestCaseID__c = cs.id;
        update con;
        CaseContactRole CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile'];
        
        //p.id='00e35000000Lr43AAC';
        //string scConId = '00335000002JrhEAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            mina.QueryCase();
        }
    }
     @IsTest(SeeAllData=true)
        public static void QueryCaseTest3(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
        Account acc = [select id from Account where name = 'MIISD']; 
         Contact con = new Contact(FirstName = 'Test', LastName = 'Test',Title='Master',AccountId = acc.id);
        insert con;
        Case cs = new Case(ContactId = con.id, Subject = 'TestCase', Status = 'Completed');
        insert cs;
        con.LatestCaseID__c = cs.id;
        update con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile']; 
        //p.id='00e35000000Lr43AAC';
        //string scConId = '00335000002JrhEAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            mina.QueryCase();
        }
    }
    @IsTest(SeeAllData=true)
        public static void QueryCaseTest4(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
        Account acc = [select id from Account where name = 'MIISD']; 
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',Title='Master',AccountId = acc.id);
        insert con;
        Case cs = new Case( ContactId = con.id, Subject = 'TestCase',Status = 'Completed');
        insert cs;
        con.LatestCaseID__c = cs.id;
        update con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile']; 
        //p.id='00e35000000Lr43AAC';
        //string scConId = '00335000002JrhEAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            mina.QueryCase();
        }
    }
    @IsTest(SeeAllData=true)
    public static void QueryCaseTest5(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Case cs = new Case(Subject = 'TestCase', Status = 'Completed');
        insert cs;
        PageReference pageRef = Page.Needs_Assessment_Process;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
        ApexPages.currentPage().getParameters().put('AssessmentConId','');
        //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
        Account acc = [select id from Account where name = 'MIISD']; 
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id);
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        //p.id='00e35000000Lr2qAAC';
        //string scConId = '00335000002JrhEAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            mina.QueryCase();
        }
    }
     @IsTest(SeeAllData=true)
        public static void QueryCaseTest6(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Case cs = new Case(Subject = 'TestCase', Status = 'Completed');
        insert cs;
        PageReference pageRef = Page.Needs_Assessment_Process;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
        ApexPages.currentPage().getParameters().put('PrimaryContactId','');
        //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
        Account acc = [select id from Account where name = 'MIISD']; 
         Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id);
        insert con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
         //p.id='00e35000000Lr2qAAC';
        //string scConId = '00335000002JrhEAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            mina.EditAvatar = 'Yes';
            mina.QueryCase();
            PageReference pageRef1 = Page.Needs_Assessment_Process;
            Test.setCurrentPage(pageRef1);
            ApexPages.Standardcontroller sc1 = new ApexPages.Standardcontroller(cs);
            ApexPages.currentPage().getParameters().put('CasesId',cs.id); 
            MI_NeedsAssessment mina1    = new MI_NeedsAssessment();
        }
    }
    @IsTest(SeeAllData=true)
        public static void QueryCaseTest7(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
        Account acc = [select id from Account where name = 'MIISD']; 
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',Title='Master',AccountId = acc.id,BirthDate=date.today());
        insert con;
        Case cs = new Case(ContactId = con.id, Subject = 'TestCase', Status = 'Completed');
        insert cs;
        con.LatestCaseID__c = cs.id;
        update con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile'];
        //p.id='00e35000000Lr43AAC';
        //string scConId = '00335000002JrhEAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            mina.QueryCase();
        }
    }
   /* @isTest public static void QueryCaseTest8(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        Account acc = new Account (Name = 'newAcc');  
        insert acc;
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',Title='Master',AccountId = acc.id,BirthDate=date.today());
        insert con;
        Case cs = new Case( ContactId = con.id, Subject = 'TestCase',Status = 'Completed');
        insert cs;
        con.LatestCaseID__c = cs.id;
        update con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Citizen Profile'];
        p.id='00e35000000Lr43AAC';
        string scConId = '00335000002JrhEAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        System.runAs(u) {
            mina.QueryCase();
        }
    }*/
     @IsTest(SeeAllData=true)
    public static void QueryCaseTest9(){
        MI_NeedsAssessment mina = new MI_NeedsAssessment();
        //Account acc = new Account (Name = 'newAcc');  
        //insert acc;
        Account acc = [select id from Account where name = 'MIISD']; 
         Contact con = new Contact(FirstName = 'Test', LastName = 'Test',Title='Master',AccountId = acc.id,BirthDate=date.today(),Is_Primary__c=true);
        insert con;
        Contact con1 = new Contact(FirstName = 'Test', LastName = 'Test',Title='Master',AccountId = acc.id,BirthDate=date.today(),Is_Primary__c=false);
        insert con1;
        Case cs = new Case(ContactId = con.id, Subject = 'TestCase', Status = 'Completed');
        insert cs;
        PageReference pageRef = Page.Needs_Assessment_Process;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(cs);
        ApexPages.currentPage().getParameters().put('AssessmentConId',con.id);
        con.LatestCaseID__c = cs.id;
        update con;
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile']; 
        //p.id='00e35000000Lr2qAAC';
        //string scConId = '00335000002JrhEAAS';
        User u = new User(Alias = 'test', Email='test@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='test@test.com', ContactId = con.id);
        Relationship__c r = new Relationship__c(Primary_Contact__c=con.id,Secondary_Contact__c=con1.id,Case_Details__c=cs.id);   
        insert r; 
        System.runAs(u) {
            mina.QueryCase();
            mina.getDomainStatus();
            
        }
        CaseDomains__c cd=new CaseDomains__c(Case__c=cs.id,Status__c=true);
        insert cd;
        MI_NeedsAssessment.successPlan(cs.id,con.id);
        mina.UpdateRelationships(cs.id,con.id,con1.id,'Primary Contact');
        mina.UpdateReverseRelationships(cs.id,con.id,con1.id,'Primary Contact');
        ApexPages.currentPage().getParameters().put('NewCon','Yes');
        mina.QueryCase();
        mina.CreateData(con.id,cs.id);
        MI_NeedsAssessment.removeLock(cs.id,'Scheduled');
        mina.markNeedsComplete();
        MI_NeedsAssessment.CompleteReassessment(cs.id,con.id);
        MI_NeedsAssessment.getLastUpdDt(cs.id);
        
        
        
        
        
    }
}