public class MI_CalendarLegend{
    //Property to display Event Name
    public String title {get;set;}
    //Property to indicate Style of the event
    public String style {get;set;}
    
    public MI_CalendarLegend(String sTitle , String sBGColor, String sBorderColor)
    {
        title = sTitle; 
        style ='background: ' + sBGColor + '; border-color: ' + sBorderColor;
    }
}