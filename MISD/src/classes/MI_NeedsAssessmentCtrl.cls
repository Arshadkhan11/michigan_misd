public class MI_NeedsAssessmentCtrl{
    
    public string   conId;
    public Case     contactCase {get;set;}
    public Integer  AssmtScoreNum, AssmtResponseNum;
    public string   successCoach {get;set;}
    public Boolean  showSuccessPlan{get;set;}
    public Boolean  showGoals{get;set;}
    public Boolean  showNotPriorityGoals{get;set;}
    public List<Domain__c> unImpDomains {get;set;}
    public boolean startNeedsButton { get;set;}
    public List<GoalsWrapper> GoalsList{get;set;}
    public Integer percentComplete {get;set;}
    public List<CaseDomains__c> DomainsUnselected{get;set;}
    public List<CaseDomains__c> DomainsUnPriority{get;set;}
    public List<Case> casesList {get;set;}
    public boolean caselocked {get;set;}
    public String caselockedby {get;set;}
    
    public MI_NeedsAssessmentCtrl(){
        caselocked = false;
        string usrId = UserInfo.getUserId();
        User u = [Select id, ContactId from USER WHERE id =:usrId];
        contactCase = new Case();
        conId = u.ContactId;
        //start - newly added - Rameez - 30th May 2016
        if(apexpages.currentpage().getparameters().get('AssessmentConId') != null && apexpages.currentpage().getparameters().get('AssessmentConId') != '') {
            conId = apexpages.currentpage().getparameters().get('AssessmentConId');
        }
        //end - newly added - Rameez - 30th May 2016
        showSuccessPlan = false;
        showGoals = false;
        showNotPriorityGoals = false;
        startNeedsButton = true;
        unImpDomains = new List<Domain__c>();
        GoalsList = new List<GoalsWrapper>();
        DomainsUnselected = new List<CaseDomains__c>();
        DomainsUnPriority = new List<CaseDomains__c>();
        
        casesList = [SELECT id, contact.Name,Success_Coach_Name__c,Assess_Locked_By__c,Assess_Locked_By__r.Name,lastmodifiedDate, CreatedDate,(SELECT id FROM SP_Goals__r WHERE CountMilestones__c > 0 AND SuccessPlanId__r.Status__c='Active') 
                                    FROM Case
                                    WHERE contactId = : conId];
        if(!casesList.isEmpty()){
            contactCase = casesList[0];
            successCoach = contactCase.Success_Coach_Name__c; 
            if(casesList[0].Assess_Locked_By__c != null)
            {
                caselocked = true;
                caselockedby = casesList[0].Assess_Locked_By__r.Name;
            }
        }
        if(!contactCase.sp_goals__r.isEmpty()){
            showSuccessPlan = true;
        }        
        List<AssessmentScores__c> ascores = [SELECT id 
                                                FROM AssessmentScores__c
                                                WHERE Contact__c  = :conId AND NeedsAssessmentId__r.Status__c='Active'];
        if(!ascores.isEmpty()){
            startNeedsButton = false;    
        }
        List<CaseDomains__c> caseDomainsUnselected = [Select Domain__c, Domain__r.Name__c, Domain__r.Category__c, Domain__r.Important__c, Status__c 
                                                            FROM CaseDomains__c 
                                                            WHERE case__c = :contactCase.id AND NeedsAssessmentId__r.Status__c='Active'];
        for(CaseDomains__c cd :caseDomainsUnselected){  
            if(!cd.Status__c){
                DomainsUnselected.add(cd);  
            }else if(cd.Status__c && !cd.Domain__r.Important__c){
                DomainsUnPriority.add(cd);
            }
        }
        if(!DomainsUnPriority.isEmpty())
            showGoals = true;
        if(!DomainsUnselected.isEmpty())
            showNotPriorityGoals = true;   
        system.debug('\n DomainsUnPriority:: ' + DomainsUnPriority + '\n DomainsUnselected: ' + DomainsUnselected);
        getSuccessPlanGoals();
    }
    
    public PageReference deleteData(){ 
        
        system.debug('inside delete data');
        for(case c : caseslist)
        {          
          /*system.debug('case details:'+c);
          List<Attachment> a =[SELECT Id, Body FROM Attachment WHERE ParentId = :c.Id AND Name = 'NeedsAssesment.txt'];
          if(a.size() > 0)
          {delete a;}
          Attachment newFile = new Attachment();
          newFile.ParentId = c.Id;
       
            
          List<AssessmentScores__c> scoresList = [SELECT id FROM AssessmentScores__c where Case__c = :c.Id ];
          delete [SELECT id FROM AssessmentResponses__c WHERE DomainScores__c IN : scoresList];         
          delete [SELECT id FROM CaseDomains__c WHERE Case__c = :c.Id];                      
          delete scoresList;     
            
          List <sp_transactional__C> TransDelLst = [select id, type__C, name__c from sp_transactional__C where goal__c in (select id from sp_Goal__C where case__C = :c.Id)];
          if(TransDelLst.size()>0)
          {delete TransDelLst ;}
          List <sp_goal__C> GoalsDelLst = [select id from sp_Goal__C where case__C = :c.Id];
          if(GoalsDelLst.size()>0)
          {delete GoalsDelLst ; }*/
  
          //Reset the Reassessment Date
          c.Reassessment_Date__c = null;
          c.CA_Completion__c = false;
          c.NA_Completion__c = false;
          c.SP_Completion__c = false;
          update c;
            
          List<NeedsAssessment__c> naclst = [Select Id,Status__c from NeedsAssessment__c where CaseId__c = :c.Id and Status__c = 'Active'];
            if(!naclst.isEmpty()){
                for(NeedsAssessment__c nac : naclst){
                    nac.Status__c = 'Inactive';
                }
                update naclst;
            }
           
          List<SuccessPlan__c> splst = [Select Id,Status__c from SuccessPlan__c where CaseId__c = :c.Id and Status__c = 'Active'];
            if(!splst.isEmpty()){
                for(SuccessPlan__c sp : splst){
                    sp.Status__c = 'Inactive';
                }
                update splst;
            }
          
          List<CommonApplication__c> calst = [Select Id,Status__c from CommonApplication__c where CaseId__c = :c.Id and Status__c = 'Active'];
            if(!calst.isEmpty()){
                for(CommonApplication__c ca : calst){
                    ca.Status__c = 'Inactive';
                }
                update calst;
            }
            
            NeedsAssessment__c nwneeds = new NeedsAssessment__c();
            nwneeds.CaseId__c = c.id;
            nwneeds.Status__c = 'Active';
            insert nwneeds;
            
            SuccessPlan__c nwsp = new SuccessPlan__c();
            nwsp.CaseId__c = c.id;
            nwsp.Status__c = 'Active';
            nwsp.NeedsAssessmentId__c = nwneeds.Id;
            insert nwsp;
        }
        
        CreateData();
        PageReference pr = new PageReference('/Needs_Assessment_Process');
        pr.setRedirect(true);
        return pr;
        
    }
    public PageReference CreateData(){        
        List<AssessmentScores__c> asmtList = new List<AssessmentScores__c>();
        AssessmentScores__c asmt;
        List<AssessmentResponses__c> asmtRespList = new List<AssessmentResponses__c>();
        AssessmentResponses__c asmtRes;
        List<CaseDomains__c> caseDomList = new List<CaseDomains__c>();
        CaseDomains__c caseDom;
        Map<Id, List<AssessmentQuestions__c>> DomainAsmtMap = new Map<Id, List<AssessmentQuestions__c>>();
        
        List<AssessmentScores__c> ascList = [SELECT id,Name from AssessmentScores__c order by Id desc LIMIT 1];
        if(!ascList.isEmpty()){
            AssmtScoreNum = Integer.valueOf(ascList [0].Name.split('-')[1]) + 1;                          
        }else{
            AssmtScoreNum = 1;
        }        
        List<AssessmentResponses__c> ascResList = [SELECT id,Name from AssessmentResponses__c order by Id desc LIMIT 1];
        if(!ascResList.isEmpty()){
            AssmtResponseNum = Integer.valueOf(ascResList [0].Name.split('-')[1]) + 1;            
        }else{
            AssmtResponseNum = 1;
        }
        system.debug('AssmtScoreNum - '+ AssmtScoreNum + 'AssmtResponseNum - ' + AssmtResponseNum);
        
        List<Domain__c> domainsList = [SELECT id,   Category__c, Name__c 
                                            FROM Domain__c order by id];
        List<NeedsAssessment__c> naclst = [Select Id from NeedsAssessment__c where CaseId__c = :contactCase.id and Status__c = 'Active'];
        if(naclst.isEmpty()){
            NeedsAssessment__c nwneeds = new NeedsAssessment__c();
            nwneeds.CaseId__c = contactCase.id;
            nwneeds.Status__c = 'Active';
            insert nwneeds;
            naclst.add(nwneeds);
            SuccessPlan__c nwsp = new SuccessPlan__c();
            nwsp.CaseId__c = contactCase.id;
            nwsp.Status__c = 'Active';
            nwsp.NeedsAssessmentId__c = nwneeds.Id;
            insert nwsp;
        }
        if(naclst.size()>0){
            String NId = naclst[0].id;
            //KD: removed DML from 'for' loop
            for(Domain__c domain : domainsList){
                asmt = new AssessmentScores__c();
                asmt.Case__c = contactCase.id;
                asmt.NeedsAssessmentId__c = NId;
                asmt.Contact__c = conId;   
                asmt.Domain__c = domain.id;
                asmt.Name = 'SC-' + String.ValueOf(AssmtScoreNum);
               // insert asmt;
                asmtList.add(asmt); 
                AssmtScoreNum++;
            }   
            insert asmtList;
            Map<Id,String> amstmap = new Map<Id,String>();
            for(AssessmentScores__c asmtmp : asmtList){
                if(!(amstmap.containsKey(asmtmp.Domain__c))){
                    amstmap.put(asmtmp.Domain__c, asmtmp.Id);
                }
            }
            for(Domain__c domain : domainsList){
               if(amstmap.containsKey(domain.Id)){
                    caseDom = new CaseDomains__c();
                    caseDom.Case__c = contactCase.id;
                    caseDom.NeedsAssessmentId__c = NId;
                    caseDom.Domain__c = domain.id;
                    caseDom.Status__c = false;
                    caseDom.AssessmentScore__c = amstmap.get(domain.Id);
                  // insert caseDom;
                   caseDomList.add(caseDom);
               }
            }
            
            insert caseDomList;
        
            for(AssessmentQuestions__c aq: [SELECT Name, Criteria__c, DataType__c, Domain__c, Options__c, Question__c,Question_Spanish__c, Options_Spanish__c, Domain_Order__c,Question_Order__c
                                                FROM AssessmentQuestions__c
                                                WHERE Domain__c IN : domainsList Order by id]){
                if(DomainAsmtMap.containsKey(aq.Domain__c)){
                    DomainAsmtMap.get(aq.Domain__c).add(aq);
                }else{
                    List<AssessmentQuestions__c> tempList = new List<AssessmentQuestions__c>();
                    tempList.add(aq);
                    DomainAsmtMap.put(aq.Domain__c, tempList);
                }
            }
            for(AssessmentScores__c ascr : [SELECT Name, Domain__c 
                                                FROM AssessmentScores__c 
                                                WHERE Contact__c = :conId and Case__c = :contactCase.id and NeedsAssessmentId__r.Status__c='Active' order by id]){
                if(DomainAsmtMap.keyset().size() > 0 && DomainAsmtMap.containsKey(ascr.Domain__c))                                            
                for(AssessmentQuestions__c aq: DomainAsmtMap.get(ascr.Domain__c)){
                    asmtRes                     = new AssessmentResponses__c();
                    asmtRes.Criteria__c         = aq.Criteria__c;
                    asmtRes.Data_Type__c        = aq.DataType__c;
                    asmtRes.Options__c          = aq.Options__c;
                    asmtRes.QuestionText__c     = aq.Question__c;
                    asmtRes.Question_Spanish__c     = aq.Question_Spanish__c;
                    asmtRes.Options_Spanish__c     = aq.Options_Spanish__c;
                    asmtRes.Question__c         = aq.id;
                    asmtRes.DomainScores__c     = ascr.id;
                    asmtRes.Domain_Order__c     = aq.Domain_Order__c;
                    asmtRes.Name                = 'AR-' + String.valueOF(AssmtResponseNum);
                    asmtRes.Question_Num__c     = aq.Name;
                    asmtRes.Question_Order__c   = aq.Question_Order__c;
                    asmtRespList.add(asmtRes);
                    AssmtResponseNum++;
                }    
            }
            insert asmtRespList;
        }
        
        
        PageReference pr = new PageReference('/Needs_Assessment_Process');
        pr.setRedirect(true);
        return pr;
    }
    
    public void getSuccessPlanGoals(){
        List<SP_Transactional__c> caseMilestones =  [SELECT id ,Name__c, Goal__r.Domain__c, 
                                                                (SELECT id,Name__c,Completed__c 
                                                                    FROM SP_Transactional__r 
                                                                    WHERE type__c = 'MilestoneSteps' OR Type__c ='MilestoneHeads'
                                                                    ORDER By Completed__c desc)
                                                        FROM SP_Transactional__c
                                                        WHERE Goal__r.Case__c = :contactCase.id AND type__c = 'Milestone' AND Status__c =true AND Goal__r.SuccessPlanId__r.Status__c='Active'];
        Decimal totalGoals = 0, compGoals = 0;
        percentComplete = 0;
        for(SP_Transactional__c sp : caseMilestones){
            GoalsWrapper gw = new GoalsWrapper();
            gw.GoalName = sp.Name__c;
            gw.GoalDomain = sp.Goal__r.Domain__c;
            List<MilestoneWrapper> mList = new List<MilestoneWrapper>();
            for(SP_Transactional__c mile : sp.SP_Transactional__r){
                MilestoneWrapper mw = new MilestoneWrapper();
                mw.StepName = mile.Name__c;
                mw.stepStatus = mile.Completed__c;
                if(mile.Completed__c){
                    compGoals++;
                }
                mList.add(mw);
                totalGoals ++;
            }
            gw.GoalSteps = mList;
            GoalsList.add(gw);
        }
        if(compGoals > 0 && totalGoals > 0)
            percentComplete = Integer.ValueOf(compGoals/totalGoals * 100);
    }        
    
    public Class GoalsWrapper{
        public String GoalName {get;set;}
        public String GoalDomain {get;set;}
        public List<MilestoneWrapper> GoalSteps{get;set;}
    }
    
    public Class MilestoneWrapper{
        public String stepName{get;set;}
        public Boolean stepStatus{get;set;}
    }    
    
}