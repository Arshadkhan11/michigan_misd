@isTest
public class MI_FamilyOverviewDash_ALTest {
    
    @isTest public static void testmain()
        {
            //System.debug('ato - BEFORE RunAs');
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
                //System.debug('ato outside'+UserInfo.getUserId());
            System.runAs (thisUser)
            {
                //System.debug('ato - WITHIN RunAs');
                Account acct = new Account(Name='TestAccount');
                insert acct;
                //System.debug('ACCOUNT: ' + acct.Id);
                Contact con = new Contact(FirstName='Tamara',LastName='Davis',AccountId=acct.Id);
                insert con;
                //System.debug('CON' + con.Id);
                Case cas= new Case(ContactId = con.Id,Status = 'Open',Origin = 'Phone',SuccessCoach__c=UserInfo.getUserId());
                insert cas;
                //System.debug('case number' + cas.Id);
                
                MI_FamilyOverviewDash_AL m = new MI_FamilyOverviewDash_AL();
                m.StartDate=System.today();
                m.ClientNameListConcat='Jones;Philip;Smith';
                m.querytaskListUserLogin();
                m.TasksSortBy='Family';
                m.querytaskListUserLogin();
                m.TasksSortBy='Task';
                m.querytaskListUserLogin();
                m.TasksSortBy='Other';
                m.querytaskListUserLogin();
            }
            //System.debug('ato - AFTER RunAs');            
        }    
    
    @isTest public static void testsearch()
        {
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs (thisUser)
            {
                //System.debug('ato - WITHIN RunAs');
                Account acct = new Account(Name='TestAccount');
                insert acct;
                //System.debug('ACCOUNT: ' + acct.Id);
                Contact con = new Contact(FirstName='Tamara',LastName='Davis',AccountId=acct.Id);
                insert con;
                //System.debug('CON' + con.Id);
                Case cas= new Case(ContactId = con.Id,Status = 'Open',Origin = 'Phone',SuccessCoach__c=UserInfo.getUserId());
                insert cas;
                //System.debug('case number' + cas.Id);
                
                MI_FamilyOverviewDash_AL m = new MI_FamilyOverviewDash_AL();
                m.SearchFlag = true;
                m.SearchString = 'James Jones';
                m.SearchClients();
                m.SearchString = null;
                m.SearchClients();
                m.SearchString = '';
                m.SearchClients();
            }
        }

    @isTest public static void testTasks()
        {
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs (thisUser)
            {
                Account acct = new Account(Name='TestAccount');
                insert acct;
                Contact con = new Contact(FirstName='Tamara',LastName='Davis',AccountId=acct.Id);
                insert con;
                Case cas= new Case(ContactId = con.Id,Status = 'Open',Origin = 'Phone',SuccessCoach__c=UserInfo.getUserId());
                insert cas;
                MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Tamara Davis', Activity_Date__c=System.now(), RecordTypeId = [select id from RecordType where DeveloperName='Task' and sObjectType='MI_Activity__c'].Id);
				insert miact;
                miact.OwnerId=UserInfo.getUserId();
                update miact;
                miact = [Select Subject__c, Client_Name__C, OwnerId, Activity_Date__c, RecordTypeId from MI_Activity__c LIMIT 1000];
                
                System.debug('DATA creation done');
                System.debug('Current session'+UserInfo.getUserId());
                System.debug('OwnerId'+miact.OwnerId);
                
                MI_FamilyOverviewDash_AL m = new MI_FamilyOverviewDash_AL();

                m.SearchFlag = false;
                m.ClientNameListConcat='Jones;Tamara Davis;Smith';
                m.querytaskListUserLogin();                
                
            }
        }    
     
    @isTest public static void testGoals()
        {
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs (thisUser)
            {
                Account acct = new Account(Name='TestAccount');
                insert acct;
                Contact con = new Contact(FirstName='Tamara',LastName='Davis',AccountId=acct.Id);
                insert con;
                Case cas= new Case(ContactId = con.Id,Status = 'Open',Origin = 'Phone',SuccessCoach__c=UserInfo.getUserId());
                insert cas;
                
                System.debug('DATA creation done');
                System.debug('Current session'+UserInfo.getUserId());
                
                
                MI_FamilyOverviewDash_AL m = new MI_FamilyOverviewDash_AL();

                m.SearchFlag = false;
                m.ClientNameListConcat='Jones;Tamara Davis;Smith';
                m.querytaskListUserLogin(); 
                m.selectedSort='Family';
                m.SortList();
                m.selectedSort='Due Date';
                m.SortList();
                m.selectedSort='Goal';
                m.SortList();
                
            }
        }        

         
    @isTest public static void testGoals2()
        {
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs (thisUser)
            {
                Account acct = new Account(Name='TestAccount');
                insert acct;
                Contact con = new Contact(FirstName='Tamara',LastName='Davis',AccountId=acct.Id);
                insert con;
                Case cas= new Case(ContactId = con.Id,Status = 'Open',Origin = 'Phone',SuccessCoach__c=UserInfo.getUserId());
                insert cas;
                
                System.debug('DATA creation done');
                System.debug('Current session'+UserInfo.getUserId());

        //Education
        Decimal dur = 120.0;
        Decimal seq = 1.0;
        Decimal dur2 = 30.0;
        Decimal seq2 = 2.0;
        SP_Goal__c vEdu = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Education',Case__c = cas.id);
        insert vEdu;


        SP_Transactional__c sptEdu = new SP_Transactional__c(Name__c='College Degree', Type__c='Milestone', Status__c=true, Milestone_Status__c='In Progress', Parent__c=vEdu.Domain__c, Goal__c = vEdu.id, Start_Date__C = date.today());
        insert sptEdu;

        Success_Plan_Master__c spmDomEdu = new Success_Plan_Master__c(Value__c='Education',Type__C = 'Domain', duration__C = dur2, Sequence__c = seq2 );
        insert spmDomEdu;

        Success_Plan_Master__c spmMilEdu = new Success_Plan_Master__c(Value__c='College Degree',Type__C = 'Milestone', parent__C = spmDomEdu.id, duration__C = dur, Sequence__c = seq);
        insert spmMilEdu;
        
        Success_Plan_Master__c spmMilStepEdu = new Success_Plan_Master__c(Value__c='Submit application',Type__C = 'MilestoneSteps', parent__C = spmMilEdu.id, duration__C = dur2, Sequence__c = seq2);
        insert spmMilStepEdu;
        Success_Plan_Master__c spmMilStepEduHead = new Success_Plan_Master__c(Value__c='Submit application Head',Type__C = 'MilestoneHeads', parent__C = spmMilEdu.id, duration__C = dur2, Sequence__c = seq2);
        insert spmMilStepEduHead;
 				
				                
                
                MI_FamilyOverviewDash_AL m = new MI_FamilyOverviewDash_AL();

                m.SearchFlag = false;
                m.ClientNameListConcat='Jones;Tamara Davis;Smith';
                m.querytaskListUserLogin(); 
                m.selectedSort='Family';
                m.SortList();
                m.selectedSort='Due Date';
                m.SortList();
                m.selectedSort='Goal';
                m.SortList();
                list<SP_Transactional__c> TransList = new list<SP_Transactional__c>();
            Translist = [Select id, Goal__r.Case__r.Contact.Name, Goal__r.Case__r.Primary_Contact_Name__c, Parent__c, Name__c,Start_Date__c 
                            from SP_Transactional__c 
                            where 
                            type__c='Milestone' and Goal__r.Case__r.SuccessCoach__c  = :UserInfo.getUserId()
                            and Milestone_Status__c != 'Completed' and Status__c = true];  
            }
        } 
    
    @isTest public static void testGoals3()
        {
            User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
            System.runAs (thisUser)
            {
                Account acct = new Account(Name='TestAccount');
                insert acct;
                Contact con = new Contact(FirstName='Tamara',LastName='Davis',AccountId=acct.Id);
                insert con;
                Case cas= new Case(ContactId = con.Id,Status = 'Open',Origin = 'Phone',SuccessCoach__c=UserInfo.getUserId());
                insert cas;
                
                System.debug('DATA creation done');
                System.debug('Current session'+UserInfo.getUserId());

        //Education
        Decimal dur = 120.0;
        Decimal seq = 1.0;
        Decimal dur2 = 30.0;
        Decimal seq2 = 2.0;
        SP_Goal__c vEdu = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Education',Case__c = cas.id);
        insert vEdu;


        SP_Transactional__c sptEdu = new SP_Transactional__c(Name__c='College Degree', Type__c='Milestone', Status__c=true, Milestone_Status__c='In Progress', Parent__c=vEdu.Domain__c, Goal__c = vEdu.id, Start_Date__C = date.today());
        insert sptEdu;

        Success_Plan_Master__c spmDomEdu = new Success_Plan_Master__c(Value__c='Education',Type__C = 'Domain', duration__C = dur2, Sequence__c = seq2 );
        insert spmDomEdu;

        Success_Plan_Master__c spmMilEdu = new Success_Plan_Master__c(Value__c='College Degree',Type__C = 'Milestone', parent__C = spmDomEdu.id, duration__C = dur, Sequence__c = seq);
        insert spmMilEdu;
        
        Success_Plan_Master__c spmMilStepEdu = new Success_Plan_Master__c(Value__c='Submit application',Type__C = 'MilestoneSteps', parent__C = spmMilEdu.id, duration__C = dur2, Sequence__c = seq2);
        insert spmMilStepEdu;
        Success_Plan_Master__c spmMilStepEduHead = new Success_Plan_Master__c(Value__c='Submit application Head',Type__C = 'MilestoneHeads', parent__C = spmMilEdu.id, duration__C = dur2, Sequence__c = seq2);
        insert spmMilStepEduHead;

                // 2nd
        SP_Transactional__c sptEdu1 = new SP_Transactional__c(Name__c='College Degree1', Type__c='Milestone', Status__c=true, Milestone_Status__c='In Progress', Parent__c=vEdu.Domain__c, Goal__c = vEdu.id, Start_Date__C = date.today());
        insert sptEdu1;

        Success_Plan_Master__c spmDomEdu1 = new Success_Plan_Master__c(Value__c='Education',Type__C = 'Domain', duration__C = dur2, Sequence__c = seq2 );
        insert spmDomEdu1;

        Success_Plan_Master__c spmMilEdu1 = new Success_Plan_Master__c(Value__c='College Degree1',Type__C = 'Milestone', parent__C = spmDomEdu1.id, duration__C = dur, Sequence__c = seq);
        insert spmMilEdu1;
        
        Success_Plan_Master__c spmMilStepEdu1 = new Success_Plan_Master__c(Value__c='Submit application1',Type__C = 'MilestoneSteps', parent__C = spmMilEdu1.id, duration__C = dur2, Sequence__c = seq2);
        insert spmMilStepEdu1;
        Success_Plan_Master__c spmMilStepEduHead1 = new Success_Plan_Master__c(Value__c='Submit application Head1',Type__C = 'MilestoneHeads', parent__C = spmMilEdu1.id, duration__C = dur2, Sequence__c = seq2);
        insert spmMilStepEduHead1;

                
				                
                
                MI_FamilyOverviewDash_AL m = new MI_FamilyOverviewDash_AL();

                m.SearchFlag = false;
                m.ClientNameListConcat='Jones;Tamara Davis;Smith';
                m.querytaskListUserLogin(); 
                m.selectedSort='Family';
                m.SortList();
                m.selectedSort='Due Date';
                m.SortList();
                m.selectedSort='Goal';
                m.SortList();
                
            }
        }     
    
}