@istest

public class MI_ClientFolioCtrlTest{

    static Contact                             con;
    static Contact                             secCon;
    static Case                                cs;
    static CaseContactRole                     CaseRole;
    static SP_Goal__c                          goal1, goal2, goal3, goal4, goal5, goal6, goal7,goal8, goal9, goal10;
    static List<SP_Goal__c>                    goalsList;
    static SP_Transactional__c                 mile1, mile2, mile3, mile4, mile5, mile6, mile7, mile8, mile9, mile10;
    static SP_Transactional__c                 serv1, serv2, serv3, serv4, serv5, serv6, serv7, serv8, serv9, serv10;
    static List<SP_Transactional__c>           milestonesList;
    static List<SP_Transactional__c>           servicesList;
    static Success_Plan_Master__c              spm1, spm2, spm3, spm4, spm5, spm6, spm7,spm8, spm9, spm10, spm11, spm12, spm13, spm14, spm15, spm16, spm17, spm18, spm19, spm20;
    static List<Success_Plan_Master__c>        successPlanList;
    static MI_Activity__c                      activity, activity2;
    
    static testmethod void myUnitTest1(){
        createTestData();
        Test.StartTest();
            ApexPages.currentPage().getParameters().put('contactId',con.id);
            MI_ClientFolioCtrl clf = new MI_ClientFolioCtrl();
            clf.sortDocs();
            clf.newActivity = activity;         
            clf.saveActivity();
            clf.savePage();
            clf.EditService();
            clf.EditInfo();
            
            ApexPages.currentPage().getParameters().put('actId', activity.Id);
            ApexPages.currentPage().getParameters().put('status', 'Completed');
            Apexpages.currentpage().getparameters().put('IntType','Phone');
            Apexpages.currentpage().getparameters().put('IntDate','/23/2016');
            Apexpages.currentpage().getparameters().put('LoggedBy','Other Coaches'); 
            clf.contactName             = con.FirstName + ' ' + con.LastName;
            clf.contactPhone            = con.MobilePhone;
            clf.contactAge              = con.Age__c;
            clf.contactChildren         = con.Children__c;
            clf.contactSSN              = con.Social_Security_Num__c;
            clf.contactDOB              = con.BirthDate;
            clf.contactIncome           = con.Income__c;
            clf.contactInsured          = con.Insurance__c;
            clf.contactEmployed         = con.Employed__c;
            clf.contactGender           = con.Gender__c;
            
            clf.SearchDB();
            clf.editActivity();
            clf.ChangeStatus();
            clf.DeleteActivity();
            clf.AddMember();
            clf.getInsuranceOptions();
            clf.getEmployOptions();
            clf.getGenderOptions();
            clf.getRelationshipOptions();   
            clf.updateOtherContacts(con.id);
            clf.secContactId = con.id;
            clf.secContact();
        Test.StopTest();
    }
    
    static testMethod void myUnitTest2(){
        createTestData();
        Test.StartTest();
            ApexPages.currentPage().getParameters().put('contactId',con.id);
            ApexPages.currentPage().getParameters().put('SelectedContactID',con.id);
            Apexpages.currentpage().getparameters().put('IntType','Phone');
            Apexpages.currentpage().getparameters().put('IntDate','06/23/2016');
            Apexpages.currentpage().getparameters().put('LoggedBy','Community Partner');
            MI_ClientFolioCtrl clf = new MI_ClientFolioCtrl();
            clf.newActivity = activity2; 
            clf.SearchDB();
            clf.saveActivity(); 
            clf.SaveInfo();
            clf.EditAvatar = 'Yes';
            clf.secContactId = con.id;
            clf.secContact();
            clf.EditDetails();
            clf.cancelSaveCf();
            clf.mStepId = mile4.id;
            clf.toDeleteStep = mile3.id;
            clf.saveMilestone();
            clf.selectedGoal = mile6.id;
            clf.createGoal();
             clf.rscId = serv1.id; 
            clf.selectedResource = serv1.id;
            clf.addResource();
            clf.toDeleteRsc = serv1.id;
            //clf.saveResources();
           
        
            clf.deleteResource();
            clf.CancelSave();
        Test.StopTest();
    }
    static testMethod void myUnitTest3(){
        createTestData();
        Test.StartTest();
            ApexPages.currentPage().getParameters().put('contactId',con.id);
            ApexPages.currentPage().getParameters().put('SelectedContactID',con.id);
            Apexpages.currentpage().getparameters().put('IntType','Phone');
            Apexpages.currentpage().getparameters().put('IntDate','06/23/2016');
            Apexpages.currentpage().getparameters().put('LoggedBy','Success Coach');
            MI_ClientFolioCtrl clf = new MI_ClientFolioCtrl();
            clf.getReverseRelationship('Father');
            clf.CancelJS();
            clf.att.Name = 'Trial.txt';
            clf.att.body = Blob.valueOf('This is to check upload function');
            clf.UploadFile();
            clf.CreateRelationships(cs.id, con.id, SecCon.Id, 'Wife');
            clf.CreateRelationships(cs.id, con.id, SecCon.Id, 'Primary Contact');
            clf.UpdateRelationships(Cs.Id, Con.Id, SecCon.Id, 'Daughter');
            clf.getFamilyList();
            clf.SearchDB();
        Test.StopTest();
    }
    
    static void createTestData(){
        con                             = new Contact();
        con.FirstName                   = 'John'; 
        con.LastName                    = 'Rey';
        con.BirthDate                   = date.today();
        con.Address__c                  = 'Test Address';
        con.Social_Security_Num__c      = '12346789';
        con.Children__c                 = 4;
        con.MobilePhone                 = '12349995';
        con.Insurance__c                = 'Not Insured';
        con.Employed__c                 = 'Not Employed';
        con.Gender__c                   = 'Male';
        con.Is_primary__c               = false;
        insert con;
        
        seccon                             = new Contact();
        seccon.FirstName                   = 'Julie'; 
        seccon.LastName                    = 'Rey';
        seccon.BirthDate                   = date.today();
        seccon.Address__c                  = 'Test Address';
        seccon.Social_Security_Num__c      = '12346789';
        
        seccon.MobilePhone                 = '12349995';
        seccon.Insurance__c                = 'Not Insured';
        seccon.Employed__c                 = 'Not Employed';
        seccon.Gender__c                   = 'Male';
        seccon.Is_primary__c               = false;
        insert secCon;
        
        cs                  = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        
        CaseRole = new CaseContactRole(CasesId = cs.id,ContactId = con.id,Role = 'Primary Contact');
        insert CaseRole;
        
        goalsList           = new List<SP_Goal__c>();
        milestonesList      = new List<SP_Transactional__c>();
        servicesList        = new List<SP_Transactional__c>();
        successPlanList     = new List<Success_Plan_Master__c>();
        
        goal1               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Health',Case__c = cs.id);
        goalsList.add(goal1);
        
        goal2               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Education',Case__c = cs.id);
        goalsList.add(goal2);
        
        goal3               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Job Skills',Case__c = cs.id);
        goalsList.add(goal3);
        
        goal4               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Employment',Case__c = cs.id);
        goalsList.add(goal4);
        
        goal5               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Wellness',Case__c = cs.id);
        goalsList.add(goal5);
        
        goal6               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Family Life',Case__c = cs.id);
        goalsList.add(goal6);
        
        goal7               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Financial',Case__c = cs.id);
        goalsList.add(goal7);
        
        goal8               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Nutrition',Case__c = cs.id);
        goalsList.add(goal8);
        
        goal9               = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Shelter',Case__c = cs.id);
        goalsList.add(goal9);
        
        goal10              = new SP_Goal__c( Title__c = 'TestTitle', Domain__c='Transport',Case__c = cs.id);
        goalsList.add(goal10);
        
        insert goalsList;
        
        spm1                = new Success_Plan_Master__c(Value__c='Health',Type__C = 'Domain' );
        successPlanList.add(spm1);
        
        spm2                = new Success_Plan_Master__c(Value__c='Education',Type__C = 'Domain' );
        successPlanList.add(spm2);
        
        spm3                = new Success_Plan_Master__c(Value__c='Job Skills',Type__C = 'Domain' );
        successPlanList.add(spm3);
        
        spm4                = new Success_Plan_Master__c(Value__c='Employment',Type__C = 'Domain' );
        successPlanList.add(spm4);
        
        spm5                = new Success_Plan_Master__c(Value__c='Transport',Type__C = 'Domain' );
        successPlanList.add(spm5);
        
        spm6                = new Success_Plan_Master__c(Value__c='Financial',Type__C = 'Domain' );
        successPlanList.add(spm6);
        
        spm7                = new Success_Plan_Master__c(Value__c='Family Life',Type__C = 'Domain' );
        successPlanList.add(spm7);
        
        spm8                = new Success_Plan_Master__c(Value__c='Shelter',Type__C = 'Domain' );
        successPlanList.add(spm8);
        
        spm9                = new Success_Plan_Master__c(Value__c='Wellness',Type__C = 'Domain' );
        successPlanList.add(spm9);
        
        spm10               = new Success_Plan_Master__c(Value__c='Nutrition',Type__C = 'Domain' );
        successPlanList.add(spm10);
        
        insert successPlanList;
        
        spm11               = new Success_Plan_Master__c(Value__c='Health Coverage',Type__C = 'Milestone', Sequence__c = 5, Duration__c = 10, Parent__c = spm1.id);
        insert spm11;
        
        spm12               = new Success_Plan_Master__c(Value__c='Apply for health coverage',Type__C = 'MilestoneSteps', Sequence__c = 1, Duration__c = 10, Parent__c = spm11.id);
        insert spm12;
        
        spm13               = new Success_Plan_Master__c(Value__c='Provide verifications',Type__C = 'MilestoneSteps', Sequence__c = 2, Duration__c = 10, Parent__c = spm11.id);
        insert spm13;
        
        spm14               = new Success_Plan_Master__c(Value__c='Establish health coverage',Type__C = 'MilestoneHeads', Sequence__c = 3, Duration__c = 10, Parent__c = spm11.id);
        insert spm14;
        
        spm15               = new Success_Plan_Master__c(Value__c='Select your health coverage plan',Type__C = 'MilestoneHeads', Sequence__c = 4, Duration__c = 10, Parent__c = spm11.id);
        insert spm15;
        
        
        mile1               = new SP_Transactional__c(Name__c='Health Coverage',  Status__c=true,Goal__c = goal1.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm1.id);
        milestonesList.add(mile1);
        
        mile2               = new SP_Transactional__c(Name__c='College Degree', Status__c=true,Goal__c = goal2.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm2.id);
        milestonesList.add(mile2);
        
        mile3               = new SP_Transactional__c(Name__c='Language',  Status__c=true,Goal__c = goal3.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm3.id);
        milestonesList.add(mile3);
        
        mile4               = new SP_Transactional__c(Name__c='Full Time Work',  Status__c=true,Goal__c = goal4.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm4.id);
        milestonesList.add(mile4);
        
        mile5               = new SP_Transactional__c(Name__c='Wellness Health Coverage',  Status__c=true,Goal__c = goal5.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm5.id);
        milestonesList.add(mile5);
        
        mile6               = new SP_Transactional__c(Name__c='Day Care',  Status__c=true,Goal__c = goal6.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm6.id);
        milestonesList.add(mile6);
        
        mile7               = new SP_Transactional__c(Name__c='Debt Management',  Status__c=true,Goal__c = goal7.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm7.id);
        milestonesList.add(mile7);
        
        mile8               = new SP_Transactional__c(Name__c='Healthy Foods',  Status__c=true,Goal__c = goal8.id, Start_Date__C = date.today(),Success_Plan_Master2__c = spm8.id);
        milestonesList.add(mile8);
        
        mile9               = new SP_Transactional__c(Name__c=' Immediate Housing Need',  Status__c=true,Goal__c = goal9.id, Start_Date__C = date.today(),Duration__c = 3,Success_Plan_Master2__c = spm9.id);
        milestonesList.add(mile9);
        
        mile10              = new SP_Transactional__c(Name__c='Medical Transportation',  Status__c=true,Goal__c = goal10.id, Start_Date__C = date.today(),Duration__c = 3,Success_Plan_Master2__c = spm10.id);
        milestonesList.add(mile10);
       
        insert milestonesList;
        
        mile10.Type__c = 'Milestone';
        update mile10;
        mile9.Type__c = 'Milestone';
        update mile9;
        
        mile8.Type__c = 'Milestone';
        update mile8;
        mile7.Type__c = 'Milestone';
        update mile7;
        mile6.Type__c = 'Milestone';
        update mile6;
        mile5.Type__c = 'Milestone';
        update mile5;
        mile4.Type__c = 'Milestone';
        update mile4;
        mile3.Type__c = 'Milestone';
        update mile3;
        mile2.Type__c = 'Milestone';
        update mile2;
        mile1.Type__c = 'Milestone';
        update mile1;
        
        serv1               = new SP_Transactional__c(Name__c='Medical Aid', Type__c='Resource', Status__c=true,Goal__c = goal1.id, Start_Date__C = date.today());
        servicesList.add(serv1);
        
        serv2               = new SP_Transactional__c(Name__c='Degree', Type__c='Service', Status__c=true,Goal__c = goal2.id, Start_Date__C = date.today());
        servicesList.add(serv2);
        
        serv3               = new SP_Transactional__c(Name__c='Language', Type__c='Service', Status__c=true,Goal__c = goal3.id, Start_Date__C = date.today());
        servicesList.add(serv3);
        
        serv4               = new SP_Transactional__c(Name__c='Learning', Type__c='Service', Status__c=true,Goal__c = goal4.id, Start_Date__C = date.today());
        servicesList.add(serv4);
        
        serv5               = new SP_Transactional__c(Name__c='healthy', Type__c='Service', Status__c=true,Goal__c = goal5.id, Start_Date__C = date.today());
        servicesList.add(serv5);
        
        serv6               = new SP_Transactional__c(Name__c='child care', Type__c='Service', Status__c=true,Goal__c = goal6.id, Start_Date__C = date.today());
        servicesList.add(serv6);
        
        serv7               = new SP_Transactional__c(Name__c='Earning', Type__c='Service', Status__c=true,Goal__c = goal7.id, Start_Date__C = date.today());
        servicesList.add(serv7);
        
        serv8               = new SP_Transactional__c(Name__c='Food', Type__c='Service', Status__c=true,Goal__c = goal8.id, Start_Date__C = date.today());
        servicesList.add(serv8);
        
        serv9               = new SP_Transactional__c(Name__c='House', Type__c='Service', Status__c=true,Goal__c = goal8.id, Start_Date__C = date.today());
        servicesList.add(serv9);
        
        serv10              = new SP_Transactional__c(Name__c='Medical transportation', Type__c='Service', Status__c=true,Goal__c = goal10.id, Start_Date__C = date.today());
        servicesList.add(serv10);
        
        insert servicesList;
        
        
        
        
        activity                        = new MI_Activity__c();
        activity.NoteBook_Time__c       = '10 AM';
        activity.NoteBook_Date__c       = system.Today();
        activity.NoteBook_WithWhom__c   = '10 AM';
        activity.NoteBook_Action__c     = '10 AM';
        activity.NoteBook_Partner__c    = '10 AM';
        activity.NoteBook_Purpose__c    = '10 AM';
        activity.NoteBook_Notes__c      = '10 AM';
        activity.subject__c             = 'new activity';
        activity.RecordTypeId           = [Select id from RecordType where SobjectType = 'MI_Activity__c' and DeveloperName='Notebook'].id;
        
        insert activity;
        
        activity2                       = new MI_Activity__c();
        activity2.subject__c            = 'new activity';
        activity2.RecordTypeId          = [Select id from RecordType where SobjectType = 'MI_Activity__c' and DeveloperName='Notebook'].id;
        activity2.NoteBook_Time__c      = '10:00 AM';
        
        insert activity2;
        
        List<StaticResource> docs = new List<StaticResource>();
 docs = [select id, name, body from StaticResource where name = 'TestJSONCommonApplicationPDF'];
     if(docs.size()>0){
     String attbody = docs[0].body.toString();
     attbody = attbody.replace('00335000002jdIYAAY', con.id);
     attbody = attbody.replace('00335000002jdIiAAI', con.id);
     List<Attachment> atttemp = new List<Attachment>();
     Attachment atttemp1= new Attachment();
     atttemp1.Body = Blob.valueOf(attbody);
     atttemp1.Name = 'CommonAppAnswers.txt';
     atttemp1.ParentId=cs.id; 
     atttemp.add(atttemp1);
         
     Attachment atttemp2= new Attachment();
     atttemp2.Body = Blob.valueOf('"Q3":"Branch","Q2":"No","Q4":"Sign Language","Q5":null,"Q6":"No","Q7A":null,"Q7B":null,"Q7C":null,"Q7D":null,"Q8":null,"Q9":null,"Q10":null,"Q11":null,"Q12":null}');
     atttemp2.Name = 'CommonAppIntro.txt';
     atttemp2.ParentId=cs.id; 
     atttemp.add(atttemp2);
          
     insert atttemp;
     }    
    }
    
}