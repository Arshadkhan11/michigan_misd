global without sharing class SuccessPlan {
    
    
    
// Rameez 
public Id CaseId {get; set;}
public String SP_CaseId {get; set;}
public String[] Milestones {get;set;}
public String[] Resources {get;set;}
public String[] EducationLst {get; set; }
public String[] HealthLst {get; set; }
public String[] TransportationLst {get; set; }
public String[] JobSkillsLst {get; set; }
public String[] EmploymentLst {get; set; }
public String[] WellnessLst {get; set; }
public String[] FamilyLifeLst {get; set; }
public String[] FinancialLst {get; set; }
public String[] NutritionLst {get; set; }
public String[] ShelterLst {get; set; }

public String[] EducationLstSvc {get; set; }
public String[] HealthLstSvc {get; set; }
public String[] TransportationLstSvc {get; set; }
public String[] JobSkillsLstSvc {get; set; }
public String[] EmploymentLstSvc {get; set; }
public String[] WellnessLstSvc {get; set; }
public String[] FamilyLifeLstSvc {get; set; }
public String[] FinancialLstSvc {get; set; }
public String[] NutritionLstSvc {get; set; }
public String[] ShelterLstSvc {get; set; }


SP_Transactional__c T;
SP_Goal__c G;
List<Success_Plan_Master__c> MasterData;
List<SP_Transactional__c > ResourceLst;
List<SP_Transactional__c > ServicesLst;
List<SP_Transactional__c> Lst;
    
    
    

    public void getSetupSuccessPlan() {
        
        
        CaseId = apexpages.currentpage().getparameters().get('SP_CaseId');

        
        Milestones = new String[]{'GED Program','Physical Health','Transportation','Wellness','Nutrition'};
        Resources = new String[]{'299','307'};
                
        SetupGoals('Health');
        SetupGoals('Transportation');
        SetupGoals('Education');
        SetupGoals('Job Skills');
        SetupGoals('Employment');
        SetupGoals('Wellness');
        SetupGoals('Family Life');
        SetupGoals('Financial');
        SetupGoals('Nutrition');
        SetupGoals('Shelter');
        

        SetupTransactional();
       
    }
    
    public void SetupGoals(String GoalName)
    {
        G = new SP_Goal__c();
        G.Case__c = CaseId;
        G.Domain__c = GoalName;
        insert G;
        
        MasterData = [SELECT ID, TYPE__C, PARENT__r.Value__c, VALUE__C FROM Success_Plan_Master__c WHERE (TYPE__C = 'Milestone' or TYPE__C = 'Resource') and Parent__r.Value__c = :GoalName];
        
        for (Success_Plan_Master__c s: MasterData) {
            T = new SP_Transactional__c();
            T.Type__c = s.TYPE__C;
            T.Name__c = s.Value__c;
            T.Status__c = false;
            T.Goal__c = G.Id;
            T.Start_Date__c = Date.today();
            T.End_Date__c = Date.today();
            T.Parent__c = s.PARENT__r.Value__c;
            insert T;
        }
        
    }
    
    public void SetupTransactional()
    {
        
        Lst = [select id, Type__c, Goal__c, Name__c, Start_Date__c, End_Date__c, Status__c, Parent__C from SP_Transactional__c where Type__c = 'Milestone' and NAME__c in :Milestones];
        
        for (SP_Transactional__c s: Lst) {
            s.Status__c = true;
        }
        update Lst;
        
        Lst = [select id, Type__c, Goal__c, Name__c, Start_Date__c, End_Date__c, Status__c, Parent__C from SP_Transactional__c where Type__c = 'Resource' and NAME__c in :Resources];
        
        for (SP_Transactional__c s: Lst) {
            s.Status__c = true;
        }
        update Lst;
    }
    
    
// Rameez end
    
    
    
    
    Public Case Assessment {get;set;}
    Public CaseContactRole CaseRole {get;set;}
    Public Contact ContactDetails {get;set;}
    Public Contact PrimaryContactDetails {get;set;}
    public List<SelectOption> SalutationOptions {get;set;}
    Public String AssessmentConId {get;set;}
    Public String CaseNo {get;set;}
    Public String PrimaryContactNo {get;set;}
    Public String CaseContactPrimaryId {get;set;}
    Public DateTime StartDate {get;set;}
    Public String StartDateFormat {get;set;}
    Public DateTime LastUpdate {get;set;}
    Public String LastUpdateFormat {get;set;}
    Public String Status {get;set;}
    Public String Role {get;set;}
    Public String ContactSalutation {get;set;}
    Public String ContactName {get;set;}
    Public DateTime BirthDate {get;set;}
    Public String BirthDateFormat {get;set;}
    Public String HomePhone {get;set;}
    Public String MobilePhone {get;set;}
    Public String Email {get;set;}
    Public String Address {get;set;}
    Public String SSN {get;set;}
    Public List<Case> CaseList;
    Public List<String> arrTest;
    public List<CaseContactRole> SecondaryContacts {get;set;}
    Public Id SecConId {get;set;}
    Public Id SecCasId {get;set;}
    Public User CaseUser {get;set;}
    Public List<User> PrimaryContactUserList;
    Public User PrimaryContactUser {get;set;}
    Public String PrimaryContactName {get;set;}
    Public String PrimaryFamilyName {get;set;}
    Public Decimal PrimaryContactAge {get;set;}
    Public String PrimaryContactUserName {get;set;}
    Public String PrimaryContactHome {get;set;}
    Public String PrimaryContactCell {get;set;}
    Public String PrimaryContactEmail {get;set;}
    Public String PrimaryContactPreference {get;set;}
    Public Boolean AddFamilyMember {get;set;}
    Public Boolean ErrorMsg {get;set;}
    Public String svNutrition {get;set;}
    Public String svEducation {get;set;}
    Public String svFinancial {get;set;}
    Public Boolean svHealthWell {get;set;}
    Public String svFamilyLife {get;set;}
    public MI_Activity__c obj {get; set; }
    Public Date evDate {get;set;}
    public time startTime {get;set;}
    public time endTime {get;set;}
    public DateTime evStartDate {get; set;}
    public DateTime evEndDate {get; set;}

    //BBANSAL - Global Variable for CaseId
    public Id CaseIdFromNeeds {get;set;}    

    //BBANSAL - toFetch the goals related to Education Domain
    public Goals__c FetchEducationGoals{get;set;}
    public List<Goals__c> SelectedEducationGoals {get;set;}
    public List<Goals__c> EducationList {get;set;}
    public Id EduGoalId{get;set;}
    //BBANSAL - toFetch the goals related to Family Domain
    public Goals__c FetchFamilyGoals{get;set;}
    public List<Goals__c> SelectedFamilyGoals {get;set;}
    public List<Goals__c> FamilyList {get;set;}
    public Id FamGoalId{get;set;}
    //BBANSAL - toFetch the goals related to Health Domain
    public Goals__c FetchHealthGoals{get;set;}
    public List<Goals__c> SelectedHealthGoals {get;set;}
    public List<Goals__c> HealthList {get;set;}
    public Id HlthGoalId{get;set;}
    //BBANSAL - toFetch the goals related to Wellness Domain
    public Goals__c FetchWellnessGoals{get;set;}
    public List<Goals__c> SelectedWellnessGoals {get;set;}
    public List<Goals__c> WellnessList {get;set;}
    public Id WellGoalId{get;set;}
    //BBANSAL - toFetch the goals related to JobSkills Domain
    public Goals__c FetchJobSkillsGoals{get;set;}
    public List<Goals__c> SelectedJobSkillsGoals {get;set;}
    public List<Goals__c> JobSkillsList {get;set;}
    public Id JobGoalId{get;set;}  
    //BBANSAL - toFetch the goals related to Financials Domain
    public Goals__c FetchFinancialsGoals{get;set;}
    public List<Goals__c> SelectedFinancialsGoals {get;set;}
    public List<Goals__c> FinancialsList {get;set;}
    public Id FinGoalId{get;set;}
    //ALAN
    // - toFetch the goals related to FoodNutrition Domain
    public Goals__c FetchFoodNutritionGoals{get;set;}
    public List<Goals__c> SelectedFoodNutritionGoals {get;set;}
    public List<Goals__c> FoodNutritionList {get;set;}
    public Id FoodNutritionGoalId{get;set;}
    // - toFetch the goals related to HousingShelter Domain
    public Goals__c FetchHousingShelterGoals{get;set;}
    public List<Goals__c> SelectedHousingShelterGoals {get;set;}
    public List<Goals__c> HousingShelterList {get;set;}
    public Id HousingShelterGoalId{get;set;}
    // - toFetch the goals related to Employment Domain
    public Goals__c FetchEmploymentGoals{get;set;}
    public List<Goals__c> SelectedEmploymentGoals {get;set;}
    public List<Goals__c> EmploymentList {get;set;}
    public Id EmploymentGoalId{get;set;}
    // - toFetch the goals related to Transportation Domain
    public Goals__c FetchTransportationGoals{get;set;}
    public List<Goals__c> SelectedTransportationGoals {get;set;}
    public List<Goals__c> TransportationList {get;set;}
    public Id TransportationGoalId{get;set;}
    
    global SuccessPlan() {
    
    EducationLst = new List<String>();
    HealthLst = new List<String>();
    TransportationLst = new List<String>();
    JobSkillsLst = new List<String>();
    EmploymentLst = new List<String>();
    WellnessLst = new List<String>();
    FamilyLifeLst = new List<String>();
    FinancialLst = new List<String>();
    NutritionLst = new List<String>();
    ShelterLst = new List<String>();
    
    EducationLstSvc = new List<String>();
    HealthLstSvc = new List<String>();
    TransportationLstSvc = new List<String>();
    JobSkillsLstSvc = new List<String>();
    EmploymentLstSvc = new List<String>();
    WellnessLstSvc = new List<String>();
    FamilyLifeLstSvc = new List<String>();
    FinancialLstSvc = new List<String>();
    NutritionLstSvc = new List<String>();
    ShelterLstSvc = new List<String>();



// Rameez start
        CaseId = apexpages.currentpage().getparameters().get('SP_CaseId');
        
        ResourceLst = [SELECT ID, TYPE__C, Goal__r.Domain__c, Name__C FROM SP_Transactional__c WHERE TYPE__C = 'Resource' and Goal__r.Case__c = :CaseId];
        for (SP_Transactional__c s: ResourceLst) {
            if(s.Goal__r.Domain__c == 'Education')
            {
                EducationLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Health')
            {
                HealthLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Transport')
            {
                TransportationLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Job Skills')
            {
                JobSkillsLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Employment')
            {
                EmploymentLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Wellness')
            {
                WellnessLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Family Life')
            {
                FamilyLifeLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Nutrition')
            {
                NutritionLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Shelter')
            {
                ShelterLst.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Financial')
            {
                FinancialLst.add(s.Name__C );
            }
        }
        
        
        ServicesLst = [SELECT ID, TYPE__C, Goal__r.Domain__c, Name__C FROM SP_Transactional__c WHERE TYPE__C = 'Service' and Goal__r.Case__c = :CaseId];
        for (SP_Transactional__c s: ServicesLst) {
            if(s.Goal__r.Domain__c == 'Education')
            {
                EducationLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Health')
            {
                HealthLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Transport')
            {
                TransportationLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Job Skills')
            {
                JobSkillsLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Employment')
            {
                EmploymentLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Wellness')
            {
                WellnessLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Family Life')
            {
                FamilyLifeLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Nutrition')
            {
                NutritionLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Shelter')
            {
                ShelterLstSvc.add(s.Name__C );
            }
            else if(s.Goal__r.Domain__c == 'Financial')
            {
                FinancialLstSvc.add(s.Name__C );
            }
        }
// Rameez end
    
    
    
        AddFamilyMember = false;
        ErrorMsg = false;
        SalutationOptions = new List<SelectOption>();
        SalutationOptions.add(new SelectOption('','--None--'));
        SalutationOptions.add(new SelectOption('Mr.','Mr.'));
        SalutationOptions.add(new SelectOption('Ms.','Ms.'));
        SalutationOptions.add(new SelectOption('Mrs.','Mrs.'));
        SalutationOptions.add(new SelectOption('Dr.','Dr.'));
        SalutationOptions.add(new SelectOption('Prof.','Prof.'));
        obj = new MI_Activity__c();
        CaseUser = [SELECT Id from User where Name = 'Salesforce Administrator' Limit 1];
        
        //BBANSAL
        EducationList = getEduList();
        FamilyList = getFamList();
        HealthList = getHlthList();
        WellnessList = getWellList();
        JobSkillsList = getJobList();
        FinancialsList = getFinList();
        FoodNutritionList = getFoodNutritionList();
        HousingShelterList = getHousingShelterList();
        EmploymentList = getEmploymentList();
        TransportationList = getTransportationList();
    }

  //BBANSAL - to save Education Goals on click of save
    public void SaveEduGoal()
    {
    update FetchEducationGoals;
        EducationList = getEduList();
    }
    public void SaveFamGoal()
    {
    update FetchFamilyGoals;
        FamilyList = getFamList();
    }

  //BBANSAL - to save Health Goals on click of save
    public void SaveHlthGoal()
    {
    update FetchHealthGoals;
        HealthList = getHlthList();
    }
  public void SaveWellGoal()
    {
    update FetchWellnessGoals;
        WellnessList = getWellList();
    }
  public void SaveJobGoal()
    {
    update FetchJobSkillsGoals;
        JobSkillsList = getJobList();
    }
  public void SaveFinGoal()
    {
    update FetchFinancialsGoals;
        FinancialsList = getFinList();
    }
  
    public void SaveTransportationGoal()
    {
    update FetchTransportationGoals;
        TransportationList = getTransportationList();
    }
    
    public void SaveEmploymentGoal()
    {
    update FetchEmploymentGoals;
        EmploymentList = getEmploymentList();
    }

    public void SaveHousingShelterGoal()
    {
    update FetchHousingShelterGoals;
        HousingShelterList = getHousingShelterList();
    }

    public void SaveFoodNutritionGoal()
    {
    update FetchFoodNutritionGoals;
        FoodNutritionList = getFoodNutritionList();
    }

  //BBANSAL - To fetch single goal on edit click
    public PageReference EduFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchEducationGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:EduGoalId LIMIT 1];
        return null;
    }
    public PageReference FamFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchFamilyGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:FamGoalId LIMIT 1];
        return null;
    }    
  //BBANSAL - To fetch single goal on edit click
    public PageReference HlthFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchHealthGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:HlthGoalId LIMIT 1];
        return null;
    }
    public PageReference WellFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchWellnessGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:WellGoalId LIMIT 1];
        return null;
    }

    public PageReference JobFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchJobSkillsGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:JobGoalId LIMIT 1];
        return null;
    }
    public PageReference FinFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchFinancialsGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:FinGoalId LIMIT 1];
        return null;
    }
//ALAN
//////// FoodNutrition
    public PageReference FoodNutritionFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchFoodNutritionGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:FoodNutritionGoalId LIMIT 1];
        return null;
    }
    
//////// HousingShelter
    public PageReference HousingShelterFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchHousingShelterGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:HousingShelterGoalId LIMIT 1];
        return null;
    }

//////// Employment
    public PageReference EmploymentFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchEmploymentGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:EmploymentGoalId LIMIT 1];
        return null;
    }    

//////// Transportation
    public PageReference TransportationFetchDetails()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        FetchTransportationGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c from Goals__c where Id=:TransportationGoalId LIMIT 1];
        return null;
    }

    //BBANSAL - To fetch List of goals on edit click
    public List<Goals__c> getEduList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // EDU -> pull goal record
        SelectedEducationGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Education'
                         ORDER BY Id];
        return SelectedEducationGoals;
    }
     //BBANSAL - To fetch List of goals on edit click
    public List<Goals__c> getFamList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // EDU -> pull goal record
        SelectedFamilyGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Family'
                         ORDER BY Id];
        return SelectedFamilyGoals;
    }
    
     //BBANSAL - To fetch List of goals on edit click
    public List<Goals__c> getHlthList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // Hlth -> pull goal record
        SelectedHealthGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Health'
                         ORDER BY Id];
        return SelectedHealthGoals;
    }
    public List<Goals__c> getWellList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // Well -> pull goal record
        SelectedWellnessGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Wellness'
                         ORDER BY Id];
        return SelectedWellnessGoals;
    }
    public List<Goals__c> getJobList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // Job -> pull goal record
        SelectedJobSkillsGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'JobSkills'
                         ORDER BY Id];
        return SelectedJobSkillsGoals;
    }
    public List<Goals__c> getFinList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // Fin -> pull goal record
        SelectedFinancialsGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Financials'
                         ORDER BY Id];
        return SelectedFinancialsGoals;
    }
    public List<Goals__c> getFoodNutritionList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // EDU -> pull goal record
        SelectedFoodNutritionGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Food/Nutrition'
                         ORDER BY Id];
        return SelectedFoodNutritionGoals;
    }
    public List<Goals__c> getHousingShelterList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // EDU -> pull goal record
        SelectedHousingShelterGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Housing/Shelter'
                         ORDER BY Id];
        return SelectedHousingShelterGoals;
    }
    public List<Goals__c> getEmploymentList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // EDU -> pull goal record
        SelectedEmploymentGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Employment'
                         ORDER BY Id];
        return SelectedEmploymentGoals;
    }
    public List<Goals__c> getTransportationList()
    {
        CaseIdFromNeeds='50035000000cdVG'; // goal record Id
        // EDU -> pull goal record
        SelectedTransportationGoals = [Select Id, Title__c, Category__c, Sub_category__c, Start_Date__c, End_Date__c, Case__c
                         from Goals__c 
                         where Case__c=:CaseIdFromNeeds AND Category__c=:'Transportation'
                         ORDER BY Id];
        return SelectedTransportationGoals;
    }    
    
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('Hom','Home')); 
        options.add(new SelectOption('Email','Email')); return options; 
    }
       
    public PageReference SavePrimaryContact() {
        if(!String.isEmpty(CaseContactPrimaryId))
        {
            ErrorMsg = false;
            ContactDetails.HomePhone = PrimaryContactHome;
            ContactDetails.MobilePhone = PrimaryContactCell;
            arrTest = PrimaryContactName.split('\\ ');
             if(arrTest.size() != 2)
            {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Please enter a First and Last Name.');
                ApexPages.addMessage(msg);
                ErrorMsg = true;
            }
            if(!String.isEmpty(PrimaryContactEmail))
            {
                if(!Pattern.matches('^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$', PrimaryContactEmail))
                {
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Invalid Email Address');
                    ApexPages.addMessage(msg);
                    ErrorMsg = true;  
                }
            }
            if(ErrorMsg == true)
            {
                return null;
            }
             ContactDetails.Email = PrimaryContactEmail;
            if(!String.isEmpty(arrTest[0]))
            {
                ContactDetails.FirstName = arrTest[0];
            }
            if(!String.isEmpty(arrTest[1]))
            {
                ContactDetails.LastName = arrTest[1];
            }
            update ContactDetails;
            ContactName = ContactDetails.FirstName + ' ' + ContactDetails.LastName;
            if(Role == 'Primary Contact')
            {
                PrimaryContactUserList = [SELECT Id, UserName from User where ContactId = :AssessmentConId Limit 1];
                
                if(!PrimaryContactUserList.isEmpty())
                {
                    PrimaryContactUser = PrimaryContactUserList[0];
                    PrimaryContactUserName = PrimaryContactUser.UserName;
                }
                
                PrimaryContactName = ContactName;
                PrimaryFamilyName = ContactName;
                PrimaryContactHome = ContactDetails.HomePhone;
                PrimaryContactDetails = [SELECT Id, Age__C from Contact where Id = :AssessmentConId Limit 1];
                PrimaryContactAge = PrimaryContactDetails.Age__c;
                PrimaryContactCell = ContactDetails.MobilePhone;
                PrimaryContactEmail = ContactDetails.Email;   
            }
        }
        return null;
  }
    
    Public List<CaseContactRole> QuerySecondaryContacts()
    {
        List<CaseContactRole> SecondaryContactsList = [SELECT Id, CasesId, ContactId, Role, Contact.Name FROM CaseContactRole WHERE CasesId = :CaseNo and ContactId != :AssessmentConId];
        return SecondaryContactsList;
    }
    
    Date parseLongDate(String dateString){
        Map <String, Integer> months = new Map <String, Integer> {'january'=>1, 'febuary'=>2
            , 'march'=>3, 'april'=>4, 'may'=>5, 'june'=>6, 'july'=>7, 'august'=>8, 'september'=>9
            , 'october'=>10, 'november'=>11, 'december'=>12};
        List <String> dateParts = dateString.toLowerCase().replace(',','').split(' ');
        Integer month = months.get(dateParts[0]);
        Integer day = Integer.valueOf(dateParts[1]);
        Integer year = Integer.valueOf(dateParts[2]);
        Date parsedDate = Date.newInstance(year,month,day);
        return parsedDate;
    }
    
    //Needed for SuccessPlan
    Public Void QueryCase(){
        AssessmentConId = apexpages.currentpage().getparameters().get('AssessmentConId');
        if(!String.isEmpty(AssessmentConId))
        {
            CaseList = [select Id, CreatedDate, ContactId, LastModifiedDate, Status, Ownerid from case where ContactId=: AssessmentConId and Status != 'Completed' Limit 1];
            if(!CaseList.isEmpty())
            {
                Assessment = CaseList[0];
                CaseNo = Assessment.Id;
                PrimaryContactNo = Assessment.ContactId;
                StartDate = Assessment.CreatedDate;
                if(StartDate != null)
                {
                    StartDateFormat = StartDate.format('MMMMM dd, yyyy');
                }
                else
                {
                    StartDateFormat = null;
                }
                LastUpdate = Assessment.LastModifiedDate;
                if(LastUpdate != null)
                {
                    LastUpdateFormat = LastUpdate.format('MMMMM dd, yyyy');
                }
                else
                {
                    LastUpdateFormat = null;
                }
                Status = Assessment.Status;
                    
                CaseRole = [SELECT Id, CasesId, ContactId, Role FROM CaseContactRole WHERE CasesId = :CaseNo and ContactId = : AssessmentConId Limit 1];
                Role = CaseRole.Role;
                CaseContactPrimaryId = CaseRole.ContactId;
                    
                ContactDetails = [select Id, Age__C, Contact_Preference__c, Salutation, FirstName, LastName, Birthdate, HomePhone, MobilePhone, Email, Address__c, Social_Security_Number__c from Contact where Id=: AssessmentConId Limit 1];
                ContactSalutation = ContactDetails.Salutation;
                ContactName = ContactDetails.FirstName + ' ' + ContactDetails.LastName;
                BirthDate = ContactDetails.Birthdate;
                if(BirthDate != null)
                {
                    BirthDateFormat = BirthDate.format('MMMMM dd, yyyy');
                }
                else
                {
                    BirthDateFormat = null;
                }
                HomePhone = ContactDetails.HomePhone;
                MobilePhone = ContactDetails.MobilePhone;
                Email = ContactDetails.Email;
                Address = ContactDetails.Address__c;
                SSN = ContactDetails.Social_Security_Number__c;
                
                PrimaryContactUserList = [SELECT UserName from User where ContactId = :AssessmentConId Limit 1];
                
                if(!PrimaryContactUserList.isEmpty())
                {
                    PrimaryContactUser = PrimaryContactUserList[0];
                    PrimaryContactUserName = PrimaryContactUser.UserName;
                }
                
                PrimaryContactName = ContactName;
                PrimaryFamilyName = ContactName;
                PrimaryContactHome = HomePhone;
                PrimaryContactAge = ContactDetails.Age__c;
                PrimaryContactCell = MobilePhone;
                PrimaryContactEmail = Email;
                PrimaryContactPreference = ContactDetails.Contact_Preference__c;
                
                SecondaryContacts = QuerySecondaryContacts();
            }
            else
            {
                ContactDetails = [select Id, Age__C, Contact_Preference__c, Salutation, FirstName, LastName, Birthdate, HomePhone, MobilePhone, Email, Address__c, Social_Security_Number__c from Contact where Id=: AssessmentConId Limit 1];
                
                ContactSalutation = ContactDetails.Salutation;
                ContactName = ContactDetails.FirstName + ' ' + ContactDetails.LastName;
                BirthDate = ContactDetails.Birthdate;
                if(BirthDate != null)
                {
                    BirthDateFormat = BirthDate.format('MMMMM dd, yyyy');
                }
                else
                {
                    BirthDateFormat = null;
                }
                HomePhone = ContactDetails.HomePhone;
                MobilePhone = ContactDetails.MobilePhone;
                Email = ContactDetails.Email;
                Address = ContactDetails.Address__c;
                SSN = ContactDetails.Social_Security_Number__c;
                
                PrimaryContactUserList = [SELECT UserName from User where ContactId = :AssessmentConId Limit 1];
                
                if(!PrimaryContactUserList.isEmpty())
                {
                    PrimaryContactUser = PrimaryContactUserList[0];
                    PrimaryContactUserName = PrimaryContactUser.UserName;
                }
                
                PrimaryContactName = ContactName;
                PrimaryFamilyName = ContactName;
                PrimaryContactHome = HomePhone;
                PrimaryContactAge = ContactDetails.Age__c;
                PrimaryContactCell = MobilePhone;
                PrimaryContactEmail = Email;
                PrimaryContactPreference = ContactDetails.Contact_Preference__c;
                
                Assessment = new Case();
                Assessment.Subject = ContactName;
                Assessment.Ownerid = CaseUser.Id;
                Assessment.ContactId = AssessmentConId;
                Assessment.Origin = 'Email';
                Assessment.Priority = 'Medium';
                insert Assessment;
                
                CaseNo = Assessment.Id;
                
                CaseList = [select Id, CreatedDate, ContactId, LastModifiedDate, Status, Ownerid from case where Id=: CaseNo Limit 1];
            
                if(!CaseList.isEmpty())
                {
                    Assessment = CaseList[0];
                    PrimaryContactNo = Assessment.ContactId;
                    StartDate = Assessment.CreatedDate;
                    if(StartDate != null)
                    {
                        StartDateFormat = StartDate.format('MMMMM dd, yyyy');
                    }
                    else
                    {
                        StartDateFormat = null;
                    }
                    LastUpdate = Assessment.LastModifiedDate;
                    if(LastUpdate != null)
                    {
                        LastUpdateFormat = LastUpdate.format('MMMMM dd, yyyy');
                    }
                    else
                    {
                       LastUpdateFormat = null; 
                    }
                    Status = Assessment.Status;
                }
                
                CaseRole = new CaseContactRole();
                CaseRole.CasesId = CaseNo;
                CaseRole.ContactId = PrimaryContactNo;
                CaseRole.Role = 'Primary Contact';
                insert CaseRole;
                
                Role = CaseRole.Role;
                CaseContactPrimaryId = CaseRole.ContactId;
            }
        }
        else
        {
            Assessment = new Case();
            Assessment.Ownerid = CaseUser.Id;
            Assessment.Origin = 'Email';
            Assessment.Priority = 'Medium';
            insert Assessment;
            
            CaseNo = Assessment.Id;
            
            CaseList = [select Id, CreatedDate, ContactId, LastModifiedDate, Status, Ownerid from case where Id=: CaseNo Limit 1];
            
            if(!CaseList.isEmpty())
            {
                Assessment = CaseList[0];
                StartDate = Assessment.CreatedDate;
                if(StartDate != null)
                {
                    StartDateFormat = StartDate.format('MMMMM dd, yyyy');
                }
                else
                {
                    StartDateFormat = null;
                }
                LastUpdate = Assessment.LastModifiedDate;
                if(LastUpdate != null)
                {
                    LastUpdateFormat = LastUpdate.format('MMMMM dd, yyyy');
                }
                else
                {
                    LastUpdateFormat = null;
                }
                Status = Assessment.Status;
            }
        }
    }
    //Needed for SuccessPlan
    public PageReference saveEvent() {
    try{
        evStartDate = DateTime.newInstance(evDate, startTime);
        evEndDate = evStartDate.addHours(1);
        Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].Id;
        if(evStartDate == null  || evEndDate == null || obj.subject__c == null || obj.subject__c == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please fill all the required fields'));
            return null;
        }
        if(evStartDate < system.now()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Event start date can not be in the past'));
            return null;
        }
        if(evStartDate >= evEndDate){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Event end date should be greater than the start date'));
            return null;
        }
        //AddNewEventVar.Appointment_Type__c = AddNewEventVar.Type;
        obj.RecordTypeId = EvRecTypeId ;
        obj.start_date__c= evStartDate;
        obj.End_date__c = evEndDate;
        insert obj;
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info, 'An Appointment has been Scheduled');
        ApexPages.addMessage(msg);
        return null;
    }
        catch(Exception e) 
        {
            system.debug('------exception-------'+e);
           ApexPages.addMessages(e) ; 
           return null;
        }
    return null;
    }
}