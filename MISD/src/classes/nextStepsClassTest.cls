@isTest
public class nextStepsClassTest {

 @IsTest(SeeAllData=true)
 public static void test1(){
     
        Account acc = [select id from Account where name = 'MIISD'];  
       
        Contact con = new Contact(FirstName = 'Test', LastName = 'Test',AccountId = acc.id , Martial_Status__c = 'Married');
        insert con;
     
        Case cs = new Case(Status= 'New', ContactId = con.id , Subject = 'TestCase');
        insert cs;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Success Coach Profile'];
        User u = new User(Alias = 'test', Email='clc@test.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='clc@test.com', ContactId = con.id);
        insert u;
     system.runAs(u){
         
         nextStepsClass ns = new nextStepsClass(con.id);
         ns.dateString = '12/12/2016';
         ns.timeString = '12:00 AM';
         ns.subject = 'Trial';
         ns.segment ='2';
         ns.save();
         ns.getDay();
         ns.getServiceList();
     }
     system.runAs(u){
         
         nextStepsClass ns = new nextStepsClass(con.id);
         ns.dateString = '12/12/2016';
         ns.timeString = '12:00 AM';
         ns.subject = '';
         ns.segment ='TBD';
         ns.save();
         ns.getDay();
         ns.getServiceList();
     }
   }
}