public with sharing class MI_ActivityCalendarController {

   

    private ApexPages.StandardController controller;
    public MI_Activity__c obj {get; set; }
    
    //BugslstCalLegend
    public Date EditStartDate {get;set;}
    public String EditStartTime {get;set;}
    public Date EditEndDate {get;set;}
    public String EditEndTime {get;set;}
    public String AppStartDate {get;set;}
    
     //BugsUserList
    public list<MI_Activity__c> UserList {get;set;}
    
    public String InitialBlockTime {get;set;}
    public DateTime InitialSD {get;set;}
    public DateTime InitialED {get;set;}
    
    
    public MI_ActivityCalendarController (ApexPages.StandardController controller) {
        this.controller = controller;
        if(!Test.isRunningTest()) {
            this.controller.addFields(new list<String>{'Parent_Appointment__c','Subject__c', 'Start_Date__c','End_Date__c', 'Appointment_Type__c', 'Block_Time__c','RecordTypeId'});
        }
        obj = (MI_Activity__c)controller.getRecord();
        
        //Bugs
        EditStartDate = Date.Today();
        //EditStartTime = Time.newInstance(18, 30, 2, 20);
        EditStartTime = null;
        EditEndDate = Date.Today();
        //EditEndTime = Time.newInstance(18, 30, 2, 20);
        EditEndTime = null;
        
        Id recid = null;
        recid = controller.getId();
        MI_Activity__c  CalendarDate =  [select id, Subject__c, Start_Date__c, End_Date__c, Appointment_Type__c,RecordType.Name, Block_Time__c 
                                          FROM MI_Activity__c WHERE Id = :recId];
                                          
            system.debug('Calendar Date:'+CalendarDate); 
            system.debug('Calendar starDate:'+CalendarDate.Start_Date__c.Date());  
            system.debug('Calendar starttime:'+CalendarDate.Start_Date__c.Time());  
                                         
            EditStartDate = CalendarDate.Start_Date__c.Date();
            EditStartTime = CalendarDate.Start_Date__c.format('h:mm a');
            EditEndDate = CalendarDate.End_Date__c.Date();
            EditEndTime = CalendarDate.End_Date__c.format('h:mm a');
            evSelectBlockTime = InitialBlockTime = CalendarDate.Block_Time__c;
            InitialSD = CalendarDate.Start_Date__c;
            InitialED = CalendarDate.End_Date__c;
            
            if(EditStartDate != null){
                String StrBirthDate = string.valueOfGmt(EditStartDate);
                String[] str = StrBirthDate.split(' ');
                String[] dts = str[0].split('-');
                Integer year = Integer.valueOf(dts[0]);
                Integer month = Integer.valueOf(dts[1]);
                Integer day = Integer.valueOf(dts[2]);
                AppStartDate = month+'/'+day+'/'+year;
        }
    
        availableUsers  = new List<SelectOption> ();
        selectedUsers = new List<SelectOption> ();
        if(obj!= null){
            system.debug('-------obj-------'+obj);
            List<Event_Attendee__c> evRelationList = [select event__c, Attendee__c from Event_Attendee__c where Event__c = :obj.Id];
            set<Id> UserIds = new set<Id> ();
            for(Event_Attendee__c evRel : evRelationList ){
                UserIds.add(evRel.Attendee__c);
            }
            List<User> usList = [select id, Name from User where IsActive = true and Id IN:UserIds  Order By Name ASC];
            for(User us : usList ){
                selectedUsers.add(new SelectOption(us.id, us.name));
            }
        }
    }

    //Property to hold list of Calendar Events
    public list<MI_Calendar> lstCalEvent{get;set;}
    public  list<MI_CalendarLegend> lstCalLegend{get;set;} 
    public MI_Activity__c AddNewEventVar {get; set; }
    public DateTime evStartDate {get; set;}
    public DateTime evEndDate {get; set;}
    public String  evSelectBlockTime {get; set;}
    
    public List<SelectOption> options {get; set; }
    public contact con {get;set;}
     public List<SelectOption> availableUsers {get;set;}
    public List<SelectOption> selectedUsers {get;set;}
    public string nameSearch {get;set;}
    public MI_Activity__c travelEvnt {get; set; }
    public boolean bCreateAppt {get;set;}
    //DateTime variable
    Public Date evDate {get;set;}
    public String startTime {get;set;}
    public String endTime {get;set;}
    
    public void generateList(){
      //BugsUserList
        Date next30 = Date.today().addDays(30); 
        String[] tempid = new String[]{}; 
        for(Event_Attendee__c eatt:[Select id, Event__r.id From Event_Attendee__c WHERE Attendee__c = :userinfo.getUserId() AND Event__r.Start_Date__C >= :Date.today() ORDER BY Event__r.Start_Date__c asc])
        {
            tempid.add(eatt.Event__r.id);
        } 
        if(tempid.size()>0)
        {
            UserList = [select id, Subject__c, Start_Date__c, End_Date__c, Appointment_Type__c,RecordType.Name FROM MI_Activity__c WHERE
                        ((Owner.Id=:UserInfo.getUserId() AND Start_Date__C >= :Date.today() AND RecordType.Name != 'Task') OR (id In :tempid))
                        ORDER BY Start_Date__c asc LIMIT 5];
        } 
        else
        {
            UserList = [select id, Subject__c, Start_Date__c, End_Date__c, Appointment_Type__c,RecordType.Name 
                    FROM MI_Activity__c 
                    WHERE
                    (Owner.Id=:UserInfo.getUserId() AND Start_Date__C >= :Date.today() AND RecordType.Name != 'Task')
                    ORDER BY Start_Date__c asc LIMIT 5];
        }
    }
    
    public MI_ActivityCalendarController () {
        generateList();        
        //List of Calendar Events

        AddNewEventVar = new MI_Activity__c();
        con = new Contact();
        options = new List<SelectOption>();
        options = getBlockTimeOptions();
        bCreateAppt = false;
        availableUsers  = new List<SelectOption> ();
        selectedUsers = new List<SelectOption> ();
        if(obj!= null){
            system.debug('-------obj-------'+obj);
            List<Event_Attendee__c> evRelationList = [select event__c, Attendee__c from Event_Attendee__c where Event__c = :obj.Id];
            set<Id> UserIds = new set<Id> ();
            for(Event_Attendee__c evRel : evRelationList ){
                UserIds.add(evRel.Attendee__c);
            }
            List<User> usList = [select id, Name from User where IsActive = true and Id IN:UserIds  Order By Name ASC];
            for(User us : usList ){
                availableUsers.add(new SelectOption(us.id, us.name));
            }
        }
        
        lstCalEvent = new list<MI_Calendar>();
        MI_Calendar oCalEvent;
        for(MI_Activity__c Ev : [SELECT id, Subject__c, Start_Date__c, End_Date__c, Appointment_Type__c, RecordType.Name, BulletinId__c  
                                    FROM MI_Activity__c 
                                    WHERE OwnerId = :userinfo.getUserId() AND (RecordType.Name = 'Event' OR RecordType.Name = 'Task' ) AND Repeat__c = false]){
            //Create a Calendar Event object for each event
            //VS:21/12:Avoid showing Tasks details on the calendar
            if(Ev.RecordType.Name == 'Event')
            {
                oCalEvent = new MI_Calendar();
                oCalEvent.title = Ev.Subject__c;
                oCalEvent.allDay = false;              
                oCalEvent.startDate = String.ValueOf(Ev.Start_Date__c);
                oCalEvent.endDate = String.ValueOf(Ev.End_Date__c); 
                if(Ev.RecordType.Name == 'Task'){
                    oCalEvent.url = '/apex/MI_TaskDetails?id=' + Ev.Id + '&retUrl='+ Apexpages.currentPage().getUrl();
                }else{
                    if(!Ev.Subject__c.startsWith('Transit'))
                        oCalEvent.url = '/apex/EditViewAppointments?id=' + Ev.Id ;
                    else
                        oCalEvent.url = 'javascript:void(0);';
                }    
                oCalEvent.textColor = 'white';
                if(Ev.Appointment_Type__c == 'New Family' ){
                    //oCalEvent.textColor = 'Red';
                    oCalEvent.backgroundColor = '#F88A62';
                }
                else if(Ev.Appointment_Type__c == 'Family Check In' ){
                    //oCalEvent.textColor = 'Green';
                    oCalEvent.backgroundColor = '#B070E6';
                }
                else if(Ev.BulletinId__c != '')
                {
                    oCalEvent.backgroundColor = '#FE8F60';
                }
                else{
                    oCalEvent.backgroundColor = '#4BC076';
                }
                //oCalEvent.backgroundColor = 'Yellow';
                lstCalEvent.add(oCalEvent);
             }
        }
        
        for(MI_Activity__c Ev : [SELECT id, Subject__c, Start_Date__c, End_Date__c, Appointment_Type__c, RecordType.Name, BulletinId__c,Number_of_Occurences__c, Repeat_Options__c  
                                    FROM MI_Activity__c 
                                    WHERE OwnerId = :userinfo.getUserId() AND RecordType.Name = 'Event' AND Repeat__c = true]){
            if(Ev.Repeat_Options__c == 'Daily'){
                Integer count = 0;
                for(Integer i = 0; count < Ev.Number_of_Occurences__c; i++){
                    DateTime myDateTime = (DateTime) Ev.Start_Date__c.addDays(i);
                    String dayOfWeek = myDateTime.format('E');
                    if(dayOfWeek != 'Sun' && dayOfWeek != 'Sat'){
                        oCalEvent = new MI_Calendar();
                        oCalEvent.title = Ev.Subject__c;
                        oCalEvent.allDay = false; 
                        oCalEvent.startDate = String.ValueOf(Ev.Start_Date__c.addDays(i));
                        oCalEvent.endDate = String.ValueOf(Ev.End_Date__c.addDays(i));
                        oCalEvent.url = '/apex/EditViewAppointments?id=' + Ev.Id;
                        oCalEvent.backgroundColor = '#4BC076';
                        oCalEvent.textColor = 'white';                      
                        lstCalEvent.add(oCalEvent);
                        count++;                        
                    }
                }               
            }
            
            if(Ev.Repeat_Options__c == 'Weekly'){
                Integer daysToAdd = 0;
                for(Integer i = 0; i < Ev.Number_of_Occurences__c; i++){
                    oCalEvent = new MI_Calendar();
                    oCalEvent.title = Ev.Subject__c;
                    oCalEvent.allDay = false; 
                    oCalEvent.startDate = String.ValueOf(Ev.Start_Date__c.addDays(daysToAdd));
                    oCalEvent.endDate = String.ValueOf(Ev.End_Date__c.addDays(daysToAdd));
                    oCalEvent.url = '/apex/EditViewAppointments?id=' + Ev.Id;
                    oCalEvent.backgroundColor = '#4BC076';
                    oCalEvent.textColor = 'white';
                    lstCalEvent.add(oCalEvent);
                    daysToAdd = daysToAdd + 7;
                }               
            }
            if(Ev.Repeat_Options__c == 'BiMonthly'){
                Integer daysToAdd = 0;
                for(Integer i = 0; i < Ev.Number_of_Occurences__c; i++){
                    oCalEvent = new MI_Calendar();
                    oCalEvent.title = Ev.Subject__c;
                    oCalEvent.allDay = false; 
                    oCalEvent.startDate = String.ValueOf(Ev.Start_Date__c.addDays(daysToAdd));
                    oCalEvent.endDate = String.ValueOf(Ev.End_Date__c.addDays(daysToAdd));
                    oCalEvent.url = '/apex/EditViewAppointments?id=' + Ev.Id;
                    oCalEvent.backgroundColor = '#4BC076';
                    oCalEvent.textColor = 'white';
                    lstCalEvent.add(oCalEvent);
                    daysToAdd = daysToAdd + 14;
                }               
            }           
            if(Ev.Repeat_Options__c == 'Monthly'){
                for(Integer i = 0; i < Ev.Number_of_Occurences__c; i++){
                    DateTime myDateTime = (DateTime) Ev.Start_Date__c.addMonths(i);
                    String dayOfWeek = myDateTime.format('E');
                    if(dayOfWeek != 'Sun' && dayOfWeek != 'Sat'){
                        oCalEvent = new MI_Calendar();
                        oCalEvent.title = Ev.Subject__c;
                        oCalEvent.allDay = false; 
                        oCalEvent.startDate = String.ValueOf(Ev.Start_Date__c.addMonths(i));
                        oCalEvent.endDate = String.ValueOf(Ev.End_Date__c.addMonths(i));
                        oCalEvent.url = '/apex/EditViewAppointments?id=' + Ev.Id;
                        oCalEvent.backgroundColor = '#4BC076';
                        oCalEvent.textColor = 'white';
                        lstCalEvent.add(oCalEvent);
                    }else if(dayOfWeek == 'Sun'){
                        oCalEvent = new MI_Calendar();
                        oCalEvent.title = Ev.Subject__c;
                        oCalEvent.allDay = false; 
                        oCalEvent.startDate = String.ValueOf(Ev.Start_Date__c.addMonths(i).addDays(1));
                        oCalEvent.endDate = String.ValueOf(Ev.End_Date__c.addMonths(i).addDays(1));
                        oCalEvent.url = '/apex/EditViewAppointments?id=' + Ev.Id;
                        oCalEvent.backgroundColor = '#4BC076';
                        oCalEvent.textColor = 'white';
                        lstCalEvent.add(oCalEvent);
                    }else if(dayOfWeek == 'Sat'){
                        oCalEvent = new MI_Calendar();
                        oCalEvent.title = Ev.Subject__c;
                        oCalEvent.allDay = false; 
                        oCalEvent.startDate = String.ValueOf(Ev.Start_Date__c.addMonths(i).addDays(2));
                        oCalEvent.endDate = String.ValueOf(Ev.End_Date__c.addMonths(i).addDays(2));
                        oCalEvent.url = '/apex/EditViewAppointments?id=' + Ev.Id;
                        oCalEvent.backgroundColor = '#4BC076';
                        oCalEvent.textColor = 'white';
                        lstCalEvent.add(oCalEvent);
                    }
                }               
            }
        }
        
        for(Holiday hd : [Select StartTimeInMinutes, RecurrenceStartDate, RecurrenceEndDateOnly, Name, ActivityDate From Holiday LIMIT :Limits.getLimitDmlRows()]){
            //Create a Calendar Event object for Holidays
            oCalEvent = new MI_Calendar();
            oCalEvent.title =hd.Name;
            oCalEvent.allDay = true;              
            oCalEvent.startDate = String.ValueOf(hd.ActivityDate);
            oCalEvent.endDate = String.ValueOf(hd.ActivityDate); 
            oCalEvent.textColor = 'Red';
            oCalEvent.backgroundColor = 'Yellow';
            lstCalEvent.add(oCalEvent);
        }
        for(Event_Attendee__c ev: [Select id, Event__r.subject__c, Event__r.Start_Date__c, Event__r.End_Date__c, Event__r.id, Event__r.Appointment_Type__c 
                                        From Event_Attendee__c 
                                        WHERE Attendee__c = :userinfo.getUserId() AND Event__r.BulletinId__c = '']){
            oCalEvent = new MI_Calendar();    
            oCalEvent.title = ev.Event__r.subject__c;
            oCalEvent.allDay = false;
            oCalEvent.startDate = String.ValueOf(Ev.Event__r.Start_Date__c);
            oCalEvent.endDate = String.ValueOf(Ev.Event__r.End_Date__c); 
            oCalEvent.url = '/apex/EditViewAppointments?id=' + Ev.Event__r.Id;
            oCalEvent.textColor = 'white';
            if(Ev.Event__r.Appointment_Type__c == 'New Family' ){
                oCalEvent.backgroundColor = '#F88A62';
            }
            else if(Ev.Event__r.Appointment_Type__c == 'Family Check In' ){
                oCalEvent.backgroundColor = '#B070E6';
            }
            else{
                oCalEvent.backgroundColor = '#4BC076';
            }
            lstCalEvent.add(oCalEvent); 
        }       
    }
    
    
    
    public void searchUser(){
        System.debug('@Hemanth: value');
        //VS : TKT-000324 Added defensive coding to make sure blank search does not list of available users
       
        if(nameSearch != null && nameSearch != '')
        {
                availableUsers  = new List<SelectOption> ();
                System.debug('Viral : Still firing for blank');
                /*string queryStr = 'select id, name from User where IsActive = true AND Name like \'%'+ string.escapeSingleQuotes(nameSearch)+ '%\' Order by Name ASC';
                List<User> userList = Database.query(queryStr);*/
            	string nameString = '%' + nameSearch + '%' ;
            	List<User> userList = [select id, name from User where IsActive = true AND Name like : nameString Order by Name ASC];
                
                for(User u: userList){
                    if(u.name != 'Chatter Expert' && u.name != 'State of Michigan Site Guest User' && u.name != 'Salesforce Administrator')
                    {
                        String vListofSelUser = '';
                        for(SelectOption option : selectedUsers)
                        {
                           vListofSelUser = vListofSelUser + option.getLabel() + '|';
                        }
                        if(!vListofSelUser.contains(u.name))
                        availableUsers.add(new SelectOption(u.id, u.name));
                    }
                    
            }
        }
        else
        {
            availableUsers  = new List<SelectOption> ();
                
                string queryStr = 'select id, name from User where IsActive = true AND Name like \'Fifijaskjdnk\' Order by Name ASC';
                List<User> userList = Database.query(queryStr);
                               
        }
        
        System.debug('@Hemanth: ' + availableUsers);
        
    }
    
    public void CreateAppointment(){
        bCreateAppt = true;
        system.debug('-------bCreateAppt ----'+bCreateAppt );
    }
    
    public PageReference save() { 
    try{        
        if(AppStartDate== null || AppStartDate == '' || startTime == null  || startTime == '' || endTime == null || endTime == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please fill all the required fields'));
            return null;
        }
        evDate = parseLongDate(AppStartDate);
        evStartDate = DateTime.parse(AppStartDate+' '+startTime);
        evEndDate = DateTime.parse(AppStartDate+' '+endTime);
        //evStartDate = DateTime.newInstance(evDate, startTime);
        //evEndDate = DateTime.newInstance(evDate, EndTime);
        
        Id EvRecTypeId = [select id from RecordType where DeveloperName='Event' and sObjectType='MI_Activity__c'].Id;
        
        if(evStartDate == null  || evEndDate == null || AddNewEventVar.subject__c == null || AddNewEventVar.subject__c == ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please fill all the required fields'));
            return null;
        }
        if(evStartDate < system.now()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Event start date/time cannot be in the past'));
            return null;
        }
        if(evStartDate >= evEndDate){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Event end time should be greater than the start time'));
            return null;
        }
        if(selectedUsers.size() == 0 ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please select at least one attendee.'));
            return null;    
        }       
        
        AddNewEventVar.RecordTypeId = EvRecTypeId ;
        AddNewEventVar.start_date__c= evStartDate;
        AddNewEventVar.End_date__c = evEndDate;
        AddNewEventVar.Block_Time__c = evSelectBlockTime;
        insert AddNewEventVar;
        System.debug('@Rameez: value of Block Time: ' + evSelectBlockTime );
        
            if (evSelectBlockTime != null && evSelectBlockTime != '') {
    
                // Auto-add travel time to schedule based on meeting location
                travelEvnt = new MI_Activity__c ();
                travelEvnt.subject__c = 'Transit- ' + AddNewEventVar.subject__c;
                travelEvnt.description__c = '';
                travelEvnt.RecordTypeId = EvRecTypeId ;
                travelEvnt.Parent_Appointment__c = AddNewEventVar.id ;
                if(evSelectBlockTime == '30 min'){
                    travelEvnt.start_date__c= AddNewEventVar.start_date__c.addMinutes(-30);
                }else if (evSelectBlockTime == '60 min'){
                    travelEvnt.start_date__c= AddNewEventVar.start_date__c.addHours(-1);
                }
                travelEvnt.End_date__c = AddNewEventVar.start_date__c;
                insert travelEvnt;
            }
            List<Event_Attendee__c> evRelationList = new List<Event_Attendee__c> ();
            Event_Attendee__c evRel;
            for(SelectOption option : selectedUsers){
                evRel = new Event_Attendee__c();
                evRel.Event__c = AddNewEventVar.Id;
                evRel.Attendee__c = option.getValue();
                evRelationList.add(evRel);
                if (evSelectBlockTime != null) {
                    evRel = new Event_Attendee__c();
                    evRel.Event__c = travelEvnt.Id;
                    evRel.Attendee__c = option.getValue();
                    evRelationList.add(evRel);
                }
            }
            if(evRelationList!= null && evRelationList.size()> 0){
                insert evRelationList ;
            }
        }
        catch(Exception e) 
        {
            system.debug('------exception-------'+e + '-- ' + e.getLineNumber());
           ApexPages.addMessages(e) ; 
           return null;
        }
        
        generateList();
        
        //PageReference pagenav = Page.MI_SuccessCoachDashboard;
        PageReference pagenav = Page.CommunitiesLanding;
          pagenav.setRedirect(true);
          return pagenav;
          
          
        
    }
    
    public PageReference SavePage() {       
        
        if(obj.Subject__c == '' || AppStartDate == null || EditStartTime == null || EditEndTime == null ){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please fill all required fields'));
            return null;
        }
        EditStartDate = parseLongDate(AppStartDate);
        system.debug('\n\n EditStartDate - ' + EditStartDate);
        //obj.start_date__c = DateTime.newInstance(EditStartDate, EditStartTime);
        obj.start_date__c = Datetime.parse(AppStartDate+' '+EditStartTime); 
        //obj.End_date__c = DateTime.newInstance(EditStartDate, EditEndTime);
        obj.End_date__c = Datetime.parse(AppStartDate+' '+EditEndTime); 
        obj.Block_Time__c = evSelectBlockTime;
        
        if(obj.start_date__c < system.now()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Event Start Date cannot be in the past'));
            return null;
        }
        if(obj.start_date__c >= obj.End_date__c){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Event End time should be greater than the Start time'));
            return null;
        }
        if(obj.start_date__c >= system.today().addYears(1)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Invalid Start Date'));
            return null;
        }
        controller.save(); // This takes care of the details for you.       
        
        
        if(InitialBlockTime != evSelectBlockTime || InitialSD != obj.start_date__c || InitialED != obj.End_Date__c){// This block is added to track the changes in blocking travel time
            List<MI_Activity__c> blockCalEvent = [SELECT id,start_date__c,End_date__c FROM MI_Activity__c WHERE Parent_Appointment__c = : obj.id];
            if(!blockCalEvent.isEmpty()){
                if(evSelectBlockTime == '30 min'){
                    blockCalEvent[0].start_date__c = obj.start_date__c.addMinutes(-30);
                }else if(evSelectBlockTime == '60 min'){
                    blockCalEvent[0].start_date__c = obj.start_date__c.addHours(-1);
                }
                blockCalEvent[0].End_date__c = obj.start_date__c;
                update blockCalEvent;
            }else{
                if(evSelectBlockTime != '' && evSelectBlockTime!= null)
                {
                     MI_Activity__c travelEvnt = new MI_Activity__c ();
                    travelEvnt.subject__c = 'Transit- ' + obj.subject__c;
                    travelEvnt.description__c = '';
                    travelEvnt.RecordTypeId = obj.RecordTypeId ;
                    travelEvnt.Parent_Appointment__c = obj.id ;
                    if(evSelectBlockTime == '30 min'){
                        travelEvnt.start_date__c = obj.start_date__c.addMinutes(-30);
                    }else if (evSelectBlockTime == '60 min'){
                        travelEvnt.start_date__c = obj.start_date__c.addHours(-1);
                    }
                    travelEvnt.End_date__c = obj.start_date__c;
                    insert travelEvnt;
                }
            }           
        }
        List<Event_Attendee__c> existingEvRelationList = [select id from Event_Attendee__c where Event__c = :obj.Id];
        if(existingEvRelationList != null){
            delete existingEvRelationList;
        }
        
        List<Event_Attendee__c> evRelationList = new List<Event_Attendee__c> ();
        Event_Attendee__c evRel;
        for(SelectOption option : selectedUsers){
            evRel = new Event_Attendee__c();
            evRel.Event__c = obj.Id;
            evRel.Attendee__c = option.getValue();
            evRelationList.add(evRel);
        }
        if(evRelationList != null && evRelationList.size() > 0 ){
            insert evRelationList ;
        }
        
        generateList();
        
        PageReference congratsPage = Page.MI_OpenCalendarView;
        congratsPage.setRedirect(true);
        return congratsPage;
    }
    
    public PageReference DeleteEvent() {
      //stdController.delete(); // This takes care of the details for you.
      if(obj != null){
        delete obj;
      }
      
      generateList();
      
      PageReference congratsPage = Page.MI_OpenCalendarView;
      congratsPage.setRedirect(true);
      return congratsPage; 
      return null;
    }
    
    
    public PageReference BacktoDashboardPage() {
      PageReference pagenav = Page.CommunitiesLanding;
          pagenav.setRedirect(true);
          return pagenav;
    }
     public void cancelevent() {
      bCreateAppt = false;  
      AddNewEventVar = new MI_Activity__c();
      AppStartDate = null;
      startTime = null;
      endTime = null;
      nameSearch = null;   
      evSelectBlockTime = null;
      selectedUsers = null;
      availableUsers = null;
    }
    public PageReference BacktoCalendarPage() {
      PageReference pagenav = Page.MI_OpenCalendarView;
          pagenav.setRedirect(true);
          return pagenav;
    }
    public PageReference testAction(){
        system.debug('\n\n TEST::> ');
        return null;
    }
    public List<SelectOption> getBlockTimeOptions(){
        List<SelectOption> options = new List<SelectOption>();
            
        Schema.DescribeFieldResult fieldResult = MI_Activity__c.Block_Time__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry f : ple){
          options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       
        return options;
    }
    
    private Date parseLongDate(String dateString){
        List <String> dateParts = dateString.split('/');
        Integer month = Integer.valueOf(dateParts[0]);
        Integer day = Integer.valueOf(dateParts[1]);
        Integer year = Integer.valueOf(dateParts[2]);
        Date parsedDate = Date.newInstance(year,month,day);
        return parsedDate;
    }
}