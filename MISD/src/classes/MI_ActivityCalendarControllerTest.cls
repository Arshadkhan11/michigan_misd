@isTest
public class MI_ActivityCalendarControllerTest {
	@isTest public static void generaltest(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test60', Email='acc60@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing60', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc60@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Appointment_Type__c = 'New Family',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today()+1,End_Date__c=Date.today()+1,Repeat__c = true,Repeat_Options__c = 'Daily',Number_of_Occurences__c=1);
            insert miact;
            MI_Activity__c miact1 = new MI_Activity__c(OwnerId = u.id, Subject__c = 'Test',Client_Name__c = 'Test',Appointment_Type__c = 'Family Check In',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1,Repeat__c = false);
            insert miact1;
            MI_Activity__c miact2 = new MI_Activity__c(OwnerId = u.id, Subject__c = 'Test',Client_Name__c = 'Test',Appointment_Type__c = 'New Family',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1,Repeat__c = false);
            insert miact2;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;
        	Holiday hol = new Holiday(Name='Test', ActivityDate = date.today());
        	insert hol;
        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController constructorTest = new MI_ActivityCalendarController();
        
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            MI_ActivityCalendarController miac = new MI_ActivityCalendarController();
        	miac.lstCalLegend = new list<MI_CalendarLegend>();
        	miac.nameSearch = 'Testing';
        	miac.evDate = date.today();
            miac.obj = miact;
            miac.CreateAppointment();
            miac.BacktoDashboardPage();
            miac.BacktoCalendarPage();
       		miac.searchUser();
        	miac.save();
            miac.DeleteEvent();
    	}	
    }
    @isTest public static void constructortest1(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test61', Email='acc61@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing61', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc61@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Appointment_Type__c = 'New Family',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today()+1,End_Date__c=Date.today()+1,Repeat__c = true,Repeat_Options__c = 'Weekly',Number_of_Occurences__c=2);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            MI_ActivityCalendarController miac = new MI_ActivityCalendarController();
    		}
    }
    @isTest public static void constructortest2(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test62', Email='acc62@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing62', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc62@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Appointment_Type__c = 'New Family',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1,Repeat__c = true,Repeat_Options__c = 'BiMonthly',Number_of_Occurences__c=3);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            MI_ActivityCalendarController miac = new MI_ActivityCalendarController();
    		}
        }
    @isTest public static void savetest1(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test63', Email='acc63@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing63', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc63@test.com',IsActive = true);
    System.runAs(u) {
           
            
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Appointment_Type__c = 'New Family',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1,Repeat__c = true,Repeat_Options__c = 'Monthly',Number_of_Occurences__c=4);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            MI_ActivityCalendarController miac = new MI_ActivityCalendarController();
        
            miac.evSelectBlockTime = '60 min';
            miac.AppStartDate = '06/20/2016';
        	miac.startTime = '12:34 PM';
        	miac.endTime = '01:34 PM';
            miaccon.AddNewEventVar = new MI_Activity__c();
        	miaccon.AddNewEventVar.subject__C = 'Test';
        	miac.save();
    		}
        System.runAs(u) {
            DateTime d =(DateTime) Date.today();
            String dayOfWeek = d.format('E');
            if(dayOfWeek == 'Sun')
                d = d.addDays(6);
            else if(dayOfWeek == 'Mon')
                d = d.addDays(5);
            else if(dayOfWeek == 'Tue')
                d = d.addDays(4);
            else if(dayOfWeek == 'Wed')
                d = d.addDays(3);
            else if(dayOfWeek == 'Thu')
                d = d.addDays(2);
            else if(dayOfWeek == 'Fri')
                d = d.addDays(1);
            else
               d = d.addDays(0);
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Appointment_Type__c = 'New Family',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = d,End_Date__c= d,Repeat__c = true,Repeat_Options__c = 'Monthly',Number_of_Occurences__c=4);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            MI_ActivityCalendarController miac = new MI_ActivityCalendarController();
        
            miac.evSelectBlockTime = '30 min';
            miac.AppStartDate = '06/20/2016';
        	miac.startTime = '12:34 PM';
        	miac.endTime = '01:34 PM';
            miaccon.AddNewEventVar = new MI_Activity__c();
        	miaccon.AddNewEventVar.subject__C = 'Test';
            Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id;
            miaccon.AddNewEventVar.RecordTypeId = EvRecTypeId ;
        	miaccon.AddNewEventVar.subject__C = 'test';
        	insert miaccon.AddNewEventVar;
        	miac.save();
    		}
        }
    
    @isTest public static void savetest2(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test64', Email='acc64@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing64', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc64@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
        	miaccon.AppStartDate = '06/16/2016';
        	miaccon.startTime = '12:34 PM';
        	miaccon.endTime = '01:34 PM';
            miaccon.evSelectBlockTime = '30 min';
       		miaccon.AddNewEventVar = new MI_Activity__c();
        	miaccon.AddNewEventVar.subject__C = null;
            Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id;
            miaccon.AddNewEventVar.RecordTypeId = EvRecTypeId ;
        	miaccon.AddNewEventVar.subject__C = 'test';
        	insert miaccon.AddNewEventVar;
        	miaccon.save();
    		}
        }
    @isTest public static void savetest3(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test65', Email='acc65@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing65', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc65@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            String s = string.valueOf(Date.today());
        	miaccon.AppStartDate = s;
        	miaccon.startTime = '02:34 PM';
        	miaccon.endTime = '01:34 PM';
            miaccon.evSelectBlockTime = '30 min';
       		miaccon.AddNewEventVar = new MI_Activity__c();
        	miaccon.AddNewEventVar.subject__C = 'Test';
            Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id;
            miaccon.AddNewEventVar.RecordTypeId = EvRecTypeId ;
        	miaccon.AddNewEventVar.subject__C = 'test';
        	insert miaccon.AddNewEventVar;
        	miaccon.save();
    		}
        }
    @isTest public static void savetest4(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test66', Email='acc66@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing66', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc66@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
        	String s = string.valueOf(Date.today());
            miaccon.AppStartDate = s;
        	miaccon.startTime = '12:34 PM';
        	miaccon.endTime = '10:34 PM';
       		miaccon.AddNewEventVar = new MI_Activity__c();
        	miaccon.AddNewEventVar.subject__C = 'Test';
        	miaccon.evSelectBlockTime = '60 min';
            Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id;
            miaccon.AddNewEventVar.RecordTypeId = EvRecTypeId ;
        	miaccon.AddNewEventVar.subject__C = 'test';
        	insert miaccon.AddNewEventVar;
        	miaccon.save();
    		}
        }
    @isTest public static void savetest5(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test67', Email='acc67@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing67', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc67@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
        	String s = string.valueOf(Date.today());
            miaccon.AppStartDate = s;
        	miaccon.startTime = '12:34 PM';
        	miaccon.endTime = '01:34 PM';
       		miaccon.AddNewEventVar = new MI_Activity__c();
        	miaccon.AddNewEventVar.subject__C = 'Test';
        	miaccon.evSelectBlockTime = '60 min';
            Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id;
            miaccon.AddNewEventVar.RecordTypeId = EvRecTypeId ;
        	miaccon.AddNewEventVar.subject__C = 'test';
        	insert miaccon.AddNewEventVar;
        	miaccon.save();
    		}
        }
    @isTest public static void savetest6(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test68', Email='acc68@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing68', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc68@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1);
            insert miact;
            
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
        	String s = string.valueOf(Date.today());
            miaccon.AppStartDate = s;
        	miaccon.startTime = '01:34 PM';
        	miaccon.endTime = '02:34 PM';
            miaccon.evSelectBlockTime = '30 min';
        	miaccon.AddNewEventVar = new MI_Activity__c();
       		Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id;
            miaccon.AddNewEventVar.RecordTypeId = EvRecTypeId ;
        	miaccon.AddNewEventVar.subject__C = 'test';
        	insert miaccon.AddNewEventVar;
        	miaccon.save();
    		}
        }
    @isTest public static void savetest7(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test68', Email='acc68@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing68', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc68@test.com',IsActive = true);
         
    System.runAs(u) {
            
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()+1);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact; 
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController();
            
            miaccon.AppStartDate = '06/15/2016';
            miaccon.startTime = '04:34 PM';
        	miaccon.endTime = '05:34 PM';
            miaccon.evSelectBlockTime = '30 min';
       		miaccon.AddNewEventVar = new MI_Activity__c();
            Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id;
            miaccon.AddNewEventVar.RecordTypeId = EvRecTypeId ;
        	miaccon.AddNewEventVar.subject__C = 'test';
        	insert miaccon.AddNewEventVar;
        	miaccon.save();
            miaccon.cancelevent();
    		}
        }
    
    
@isTest public static void savetest8(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test68', Email='acc68@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing68', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc68@test.com',IsActive = true);
         
    System.runAs(u) {
            
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id,Start_Date__c = Date.today(),End_Date__c=Date.today()-1);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact; 
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController();
            
            miaccon.AppStartDate = '06/15/2016';
            miaccon.startTime = '04:34 PM';
        	miaccon.endTime = '03:34 AM';
            miaccon.evSelectBlockTime = null;
       		miaccon.AddNewEventVar = new MI_Activity__c();
            //Id EvRecTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id;
            //miaccon.AddNewEventVar.RecordTypeId = EvRecTypeId ;
        	miaccon.AddNewEventVar.subject__C = 'test';
        	//insert miaccon.AddNewEventVar;
        	miaccon.save();
            miaccon.cancelevent();
        
        
    		}
        }    
    
    @isTest public static void savePage1(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test69', Email='acc69@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing69', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc69@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Start_Date__c = date.today(), End_Date__c =date.today(),RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id);
            insert miact;
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
        	miaccon.SavePage();
    		}
        }
    @isTest public static void savePage2(){
        
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
        User u = new User(Alias = 'test70', Email='acc70@test.com', 
            EmailEncodingKey='UTF-8', LastName='Testing70', LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', ProfileId = p.Id, 
            TimeZoneSidKey='America/Los_Angeles', UserName='acc70@test.com',IsActive = true);
    System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Start_Date__c = date.today()+1, End_Date__c =date.today()+1,RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id);
            insert miact;
            MI_Activity__c blockCalEvent = new MI_Activity__c(Start_Date__c = date.today()+1, End_Date__c =date.today()+1,subject__C = 'test', Parent_Appointment__c = miact.id);
            insert blockCalEvent; 
        
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            miaccon.AppStartDate = '06/20/2016';
            miaccon.EditStartTime = '12:30 PM';
            miaccon.EditEndTime = '02:30 PM';
            miaccon.evSelectBlockTime = '30 min';
        
        	miaccon.SavePage();
    		}
        System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Start_Date__c = date.today()+1, End_Date__c =date.today()+1,RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id);
            insert miact;
             MI_Activity__c blockCalEvent = new MI_Activity__c(Start_Date__c = date.today()+1, End_Date__c =date.today()+1,subject__C = 'test', Parent_Appointment__c = miact.id);
            insert blockCalEvent; 
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            miaccon.AppStartDate = '06/20/2016';
            miaccon.EditStartTime = '12:30 PM';
            miaccon.EditEndTime = '02:30 PM';
            miaccon.evSelectBlockTime = '60 min';
        
        	miaccon.SavePage();
    		}
        System.runAs(u) {
            MI_Activity__c miact = new MI_Activity__c(Subject__c = 'Test',Client_Name__c = 'Test',Start_Date__c = date.today()+1, End_Date__c =date.today()+1,RecordTypeId = [select id from RecordType where Name='Event' and sObjectType='MI_Activity__c'].id);
            insert miact;
            
            Event_Attendee__c eatt = new Event_Attendee__c(event__c = miact.id);
            insert eatt;
            eatt.Attendee__c = u.id;
            update eatt;
            miact.OwnerId = u.id;
            update miact;        
            PageReference pageRef = Page.MI_CalendarWidget;
            Test.setCurrentPage(pageRef);
            ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(miact);
            MI_ActivityCalendarController miaccon = new MI_ActivityCalendarController(sc);
            miaccon.AppStartDate = '06/20/2016';
            miaccon.EditStartTime = '12:30 PM';
            miaccon.EditEndTime = '02:30 PM';
            miaccon.evSelectBlockTime = '60 min';
        
        	miaccon.SavePage();
    		}
        }
}