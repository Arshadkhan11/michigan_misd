public class CitizenSuccessPlanCtrl_Reassessment{

    public String Re_CaseId {get;set;}
    public Case RecontactCase{get;set;}
    Public String ReProfileName {get;set;}
    Public Contact ReProfile {get;set;}

    public CitizenSuccessPlanCtrl_Reassessment(){
        Re_CaseId = ApexPages.currentPage().getParameters().get('SP_CaseId');
        if(Re_CaseId != null && Re_CaseId != ''){
            RecontactCase= [SELECT id, contact.Name, Active_Goal__c FROM Case WHERE id = : Re_CaseId];
            ReProfile = [select FirstName, LastName from Contact where Id = :RecontactCase.contact.Id]; 
            ReProfileName = ReProfile .FirstName + ' ' + ReProfile .LastName + ' Family';
        }
    }
   
    private void MarkStatusInactive(sObject obj) {
        if(obj!= null) {
            obj.put('Status__c','Inactive');
            update obj;
        }
    }
    
    @RemoteAction
      public static PageReference setupReferralServicesReass(String case_id, string conId, String vServices)
      {
          List<ReferralServices__c> refSerLst;
          ReferralServices__c refSer;
          List<Referral__c> refLst;
          Referral__c ref;
          String caseid = case_id.subString(0,15);
    
          refSerLst = [Select Id, Referral__c, Services__c from ReferralServices__c where Referral__r.CaseId__c = :caseid];                
          if(refSerLst.size() > 0) {
              delete refSerLst;
          }
		  List<ReferralServices__c> refsrvlst = new List<ReferralServices__c>();       
          List<SP_Transactional__c> LstSer = [SELECT Id, CaseContactName__c, Name__c, Type__c, CaseId__c, PartnerAssociated__c, Goal__c,Status__c FROM SP_Transactional__c where Status__c = true and CaseId__c=:caseid and SuccessPlanStatus__c='Active' and (Type__C = 'Service' or Type__C = 'Resource') and (PartnerAssociated__c != null) ];
          refLst = [Select Id, Primary_Contact__c, Referral_Date__c, Referral_Source__c, PartnerAssociated__c, Case__c from Referral__c where  Case__c = :caseid];
          Map<Id,Referral__c> refmap  = new Map<Id,Referral__c>();
          for(Referral__c tmpref : refLst){
              if(!refmap.containsKey(tmpref.PartnerAssociated__c)){
                  refmap.put(tmpref.PartnerAssociated__c, tmpref);
              }
          }
          for (SP_Transactional__c s: LstSer) {
               if(!refmap.containsKey(s.PartnerAssociated__c)) {
                   ref = new Referral__c();
                   ref.Primary_Contact__c = s.CaseContactName__c;
                   ref.Referral_Date__c = System.Today();
                   ref.Referral_Source__c = 'Success Plan';
                   ref.ReferralReason__c = 'Citizen has identified a goal that you can assist in achieving.';
                   ref.PartnerAssociated__c = s.PartnerAssociated__c;
                   ref.Case__c = s.CaseId__c;
                   insert ref;
               } else {
                   ref = refmap.get(s.PartnerAssociated__c);
               }
               
              refSer = new ReferralServices__c();
              refSer.Referral__c = ref.Id;
              refSer.Services__c = s.Id;
              refsrvlst.add(refSer);
              //insert refSer;    
          }
          if(refsrvlst.size()>0){
              insert refsrvlst; 
          }
          
          // Mark Success Plan complete
          List<SuccessPlan__c> sPlan = [SELECT id, Completed__c FROM SuccessPlan__c WHERE CaseId__c = :caseId AND Status__c = 'Active']; 
          if (sPlan.size() > 0) {
	            sPlan[0].Completed__c = true;
              	update sPlan[0];
          }
          
          // Mark success plan of case complete
		  List<Case> completedCase = [select Id, SP_Completion__c from case where Id =:caseId];
          if (completedCase.size() > 0) {
              completedCase[0].SP_Completion__c = true;
              update completedCase[0];
          }
          
          CitizenSuccessPlanCtrl.prePopulateCommonApp(case_id, vServices);
          
          return null;
      }
}