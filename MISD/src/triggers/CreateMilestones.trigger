trigger CreateMilestones on SP_Transactional__c (before insert, after insert, before update, after update) {
    List<Referral__c> refLst;
    Referral__c ref;
    if(Trigger.IsInsert && Trigger.isBefore){
        for(SP_Transactional__c tr : Trigger.New){
            if(tr.Type__c == 'Milestone' || tr.Type__c == 'MilestoneSteps' || tr.Type__c == 'MilestoneHeads'){
                tr.Milestone_Status__c = 'In Progress';
            }
        }
    }
    if(Trigger.IsInsert && Trigger.isAfter){
        List<Id> idsList = new List<Id>();
        List<Id> stepIds = new List<Id>();
        for(SP_Transactional__c tr : Trigger.New){  
            if(tr.Type__c == 'Milestone'){
                idsList.add(tr.id);
            }
            if(tr.Type__c == 'MilestoneSteps' || tr.Type__c == 'MileStoneHeads'){
                stepIds.add(tr.id);
            }
        }   
        if(idsList.size() > 0){
            List<SP_Transactional__c> mList = [SELECT id,SP_Name__c, Goal__r.Domain__c, Type__c, Duration__c, Sequence__c,Start_Date__c, Name__c, Goal__c,
                                                        How_It_Works__c,GoalDescription__c,Success_Plan_Master2__c,Success_Plan_Master2__r.id
                                                    FROM SP_Transactional__c
                                                    WHERE id IN : idsList];
            
            List<Success_Plan_Master__c> MileStoneStepsList = 
                [SELECT id, Name, Parent__r.Value__c,parent__r.parent__r.value__c, value__c, Type__c, Duration__c, Sequence__c,How_It_Works__c,Services_Type__c,GoalDescription__c, PartnerAssociated__c
                    FROM Success_Plan_Master__c 
                    WHERE (Type__c = 'MilestoneSteps' OR Type__c = 'MilestoneHeads' OR Type__c = 'Milestone')             
                    ORDER by Sequence__c];

            Map<String, Map<String, List<Success_Plan_Master__c>>> DomainsMap = new Map<String, Map<String, List<Success_Plan_Master__c>>>();
            Map<String, Decimal > milestoneMap = new Map<String, Decimal >();
            
            for(Success_Plan_Master__c spm: MileStoneStepsList){
                if(spm.Type__c == 'Milestone'){
                    milestoneMap.put(spm.Id, spm.Duration__c);
                }else{
                    if(DomainsMap.containsKey(spm.Parent__r.Parent__r.Value__c)){
                        if(DomainsMap.get(spm.Parent__r.Parent__r.Value__c).containsKey(spm.Parent__c)){
                            DomainsMap.get(spm.Parent__r.Parent__r.Value__c).get(spm.Parent__c).add(spm);    
                        }else{
                            List<Success_Plan_Master__c> TempList = new List<Success_Plan_Master__c>();
                            TempList.add(spm);
                            DomainsMap.get(spm.Parent__r.Parent__r.Value__c).put(spm.Parent__c,TempList );
                        }
                    }else{
                        List<Success_Plan_Master__c> TempList = new List<Success_Plan_Master__c>();
                        TempList.add(spm);    
                        Map<String, List<Success_Plan_Master__c>> TempMap = new Map<String, List<Success_Plan_Master__c>>();
                        TempMap.put(spm.Parent__c, TempList);
                        DomainsMap.put(spm.parent__r.Parent__r.value__c, TempMap);
                    }
                }
            }
            List<SP_Transactional__c> toUpsertSteps = new List<SP_Transactional__c>();
            for(SP_Transactional__c trq : mList){
                Date StepDate = trq.Start_Date__c;
                Decimal Sequence ; 
                Decimal TotalDuration = 0;
                for(Success_Plan_Master__c spm: DomainsMap.get(trq.Goal__r.Domain__c).get(trq.Success_Plan_Master2__c)){
                    SP_Transactional__c sp = new SP_Transactional__c();
                    sp.Name__c = spm.Value__c;
                    sp.Sequence__c = spm.Sequence__c;
                    sp.Duration__c = spm.Duration__c;
                    sp.Start_Date__c = StepDate;                    
                    sp.Parent_Milestone__c = trq.id;
                    sp.Goal__c = trq.Goal__c;
                    sp.Type__c = spm.Type__c;
                    sp.Services_Type__c = spm.Services_Type__c;
                    sp.End_Date__c = sp.Start_Date__c.addDays(Integer.ValueOF(sp.Duration__c));        
                    //sp.How_It_Works__c = spm.How_It_Works__c;
                    sp.PartnerAssociated__c = spm.PartnerAssociated__c;                 
                    sp.GoalDescription__c = spm.GoalDescription__c;
                    sp.SP_Name__c = spm.Name;
                    toUpsertSteps.add(sp);
                    
                    Sequence = sp.Sequence__c;                  
                    StepDate = sp.End_Date__c;
                    TotalDuration = TotalDuration + sp.Duration__c;
                }   
                trq.Sequence__c = Sequence + 1;
                trq.Duration__c = milestoneMap.get(trq.Success_Plan_Master2__c);
                trq.End_Date__c = StepDate.addDays(Integer.valueOf(trq.Duration__c));
                trq.Total_Duration__c = TotalDuration + trq.Duration__c;
                toUpsertSteps.add(trq);
            }
            upsert toUpsertSteps;
        }
        if(stepIds.size() > 0){
            List<SP_Transactional__c> mList = [SELECT id,SP_Name__c, CaseContactName__c, Email__c, Services_About_Us__c, Services_Button_Text__c, Services_Type__c,Services_Type_Description__c, Goal__r.Domain__c, Type__c, Duration__c, Sequence__c,Start_Date__c, Name__c, Goal__c,Parent_Milestone__r.Name__c, CaseId__c
                                                    FROM SP_Transactional__c
                                                    WHERE id IN : stepIds];
            List<String> stepNames = new List<String>();
            for(SP_Transactional__c sp : mList){
                stepNames.add(sp.SP_Name__c);
            }
            List<Success_Plan_Master__c> MileStoneStepsList = [SELECT id, Parent__r.Name,PartnerAssociated__c, Email__c, Services_About_Us__c, Services_Button_Text__c, Services_Type__c,Services_Type_Description__c, Phone_Number__c, Website__c, Location__c,value__c,type__c,Parent__r.value__c ,
                                                                        Parent__r.Parent__r.Value__c,How_It_Works__c,What_To_Do__c, GoalDescription__c
                                                                    FROM Success_Plan_Master__c 
                                                                    WHERE type__c IN ('Service','Resource') AND Parent__r.Name IN : stepNames];
            Map<String, List<Success_Plan_Master__c>> serviceMasterMap = new Map<String, List<Success_Plan_Master__c>>();
            List<SP_Transactional__c> toUpsertSteps = new List<SP_Transactional__c>();
            
            for(Success_Plan_Master__c spm : MileStoneStepsList){
                /*if(serviceMasterMap.containsKey(spm.Parent__r.value__c)){
                    serviceMasterMap.get(spm.Parent__r.value__c).add(spm);
                }else{
                    List<Success_Plan_Master__c> tList = new List<Success_Plan_Master__c>();
                    tList.add(spm);
                    serviceMasterMap.put(spm.Parent__r.value__c,tList);
                }*/
                if(serviceMasterMap.containsKey(spm.Parent__r.Name)){
                    serviceMasterMap.get(spm.Parent__r.Name).add(spm);
                }else{
                    List<Success_Plan_Master__c> tList = new List<Success_Plan_Master__c>();
                    tList.add(spm);
                    serviceMasterMap.put(spm.Parent__r.Name,tList);
                }
            }
            
            String csid ='';
            if(mList.size()>0){
                csid = mList[0].CaseId__c;
            }
            List<Referral__c> casereflst = [Select Id, Primary_Contact__c, Referral_Date__c, Referral_Source__c, PartnerAssociated__c, Case__c from Referral__c where Case__c = :csid];
            Map<Id , Referral__c> caserefmap = new Map<Id , Referral__c>();
            for(Referral__c tempref : casereflst){
                if(!caserefmap.containsKey(tempref.PartnerAssociated__c)){
                    caserefmap.put(tempref.PartnerAssociated__c, tempref);
                }
            }
            Set<Referral__c> referralset = new Set<Referral__c>();
            List<Referral__c> referrallst = new List<Referral__c>();
            for(SP_Transactional__c sp : mList){
                if(serviceMasterMap.containsKey(sp.SP_Name__c)){
                    Boolean isDefault = true;
                    for(Success_Plan_Master__c service : serviceMasterMap.get(sp.SP_Name__c)){
                        if(sp.Parent_Milestone__r.Name__c == service.Parent__r.Parent__r.Value__c){
                            SP_Transactional__c tr = new SP_Transactional__c();
                            tr.Name__c = service.Value__c;
                            tr.Phone_Number__c = service.Phone_Number__c;
                            tr.Website__c = service.Website__c;
                            tr.Location__c = service.Location__c;
                            tr.Goal__c = sp.Goal__c;
                            tr.Parent_Milestone__c = sp.id;
                            tr.Type__c = service.Type__c;
                            tr.What_To_Do__c = service.What_To_Do__c;
                            tr.How_It_Works__c = service.How_It_Works__c;
                            tr.Services_About_Us__c = service.Services_About_Us__c;
                            tr.Services_Button_Text__c = service.Services_Button_Text__c;
                            tr.Services_Type__c = service.Services_Type__c;
                            tr.Services_Type_Description__c = service.Services_Type_Description__c;
                            tr.Email__c = service.Email__c;
                            tr.GoalDescription__c = service.GoalDescription__c;
                            tr.PartnerAssociated__c = service.PartnerAssociated__c;
                            //if(isDefault)
                              //  tr.Status__c = true;
                            isDefault = false;
                            toUpsertSteps.add(tr);
                            
                            if(tr.PartnerAssociated__c != null) {
                                refLst = new List<Referral__c>();
                                //refLst = [Select Id, Primary_Contact__c, Referral_Date__c, Referral_Source__c, PartnerAssociated__c, Case__c from Referral__c where Case__c = :sp.CaseId__c and PartnerAssociated__c = :service.PartnerAssociated__c];
//                                System.debug('@Rameez - user ' + sp.Goal__r.Case__r.Contact.FirstName + ' ' + sp.Goal__r.Case__r.Contact.LastName);
                                if(caserefmap.containsKey(service.PartnerAssociated__c)){
                                   refLst.add(caserefmap.get(service.PartnerAssociated__c));
                                }
                                if(refLst.size() == 0) {
                                    ref = new Referral__c();
                                } else {
                                    ref = refLst[0];
                                }
                                ref.Primary_Contact__c =  sp.CaseContactName__c;
                                ref.Referral_Date__c = System.Today();
                                ref.Referral_Source__c = 'Success Plan';
                                ref.ReferralReason__c = 'Citizen has identified a goal that you can assist in achieving.';
                                ref.PartnerAssociated__c = service.PartnerAssociated__c;
                                ref.Case__c = sp.CaseId__c;
                                //upsert ref;
                                referralset.add(ref);
                             }
                            
                        }
                    }
                }
            }
            upsert toUpsertSteps;
            for(Referral__c tmpref : referralset){
                referrallst.add(tmpref);
            }
            if(referrallst.size()>0){
                upsert referrallst;
            }
        }
    }
    If(Util.isTriggerUpdate){
        Util.isTriggerUpdate = false;
        if(Trigger.isUpdate && Trigger.isBefore){    
            List<Id> idsList = new List<Id>();
            List<SP_Transactional__c> toUpdateSteps = new List<SP_Transactional__c>();
            Map<Id, List<SP_Transactional__c>> parentMilestonesMap = new Map<Id, List<SP_Transactional__c>>();
            Map<Id, SP_Transactional__c> parentMap = new Map<Id, SP_Transactional__c>();
            
            for(SP_Transactional__c tr: Trigger.New){
                Integer dur ;
                if(tr.End_Date__c != Trigger.oldMap.get(tr.id).End_Date__c){
                    if(tr.Type__c == 'Milestone'){
                        dur = Integer.ValueOf(tr.Total_Duration__c);
                    }else if(tr.Type__c == 'MilestoneSteps' || tr.Type__c == 'MilestoneHeads'){
                        dur = Integer.ValueOf(tr.Duration__c);
                    }
                    tr.Start_Date__c = tr.End_Date__c.addDays(-dur);    
                }
                if(tr.Start_Date__c != Trigger.oldMap.get(tr.id).Start_Date__c){
                    if(tr.Type__c == 'Milestone'){
                        dur = Integer.ValueOf(tr.Total_Duration__c);
                    }else if(tr.Type__c == 'MilestoneSteps' || tr.Type__c == 'MilestoneHeads'){
                        dur = Integer.ValueOf(tr.Duration__c);
                    }
                    tr.End_Date__c = tr.Start_Date__c.addDays(dur);
                    
                    if(tr.Type__c == 'Milestone'){
                        idsList.add(tr.id);
                    }
                }
                if(tr.Completed__c != Trigger.oldMap.get(tr.id).Completed__c){
                    if(tr.Completed__c == true){
                        tr.Milestone_Status__c = 'Completed';
                    }else{
                        tr.Milestone_Status__c = 'In Progress';
                    }
                }   
                
            }
            system.debug('\n\n idsList: ' + idsList);
            if(idsList.size() > 0 ){
                for(SP_Transactional__c tr : [SELECT id, Goal__r.Domain__c, Type__c, Duration__c, Sequence__c,Start_Date__c, Name__c, Parent_Milestone__c
                                                        FROM SP_Transactional__c
                                                        WHERE (Parent_Milestone__c IN : idsList OR id IN : idsList)
                                                        Order By Sequence__c]){
                                                            
                    if(tr.Type__c == 'MilestoneHeads' || tr.type__c == 'MilestoneSteps'){
                        if(parentMilestonesMap.containsKey(tr.Parent_Milestone__c)){
                            parentMilestonesMap.get(tr.Parent_Milestone__c).add(tr);
                        }else{
                            List<SP_Transactional__c> tempList = new List<SP_Transactional__c>();
                            tempList.add(tr);
                            parentMilestonesMap.put(tr.Parent_Milestone__c, tempList);
                        }
                    }else{
                        parentMap.put(tr.id, Trigger.NewMap.get(tr.id));
                    }               
                }               
                for(String trId : parentMilestonesMap.keyset()){
                    Date StepDate = parentMap.get(trId).Start_Date__c;
                    Date StepEndDate;
                    for(SP_Transactional__c tr: parentMilestonesMap.get(trId)){
                        tr.Start_Date__c = StepDate;
                        tr.End_Date__c = tr.Start_Date__c.addDays(Integer.ValueOF(tr.Duration__c));
                        StepDate = tr.End_Date__c;
                        toUpdateSteps.add(tr);
                    }                    
                }
                system.debug('\n\n ' + toUpdateSteps);
                update toUpdateSteps;
            }
        }
    }  
    if(Trigger.isUpdate && Trigger.isAfter){
        Map<Id, Id> goalCaseMap = new Map<Id, Id>();
        for(SP_Transactional__c sp: trigger.new){
            if(sp.Milestone_Status__c == 'Completed' && sp.Status__c==true){
                goalCaseMap.put(sp.Goal__c, sp.CaseId__c);
            }
        }
        Map<Id, Id> caseContMap = new Map<id, id>();
        if(goalCaseMap.values().size()>0)
        for(Case c : [SELECT id, ContactId from Case where Id=: goalCaseMap.values()]){
            caseContMap.put(c.id, c.contactId);
        }
        
        Map<id, User> contactUserMap= new Map<Id, User>();
        if(caseContMap.values().size() > 0 )
        for(User u: [select Id,Name from User where ContactId=:caseContMap.values()]){
            contactUserMap.put(u.contactId, u);
        }
        system.debug('\n\n contactUserMap:: ' + contactUserMap);
        Map<id, List<SP_Transactional__c>> goalResourcesMap = new Map<id, List<SP_Transactional__c>>();
        if(contactUserMap != null && contactUserMap.keyset().size() > 0 ){
            for(SP_Transactional__c sp:  [SELECT id, Type__c,Name__c,Status__c,Goal__c 
                                            FROM SP_Transactional__c 
                                            WHERE Goal__c=:goalCaseMap.keyset() and Type__c='Resource' and Status__c=true]){
                if(goalResourcesMap.containsKey(sp.Goal__c)){
                    goalResourcesMap.get(sp.Goal__c).add(sp);
                }else{
                    List<SP_Transactional__c> tList = new List<SP_Transactional__c>();
                    tList.add(sp);
                    goalResourcesMap.put(sp.Goal__c, tList);
                }                               
                                                    
            }
        }
        Feedback__c ff; 
        
        for(SP_Transactional__c Goal: trigger.new){
            if(goalResourcesMap != null && goalResourcesMap.keyset() != null && goalResourcesMap.containsKey(Goal.Goal__c))
            for(SP_Transactional__c rsrc: goalResourcesMap.get(Goal.Goal__c)){
                if(contactUserMap.containsKey(caseContMap.get(goalCaseMap.get(Goal.Goal__c)))){
                    String Description = 'Please provide feedback on the resource that recently assisted you: '+rsrc.Name__c+'.';
                    ff = new Feedback__c();
                    ff.ResourceUsed__c = true;
                    ff.ParentId__c = rsrc.Id;
                    ff.ResourceName__c=rsrc.Name__c;
                    insert ff;
                    Id notId = MI_CreateNotification.CreateNotification(Description,contactUserMap.get(caseContMap.get(goalCaseMap.get(Goal.Goal__c))).id,Goal.Id,'Resource Feedback',ff.Id,Goal.CaseId__c);
                }
            }
        }       
    } 
    
}