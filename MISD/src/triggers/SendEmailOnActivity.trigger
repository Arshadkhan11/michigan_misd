trigger SendEmailOnActivity on MI_Activity__c (after update, before delete) {
    
       
    DateTime GMTDate;
    String RecordName;    
    List<Event_Attendee__c> evRelList;          //attendee and owner both
    List<MI_Activity__c> evRelListNoAttendee;   //to get calendar owner when no Attendee
    List<MI_Activity__c> evListOfTasks;         //to get task and its owner details
    String SubjectOfTaskEvent;                  //to get Subject of task

    String senderId = '15172524697';            //SenderId assing to user by default it is 'smsMagic'
    String MobilePhoneOwner;
    String ContactNameOwner;
    String OwnerId;
    String TaskEmailId;  
    
    List<smagicinteract__smsmagic__c> smsObjectList = new List<smagicinteract__smsmagic__c>();
    List<MI_Activity__c > evToUpdate = new List<MI_Activity__c > ();
    set<Id> evIds = new set<Id> ();

    String eventTaskDate;
    String Sub;
    String SMSTextCal;
    String SMSTextTask;
    String EmailBodyCal;
    String EmailBodyTask;
    String[] toAddressCalendar = new List<String>();
    
    public void SendSMSMethod(String sendrId, String Mob, String Ownr, String SMSText) //common method to send SMS
    {
        smagicinteract__smsMagic__c smsObject = new smagicinteract__smsMagic__c();
        smsObject.smagicinteract__SenderId__c = sendrId;
        smsObject.smagicinteract__PhoneNumber__c = Mob;
        smsObject.smagicinteract__Name__c =Ownr;
        smsObject.smagicinteract__ObjectType__c = 'Contact';
        smsObject.smagicinteract__disableSMSOnTrigger__c =0;
        smsObject.smagicinteract__external_field__c = smagicinteract.ApexAPI.generateUniqueKey();
        smsObject.smagicinteract__SMSText__c = SMSText;
        smsObjectList.add(smsObject);
        Database.insert(smsObjectList,false);        
    }
    if(Trigger.isUpdate)
    for(MI_Activity__c ev1: trigger.new)
    {
        //this is to make sure that Start date is populated when Task due date is modified
        if(ev1.Start_Date__c != ev1.Activity_Date__c && ev1.Activity_Date__c != null)
        {
            List<MI_Activity__c > Act = new List<MI_Activity__c > ();
            MI_Activity__c Act2 = new MI_Activity__c();
            Act2.Id=ev1.Id;
            Act2.Start_Date__c = ev1.Activity_Date__c;
            Act.add(Act2);
            Database.update(Act,false);
        }

        //convert the time to user's timezone
        GMTDate = ev1.Start_Date__c;
        if(ev1.Email_Sent__c == true)
        {        
            eventTaskDate = GMTDate.format('MM/dd/yyyy HH:mm a','America/New_York');
        }
        
        SubjectOfTaskEvent = ev1.Subject__c;

        RecordName = [select Name from RecordType where Id=:ev1.RecordTypeId].Name;   

        if(RecordName == 'Event')
        {
            evRelList = [select id, Event__c,Event__r.Subject__c,Event__r.Email_Sent__c, Event__r.OwnerId, Event__r.Owner.Name, Event__r.Owner.Email, Event__r.Owner.Phone, Attendee__c, Attendee__r.Name,Attendee__r.Appointment_Text__c,Attendee__r.Appointment_Email__c, Attendee__r.Email, Attendee__r.MobilePhone from Event_Attendee__c where Event__C IN :trigger.new];
            if(evRelList.size()==0)//if there is not attendee on the calendar
            {
                evRelListNoAttendee = [select id, OwnerId,Email_Sent__c,Subject__c, Owner.Name, Owner.Email, Owner.Phone from MI_Activity__c where Id=:ev1.Id];
            }
            
        }
        if(RecordName == 'Task')
        {
            evListOfTasks = [select id, OwnerId,Subject__c, Owner.Name, Owner.Email, Owner.Phone from MI_Activity__c where Id=:ev1.Id];
        }

        SMSTextCal = 'Reminder -- Hi, You have a Meeting coming up at '+eventTaskDate+' with Subject - "'+SubjectOfTaskEvent+'". This is an auto generated reminder. Please do not respond to this SMS. Thanks!';
        SMSTextTask = 'Reminder -- Hi, You have a Task due at '+eventTaskDate+' with Subject - "'+SubjectOfTaskEvent+'". This is an auto generated reminder. Please do not respond to this SMS. Thanks!';
        EmailBodyCal='<font face="arial">Hi, <br/> <br/>You have a Meeting coming up at '+eventTaskDate+' with Subject - "'+SubjectOfTaskEvent+'".<br/><br/> <b><font size="2">Note: This is an auto generated reminder. Please do not respond to this email.</font></b><br/><br/>Thanks!</font>';
        EmailBodyTask='<font face="arial">Hi, <br/> <br/>You have a Task due at '+eventTaskDate+' with Subject - "'+SubjectOfTaskEvent+'".<br/><br/> <b><font size="2">Note: This is an auto generated reminder. Please do not respond to this email.</font></b><br/><br/>Thanks!</font>';
    //}
     
        if(RecordName == 'Event') // for calendar
        {
           if(evRelList.size()==0)//if no attendee present then send SMS and prepare the email list
           {
               for(MI_Activity__c evNoAttendeeList : evRelListNoAttendee)
               {
                    OwnerId=evNoAttendeeList.OwnerId;
                    //bugs
                    User checkuser = [select Appointment_Text__c,Appointment_Email__c from user where id = :OwnerId];
                    ContactNameOwner = evNoAttendeeList.Owner.Name;
                   
                    if(checkuser.Appointment_Email__c == true)
                     toAddressCalendar.add(evNoAttendeeList.Owner.Email); //prepare the email list for owner only
                   
                    if(OwnerId != '' && OwnerId != null && evNoAttendeeList.Email_Sent__c == true) // found owner and now send SMS to him
                    {
                        MobilePhoneOwner = [select Id,MobilePhone from User where Id=:OwnerId].MobilePhone;
                        //Bugs
                        if(checkUser.Appointment_Text__c == true) {
                        SendSMSMethod(senderId, MobilePhoneOwner, ContactNameOwner, SMSTextCal);
                        system.debug('SMS1');
                        }
                    }
               }
           }
           else //it has attendee and owner both. common query has both the details. Fetch from there.
           {
               boolean SMSSentFlag = false;
               for(Event_Attendee__c evRel : evRelList )
               {
                    OwnerId = evRel.Event__r.OwnerId;
                    //bugs
                    User checkuser = [select Appointment_Text__c, Appointment_Email__c from user where id = :OwnerId];
                    if(checkuser.Appointment_Email__c == true)
                     toAddressCalendar.add(evRel.Event__r.Owner.Email);
                   
                    if(evRel.Attendee__r.Email != null && evRel.Attendee__r.Email != '')
                    {
                        //bugs
                        if(evRel.Attendee__r.Appointment_Email__c == true)
                        toAddressCalendar.add(evRel.Attendee__r.Email);
                    }
                   
                    String MobilePhoneAttendee = evRel.Attendee__r.MobilePhone; // Mobile number to whom user want to send sms    
                    ContactNameOwner = evRel.Event__r.Owner.Name;
                    String ContactNameAttendee = evRel.Attendee__r.Name;
                    SMSSentFlag = evRel.Event__r.Email_Sent__c;
                  
                   //Send SMS for each attendee
                    if(MobilePhoneAttendee != '' && MobilePhoneAttendee != null && evRel.Event__r.Email_Sent__c == true)
                    {
                        
                        //bugs
                        if(evRel.Attendee__r.Appointment_Text__c == true){
                        SendSMSMethod(senderId, MobilePhoneAttendee, ContactNameAttendee, SMSTextCal);
                        system.debug('SMS1');
                        }
                    }
                }
                //send SMS to owner
                if(OwnerId != '' && OwnerId != null && SMSSentFlag == true) // found owner and now send SMS to him
                {
                    MobilePhoneOwner = [select Id,MobilePhone from User where Id=:OwnerId].MobilePhone;
                    User checkuser = [select Appointment_Text__c, Appointment_Email__c from user where id = :OwnerId];
                    //bugs
                    if(checkuser.Appointment_Text__c == true)
                    {
                        SendSMSMethod(senderId, MobilePhoneOwner, ContactNameOwner, SMSTextCal);
                        system.debug('SMS2');
                     }
                }           
           }
        }
        if(RecordName == 'Task')
        {
           for(MI_Activity__c evTask : evListOfTasks)
           {
                OwnerId=evTask.OwnerId;
                ContactNameOwner = evTask.Owner.Name;
                TaskEmailId = evTask.Owner.Email;
           }
        }
        //Pull the Organizer's phone number from Owner Id

        if(ev1.Email_Sent__c == true)
        {
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddressTask = new List<String>();
            String body;
            if(RecordName == 'Event')
            {
                mail.setToAddresses(toAddressCalendar);                
                body = EmailBodyCal;
                Sub = 'Meeting Reminder - '+eventTaskDate;
            }
            else
            {
            //Send SMS for Owner of the task.
            User checkuser = [select Appointment_Text__c,Appointment_Email__c from user where id = :OwnerId];
                if(OwnerId != '' && OwnerId != null && ev1.Email_Sent__c == true)
                {                
                    MobilePhoneOwner = [select Id,MobilePhone from User where Id=:OwnerId].MobilePhone;
                    
                    //Bugs
                    if(checkuser.Appointment_Text__c == true){
                       system.debug('Sending msg to task owner');
                       SendSMSMethod(senderId, MobilePhoneOwner, ContactNameOwner, SMSTextTask);
                      }
                }
                //bugs
                if(checkuser.Appointment_Email__c == true)
                {
                 toAddressTask.add(TaskEmailId);
                 system.debug('Mail for task user:'+toAddressTask);
                }
                System.debug('RRP---'+toAddressTask);
                mail.setToAddresses(toAddressTask);
                body = EmailBodyTask;
                Sub = 'Task Due Reminder - '+eventTaskDate;
            }
            mail.setSubject(Sub);
            mail.setHtmlBody(body);
            if(toAddressTask.size() >0 || toAddressCalendar.size() >0)
            {
                system.debug('Sending mail to owner:');
                Messaging.sendEmail(new Messaging.SingleEMailMessage[]{mail});                
            }
            MI_Activity__c evt = new MI_Activity__c (id=ev1.Id);
            evt.Email_sent__c = false;
            evToUpdate.add(evt);
        }

        if(evToUpdate.size()>0)
        {
            update evToUpdate;
        }
    }
    
    if(Trigger.isDelete){
        for(Event_Attendee__c evatt:[select event__R.id, id from event_attendee__c where event__r.id IN : Trigger.OldMap.keyset()])
          {
              delete evatt;
          }
        List<MI_Activity__c> toDeleteAppointments = [SELECT id FROM MI_Activity__c WHERE Parent_Appointment__c IN : Trigger.OldMap.keyset()];
        system.debug('\n size --' +  toDeleteAppointments.size());
        delete toDeleteAppointments;
    }
    
    
    
    
    
}