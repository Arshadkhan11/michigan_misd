trigger UpdateActDate on MI_Activity__c (before insert) {
//This is to make sure SMS functionality works fine. Start date is the baseline field for SMS Workflow rule.
    for(MI_Activity__c ev: trigger.new)
    {
       	if(ev.Start_Date__c == null && ev.Activity_Date__c != null)
        {
	   		ev.Start_Date__c = ev.Activity_Date__c;
        }
    }
}