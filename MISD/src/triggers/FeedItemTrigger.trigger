trigger FeedItemTrigger on FeedItem (after insert) 
{
    public void SendSMSMethod(String sendrId, String Mob, String Ownr, String SMSText) //common method to send SMS
    {
        List<smagicinteract__smsmagic__c> smsObjectList = new List<smagicinteract__smsmagic__c>();
        smagicinteract__smsMagic__c smsObject = new smagicinteract__smsMagic__c();
        smsObject.smagicinteract__SenderId__c = sendrId;
        smsObject.smagicinteract__PhoneNumber__c = Mob;
        smsObject.smagicinteract__Name__c =Ownr;
        smsObject.smagicinteract__ObjectType__c = 'Contact';
        smsObject.smagicinteract__disableSMSOnTrigger__c =0;
        smsObject.smagicinteract__external_field__c = smagicinteract.ApexAPI.generateUniqueKey();
        smsObject.smagicinteract__SMSText__c = SMSText;
        smsObjectList.add(smsObject);
        Database.insert(smsObjectList,true);        
    }
    public void getConversation(Id FdId)
    {
       // ConnectApi.FeedElement feedItem = ConnectApi.ChatterFeeds.getFeedElement('0DB35000000003ZGAQ', FdId);
        ConnectApi.FeedElement feedItem = ConnectApi.ChatterFeeds.getFeedElement('0DB35000000003jGAA', FdId);
        List<ConnectApi.MessageSegment> messageSegments = feedItem.body.messageSegments;
        Id MentionId;
        String MentionName;
        String MessageBody;
        for (ConnectApi.MessageSegment messageSegment : messageSegments) 
        {
            if (messageSegment instanceof ConnectApi.MentionSegment) 
            {
                ConnectApi.MentionSegment mentionSegment = (ConnectApi.MentionSegment) messageSegment;
                MentionId=mentionSegment.record.Id;
                MentionName=mentionSegment.Name;
            }
            if (messageSegment instanceof ConnectApi.TextSegment) 
            {
                MessageBody=messageSegment.text;
            }
        }
        user us = [select Id,MobilePhone from user where Id=:MentionId LIMIT 1];
        if(us.MobilePhone <> '' && us.MobilePhone <> null)
        {
            SendSMSMethod('5172524697', us.MobilePhone, MentionName, MessageBody);
        }
    }
    for(FeedItem fi : Trigger.New) 
    {
        getConversation(fi.Id);
    }
}