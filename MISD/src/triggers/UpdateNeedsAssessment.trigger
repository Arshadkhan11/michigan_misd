trigger UpdateNeedsAssessment on NeedsAssessment__c (before insert) {
	 for(NeedsAssessment__c narec : Trigger.New){
            narec.AssessmentDate__c = System.today();
        }
}