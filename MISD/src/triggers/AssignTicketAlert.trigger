trigger AssignTicketAlert on DDTracker__TKR_Bug__c (after update) { 
    
    for(DDTracker__TKR_Bug__c cs : Trigger.New)  
    {
        // Instantiating a notification
        Messaging.PushNotification msg = 
            new Messaging.PushNotification();

        // Assembling the necessary payload parameters for Apple.
        // Apple params are: 
        // (<alert text>,<alert sound>,<badge count>,
        // <free-form data>)
        // This example doesn't use badge count or free-form data.
        // The number of notifications that haven't been acted
        // upon by the intended recipient is best calculated
        // at the time of the push. This timing helps
        // ensure accuracy across multiple target devices.
        Map<String, Object> payload = 
            Messaging.PushNotificationPayload.apple(
                'Defect ' + cs.Name + ' has been assigned to ' + cs.DDTracker__Defect_Developer__c + ' with status ' + cs.DDTracker__Status__c + '. Summary: ' + cs.DDTracker__Problem__c, '', null, null);

        // Adding the assembled payload to the notification
        msg.setPayload(payload);

        // Getting recipient users
        String userId1 = cs.DDTracker__Defect_Developer__r.Id;
//        String userId2 = cs.LastModifiedById;

        // Adding recipient users to list
        Set<String> users = new Set<String>();
        users.add(userId1);
//        users.add(userId2);                       

        // Sending the notification to the specified app and users.
        // Here we specify the API name of the connected app.  
  //      msg.send('TKR_Tracker_Core', users);
    }
}