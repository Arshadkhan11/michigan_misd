trigger SendNotificationForGoal on SP_Transactional__c (before update)
{
    for(SP_Transactional__c Goal: trigger.new)
    {
        system.debug('BBBB-'+Goal.NotificationFlag__c);
        if(Goal.NotificationFlag__c == true)
        {
            String Description = 'You missed a milestone for your goal of '+Goal.Name__c+' – did you forget to update it?';
			//Id ConId='00535000000IaAu';
            Id ConId = [select ContactId from Case where Id=:Goal.CaseId__c].ContactId;
			List<User> u = [select Id,Name from User where ContactId=:ConId];
			if(u.size()>0)
            {
            	system.debug('BBBB1');
                for(User uu:u)
                {
                    Id UserId = uu.Id;
                    Id notId = MI_CreateNotification.CreateNotification(Description,UserId,Goal.Id,'Milestone','',Goal.CaseId__c);
                    Goal.NotificationFlag__c = false;    
                }
            }
        }
    }
}